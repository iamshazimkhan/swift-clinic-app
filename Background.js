import * as React from 'react';
import Svg, {Path, Mask, G, Defs, LinearGradient, Stop} from 'react-native-svg';

function SvgBackgroundmain(props) {
  return (
    <Svg
      width={375}
      height={275}
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}>
      <Path fill="#fff" d="M0 0h375v275H0z" />
      <Mask
        id="backgroundmain_svg__a"
        maskUnits="userSpaceOnUse"
        x={0}
        y={0}
        width={375}
        height={275}>
        <Path fill="#fff" d="M0 0h375v275H0z" />
      </Mask>
      <G mask="url(#backgroundmain_svg__a)">
        <Path
          opacity={0.15}
          d="M261.608 241.172C182.212 222.076 113.641 129.009 138.837 61.4c20.487-54.888 99.403-85.736 123.2-66.65 22.619 17.909-12.238 73.5 14.623 98.477 26.861 24.977 79.625-16.477 107.664 10.62 22.62 21.206 20.508 68.558 1.902 98.475-27.085 43.578-87.394 47.809-124.618 38.85z"
          fill="url(#backgroundmain_svg__paint0_linear)"
        />
      </G>
      <Defs>
        <LinearGradient
          id="backgroundmain_svg__paint0_linear"
          x1={334.988}
          y1={77.56}
          x2={249.186}
          y2={202.723}
          gradientUnits="userSpaceOnUse">
          <Stop stopColor="#47B3C0" stopOpacity={0.3} />
          <Stop offset={1} stopColor="#46A5B0" stopOpacity={0} />
        </LinearGradient>
      </Defs>
    </Svg>
  );
}

export default SvgBackgroundmain;

