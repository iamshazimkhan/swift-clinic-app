import {mediaDevices, MediaStream, RTCIceCandidate, RTCPeerConnection, RTCSessionDescription, RTCView} from 'react-native-webrtc';
import RNCallKeep from 'react-native-callkeep';
import BackgroundTimer from 'react-native-background-timer';
import NavigationService from '../navigations/NavigationService';
import {NavigationActions} from 'react-navigation';
import AsyncStorage from '@react-native-community/async-storage';
import {Janus, JanusVideoRoomPlugin} from 'react-native-janus';
import constants from '../constants';

Janus.setDependencies({
  RTCPeerConnection,
  RTCSessionDescription,
  RTCIceCandidate,
  MediaStream,
});

var conn ;
var ch_id = null;
var co_id = null;
var is_answered = false;



RNCallKeep.setup({
  ios: {
    appName: 'CallKeepDemo',
  },
  android: {
     alertTitle: 'Permissions required',
    alertDescription: 'This application needs to access your phone accounts',
    cancelButton: 'Cancel',
    okButton: 'ok',
  },
});

const callUUID = "ddeb27fb-d9a0-4624-be4d-4615062daed4";
const privateNumber = 9812111111;

// const getNewUuid = () => 
const format = uuid => uuid.split('-')[0];
const getRandomNumber = () => String(Math.floor(Math.random() * 100000));

// const displayIncomingCall = (number) => {
//   const callUUID = "ddeb27fb-d9a0-4624-be4d-4615062daed4";
//   // addCall(callUUID, number);
// }

const log = (text) => {
console.info(text);
// setLog(logText + "\n" + text);
};

const addCall = (callUUID, number) => {
};

const displayIncomingCall = (number,doctor_name) => {
addCall(callUUID, number);

log(`[displayIncomingCall], number: ${number}`);

RNCallKeep.displayIncomingCall(callUUID, 'Dr. ' + doctor_name,'Dr. ' + doctor_name);
};

const displayIncomingCallNow = async (doctor_name,consultation_id,channel_id) => {
  console.log('Lets checks the variables ::::', consultation_id, channel_id)
  //  await AsyncStorage.setItem('consultation_id',consultation_id);
  //  await AsyncStorage.setItem('channel_id',channel_id);
   ch_id = channel_id;
   co_id = consultation_id;
  await displayIncomingCall(getRandomNumber(),doctor_name);
};

const displayIncomingCallDelayed = () => {
BackgroundTimer.setTimeout(() => {
  displayIncomingCall(getRandomNumber());
}, 3000);
};

const answerPressed = async () => {
    await console.log('---------------Vars --------->',ch_id,co_id);
     is_answered = true;
  await NavigationService.navigate(
    NavigationActions.navigate({
      routeName: 'PatientVideoScreen',
      params: {
        // channel_id:1234567,
        channel_id: ch_id === null ? 111 : ch_id,
        consultation_id: co_id === null ? 111 : co_id,
        isIncomingCall: true
      }
    }),
  );
   RNCallKeep.endAllCalls();
}

const answerCall = async ({ callUUID }) => {
const number = 9812111495;
console.log(`[Phone Picked ---------->] ${format(callUUID)}, number: ${number}`);

// RNCallKeep.startCall(callUUID, number, number);
 RNCallKeep.backToForeground();
// navigation.navigate('CallScreen');


BackgroundTimer.setTimeout(() => {
  console.log(`[setCurrentCallActive] ${format(callUUID)}, number: ${number}`);
  RNCallKeep.setCurrentCallActive(callUUID);
  answerPressed();
}, 1000);
};

const didPerformDTMFAction = ({ callUUID, digits }) => {
const number = privateNumber;
console.log(`[didPerformDTMFAction], number: ${number} (${digits})`);
};

const didReceiveStartCallAction = ({ handle }) => {
if (!handle) {
  // @TODO: sometime we receive `didReceiveStartCallAction` with handle` undefined`
  return;
}
const callUUID = "ddeb27fb-d9a0-4624-be4d-4615062daed4";
addCall(callUUID, handle);

console.log(`[didReceiveStartCallAction] ${callUUID}, number: ${handle}`);

RNCallKeep.startCall(callUUID, handle, handle);

BackgroundTimer.setTimeout(() => {
  log(`[setCurrentCallActive] , number: ${handle}`);
  RNCallKeep.setCurrentCallActive(callUUID);
}, 1000);
};

const didPerformSetMutedCallAction = ({ muted, callUUID }) => {
const number = privateNumber;
// log(`[didPerformSetMutedCallAction] , number: ${number} (${muted})`);
};

const didToggleHoldCallAction = ({ hold, callUUID }) => {
const number = privateNumber
console.log(`[didToggleHoldCallAction], number: ${number} (${hold})`);

setCallHeld(callUUID, hold);
};

const endCall = async ({ callUUID }) => {
  if(is_answered){
    return;
  }

const handle = privateNumber;
  try {
    this.janus = new Janus('wss://swift-video.swiftclinic.com/');
    this.janus.setApiSecret('janusrocks');
    await this.janus.init();

    this.videoRoom = new JanusVideoRoomPlugin(this.janus);
    
    let room_id = parseInt(ch_id)

    this.videoRoom.setRoomID(room_id);
    this.videoRoom.setDisplayName('can');

    await this.videoRoom.createPeer();
    await this.videoRoom.connect();
    await this.videoRoom.join();
    await this.janus.destroy();
    this.janus.socket = null;
    this.janus = null;

  } catch (e) {
    console.error('main', JSON.stringify(e));
  }
// removeCall(callUUID);
};

hangUpBuddy = async () => {
   console.log('Delete Data ::::')
  conn.send(JSON.stringify({
    type: 'leave',
  }));
}

const hangup = (callUUID) => {
RNCallKeep.endCall(callUUID);
// removeCall(callUUID);
};

const setOnHold = (callUUID, held) => {
const handle = privateNumber;
RNCallKeep.setOnHold(callUUID, held);
// log(`[setOnHold:, number: ${handle}`);
};

const setOnMute = (callUUID, muted) => {
const handle = privateNumber
RNCallKeep.setMutedCall(callUUID, muted);
// log(`[setMutedCall:, number: ${handle}`);

};

const onendCallFunction = async (RoutName,consultation_id) => {
    await console.log('End Call -----******------------>',RoutName, consultation_id);
    await RNCallKeep.endAllCalls();
    if(RoutName === 'Doctor')
    {
    NavigationService.navigate(
        NavigationActions.navigate({
          routeName: 'ActiveConsultations',
          params: {
            id: consultation_id
          }
        }),
      );
    }else{
        NavigationService.navigate(
            NavigationActions.navigate({
              routeName: 'Home',
              params: {
                id: consultation_id
              }
            }),
          ); 
    }
}

const updateDisplay = (callUUID) => {
const number = privateNumber;
// Workaround because Android doesn't display well displayName, se we have to switch ...
if (isIOS) {
  RNCallKeep.updateDisplay(callUUID, 'New Name', number);
} else {
  RNCallKeep.updateDisplay(callUUID, number, 'New Name');
}

console.log(`[updateDisplay:`);
};

// const rejectCall = async () => {
//   console.log('Reject Call Working fine');
// }

 

RNCallKeep.addEventListener('answerCall', answerCall);
// RNCallKeep.addEventListener('rejectCall', rejectCall);
RNCallKeep.addEventListener('didPerformDTMFAction', didPerformDTMFAction);
RNCallKeep.addEventListener('didReceiveStartCallAction', didReceiveStartCallAction);
RNCallKeep.addEventListener('didPerformSetMutedCallAction', didPerformSetMutedCallAction);
RNCallKeep.addEventListener('didToggleHoldCallAction', didToggleHoldCallAction);
RNCallKeep.addEventListener('endCall', endCall);

module.exports = {
displayIncomingCallNow:displayIncomingCallNow,
onendCallFunction: onendCallFunction
}
