import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import messaging from '@react-native-firebase/messaging';
import {displayIncomingCallNow} from './services/CallKeep';
import AsyncStorage from '@react-native-community/async-storage';

messaging().onNotificationOpenedApp((remoteMessage) => {
  console.log(
    'Remote message Background state Notification ------------>',
    remoteMessage,
  );
});

isLoggedInPatient = async (doctr, consult, channel) => {
  const d = await AsyncStorage.getItem('isLoggedIn');
  const l = await AsyncStorage.getItem('user_level');
  console.log('Logged IN ----------------->', d);
  if (d === 'true') {
    if (l === '1') {
      displayIncomingCallNow(doctr, consult, channel);
    } else if (l === '2') {
      console.log('Doctor account');
    }
  }
};
// Check whether an initial notification is available
messaging()
  .getInitialNotification()
  .then((remoteMessage) => {
    if (
      remoteMessage.data.videocall === true ||
      remoteMessage.data.videocall === 'true'
    ) {
      console.log('Videocall Notification B');
      isLoggedInPatient(
        remoteMessage.data.doctor_name,
        remoteMessage.data.consultation_id,
        remoteMessage.data.channel_id,
      );
    }
  });

messaging().setBackgroundMessageHandler(async (remoteMessage) => {
  console.log('Message handled in the background!', remoteMessage);
  console.log('Variables in Index :::::', remoteMessage.data.channel_id);
  const doctr = remoteMessage.data.doctor_name;
  const channel = remoteMessage.data.channel_id;
  const consult = remoteMessage.data.consultation_id;
  if (
    remoteMessage.data.videocall === true ||
    remoteMessage.data.videocall === 'true'
  ) {
    console.log('Videocall Notification C');
    isLoggedInPatient(doctr, consult, channel);
  }
});

// AppRegistry.registerHeadlessTask('RNCallKeepBackgroundMessage', () => ({ name, callUUID, handle }) => {
//     // Make your call here

//     return Promise.resolve();
//   });

AppRegistry.registerComponent(appName, () => App);
