import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import {NavigationActions} from 'react-navigation';
// const baseURL = 'https://swift-clinic.theyard.design/';
const baseURL = 'https://platform.swiftclinic.com/';

import NavigationService from '../navigations/NavigationService';

/** ----------------Request --------------------- */

const isHandlerEnabled = (config = {}) => {
  return config.hasOwnProperty('handlerEnabled') && !config.handlerEnabled
    ? false
    : true;
};

const requestHandler = async (request) => {
  if (isHandlerEnabled(request)) {
    // console.log('------------MiddleWare-----------');
    let token = await AsyncStorage.getItem('token');
    request.headers.Authorization = 'Bearer ' + token;
  }
  return request;
};

axios.interceptors.request.use((request) => requestHandler(request));

/** -------------------Response --------------------- */

axios.interceptors.response.use(
  (response) => {
    // console.log('Response Interceptor', response)
    // if (response.data.error === true) {
    //   AsyncStorage.clear();
    //   NavigationService.navigate(
    //     NavigationActions.navigate({
    //       routeName: 'Auth',
    //     }),
    //   );
    // }
    return response;
  },
  async function (error) {
    // Do something with response error
    if (error.response.status === 401) {
      console.log('unauthorized, logging out ...');
      AsyncStorage.clear();
      NavigationService.navigate(
        NavigationActions.navigate({
          routeName: 'Auth',
        }),
      );
    }
    // console.log('Error part');
    return Promise.reject(error.response);
  },
);

/** -------------End Request ---------------- */

const GetSymptoms = async () => {
  const settings = {
    url: baseURL + 'api/symptoms',
    method: 'GET',
    timeout: 0,
    headers: {
      Accept: 'application/json',
    },
    processData: false,
    contentType: false,
  };

  return await axios(settings)
    .then((res) => {
      // console.log('Axios ----->', res.data);
      return res;
    })
    .catch((err) => {
      console.log('err ********** -------------- *****', err);
    });
};

/***************--------------- Disconnect API ----------------- ******* */

const disconnectAPI = async () => {
  const channel_id = await AsyncStorage.getItem('channel_id');
  console.log(' End Call :::::::: Working fine.');

  var thisURL = `api/consultation/disconnect-call/${channel_id}`;

  const settings = {
    url: baseURL + thisURL,
    method: 'GET',
    timeout: 0,
    headers: {
      Accept: 'application/json',
    },
    processData: false,
    contentType: false,
  };

  console.log('URL :::::', settings);

  return await axios(settings)
    .then((res) => {
      console.log('Axios ----->', res.data);
      return res;
    })
    .catch((err) => {
      console.log('err ********** -------------- *****', err);
    });
};

/** ------------- Get Doctor List ------------ */

const GetDoctors = async (id) => {
  await console.log('Doctor id in list :', id);
  const settings = {
    url: baseURL + 'api/doctor/list/' + id,
    method: 'GET',
    timeout: 0,
    headers: {
      Accept: 'application/json',
    },
    processData: false,
    contentType: false,
  };

  return await axios(settings)
    .then((res) => {
      //   console.log('Axios Doctors----->',res.data);
      return res;
    })
    .catch((err) => {
      console.log(err);
    });
};

/** -------------Add a consultant------------ */

const AddBooking = async (symptom_id, doctor_id, showDate, location) => {
  console.log('affsf', symptom_id, doctor_id, showDate, location);
  const settings = {
    url: baseURL + 'api/doctor/appointment',
    method: 'POST',
    timeout: 0,
    headers: {
      Accept: 'application/json',
    },
    processData: false,
    contentType: false,
    data: {
      symptom_id: symptom_id,
      doctor: doctor_id,
      slot_time: showDate,
      location: location,
    },
  };

  return await axios(settings)
    .then((res) => {
      // console.log('Axios Add booking----->', res.data);
      return res;
    })
    .catch((err) => {
      console.log(err);
    });
};

/** -------------Add a Payment------------ */

const AddPayment = async (consultation_id, amount, defaultCardID, type) => {
  const settings = {
    url: baseURL + 'api/doctor/appointment/payment',
    method: 'POST',
    timeout: 0,
    headers: {
      Accept: 'application/json',
    },
    processData: false,
    contentType: false,
    data: {
      consultation_id: consultation_id,
      amount: amount,
      card: defaultCardID,
      type: type,
    },
  };

  return await axios(settings)
    .then((res) => {
      // console.log('Axios Add booking----->', res.data);
      return res;
    })
    .catch((err) => {
      console.log(err);
    });
};

/** -------------Follow up Payment------------ */

const followUp = async (consultation_id, time) => {
  const settings = {
    url: baseURL + 'api/doctor/appointment-followup',
    method: 'POST',
    timeout: 0,
    headers: {
      Accept: 'application/json',
    },
    processData: false,
    contentType: false,
    data: {
      consultation_id: consultation_id,
      slot_time: time,
    },
  };

  return await axios(settings)
    .then((res) => {
      // console.log('Axios Add booking----->', res.data);
      return res;
    })
    .catch((err) => {
      console.log(err);
    });
};

/** ------------- Get Upcoming Consultations List ------------ */

const GetUpcomingConsultations = async () => {
  const settings = {
    url: baseURL + 'api/doctor/appointments',
    method: 'GET',
    timeout: 0,
    headers: {
      Accept: 'application/json',
    },
    processData: false,
    contentType: false,
  };

  return await axios(settings)
    .then((res) => {
      // console.log('Axios Upcoming consultations----->', res.data);
      return res;
    })
    .catch((err) => {
      console.log(err);
    });
};

/** ------------- Get Profile Data List ------------ */

const GetProfileConsultations = async () => {
  const settings = {
    url: baseURL + 'api/user/profile-status',
    method: 'GET',
    timeout: 0,
    headers: {
      Accept: 'application/json',
    },
    processData: false,
    contentType: false,
  };

  return await axios(settings)
    .then((res) => {
      // console.log('Axios Profile consultations----->', res.data);
      return res;
    })
    .catch((err) => {
      console.log(err);
    });
};

/** ------------- Get Previous Consultations List ------------ */

const GetPreviousConsultations = async () => {
  const settings = {
    url: baseURL + 'api/doctor/appointments/history',
    method: 'GET',
    timeout: 0,
    headers: {
      Accept: 'application/json',
    },
    processData: false,
    contentType: false,
  };

  return await axios(settings)
    .then((res) => {
      // console.log('Axios Upcoming consultations----->', res.data);
      return res;
    })
    .catch((err) => {
      console.log(err);
    });
};

/** ------------- Get Consultations Details ------------ */

const GetConsultationsDetails = async (consulation_id) => {
  const settings = {
    url: baseURL + 'api/doctor/appointment/' + consulation_id + '/detail',
    method: 'GET',
    timeout: 0,
    headers: {
      Accept: 'application/json',
    },
    processData: false,
    contentType: false,
  };

  return await axios(settings)
    .then((res) => {
      // console.log('Axios consultations Details----->', res.data);
      return res;
    })
    .catch((err) => {
      console.log(err);
    });
};

/** -------------Add Questionaire ----------- */

const AddQuestionaire = async (d, consultation_id) => {
  const settings = {
    url: baseURL + 'api/patient/questionaire',
    method: 'POST',
    timeout: 0,
    headers: {
      Accept: 'application/json',
    },
    processData: false,
    contentType: false,
    data: {
      data: d,
      consultation_id: consultation_id,
    },
  };

  return await axios(settings)
    .then((res) => {
      console.log('Axios Add Questionaire----->', res.data);
      return res;
    })
    .catch((err) => {
      console.log(err);
    });
};

/** -------------Add Consent Form ----------- */

const AddConsentForm = async (d, consultation_id) => {
  const settings = {
    url: baseURL + 'api/patient/consent-form',
    method: 'POST',
    timeout: 0,
    headers: {
      Accept: 'application/json',
    },
    processData: false,
    contentType: false,
    data: {
      data: d,
      consultation_id: consultation_id,
    },
  };

  return await axios(settings)
    .then((res) => {
      console.log('Axios Add Questionaire----->', res.data);
      return res;
    })
    .catch((err) => {
      console.log(err);
    });
};

/** ------------- Get Doctors History List ------------ */

const GetDoctorsPreviousConsultations = async () => {
  const settings = {
    url: baseURL + 'api/patient/appointments/history',
    method: 'GET',
    timeout: 0,
    headers: {
      Accept: 'application/json',
    },
    processData: false,
    contentType: false,
  };

  return await axios(settings)
    .then((res) => {
      // console.log('Axios Upcoming consultations----->', res.data);
      return res;
    })
    .catch((err) => {
      console.log(err);
    });
};

/** -------------Reschedule Booking consultant------------ */

const PatientRescheduling = async (consultation_id, showDate) => {
  console.log(showDate, 'khurram');
  const settings = {
    url: baseURL + 'api/doctor/appointment/reschedule',
    method: 'POST',
    timeout: 0,
    headers: {
      Accept: 'application/json',
    },
    processData: false,
    contentType: false,
    data: {
      consultation_id: consultation_id,
      slot_time: showDate,
    },
  };

  return await axios(settings)
    .then((res) => {
      console.log('Axios Reschduling----->', res);
      return res;
    })
    .catch((err) => {
      console.log(err);
    });
};

/** -------------Doctor Reschedule Booking consultant------------ */

const DoctorRescheduling = async (consultation_id, showDate) => {
  const settings = {
    url: baseURL + 'api/patient/appointment/reschedule',
    method: 'POST',
    timeout: 0,
    headers: {
      Accept: 'application/json',
    },
    processData: false,
    contentType: false,
    data: {
      consultation_id: consultation_id,
      slot_time: showDate,
    },
  };

  return await axios(settings)
    .then((res) => {
      // console.log('Axios Reschduling----->', res.data);
      return res;
    })
    .catch((err) => {
      console.log(err);
    });
};

/** ------------- Get User Profile ------------ */

const GetUserProfile = async () => {
  const settings = {
    url: baseURL + 'api/user',
    method: 'GET',
    timeout: 0,
    headers: {
      Accept: 'application/json',
    },
    processData: false,
    contentType: false,
  };

  return await axios(settings)
    .then((res) => {
      // console.log('Axios Profile----->');
      return res;
    })
    .catch((err) => {
      console.log(err);
    });
};

/** ------------- Get All API Data For Doctor Consultations ------------ */

const getAllConsultationData = async () => {
  const settings = {
    url: baseURL + 'api/patient/appointments-all',
    method: 'GET',
    timeout: 0,
    headers: {
      Accept: 'application/json',
    },
    processData: false,
    contentType: false,
  };

  return await axios(settings)
    .then((res) => {
      // console.log('Axios All data----->');
      return res;
    })
    .catch((err) => {
      console.log(err);
    });
};

/** -------------Accept and Reject Api----------- */

const AcceptOrRejectApi = async (consulation_id, response) => {
  const settings = {
    url: baseURL + 'api/patient/appointment/response',
    method: 'POST',
    timeout: 0,
    headers: {
      Accept: 'application/json',
    },
    processData: false,
    contentType: false,
    data: {
      consultation_id: consulation_id,
      response: response,
    },
  };

  return await axios(settings)
    .then((res) => {
      // console.log('Axios Reschduling----->', res.data);
      return res;
    })
    .catch((err) => {
      console.log(err);
    });
};

/** -------------  Add Available Timmmings  ----------- */

const AddAvailableTimmings = async (showDate, startTime) => {
  const settings = {
    url: baseURL + 'api/doctor/time',
    method: 'POST',
    timeout: 0,
    headers: {
      Accept: 'application/json',
    },
    processData: false,
    contentType: false,
    data: {
      date: showDate,
      times: startTime,
    },
  };

  return await axios(settings)
    .then((res) => {
      // console.log('Axios Reschduling----->', res.data);
      return res;
    })
    .catch((err) => {
      console.log(err);
    });
};

// ---------------------- Get Cards ----------------------------

const getAllUserCards = async () => {
  const settings = {
    url: baseURL + 'api/payment/card',
    method: 'GET',
    timeout: 0,
    headers: {
      Accept: 'application/json',
    },
    processData: false,
    contentType: false,
  };

  return await axios(settings)
    .then((res) => {
      // console.log('Axios Card Data----->',res.data.data);
      return res;
    })
    .catch((err) => {
      console.log(err);
    });
};

/** -------------  Add a new Card----------- */

const AddNewCard = async (
  cardNumber,
  CardCVV,
  cardMonth,
  cardYear,
  cardName,
) => {
  const settings = {
    url: baseURL + 'api/payment/card',
    method: 'POST',
    timeout: 0,
    headers: {
      Accept: 'application/json',
    },
    processData: false,
    contentType: false,
    data: {
      card: cardNumber,
      cvv: CardCVV,
      month: cardMonth,
      year: cardYear,
      name: cardName,
    },
  };

  return await axios(settings)
    .then((res) => {
      // console.log('Axios Reschduling----->', res.data.data);
      return res;
    })
    .catch((err) => {
      console.log(err);
    });
};

// ----------------------------- Get All Locations -------------------

const GetMRILocation = async () => {
  const settings = {
    url: baseURL + 'api/locations',
    method: 'GET',
    timeout: 0,
    headers: {
      Accept: 'application/json',
    },
    processData: false,
    contentType: false,
  };

  return await axios(settings)
    .then((res) => {
      // console.log('Axios MRI Locations----->', res.data);
      return res;
    })
    .catch((err) => {
      console.log(err);
    });
};

/** -------------Reschedule Booking consultant------------ */

const startCall = async (consultation_id) => {
  const settings = {
    url: baseURL + 'api/patient/request-call',
    method: 'POST',
    timeout: 0,
    headers: {
      Accept: 'application/json',
    },
    processData: false,
    contentType: false,
    data: {
      consultation_id: consultation_id,
    },
  };

  return await axios(settings)
    .then((res) => {
      // console.log('Axios Reschduling----->', res.data);
      return res;
    })
    .catch((err) => {
      console.log(err);
    });
};

/** -------------Add Review----------- */

const addReview = async (
  consultation_id,
  doctor_id,
  rating,
  review,
  user_id,
) => {
  const settings = {
    url: baseURL + 'api/doctor/review',
    method: 'POST',
    timeout: 0,
    headers: {
      Accept: 'application/json',
    },
    processData: false,
    contentType: false,
    data: {
      patient_id: user_id,
      doctor_id: doctor_id,
      consultation_id: consultation_id,
      review_star: rating,
      review_message: review,
    },
  };

  return await axios(settings)
    .then((res) => {
      console.log('Axios Add Review----->', res.data);
      return res;
    })
    .catch((err) => {
      console.log(err);
    });
};

// ------------------------- Get Profile Data --------------------

const GetProfileData = async () => {
  const settings = {
    url: baseURL + 'api/user',
    method: 'GET',
    timeout: 0,
    headers: {
      Accept: 'application/json',
    },
    processData: false,
    contentType: false,
  };

  return await axios(settings)
    .then((res) => {
      // console.log('Axios Profile Data----->', res.data);
      return res;
    })
    .catch((err) => {
      console.log(err);
    });
};

/** ------------- Get Notifications  ------------ */

const GetPatientNotifications = async () => {
  const settings = {
    url: baseURL + 'api/notifications',
    method: 'GET',
    timeout: 0,
    headers: {
      Accept: 'application/json',
    },
    processData: false,
    contentType: false,
  };

  return await axios(settings)
    .then((res) => {
      // console.log('Axios Upcoming consultations----->', res.data);
      return res;
    })
    .catch((err) => {
      console.log(err);
    });
};

/** -------------Seen Notification------------ */

const seenNotification = async (notification) => {
  const settings = {
    url: baseURL + 'api/notification/seen',
    method: 'POST',
    timeout: 0,
    headers: {
      Accept: 'application/json',
    },
    processData: false,
    contentType: false,
    data: {
      notification: notification,
    },
  };

  return await axios(settings)
    .then((res) => {
      // console.log('Axios Reschduling----->', res.data);
      return res;
    })
    .catch((err) => {
      console.log(err);
    });
};

/**------------------------- Complete api -----------------------------  */

const setStatusOfConsultation = async (consultation_id) => {
  const settings = {
    url: baseURL + 'api/consultation/update-status',
    method: 'POST',
    timeout: 0,
    headers: {
      Accept: 'application/json',
    },
    processData: false,
    contentType: false,
    data: {
      consultation_id: consultation_id,
      status: 'initial_consultation_completed',
    },
  };

  return await axios(settings)
    .then((res) => {
      // console.log('Axios Reschduling----->', res.data);
      return res;
    })
    .catch((err) => {
      console.log(err);
    });
};

/**------------------------- Upload Report -----------------------------  */

const uploadReport = async (consultation_id, fileToUpload) => {
  const data = new FormData();
  data.append('consultation_id', consultation_id);
  data.append('doctor_report', fileToUpload);
  const settings = {
    url: baseURL + 'api/patient/appointment/upload-report',
    method: 'POST',
    timeout: 0,
    headers: {
      Accept: 'application/json',
    },
    processData: false,
    contentType: false,
    data: data,
  };

  return await axios(settings)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      console.log(err);
    });
};

/**------------------------- Complete api -----------------------------  */

const confirmReschedulePatient = async (consultation_id, status) => {
  const settings = {
    url: baseURL + 'api/doctor/appointment/accept-reschedule',
    method: 'POST',
    timeout: 0,
    headers: {
      Accept: 'application/json',
    },
    processData: false,
    contentType: false,
    data: {
      consultation_id: consultation_id,
      accept: status,
    },
  };

  return await axios(settings)
    .then((res) => {
      console.log('Axios Reschduling----->', res.data);
      return res;
    })
    .catch((err) => {
      console.log(err);
    });
};

/**------------------------- Complete api -----------------------------  */

const confirmRescheduleDoctor = async (consultation_id, status) => {
  const settings = {
    url: baseURL + 'api/patient/appointment/reschedule-accept',
    method: 'POST',
    timeout: 0,
    headers: {
      Accept: 'application/json',
    },
    processData: false,
    contentType: false,
    data: {
      consultation_id: consultation_id,
      accept: status,
    },
  };

  return await axios(settings)
    .then((res) => {
      console.log('Axios Reschduling----->', res.data);
      return res;
    })
    .catch((err) => {
      console.log(err);
    });
};

/** -------------On chnage Pssword------------ */

const ChangePasswordRequest = async (old_password, new_password) => {
  const settings = {
    url: baseURL + 'api/update-password',
    method: 'POST',
    timeout: 0,
    headers: {
      Accept: 'application/json',
    },
    processData: false,
    contentType: false,
    data: {
      password_old: old_password,
      password_new: new_password,
    },
  };

  return await axios(settings)
    .then((res) => {
      console.log('passwro---->', res.data);
      return res;
    })
    .catch((err) => {
      console.log(err);
    });
};

/** ------------- Get Doctor Time ------------ */

const GetDoctorTime = async (doctor_id) => {
  const settings = {
    url: baseURL + 'api/doctor/' + doctor_id + '/time',
    method: 'GET',
    timeout: 0,
    headers: {
      Accept: 'application/json',
    },
    processData: false,
    contentType: false,
  };

  return await axios(settings)
    .then((res) => {
      console.log('Doctor Time --------->', res.data);
      return res;
    })
    .catch((err) => {
      console.log(err);
    });
};

// --------------------------- Post Data ---------------------------------

const addMriRequest = async (consulation_id, description, items) => {
  const settings = {
    url: baseURL + 'api/patient/appointment/mri-test',
    method: 'POST',
    timeout: 0,
    headers: {
      Accept: 'application/json',
    },
    processData: false,
    contentType: false,
    data: {
      consultation: consulation_id,
      detail: description,
      test: items,
    },
  };

  return await axios(settings)
    .then((res) => {
      // console.log('Axios Reschduling----->', res.data);
      return res;
    })
    .catch((err) => {
      console.log(err);
    });
};

// --------------------------- Post Data ---------------------------------

const resetConsultation = async (
  consultation_id,
  symptom_id,
  doctor_id,
  time,
) => {
  const settings = {
    url: baseURL + 'api/doctor/appointment-reset',
    method: 'POST',
    timeout: 0,
    headers: {
      Accept: 'application/json',
    },
    processData: false,
    contentType: false,
    data: {
      consultation: consultation_id,
      symptom_id: symptom_id,
      doctor: doctor_id,
      slot_time: time,
    },
  };

  return await axios(settings)
    .then((res) => {
      // console.log('Axios Reschduling----->', res.data);
      return res;
    })
    .catch((err) => {
      console.log(err);
    });
};

const getDoctorAvailableTime = async (id, date) => {
  const settings = {
    url: `${baseURL}api/doctor/${id}/time?date=${date}`,
    method: 'GET',
    timeout: 0,
    headers: {
      Accept: 'application/json',
    },
  };

  return await axios(settings)
    .then((res) => {
      //   console.log('Axios Doctors----->',res.data);
      return res;
    })
    .catch((err) => {
      console.log(err);
    });
};

const getDoctorBookAvailableTime = async (id, date) => {
  const settings = {
    url: `${baseURL}api/doctor/${id}/available_and_booked_slots?date=${date}`,
    method: 'GET',
    timeout: 0,
    headers: {
      Accept: 'application/json',
    },
  };

  return await axios(settings)
    .then((res) => {
      //   console.log('Axios Doctors----->',res.data);
      return res;
    })
    .catch((err) => {
      console.log(err);
    });
};

const createDoctorAvailableTime = async (dateTime) => {
  const settings = {
    url: `${baseURL}/api/doctor/time_new`,
    method: 'POST',
    timeout: 0,
    headers: {
      Accept: 'application/json',
    },
    processData: false,
    contentType: false,
    data: dateTime,
  };

  return await axios(settings)
    .then((res) => {
      // console.log('Axios Reschduling----->', res.data);
      return res;
    })
    .catch((err) => {
      console.log(err);
    });
};

module.exports = {
  resetConsultation: resetConsultation,
  getDoctorBookAvailableTime: getDoctorBookAvailableTime,
  addMriRequest: addMriRequest,
  GetDoctorTime: GetDoctorTime,
  GetSymptoms: GetSymptoms,
  GetDoctors: GetDoctors,
  AddBooking: AddBooking,
  createDoctorAvailableTime: createDoctorAvailableTime,
  getDoctorAvailableTime: getDoctorAvailableTime,
  AddPayment: AddPayment,
  GetUpcomingConsultations: GetUpcomingConsultations,
  GetPreviousConsultations: GetPreviousConsultations,
  GetConsultationsDetails: GetConsultationsDetails,
  AddQuestionaire: AddQuestionaire,
  GetDoctorsPreviousConsultations: GetDoctorsPreviousConsultations,
  PatientRescheduling: PatientRescheduling,
  GetUserProfile: GetUserProfile,
  getAllConsultationData: getAllConsultationData,
  AcceptOrRejectApi: AcceptOrRejectApi,
  DoctorRescheduling: DoctorRescheduling,
  GetProfileConsultations: GetProfileConsultations,
  AddAvailableTimmings: AddAvailableTimmings,
  getAllUserCards: getAllUserCards,
  AddNewCard: AddNewCard,
  GetMRILocation: GetMRILocation,
  startCall: startCall,
  addReview: addReview,
  GetProfileData: GetProfileData,
  GetPatientNotifications: GetPatientNotifications,
  seenNotification: seenNotification,
  setStatusOfConsultation: setStatusOfConsultation,
  uploadReport: uploadReport,
  confirmRescheduleDoctor: confirmRescheduleDoctor,
  confirmReschedulePatient: confirmReschedulePatient,
  ChangePasswordRequest: ChangePasswordRequest,
  AddConsentForm: AddConsentForm,
  followUp: followUp,
};
