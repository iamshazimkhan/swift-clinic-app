import React from 'react';
import { View, Image, Text } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

const MasterCardBox = (props) => {
    return(
        <View onStartShouldSetResponder={props.dropDown}
        style={{
          borderWidth: 1,
          paddingLeft: 10,
          paddingRight: 10,
          marginTop: 20,
          width: '100%',
          borderColor: '#33909F',
          borderRadius: 10,
          flexDirection: 'row',
          justifyContent: 'space-between'
        }}>
        <View style={{flexDirection: 'row'}}>
          <View>
            <Image
              style={{width: 70, height: 50}}
              source={require('../mastercard_logo.jpg')}></Image>
          </View>
          <View style={{flexDirection: 'column', justifyContent: 'center', paddingLeft: 10}}>
    <Text>{props.name}</Text>
      <Text style={{color: '#646464'}}>{props.cardName}</Text>
          </View>
        </View>
        <Icon
        style={{justifyContent:'center',marginTop: 13}}
        color={props.Colour ? props.Colour : "#ffffff"}
        size={15}
        name="chevron-down"
      />
      </View>
    )
}
export default MasterCardBox;