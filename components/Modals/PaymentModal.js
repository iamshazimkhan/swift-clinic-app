import React, {useState} from 'react';
import {
  Platform,
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  Modal,
  TextInput,
  Image,
  FlatList,
  TouchableOpacity
} from 'react-native';
import {set} from 'react-native-reanimated';
import Icon from 'react-native-vector-icons/FontAwesome';
import LinearGradient from 'react-native-linear-gradient';
import Button from '../../components/Buttons/button';
import MasterCardBox from '../../components/MasterCardBox';

const PaymentModal = (props) => {
    const [data,setData] = useState(
      { id : 1, name: '*** 1234'},
      { id : 2, name: '*** 4567'},
      { id : 3, name: '*** 7890'},
      )

      const Function = () => {
        console.log('Funcation Called')
      }

  return (
    <Modal
      animationType={'slide'}
      transparent={true}
      visible={props.Visibilty}
      onRequestClose={() => {
        console.log('Modal has been closed.');
      }}>
        <View style={styles.modal}>
        <Icon
          onPress={props.closeModal}
          style={{alignSelf: 'flex-end', marginRight: 17}}
          color="#33909F"
          size={24}
          name="times-circle"
        />
        <Text style={styles.text}>Payment</Text>

        {/* ----------------------- MasterCard Box ---------------------- */}
       {props.extra ? ( 
        <View style={{flexDirection: 'row', marginTop: 35, width: '100%', justifyContent: 'space-between'}}>
          <Text style={{ fontSize: 14, color : 'grey'}}>15 minutes extra consultation</Text>
          <Text style={{ fontSize: 16 , textAlign: 'right', fontWeight: 'bold'}}>{props.amount + '.00'}</Text>
        </View>
        ) : null }

        <MasterCardBox dropDown={props.showCardFaltList} cardName={props.defaultCardName} Colour="#33909F" name={props.defaultCardNumber}></MasterCardBox>
        {props.Visible === true ? (
        <View style={{width: '100%'}}>
        <Text style={{marginTop: 10}}>Choose Cards</Text>
        </View>
                ) : null }
                        {props.Visible === true ? (
        <FlatList
          style={{width: '100%'}}
          data={props.cardData}
          keyExtractor={(item) => item.id.toString()}
          renderItem={({item}) => (
            <TouchableOpacity onPress={() => props.onSelectListCard(item)}>
            <MasterCardBox name={"**** " + item.card_digit} cardName={item.card_name}></MasterCardBox>
            </TouchableOpacity>
          )}
        />
        ) : null }

        <View onStartShouldSetResponder={props.addCard} style={{justifyContent: 'flex-start', width: '100%', marginTop: 20, flexDirection: 'row'}}>
        <Icon
          style={{alignSelf: 'flex-end', marginRight: 17}}
          color="#33909F"
          size={24}
          name="plus-circle"
        />
          <Text style={{textAlign: 'left', textAlign: 'justify', width: '100%', marginTop: 3}}>Add a new Card</Text>
        </View>

        {/* --------------------------- Payment Box --------------------- */}
        <View style={{flexDirection: 'row', marginTop: 25, width: '100%', justifyContent: 'space-between'}}>
          <Text style={{margin: 20, fontSize: 19, flex: 1}}>Total</Text>
          <Text style={{margin: 20, fontSize: 19, flex: 1, textAlign: 'right', fontWeight: 'bold'}}>{props.amount + '.00'}</Text>
        </View>
        <LinearGradient colors={['#78C2CB', '#33909F']} style={{ shadowColor: "dodgerblue",
        shadowOffset: { width: 0, height: 4 },
        shadowRadius: 6,
        shadowOpacity: 0.7,
        elevation: 7, 
        padding: 15, backgroundColor: props.disableButton === false ? "#0158ff" : "skyblue" ,alignSelf: 'stretch', borderRadius: 10, marginRight: 40, marginLeft: 40, padding: 15}}>
        <TouchableOpacity onPress={props.addPayment}>
        {/* <Button title="Pay Nows" backgroundColor={props.disableButton === false ? "#0158ff" : "skyblue" } onClick={props.addPayment}></Button> */}
        <Text style={{textAlign: 'center', width: '100%', height: 55 , padding: 16,fontSize: 16 ,color : props.disableButton === false ? "white" : "grey" }}>Pay Now</Text>
        </TouchableOpacity>
        </LinearGradient>
        </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modal: {
    height: '100%',
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 40,
    alignItems: 'center',
    backgroundColor: '#fefefe',
    width: '100%',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#fff',
    position: 'absolute',
  },
  text: {
    textAlign: 'center',
    fontSize: 20,
    width: '100%',
    fontWeight: 'bold',
  },
  container: {
    marginTop: 50,
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderRadius: 10,
    width: '100%',
    height: 60,
    backgroundColor: '#0158ff',
  },
  inputText: {
    marginTop: 20,
    paddingLeft: 10,
    borderRadius: 8,
    height: 50,
    alignSelf: 'stretch',
    borderWidth: 1,
    borderColor: '#dcecef',
    marginBottom: 20,
  },
});

export default PaymentModal;
