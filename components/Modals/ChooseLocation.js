import React, {useState} from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Modal,
  TextInput,
  Image,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import {set} from 'react-native-reanimated';
import Icon from 'react-native-vector-icons/FontAwesome';
import Button from '../Buttons/button';
import MasterCardBox from '../MasterCardBox';
import LocationBox from '../LocationBox';

const ChooseLocation = (props) => {
  return (
    <Modal
      animationType={'fade'}
      transparent={true}
      visible={props.Visibilty}
      onRequestClose={() => {
        console.log('Modal has been closed.');
      }}>
      <View style={styles.modal}>
        {/* <Icon
          onPress={props.closeModal}
          style={{alignSelf: 'flex-end', marginRight: 20}}
          color="#33909F"
          size={24}
          name="times-circle"
        /> */}
        <TouchableOpacity onPress={() => props.closeModal()}>
          <Image
            style={{alignSelf: 'flex-end'}}
            // color="black"
            source={require('../../assets/times-circle.png')}
            // size={24}
            // name="times-circle"
          />
        </TouchableOpacity>

        <Text style={styles.text}>Scan location</Text>

        {/* ----------------------- MasterCard Box ---------------------- */}
        <View>
          <TextInput style={styles.inputText} placeholder="Location" />
        </View>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            width: '100%',
            marginBottom: 20,
          }}>
          <Image source={require('../../assets/map-marker.png')} />
          <Text
            style={{
              fontSize: 14,
              marginLeft: 10,
              textAlign: 'left',
              fontFamily: 'Axiforma-Medium',
              // fontWeight: 'bold',

              textDecorationLine: 'underline',
            }}>
            Current location
          </Text>
        </View>
        <FlatList
          style={{width: '100%'}}
          data={props.cardData}
          horizontal={false}
          keyExtractor={(item) => item.id.toString()}
          renderItem={({item}) => (
            <TouchableOpacity
              style={{marginBottom: 10}}
              onPress={() => props.onSelectListCard(item)}>
              <LocationBox
                name={item.name}
                cardName={item.address}></LocationBox>
            </TouchableOpacity>
          )}
        />
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modal: {
    padding: 30,
    // alignItems: 'center',
    backgroundColor: '#fefefe',
    height: '100%',
    width: '100%',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#fff',
    bottom: 0,
    position: 'absolute',
    paddingBottom: 100,
  },
  text: {
    textAlign: 'center',
    fontSize: 20,
    width: '100%',
    paddingBottom: 40,
    fontFamily: 'Axiforma-Bold',
    // fontWeight: 'bold',
  },
  container: {
    marginTop: 50,
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderRadius: 10,
    width: '100%',
    height: 60,
    backgroundColor: '#0158ff',
  },
  inputText: {
    marginTop: 20,
    paddingLeft: 20,
    borderRadius: 8,
    height: 50,
    alignSelf: 'stretch',
    borderWidth: 1,
    borderColor: '#cae3ff',
    marginBottom: 20,
  },
});

export default ChooseLocation;
