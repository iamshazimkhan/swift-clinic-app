import React, {useState} from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Modal,
  Image,
} from 'react-native';
import Button from '../Buttons/button';
import CustomButton from '../Buttons/CustomButton';
import Icon from 'react-native-vector-icons/FontAwesome';

const AcceptRejectModal = (props) => {
  return (
    <Modal
      animationType={'slide'}
      transparent={true}
      visible={props.timeModal}
      onRequestClose={() => {
        console.log('Modal has been closed.');
      }}>
      <View style={styles.modal}>
      <Icon
          onPress={props.closeModal}
          style={{alignSelf: 'flex-end', marginRight: 17}}
          color="#0375FF"
          size={24}
          name="times-circle"
        />
        <Image style={{height: 100, width: 100, marginBottom: 20, marginTop: 40}} source={require('../../timelogo.png')}></Image>
        <Text>{props.text}</Text>
        <View style={{flexDirection: 'row'}}>
          <View style={{flex: 1}}>
        <Button  title="Accept" onClick={props.onAccept}></Button>
        </View>
        <View style={{flex: 1}}>
        <CustomButton colour="red" title="Reject" onClick={props.onReject}></CustomButton>
        </View>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modal: {
    padding: 30,
    alignItems: 'center',
    backgroundColor: '#f7f7f7',
    width: '100%',
    bottom: 0,
    borderRadius: 10,
    position: 'absolute',
    borderWidth: 1,
    borderColor: '#fff',
    justifyContent: 'center',
    alignSelf: 'center'
  },
});

export default AcceptRejectModal;
