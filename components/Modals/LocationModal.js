import React, {useState} from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Modal,
  TextInput,
} from 'react-native';
import {set} from 'react-native-reanimated';
import Icon from 'react-native-vector-icons/FontAwesome';
import Button from '../../components/Buttons/button';

const LocationModal = (props) => {
  return (
    <Modal
      animationType={'slide'}
      transparent={true}
      visible={props.Visibilty}
      onRequestClose={() => {
        console.log('Modal has been closed.');
      }}>
      <View style={styles.modal}>
        <Icon
          onPress={props.closeModal}
          style={{alignSelf: 'flex-end', marginRight: 17}}
          color="#0375FF"
          size={24}
          name="times-circle"
        />
        <Text style={styles.text}>Scan Location</Text>
        <TextInput placeholder="Location" style={styles.inputText} value={props.locationValue} onChangeText={props.onChangeValue}></TextInput>
        <View onStartShouldSetResponder={props.findLocation} style={{flexDirection: 'row', marginTop: 40}}>
          <Icon
            style={{alignSelf: 'flex-end', marginRight: 17}}
            color="#0375FF"
            size={24}
            name="map-marker"
          />
          <Text style={{textDecorationLine: 'underline'}}>Current Location</Text>
        </View>
        <Button title="Confirm Location" onClick={props.setLocation}></Button>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modal: {
    padding: 30,
    alignItems: 'center',
    backgroundColor: '#fefefe',
    height: 400,
    width: '100%',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#fff',
    bottom: 0,
    position: 'absolute',
  },
  text: {
    textAlign: 'center',
    fontSize: 20,
    width: '100%',
    fontWeight: 'bold',
  },
  container: {
    marginTop: 50,
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderRadius: 10,
    width: '100%',
    height: 60,
    backgroundColor: '#0158ff',
  },
  inputText: {
    marginTop: 20,
    paddingLeft: 10,
    borderRadius: 8,
    height: 50,
    alignSelf: 'stretch',
    borderWidth: 1,
    borderColor: '#cae3ff',
    marginBottom: 20,
  },
});

export default LocationModal;
