import React, {useState} from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Modal,
  Image,
} from 'react-native';
import Button from '../Buttons/button';

const PaymentSuccessfull = (props) => {
  return (
    <Modal
      animationType={'slide'}
      transparent={true}
      visible={props.PSVisibility}
      onRequestClose={() => {
        console.log('Modal has been closed.');
      }}>
      <View style={styles.modal}>
        <Image style={{height: 100, width: 100, marginBottom: 20, marginTop: -40}} source={require('../../paymentsuccessfull.png')}></Image>
        <Text>Payment is successfully processed.</Text>
        <Button title="Continue" onClick={props.setPaymentComplete}></Button>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modal: {
    padding: 30,
    alignItems: 'center',
    backgroundColor: '#fefefe',
    width: '100%',
    height: '100%',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#fff',
    justifyContent: 'center',
    alignSelf: 'center'
  },
});

export default PaymentSuccessfull;
