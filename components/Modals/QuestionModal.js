import React, {useState} from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button,
  Modal,
  TouchableOpacity,
} from 'react-native';
import {set} from 'react-native-reanimated';
import Icon from 'react-native-vector-icons/FontAwesome';

const QuestionModal = (props) => {
  return (
    <Modal
      animationType={'slide'}
      transparent={true}
      visible={props.Visibilty}
      onRequestClose={() => {
        console.log('Modal has been closed.');
      }}>
      <View style={styles.modal}>
        <Icon
          onPress={props.closeModal}
          style={{alignSelf: 'flex-end', marginRight: 17}}
          color="#0375FF"
          size={24}
          name="times-circle"
        />
        <Text style={styles.text}>Health Questionaire</Text>
        <Text style={{marginTop: 20, fontSize: 15, color: 'grey', fontFamily : 'Axiforma-Thin', lineHeight: 18}}>
          We require more information prior to your consultation to provide you
          with the best possible care. Please answer all questions as thoroughly
          as possible
        </Text>
        <TouchableOpacity
          style={styles.container}
          onPress={props.onClickAnswer}>
          <View style={{alignSelf: 'center', flexDirection: 'row'}}>
            <Icon
              style={{textAlignVertical: 'center', marginLeft: 30}}
              color="#ffffff"
              size={18}
              name="address-book"
            />
            <Text
              style={{
                fontSize: 14,
                color: '#171717',
                color: '#ffffff',
                width: '100%',
                marginLeft: 10,
              }}>
              Answer the questions
            </Text>
          </View>
          <View style={{alignSelf: 'center'}}>
            <Icon
              style={{textAlignVertical: 'center', marginRight: 17}}
              color="#ffffff"
              size={18}
              name="chevron-right"
            />
          </View>
        </TouchableOpacity>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modal: {
    padding: 30,
    alignItems: 'center',
    backgroundColor: '#fefefe',
    height: 420,
    width: '100%',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#fff',
    bottom: 0,
    position: 'absolute',
  },
  text: {
    textAlign: 'center',
    fontSize: 20,
    width: '100%',
    fontWeight: 'bold',
  },
  container: {
    marginLeft: 50,
    marginTop: 50,
    marginRight: 50,
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderRadius: 10,
    height: 60,
    backgroundColor: '#0158ff',
  },
});

export default QuestionModal;
