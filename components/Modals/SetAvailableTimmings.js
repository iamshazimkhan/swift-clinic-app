import React, {useState} from 'react';
import {
  View,
  Button,
  Modal,
  StyleSheet,
  TouchableOpacity,
  Text,
  FlatList,
} from 'react-native';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import Moment from 'moment';
import Icon from 'react-native-vector-icons/FontAwesome';

const SetAvailableTimmings = ({
  Date,
  selectedDateCallback,
  TimeVisible,
  onClick,
  closeModal,
  specificTimeData,
}) => {
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);

  // ----------------------- Date --------------------

  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const handleConfirm = (date) => {
    console.log('A date has been picked: ', date);
    hideDatePicker();

    selectedDateCallback(date);
  };

  return (
    <Modal
      animationType={'slide'}
      transparent={true}
      visible={TimeVisible}
      onRequestClose={() => {
        console.log('Modal has been closed.');
      }}>
      <View style={styles.modal}>
        <View style={{width: '100%', marginBottom: 7}}>
          <Icon
            onPress={closeModal}
            style={{alignSelf: 'flex-end', marginRight: 17}}
            color="#0375FF"
            size={24}
            name="times-circle"
          />
        </View>
        <View style={{width: '100%', marginBottom: 7}}>
          <Text style={{textAlign: 'left'}}>Select Time:</Text>
        </View>

        {/* ----------------------- Date ------------------- */}

        <View
          onStartShouldSetResponder={showDatePicker}
          style={styles.inputText}>
          <TouchableOpacity onPress={showDatePicker}>
            <Text style={{textAlign: 'center'}}>{Date}</Text>
          </TouchableOpacity>
          <DateTimePickerModal
            style={{width: 10, height: '10'}}
            isVisible={isDatePickerVisible}
            mode="time"
            onConfirm={handleConfirm}
            onCancel={hideDatePicker}
          />
        </View>

        {/* -------------------------- End date ---------------- */}

        {specificTimeData.length === 0 ? null : (
          <View style={{width: '100%', marginTop: 30}}>
            <Text style={{textAlign: 'left'}}>Select Time Between:</Text>
            <FlatList
              scrollEnabled={false}
              data={specificTimeData}
              horizontal={false}
              numColumns={2}
              renderItem={({item}) => (
                <View
                  style={{
                    padding: 10,
                    margin: 5,
                    borderRadius: 10,
                    borderColor: 'skyblue',
                    backgroundColor: '#b3d8ea',
                  }}>
                  <Text style={{color: '#ffffff'}}>
                    {item.start_time + ' - ' + item.end_time}
                  </Text>
                </View>
              )}
            />
          </View>
        )}

        <View style={{width: '100%'}}>
          <TouchableOpacity
            onPress={onClick}
            style={{
              alignSelf: 'stretch',
              backgroundColor: '#0158ff',
              borderRadius: 4,
              height: 50,
              justifyContent: 'center',
              marginTop: 30,
              marginBottom: 60,
            }}>
            <Text style={{color: '#ffffff', textAlign: 'center'}}>
              Confirm Time
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modal: {
    paddingLeft: 30,
    paddingRight: 30,
    paddingTop: 30,
    paddingBottom: 60,
    alignItems: 'center',
    backgroundColor: '#fefefe',
    width: '100%',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#fff',
    bottom: 0,
    position: 'absolute',
  },
  inputText: {
    marginTop: 10,
    paddingLeft: 10,
    borderRadius: 8,
    height: 50,
    alignSelf: 'stretch',
    borderWidth: 1,
    borderColor: '#cae3ff',
    justifyContent: 'center',
  },
});

export default SetAvailableTimmings;
