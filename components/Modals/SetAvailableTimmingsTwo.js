import React, {useState, useEffect} from 'react';
import {
  View,
  Button,
  Modal,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  Text,
  FlatList,
  Image,
} from 'react-native';
import {RadioButton} from 'react-native-paper';
import {getDoctorBookAvailableTime} from '../../api/ConsultationApi';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import Moment from 'moment';
import Icon from 'react-native-vector-icons/FontAwesome';
import LinearGradient from 'react-native-linear-gradient';
import moment from 'moment';

const SetAvailableTimmingsTwo = ({
  Date,
  TimeVisible,
  setState,
  showCalender,
  dat,
  id,
  name,
  onClick,
  closeModal,
  specificTimeData,
  showDatePicker,
}) => {
  // ----------------------- Date --------------------
  let startTime = moment(specificTimeData[0].start_time, ['h:mm A']);
  let endTime = moment(specificTimeData[0].end_time, ['h:mm A']);
  const [checked, setChecked] = React.useState('first');

  // duration in hours
  // let duration = moment.duration(endTime.diff(startTime));

  // // duration in hours
  // let hours = parseInt(duration.asHours()) + 1;
  // // let minDuration = moment.duration(`11:00 AM`.diff(startTime));
  // // duration in minutes
  // let minutes = parseInt(duration.asMinutes()) % 60;
  let timeStops = [];
  const [times, setTimes] = useState();
  let availableTiming = async () => {
    console.log('Rizwan ======>', id, Date);
    let result = await getDoctorBookAvailableTime(id, dat);
    setTimes(result.data.data);
    console.log('sheikh', id, dat, result.data.data);
    console.log('Rizwan', result);
  };

  useEffect(() => {
    availableTiming();
  }, [dat]);

  while (startTime <= endTime) {
    timeStops.push(new moment(startTime).format('HH:mm A'));
    startTime.add(15, 'minutes');
  }

  const [confirm, setConfirm] = useState();
  const [index, setIndex] = useState();

  return (
    <Modal
      animationType={'slide'}
      transparent={true}
      visible={TimeVisible}
      onRequestClose={() => {
        console.log('Modal has been closed.');
      }}>
      <View style={styles.modal}>
        <View style={{width: '100%', marginBottom: 7}}>
          {/* <Icon
            onPress={closeModal}
            style={{alignSelf: 'flex-end', marginRight: 0}}
            color="#33909F"
            size={24}
            name="times-circle"
          /> */}
          <TouchableOpacity onPress={() => closeModal()}>
            <Image
              onPress={() => closeModal()}
              style={{alignSelf: 'flex-end'}}
              // color="black"
              source={require('../../assets/times-circle.png')}
              // size={24}
              // name="times-circle"
            />
          </TouchableOpacity>
        </View>

        <View style={{width: '100%', marginBottom: 7}}>
          <Text style={styles.meeting}>Select Time</Text>
          <Text
            style={{
              marginTop: 17,
              fontSize: 14,
              color: '#8c8c8c',
              textTransform: 'uppercase',
              marginBottom: 5,
            }}>
            Selected Date
          </Text>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text
              style={{
                color: '#32919F',
                fontWeight: 'bold',
                fontSize: 17,
                marginTop: 5,
                textTransform: 'capitalize',
              }}>
              {moment(dat, 'YYYY-MM-DD').format('DD MMMM YYYY')}
            </Text>
            <Text
              style={[styles.heading, {textDecorationLine: 'underline'}]}
              onPress={showCalender}>
              Change
            </Text>
          </View>
        </View>

        {/* ----------------------- Date ------------------- */}
        <ScrollView style={styles.inputText}>
          {console.log('Rizwan ==========>', times)}
          {times &&
            times.map((item, i) => (
              <>
                {console.log('item', confirm, 'index', index)}
                {index == i ? (
                  <View style={{flexDirection: 'row', marginTop: 9}}>
                    <TouchableOpacity style={styles.item}>
                      <Text
                        style={{
                          color: '#32919F',
                          marginLeft: 20,
                        }}>
                        {moment(item.start_time, ['HH.mm']).format('h:mm A')}
                      </Text>
                    </TouchableOpacity>
                    <LinearGradient
                      colors={['#78C2CB', '#33909F']}
                      style={styles.confirm}>
                      <TouchableOpacity
                        // style={styles.confirm}
                        onPress={() => {
                          setState(item.start_time);
                        }}>
                        <Text
                          style={{
                            color: '#ffffff',
                            textAlign: 'center',
                          }}>
                          Confirm
                        </Text>
                      </TouchableOpacity>
                    </LinearGradient>
                  </View>
                ) : (
                  <TouchableOpacity
                    style={item.booked ? styles.buttonDisable : styles.button1}
                    onPress={() => {
                      setIndex(i);
                    }}
                    disabled={item.booked}>
                    <Text
                      style={{
                        color: item.booked ? 'white' : '#32919F',
                        marginLeft: 20,
                      }}>
                      {moment(item.start_time, ['HH.mm']).format('h:mm A')}
                    </Text>
                  </TouchableOpacity>
                )}
              </>
            ))}
        </ScrollView>
        {/* <View
          onStartShouldSetResponder={showDatePicker}
          style={styles.inputText}>
          <TouchableOpacity onPress={showDatePicker}>
            <Text style={{textAlign: 'center'}}>{Date}</Text>
          </TouchableOpacity>
        </View> */}

        {/* -------------------------- End date ---------------- */}

        {/* {specificTimeData.length === 0 ? null : (
          <View style={{width: '100%', marginTop: 30}}>32919F
            <Text style={{textAlign: 'left'}}>Select Time Between:</Text>
            <FlatList
              scrollEnabled={false}
              data={specificTimeData}
              horizontal={false}
              numColumns={2}
              renderItem={({item}) => (
                <View
                  style={{
                    padding: 10,
                    margin: 5,
                    borderRadius: 10,
                    borderColor: 'skyblue',
                    backgroundColor: '#b3d8ea',
                  }}>
                  <Text style={{color: '#ffffff'}}>
                    {item.start_time + ' - ' + item.end_time}
                  </Text>
                </View>
              )}
            />
          </View>
        )}

        <View style={{width: '100%'}}>
          <LinearGradient
            colors={['#78C2CB', '#33909F']}
            style={{
              alignSelf: 'stretch',
              backgroundColor: '#0158ff',
              borderRadius: 4,
              height: 70,
              justifyContent: 'center',
              marginTop: 30,
              marginBottom: 60,
            }}>32919F
            <TouchableOpacity onPress={onClick}>
              <Text
                style={{
                  fontSize: 19,
                  fontFamily: 'Axiforma-Book',
                  color: '#ffffff',
                  textAlign: 'center',
                  width: '100%',
                  alignSelf: 'center',
                  textAlignVertical: 'center',
                }}>
                Confirm Time
              </Text>
            </TouchableOpacity>
          </LinearGradient>
        </View> */}
        <View
          style={{
            flexDirection: 'row',
            paddingLeft: 15,
            paddingTop: 30,
            paddingBottom: 18,
          }}>
          <View style={{flex: 1}}>
            <Image source={require('../../assets/images/available.png')} />
          </View>
          <View style={{flex: 7, alignSelf: 'center'}}>
            <Text style={{color: '#33909F'}}>Available</Text>
          </View>
        </View>
        <View style={{flexDirection: 'row', paddingLeft: 15}}>
          <View style={{flex: 1}}>
            <Image source={require('../../assets/images/unavailable.png')} />
          </View>
          <View style={{flex: 7, alignSelf: 'center'}}>
            <Text style={{color: 'grey'}}>Not Available</Text>
          </View>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modal: {
    paddingLeft: 30,
    paddingRight: 30,
    paddingTop: 30,
    paddingBottom: 60,
    alignItems: 'center',
    backgroundColor: '#fefefe',
    width: '100%',
    height: '100%',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#fff',
    bottom: 0,
    position: 'absolute',
  },

  meeting: {
    marginTop: 10,
    fontWeight: 'bold',
    fontSize: 20,
    textAlign: 'center',
  },

  heading: {
    marginTop: 5,

    fontSize: 17,
  },

  inputText: {
    // marginTop: 10,
    paddingLeft: 0,
    borderRadius: 8,
    height: '50%',
    width: '100%',
    // alignSelf: 'stretch',
    // borderWidth: 1,
    // borderColor: '#dcecef',
    // justifyContent: 'center',
  },

  confirm: {
    backgroundColor: '#32919F',
    width: '49%',
    height: 48,
    shadowColor: 'rgba(130, 204, 213, 0.31)',
    shadowOffset: {width: 0, height: 1},
    elevation: 3,
    justifyContent: 'center',
    borderRadius: 4,
    marginRight: 10,
  },

  item: {
    shadowColor: 'rgba(130, 204, 213, 0.31)',
    shadowOffset: {width: 0, height: 1},
    backgroundColor: 'white',
    elevation: 3,
    width: '47%',
    margin: 3,
    marginRight: 10,
    height: 48,
    justifyContent: 'center',
    borderRadius: 4,
  },

  button1: {
    width: '97%',
    marginTop: 10,
    marginBottom: 10,
    marginLeft: '2%',
    borderRadius: 4,
    shadowOffset: {width: 0, height: 2},
    height: 48,
    justifyContent: 'center',
    backgroundColor: '#FFFFFF',
    shadowColor: '#82CCD5',

    shadowOpacity: 0.13,
    // shadowRadius: 10.0,
    elevation: 7,

    // borderWidth: 1,
    // backgroundColor: 'white',
    // shadowColor: '#000',
    // overflow: 'hidden',
    // shadowOffset: {width: -10, height: 10},
    // shadowOpacity: 0.8,
    // shadowRadius: 50,
    // elevation: 50,
  },
  buttonDisable: {
    width: '100%',
    marginTop: 10,
    borderRadius: 4,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#EDEDED',
  },
});

export default SetAvailableTimmingsTwo;
