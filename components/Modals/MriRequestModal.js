import React, {useState} from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Modal,
  TextInput,
  Image,
  FlatList,
  TouchableOpacity
} from 'react-native';
import {set} from 'react-native-reanimated';
import Icon from 'react-native-vector-icons/FontAwesome';
import DropDownPicker from 'react-native-dropdown-picker';
import MultiSelect from 'react-native-multiple-select';
import Button from '../Buttons/button';

const MriRequestModal = (props) => {
    const [data,setData] = useState(
      [
      {
        label: 'Select security question',
        value: '3',
      },
      {
        label: 'Select security question',
        value: '4',
      }
    ]
      )

      const [ selectedItems, setSelectedItems] = useState(null)

      const onChange = () => {
        console.log('Value')
      }

      const onSelectedItemsChange = (selectedItems) => {
        console.log('Selected Items -------->', selectedItems);
        setSelectedItems(selectedItems)
      };
    

  return (
    <Modal
      animationType={'slide'}
      transparent={true}
      visible={true}
      onRequestClose={() => {
        console.log('Modal has been closed.');
      }}>
      <View style={styles.modal}>
        <Icon
          onPress={props.closeModal}
          style={{alignSelf: 'flex-end', marginRight: 17}}
          color="#33909F"
          size={24}
          name="times-circle"
        />
        <Text style={styles.text}>MRI Request</Text>
<View style={{backgroundColor: 'yellow', width: '100%'}}>
          <MultiSelect
            style={styles.inputFields}
            hideTags
            items={[
              {
                id: '1',
                name: 'A',
              },
              {
                id: '2',
                name: 'B',
              },
              {
                id: '3',
                name: 'C',
              },
              {
                id: '4',
                name: 'D',
              },
            ]}
            uniqueKey="id"
            ref={(component) => {
              multiSelect = component;
            }}
            onSelectedItemsChange={onSelectedItemsChange}
            selectedItems={selectedItems}
            selectText="Pick Items"
            searchInputPlaceholderText="Search Items..."
            onChangeInput={(text) => console.log(text)}
            altFontFamily="ProximaNova-Light"
            tagRemoveIconColor="#CCC"
            tagBorderColor="#CCC"
            tagTextColor="#CCC"
            selectedItemTextColor="#CCC"
            selectedItemIconColor="#CCC"
            itemTextColor="#000"
            displayKey="name"
            searchInputStyle={{color: '#CCC'}}
            submitButtonColor="#0076fe"
            submitButtonText="Add"
          />
        <View>
          {/* {this.multiSelect &&
            this.multiSelect.getSelectedItemsExt(selectedItems)} */}
        </View>

<Button title="Pay Now" onClick={onChange}></Button>


</View>


      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  input: {
    marginTop: 10,
    paddingLeft: 10,
    borderRadius: 8,
    height: 49,
    alignSelf: 'stretch',
    marginLeft: 40,
    marginRight: 40,
    borderWidth: 1,
    borderColor: '#cae3ff',
    marginBottom: 20,
  },
  modal: {
    padding: 30,
    alignItems: 'center',
    backgroundColor: '#fefefe',
    width: '100%',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#fff',
    bottom: 0,
    position: 'absolute',
  },
  text: {
    textAlign: 'center',
    fontSize: 20,
    width: '100%',
    fontWeight: 'bold',
  },
  container: {
    marginTop: 50,
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderRadius: 10,
    width: '100%',
    height: 60,
    backgroundColor: '#0158ff',
  },
  inputText: {
    marginTop: 20,
    paddingLeft: 10,
    borderRadius: 8,
    height: 50,
    alignSelf: 'stretch',
    borderWidth: 1,
    borderColor: '#cae3ff',
    marginBottom: 20,
  },

});

export default MriRequestModal;
