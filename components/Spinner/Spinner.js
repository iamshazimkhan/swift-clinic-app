import React, {Component} from 'react';
import Spinner from 'react-native-loading-spinner-overlay';

 const SpinnerComponent = (props) => {
    return(
        <Spinner
        textContent=''
        visible={props.Visible}
        textStyle={{color: '#FFF'}}></Spinner>
    )
}

export default SpinnerComponent;
