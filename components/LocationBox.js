import React from 'react';
import {View, Image, Text} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

const LocationBox = (props) => {
  return (
    <View
      onStartShouldSetResponder={props.dropDown}
      style={{
        borderWidth: 1,
        paddingLeft: 10,
        paddingRight: 10,

        width: '100%',
        borderColor: '#dcecef',
        borderRadius: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
      }}>
      <View style={{flexDirection: 'row', margin: 10}}>
        {/* <View style={{alignSelf: 'center'}}>
          <Icon
            style={{  alignSelf: 'center'}}
            color="#33909F"
            size={24}
            name="map-marker"
          />
          </View> */}
        <View
          style={{
            flexDirection: 'column',
            justifyContent: 'center',
            paddingLeft: 10,
          }}>
          <Text
            style={{
              color: 'black',
              fontSize: 14,
              fontFamily: 'Axiforma-Regular',
              lineHeight: 24,
            }}>
            {props.name + ','}
          </Text>
          <Text
            style={{
              color: 'black',
              fontSize: 14,
              fontFamily: 'Axiforma-Regular',
              lineHeight: 24,
            }}>
            {props.cardName}
          </Text>
        </View>
      </View>
      <Icon
        style={{justifyContent: 'center', marginTop: 13}}
        color={props.Colour ? props.Colour : '#ffffff'}
        size={15}
        name="chevron-down"
      />
    </View>
  );
};
export default LocationBox;
