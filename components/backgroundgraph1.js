import * as React from 'react';
import Svg, {Mask, Path, G, Defs, LinearGradient, Stop} from 'react-native-svg';

function SvgGraph(props) {
  return (
    <Svg
      width={800}
      height={275}
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}>
      <Mask
        id="graph_svg__a"
        maskUnits="userSpaceOnUse"
        x={0}
        y={0}
        width={700}
        height={275}>
        <Path fill="#fff" d="M0 0h375v275H0z" />
      </Mask>
      <G mask="url(#graph_svg__a)">
        <Path
          opacity={0.15}
          d="M279.608 155.172C200.212 136.076 131.641 43.009 156.837-24.6c20.487-54.888 99.403-85.736 123.2-66.65 22.619 17.909-12.238 73.5 14.623 98.477 26.861 24.977 79.625-16.477 107.664 10.62 22.62 21.206 20.508 68.559 1.902 98.475-27.085 43.578-87.394 47.809-124.618 38.85z"
          fill="url(#graph_svg__paint0_linear)"
        />
      </G>
      <Defs>
        <LinearGradient
          id="graph_svg__paint0_linear"
          x1={352.988}
          y1={-8.44}
          x2={267.186}
          y2={116.723}
          gradientUnits="userSpaceOnUse">
          <Stop stopColor="#0486FF" />
          <Stop offset={1} stopColor="#04F" stopOpacity={0} />
        </LinearGradient>
      </Defs>
    </Svg>
  );
}

export default SvgGraph;

