import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Image, View, Text,StyleSheet,TouchableOpacity} from 'react-native';


const NoDataBox = (props) => {
  return (
    <View style={{padding: 10}}>
   <View style={styles.container}>
   {/* <TouchableOpacity onPress={props.onClick} style={{flexDirection: 'column', width: '100%'}}> */}
   <View style={{flexDirection: 'row', justifyContent: 'center'}}>
         <Image source={require('../../assets/bell.jpeg')} style={{width: 60, height: 60, alignSelf: 'center'}}></Image>     
         <Text style={{ color: '#171717', textAlign: 'center',alignSelf: 'center', color: 'skyblue', fontFamily : 'Axiforma-Book'}}>{props.title}</Text>
         </View>
   </View>
   </View>
);
}

const styles = StyleSheet.create({
    container: {
      marginTop: 10,
      flexDirection: 'row',
      backgroundColor:'white',
      borderRadius: 10,
      shadowColor: "dodgerblue",
      shadowOffset: { width: 0, height: 4 },
      shadowRadius: 6,
      shadowOpacity: 0.2,
      elevation: 7, 
      padding: 15
    },
});

export default NoDataBox;