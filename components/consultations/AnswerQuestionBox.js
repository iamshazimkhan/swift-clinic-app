import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Button, View, Text,StyleSheet,TouchableOpacity} from 'react-native';
const AnswerQuestionBox = (props) => {
   return (
    <View style={{paddingRight:10,paddingLeft:10, paddingBottom: 12}}>
      <View style={styles.container}>
      <TouchableOpacity onPress={props.onClick} style={{flexDirection: 'row', justifyContent: 'space-between', width: '100%'}}>
          <View style={{flexDirection: 'column', width: '70%'}}>
              <Text style={{fontSize:14, color: '#171717', fontWeight:'500', width: '100%'}}>Health/ MRI questionaire</Text>
              <Text style={{fontSize:12, color:'#646464' , fontWeight:'300',marginTop:5}}>{props.doctorName}</Text>
              </View>
          <View style={{alignSelf:'center'}}>
        <Icon
        style={{textAlignVertical: 'center', marginRight: 7}}
        color="#33909F"
        size={18}
        name="chevron-right"
      /></View>
      </TouchableOpacity>
      </View>
      </View>
   );
}

const styles = StyleSheet.create({
    container: {
        marginTop: 10,
        flexDirection: 'row',
        backgroundColor:'white',
        justifyContent:'space-between',
        borderRadius: 10,
        shadowColor: "dodgerblue",
        shadowOffset: { width: 0, height: 4 },
        shadowRadius: 6,
        shadowOpacity: 0.2,
        elevation: 7, 
        padding: 15
    }
});

export default AnswerQuestionBox;