import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Button, View, Text, StyleSheet, TouchableOpacity, ImageBackground} from 'react-native';
const AddConsultation = (props) => {
  return (
    <View style={{borderRadius: 10}}>
    <ImageBackground
    source={require('../../button.png')}
    imageStyle={{borderRadius: 10, minWidth : '100%'}}
      style={[
        styles.container,
        {
          marginTop: props.Top,
          marginBottom: props.Bottom,
        },
      ]}>
        {/* <Image source={require('../../btnbk.png')}> </Image> */}
              <TouchableOpacity onPress={props.onClick} style={{flexDirection: 'row', justifyContent: 'space-between', width: '100%'}}>

      <View style={{alignSelf: 'center'}}>
        <Icon
          style={{textAlignVertical: 'center'}}
          color="#ffffff"
          size={22}
          name="user-md"
        />
      </View>
      <View style={{flexDirection: 'column'}}>
        <TouchableOpacity onPress={props.onClick}>
          <Text
            style={{
              fontSize: 18,
              color: '#171717',
              fontWeight: '600',
              color: '#ffffff',
              paddingLeft:10,
            }}>
            New Consultation
          </Text>
        </TouchableOpacity>
        <Text style={{fontSize: 14, color: '#fff', paddingLeft:10,paddingRight:15}}>
          Book a new appointment 
        </Text>
      </View>
      <View style={{alignSelf: 'center'}}>
        <Icon
          style={{textAlignVertical: 'center'}}
          color="#ffffff"
          size={18}
          name="chevron-right"
        />
      </View>
      </TouchableOpacity>
    </ImageBackground>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginLeft: 10,
    marginRight: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 22,
    paddingBottom: 22,
    borderRadius: 20,
    shadowColor: "dodgerblue",
    shadowOffset: { width: 0, height: 4 },
    shadowRadius: 6,
    shadowOpacity: 0.2,
    elevation: 7,
  },
});

export default AddConsultation;
