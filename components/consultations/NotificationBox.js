import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Button, View, Text,StyleSheet,TouchableOpacity} from 'react-native';


const NotificationBox = (props) => {
  return (
    <View style={{padding: 10}}>
   <View style={styles.container}>
   <TouchableOpacity onPress={props.onClick} style={{flexDirection: 'row', justifyContent: 'space-between', width: '100%'}}>
       <View style={{flex: 5}}>
           <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
             {props.colour === 0 ? (
           <Icon
     style={{textAlignVertical: 'center', marginRight: 7}}
     color="red"
     size={10}
     name="star"
   />
             ) : null }
         <Text style={{fontSize:15, color: '#171717', fontWeight:'500', fontFamily: 'Axiforma-Book'}}>{props.title}</Text>
         </View>
         <View style={{flexDirection: 'row'}}>
<Text style={{fontSize:12, color:'#646464', fontFamily : 'Axiforma-Thin', lineHeight: 17}}>{props.text}</Text>
</View>
           </View>
       <View style={{alignSelf: 'center', flex: 1}}>
     <Icon
     style={{textAlignVertical: 'center', alignSelf: 'flex-end'}}
    color={props.iCon}
     size={18}
     name="chevron-right"
   /></View>
   </TouchableOpacity>
   </View>
   </View>
);
}

const styles = StyleSheet.create({
    container: {
      marginTop: 10,
      flexDirection: 'row',
      backgroundColor:'white',
      justifyContent:'space-between',
      borderRadius: 10,
      shadowColor: "dodgerblue",
      shadowOffset: { width: 0, height: 4 },
      shadowRadius: 6,
      shadowOpacity: 0.2,
      elevation: 7, 
      padding: 15
    },
});

export default NotificationBox;