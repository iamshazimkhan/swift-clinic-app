import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Button, View, Text,StyleSheet,TouchableOpacity} from 'react-native';
const ContentBox = (props) => {
   return (
       <View style={{padding: 10}}>
      <View style={styles.container}>
      <TouchableOpacity onPress={props.onClick} style={{flexDirection: 'row', justifyContent: 'space-between', width: '100%'}}>
          <View>
              <View>
            <Text style={{fontSize:15, color: '#171717', fontWeight:'900', lineHeight: 21.2}}>{props.title}</Text>
            </View>
            <View style={{flexDirection: 'row'}}>
   <Text style={{fontSize:14, color:'#646464',fontFamily : 'Axiforma-Thin', lineHeight: 21.2}}>{props.doctorName}</Text>
   <Icon
        style={{textAlignVertical: 'center', marginRight: 7}}
        color={props.colour}
        size={10}
        name="star"
      />
   </View>
              </View>
          <View style={{alignSelf: 'center'}}>
        <Icon
        style={{textAlignVertical: 'center', marginRight: 7}}
        color="#33909F"
        size={18}
        name="chevron-right"
      /></View>
      </TouchableOpacity>
      </View>
      </View>
   );
}

const styles = StyleSheet.create({
    container: {
        marginTop: 10,
        flexDirection: 'row',
        backgroundColor:'white',
        justifyContent:'space-between',
        borderRadius: 10,
        shadowColor: "dodgerblue",
        shadowOffset: { width: 0, height: 4 },
        shadowRadius: 6,
        shadowOpacity: 0.2,
        elevation: 7, 
        padding: 15
    },
});

export default ContentBox;