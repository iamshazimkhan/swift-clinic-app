import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import {StyleSheet, View, TextInput, Text, Image, Alert} from 'react-native';
import exampleImage from '../../bulb.png';
const exampleImageUri = Image.resolveAssetSource(exampleImage).uri

const SymtomBox = (props) => {
   return (
       <View style={{flexDirection: 'column'}}>
    <View style={styles.symptomImageBox}>
        <Image style={[styles.dp,{opacity: props.opacity}]} source={{ uri : props.URL }} ></Image>
        </View>
   <Text style={{textAlign: 'center', color: props.color,  flexWrap: 'wrap', width: 85, marginLeft: 10, marginRight: 10}}>{props.title}</Text>
        </View>
   );
}

const styles = StyleSheet.create({

    dp: {
      flex: 1,
      width: '90%',
      height: 70,
      resizeMode: 'contain'
      },
      symptomImageBox: {
        padding: 10,
        margin:10,
        alignSelf: 'stretch',
        backgroundColor: 'white',
        borderRadius: 10,
        elevation: 5,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.5,
        shadowRadius: 5,
      },
      textBox: {
        margin:10,
      }
});

export default SymtomBox;