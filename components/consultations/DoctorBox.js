import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Rating, AirbnbRating} from 'react-native-ratings';

import {StyleSheet, View, TextInput, Text, Image, Alert} from 'react-native';

const ratingCompleted = async (rating) => {
    console.log('Rating is: ' + rating);
  }

const DoctorBox = (props) => {
  return (
    <View style={styles.mainBox}>
      <View style={{flex: 1}}>
      <View style={styles.dpBox}>
        <Image style={styles.dp} source={{uri: props.imageUrl === null ? 'https://tvscheduleindia.com/avatar_doctor.png'  : 'https://platform.swiftclinic.com/storage/profile/' + props.imageUrl}}></Image>
      </View>
      </View>
      <View style={{padding: 10, flex: 4}}>
        <Text style={{fontWeight: 'bold', fontSize: 17}}>
          {' '}
          {props.name}{' '}
        </Text>
        <View style={{ flexDirection: 'row'}}>
        <Rating
          onFinishRating={ratingCompleted}
          style={{ paddingTop:10, paddingBottom: 10, alignSelf: 'flex-start'}}
          readonly={true}
          imageSize={15}
          startingValue={props.reviewStar}
        />
        <Text style={{marginTop: 7, marginLeft: 10}}>{props.Reviews}</Text>
        </View>
        {
        (props.about === null || props.about === "") ? null : (<Text style={{textAlign: 'justify', fontFamily : 'Axiforma-Thin', lineHeight: 21.2, fontWeight: '400'}}> { props.about.substring(0, 70).concat('...')} </Text>)
        }
        <View style={{flexDirection: 'row', marginTop: 5}}>
          <Text style={{fontWeight: 'bold'}}>Read More </Text>
          <Icon
            style={{textAlignVertical: 'center', marginLeft: 6, marginTop: 2}}
            color="#33909F"
            size={12}
            name="chevron-right"
          />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  mainBox: {
    flexDirection: 'row',
  },
  // dp: {
  //   alignSelf: 'center',
  //   borderWidth: 2,
  //   marginTop: 14,
  //   borderRadius: 50,
  //   width: 60,
  //   height: 60,
  // },
  dp: {
    alignSelf: 'center',
    borderRadius: 30,
    width: 60,
    height: 60,
  },

  dpBox: {
    justifyContent: 'center',
    borderWidth: 2,
    borderColor: '#33909F',
    marginLeft: 10,
    borderRadius: 31,
    width: 62,
    height: 62,
  },
});

export default DoctorBox;
