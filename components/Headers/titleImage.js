import React from 'react';
import {Image, StyleSheet, View} from 'react-native';
import SvgLogo from './ImageWithTitle'
import SvgGraph from '../backgroundgraph';

const TitleImage = () => {
    return(
      <View>
          <View style={styles.image}>
        <SvgLogo />
        {/* <Image style={{justifyContent: 'center', alignSelf: 'center'}} source={require('../../heartfulllogo.png')}></Image> */}
        </View>
        </View>
    )};
    
    const styles = StyleSheet.create({
      image: {
        alignSelf: 'center',
        marginTop: 20,
        width: '100%',
        alignItems: 'center'
      }
    });

export default TitleImage;