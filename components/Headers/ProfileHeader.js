import React,{Component} from 'react';
import {Image, StyleSheet, View, Alert, Text} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import SvgLogoone from '../../components/Headers/logosvg';
import SvgHeart from '../../components/Headers/SideHeart';

const ProfileHeader = (props) => {
    return(
  <View
    style={{
      marginTop: 25,
      flexDirection: 'row',
      justifyContent: 'space-between',
    }}>
      <View>
      {props.backShow === true ? (
    <View onStartShouldSetResponder={props.Back} style={{flex: 1, alignSelf: 'center' , flexDirection: 'row'}}>
      <Icon
        style={{textAlignVertical: 'center', marginLeft: 20}}
        color="#E73988"
        name="arrow-left"
        size={20}
      />
      <Text
        style={{textAlignVertical: 'center', marginLeft: 10, color: 'grey'}}>
        Back
      </Text>
    </View>
      ) : null }
      </View>
    <View
      // onStartShouldSetResponder={() => Alert.alert('Header')}
      style={{alignItems: 'center', marginRight: 10}}>
        <SvgHeart></SvgHeart>
    </View>
  </View>
    )
};

const styles = StyleSheet.create({
  image: {
    marginRight: 20,
  },
  logoView: {
    backgroundColor: 'grey',
    alignItems: 'flex-end',
  },
});

export default ProfileHeader;
