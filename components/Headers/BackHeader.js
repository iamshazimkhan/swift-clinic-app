import React from 'react';
import {Image, StyleSheet, View, Alert, Text} from 'react-native';
import Icon from 'react-native-ionicons';

const BackHeader = () => (
  <View
    style={{
      marginTop: 5,
      flexDirection: 'row',
      justifyContent: 'space-between',
    }}>
    {/* <View style={{flex: 1, flexDirection: 'row'}}>
      <Icon
        style={{textAlignVertical: 'center', marginLeft: 20}}
        color="#0375FF"
        name="arrow-round-back"
      />
      <Text
        style={{textAlignVertical: 'center', marginLeft: 10, color: 'grey'}}>
        Back
      </Text>
    </View> */}
    <View
      // onStartShouldSetResponder={() => Alert.alert('Header')}
      style={{alignItems: 'center', marginRight: 10}}>
      <Image style={styles.image} source={require('../../logo.png')} />
    </View>
  </View>
);

const styles = StyleSheet.create({
  image: {
    marginTop: 20,
    marginRight: 20,
  },
  logoView: {
    backgroundColor: 'grey',
    alignItems: 'flex-end',
  },
});

export default BackHeader;
