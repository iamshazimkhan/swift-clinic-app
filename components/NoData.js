import React, {Component} from 'react';
import { View } from 'react-native';
import {Image} from 'react-native';

const NoData = () => {
    return(
        <View style={{width: '100%'}}>
        <Image style={{alignSelf: 'center', marginTop: 40, flex: 1 , resizeMode: 'contain' }} source={require('../nodatafound.png')}></Image>
        </View>
    )
}

export default NoData;