import React from 'react';
import {TouchableOpacity, Text} from 'react-native';
const CustomButton = (props) => {
   return (
        <TouchableOpacity onPress={props.onClick} style={{alignSelf: 'stretch', backgroundColor: props.colour, borderRadius: 4, height: 50, justifyContent: 'center',marginTop: 30, marginBottom: 60}}>
            <Text style={{color: '#ffffff', textAlign: 'center'}}>{props.title}</Text>
        </TouchableOpacity>
   );
}

export default CustomButton;