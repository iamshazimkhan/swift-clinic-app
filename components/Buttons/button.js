import React from 'react';
import {TouchableOpacity, Text} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

const Button = ({borderRadius = 4, ...props}) => {
  return (
    <LinearGradient
      colors={['#78C2CB', '#33909F']}
      style={{
        alignSelf: 'stretch',
        backgroundColor: '#0158ff',
        borderRadius,
        height: 70,
        justifyContent: 'center',
        marginTop: 30,
        marginBottom: 60,
      }}>
      <TouchableOpacity
        style={{
          height: '100%',
          justifyContent: 'center',
          alignContent: 'center',
          alignItems: 'center',
        }}
        onPress={props.onClick}>
        <Text
          style={{
            color: '#ffffff',
            textAlign: 'center',
            width: '100%',
            alignSelf: 'center',
            justifyContent: 'center',
            textAlignVertical: 'center',
            fontSize: 19,
            fontFamily: 'Axiforma-Book',
          }}>
          {props.title}
        </Text>
      </TouchableOpacity>
    </LinearGradient>
  );
};

export default Button;
