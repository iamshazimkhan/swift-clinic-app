import AwesomeAlert from 'react-native-awesome-alerts';
import React, {Component} from 'react';
import { View } from 'react-native';

const AlertCallRequest = (props) => {
    return(
        <View>
                <AwesomeAlert
                show={props.visibleAlert}
                showProgress={false}
                title={props.title}
                message={props.message}
                closeOnTouchOutside={true}
                closeOnHardwareBackPress={false}
                showCancelButton={true}
                showConfirmButton={true}
                cancelText="Decline"
                confirmText="Answer"
                confirmButtonColor="blue"
                cancelButtonColor="red"
                onCancelPressed={props.cancelPressed}
                onConfirmPressed={props.answerPressed}
              />
        </View>
    )
}

export default AlertCallRequest;