import AwesomeAlert from 'react-native-awesome-alerts';
import React, {Component} from 'react';
import { View } from 'react-native';

const AwesomeAlertComponent = (props) => {
    return(
        <View>
                <AwesomeAlert
                show={props.visibleAlert}
                showProgress={false}
                title={props.title}
                message={props.message}
                closeOnTouchOutside={true}
                closeOnHardwareBackPress={false}
                // showCancelButton={true}
                showConfirmButton={true}
                // cancelText="No, cancel"
                confirmText="OK"
                confirmButtonColor="#33909F"
                onConfirmPressed={props.closeAlert}
              />
        </View>
    )
}

export default AwesomeAlertComponent;