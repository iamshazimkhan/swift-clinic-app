import React, {useState, useEffect} from 'react';
import {
  View,
  Button,
  Modal,
  StyleSheet,
  TouchableOpacity,
  Text,
  ScrollView,
  TouchableHighlight,
  Image,
} from 'react-native';
import {BoxShadow} from 'react-native-shadow';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import LinearGradient from 'react-native-linear-gradient';
import Moment from 'moment';
import Icon from 'react-native-vector-icons/FontAwesome';
import moment from 'moment';
import CalenderModal from '../Date/CalenderModal';
import {getDoctorAvailableTime} from '../../api/ConsultationApi';
import AsyncStorage from '@react-native-community/async-storage';

const AvailableTimmings = ({
  Date,
  startTime,
  showCalender,
  showStartTimePicker,
  showEndTimePicker,
  endTime,
  TimeVisible,
  onClick,
  closeModal,
  timeSelection,
}) => {
  // ----------------------- Date -------------------
  // ----------------------------- Start Time ---------------------------
  let startTime1 = moment('08:00 AM', ['h:mm A']);
  let endTime1 = moment('10:00 PM', ['h:mm A']);

  let timeStops = [];
  const shadowOpt = {
    width: 100,
    height: 100,
    color: '#000',
    border: 2,
    radius: 3,
    opacity: 0.2,
    x: 0,
    y: 3,
    style: {marginVertical: 5},
  };
  while (startTime1 < endTime1) {
    timeStops.push(new moment(startTime1, ['HH.mm']).format('h:mm A'));
    startTime1.add(30, 'minutes');
  }

  let [selectArr, setSelectArr] = useState([]);

  let selectionArray = (item) => {
    let copySelectArr = [...selectArr];
    setSelectArr(copySelectArr);

    if (copySelectArr.includes(item)) {
      const index = copySelectArr.indexOf(item);
      if (index > -1) {
        copySelectArr.splice(index, 1);
      }
    } else {
      copySelectArr.push(item);
    }
    setSelectArr(copySelectArr);
  };

  const [selectedTime, setSelectedTime] = useState();
  const gettime = async () => {
    let id = await AsyncStorage.getItem('id');
    let result = await getDoctorAvailableTime(id, Date);
    let times = result.data.data;
    let timeArray = [];
    if (times != 0) {
      times.forEach((item) => {
        timeArray.push(moment(item.start_time, ['HH.mm']).format('h:mm A'));
      });
      setSelectArr(timeArray);
      console.log(
        'qwerty',
        times,
        'result',
        timeStops,
        // result.data.data,
        // moment(result.data.data[0].start_time, ['HH.mm']).format('h:mm A'),
      );
    }
  };

  useEffect(() => {
    console.log('khurram');
    gettime();
  }, []);

  return (
    <Modal
      animationType={'slide'}
      transparent={true}
      // style={{height: '90%'}}
      visible={TimeVisible}
      onRequestClose={() => {
        console.log('Modal has been closed.');
      }}>
      <View style={styles.modal}>
        <View style={{width: '100%', marginBottom: 7}}>
          <TouchableOpacity onPress={() => closeModal()}>
            <Image
              onPress={() => closeModal()}
              style={{alignSelf: 'flex-end'}}
              // color="black"
              source={require('../../assets/times-circle.png')}
              // size={24}
              // name="times-circle"
            />
          </TouchableOpacity>
        </View>

        <View style={{width: '100%', marginBottom: 33}}>
          <Text style={{textAlign: 'center', fontWeight: 'bold', fontSize: 18}}>
            Select Time
          </Text>
        </View>
        <View style={{width: '100%', marginBottom: 7}}>
          <Text
            style={{
              fontSize: 12,
              color: '#828282',
              textTransform: 'uppercase',
              marginBottom: 5,
              fontFamily: 'Axiforma-Regular',
            }}>
            Selected Date
          </Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            width: '100%',
            paddingBottom: 10,
          }}>
          <Text
            style={{
              fontSize: 16,
              color: '#32919F',
              textTransform: 'capitalize',
            }}>
            {moment(Date).format('DD MMMM YYYY')}
          </Text>
          <Text
            onPress={showCalender}
            style={{
              fontSize: 13,
              color: 'black',
              marginRight: 14,
              fontFamily: 'Axiforma-Regular',
              textDecorationLine: 'underline',
            }}>
            Change
          </Text>
        </View>
        {/* ----------------------- Date ------------------- */}

        {/* -------------------------- End date ---------------- */}
        {Date != 'Select Date' ? (
          <>
            {/* ---------------------- Start Time ------------------ */}
            <ScrollView style={{width: '100%'}}>
              {timeStops.slice(0, 30).map((item, i) => {
                return (
                  <>
                    <View key={i}>
                      <TouchableOpacity
                        style={[
                          selectArr.includes(timeStops[i])
                            ? styles.buttonSelect
                            : styles.buttonUnSelect,
                          {
                            flex: 1,
                            padding: 10,

                            marginRight: 3,
                          },
                        ]}
                        onPress={() => selectionArray(timeStops[i])}>
                        {selectArr.includes(timeStops[i]) ? (
                          <Image
                            source={require('../../assets/images/tickbox.png')}
                            style={{marginRight: 11, marginTop: 2}}
                          />
                        ) : null}

                        <Text
                          style={[
                            selectArr.includes(timeStops[i])
                              ? styles.textSelect
                              : styles.textUnSelect,

                            {},
                          ]}>
                          {item}
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </>
                );
              })}
            </ScrollView>
            {/* ------------------------------ End Time ------------------- */}

            {/* ------------------------------ end end time ------------------------- */}
            {selectArr.length != 0 ? (
              <View style={{width: '100%'}}>
                <LinearGradient
                  colors={['#78C2CB', '#33909F']}
                  style={{
                    width: '98%',
                    alignSelf: 'stretch',
                    backgroundColor: '#0158ff',
                    borderRadius: 4,
                    height: 48,
                    marginRight: 40,
                    marginLeft: 2,
                    justifyContent: 'flex-start',
                    marginTop: 20,
                  }}>
                  <TouchableOpacity
                    onPress={() => timeSelection(selectArr)}
                    style={{
                      width: '100%',
                      height: '100%',
                      justifyContent: 'center',
                      borderRadius: 4,
                    }}>
                    <Text
                      style={{
                        color: '#ffffff',
                        textAlign: 'center',
                        fontSize: 14,
                        // fontFamily: 'Axiforma-Regular',
                        marginTop: 5,
                        marginBottom: 5,
                      }}>
                      Confirm Availability
                    </Text>
                  </TouchableOpacity>
                </LinearGradient>
              </View>
            ) : null}
          </>
        ) : null}
        <View
          style={{
            flexDirection: 'row',
            paddingLeft: 5,
            paddingTop: 30,
            paddingBottom: 18,
          }}>
          <View style={{flex: 1}}>
            <Image source={require('../../assets/images/available.png')} />
          </View>
          <View style={{flex: 7, alignSelf: 'center'}}>
            <Text style={{color: '#33909F'}}>Available</Text>
          </View>
        </View>
        <View style={{flexDirection: 'row', paddingLeft: 5}}>
          <View style={{flex: 1}}>
            <Image source={require('../../assets/images/unavailable.png')} />
          </View>
          <View style={{flex: 7, alignSelf: 'center'}}>
            <Text style={{color: 'grey'}}>Not Available</Text>
          </View>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modal: {
    paddingLeft: 30,
    paddingRight: 30,
    paddingTop: 30,
    paddingBottom: 60,
    alignItems: 'center',
    backgroundColor: '#fefefe',
    width: '100%',
    height: '100%',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#fff',
    bottom: 0,
    position: 'absolute',
  },
  inputText: {
    marginTop: 10,
    paddingLeft: 10,
    borderRadius: 8,
    height: 50,
    alignSelf: 'stretch',
    borderWidth: 1,
    borderColor: '#dcecef',
    justifyContent: 'center',
  },
  buttonUnSelect: {
    width: '97%',
    marginTop: 10,
    marginBottom: 10,
    paddingLeft: 20,
    borderRadius: 4,
    marginHorizontal: '1%',
    // shadowOffset: {width: -8, height: 2},
    height: 48,
    justifyContent: 'center',
    backgroundColor: '#FFFFFF',
    shadowColor: 'rgba(130, 204, 213, 0.31)',
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.2,
    // shadowOpacity: 1,
    // shadowRadius: 10.0,
    elevation: 7,
  },
  buttonSelect: {
    width: '97%',
    marginTop: 10,
    marginBottom: 10,
    paddingLeft: 20,
    borderRadius: 4,
    marginHorizontal: '1%',
    backgroundColor: '#EDEDED',
    marginHorizontal: '1%',

    alignItems: 'center',
    height: 48,
    flexDirection: 'row',
  },
  textUnSelect: {
    color: '#32919F',
    fontFamily: 'Axiforma-Regular',
  },
  textSelect: {
    color: '#32919F',
    fontFamily: 'Axiforma-Regular',
  },
});

export default AvailableTimmings;
