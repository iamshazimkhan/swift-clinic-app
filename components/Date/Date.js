import React, { useState } from "react";
import { View, Text, TouchableOpacity, StyleSheet } from "react-native";
import DateTimePickerModal from "react-native-modal-datetime-picker";

const DatePicker = ({ Date,selectedDateCallback }) => {
    const [isDatePickerVisible, setDatePickerVisibility] = useState(false);

    const showDatePicker = () => {
        setDatePickerVisibility(true);
    };

    const hideDatePicker = () => {
        setDatePickerVisibility(false);
    };

    const handleConfirm = (date) => {
        console.log("A date has been picked: ", date);
        hideDatePicker();

        selectedDateCallback(date);
    };

    return (
        <View onStartShouldSetResponder={showDatePicker} style={styles.inputText}>
            <TouchableOpacity onPress={showDatePicker}><Text style={{textAlign: 'center'}}>{Date}</Text></TouchableOpacity>
            <DateTimePickerModal
            style={{width: 10, height: '10'}}
                isVisible={isDatePickerVisible}
                mode="datetime"
                onConfirm={handleConfirm}
                onCancel={hideDatePicker}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    inputText: {
        marginTop: 10,
        paddingLeft: 10,
        borderRadius: 8,
        height: 50,
        alignSelf: 'stretch',
        borderWidth: 1,
        borderColor: '#cae3ff',
        justifyContent: 'center'
      }
})

export default DatePicker;