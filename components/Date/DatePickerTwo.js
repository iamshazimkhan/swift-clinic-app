import React, {useState} from 'react';
import {View, Text, TouchableOpacity, StyleSheet, Image} from 'react-native';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import moment from 'moment';

const DatePickerTwo = ({Date, showCalender, Time}) => {
  let date;
  console.log(
    moment(Date, 'YYYY-MM-DD h:mm A').format('h:mm A.      DD / MM / YYYY '),
    'asw',
    Date,
  );
  if (Time == 'Select Time') {
    date = 'Select Date';
  } else {
    if (Date == 'Select Date') {
      date = Date;
      // YYYY;
    } else {
      date = moment(Date, 'YYYY-MM-DD h:mm').format(
        'h:mm A.      DD / MM / YYYY ',
      );
      // date = Date;
    }
  }
  // console.log(Date, 'sdj', date);
  return (
    <View onStartShouldSetResponder={showCalender} style={styles.inputText}>
      <TouchableOpacity onPress={showCalender}>
        <Text style={{fontWeight: '800', marginLeft: 12, color: '#646464'}}>
          {date}
        </Text>
        <Image
          style={{position: 'absolute', top: '40%', right: 17}}
          source={require('../../assets/angle-right.png')}
        />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  inputText: {
    marginTop: 10,
    paddingLeft: 10,
    borderRadius: 8,
    height: 50,
    position: 'relative',
    alignSelf: 'stretch',
    borderWidth: 1.2,
    borderColor: '#dcecef',
    justifyContent: 'center',
  },
});

export default DatePickerTwo;
