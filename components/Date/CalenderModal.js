import {Calendar, CalendarList, Agenda} from 'react-native-calendars';
import React, {useState} from 'react';
import {RadioButton} from 'react-native-paper';
import Icon from 'react-native-vector-icons/FontAwesome';
import {
  Image,
  Modal,
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
} from 'react-native';

const CalenderModal = (props) => {
  const clickSelectedDate = async (day) => {
    console.log('Selected Date -------->', day.dateString);
    props.selectedDateCallback(day.dateString);
  };
  const [checked, setChecked] = React.useState('first');
  let {name} = props;
  let _renderArrow = (direction) => {
    if (direction === 'left') {
      return <Image source={require('../../assets/images/leftArrow.svg')} />;
    } else {
      return <Image source={require('../../assets/images/rightArrow.svg')} />;
    }
  };
  return (
    <Modal
      animationType={'slide'}
      transparent={true}
      visible={props.Visibilty}
      onRequestClose={() => {
        console.log('Modal has been closed.');
      }}>
      <View style={styles.modal}>
        <TouchableOpacity onPress={props.closeModal}>
          <Image
            style={{alignSelf: 'flex-end', marginRight: 17, marginBottom: 7}}
            source={require('../../assets/times-circle.png')}

            // color="#33909F"
            // size={24}
            // name="times-circle"
          />
        </TouchableOpacity>
        {/* <Text style={styles.meeting}>{name}</Text> */}
        <Text style={styles.heading}>Select Date</Text>

        <View style={styles.calender}>
          <Calendar
            current={{
              '2012-05-18': {
                marked: true,
                dotColor: 'red',
                activeOpacity: 0,
                disabled: false,
              },
            }}
            disableAllTouchEventsForDisabledDays={props.disabledDays}
            minDate={'2021-07-14'}
            horizontal={true}
            pagingEnabled={true}
            hideExtraDays={true}
            disabledByDefault={true}
            onDayPress={clickSelectedDate}
            disableTouchEvent={true}
            // renderArrow={_renderArrow}
            // disableAllTouchEventsForDisabledDays={props.disabled}
            enableSwipeMonths={true}
            // Collection of dates that have to be marked. Default = {}
            markedDates={props.customDateArray}
            markingType={'custom'}
            theme={{
              monthTextColor: 'black',
              textMonthFontWeight: 'bold',
              textMonthFontSize: 20,
            }}
          />
        </View>
        <View
          style={{
            flexDirection: 'row',
            paddingLeft: 15,
            paddingTop: 30,
            paddingBottom: 18,
          }}>
          <View style={{flex: 1}}>
            <Image source={require('../../assets/images/available.png')} />
          </View>
          <View style={{flex: 7, alignSelf: 'center'}}>
            <Text style={{color: '#33909F'}}>Available</Text>
          </View>
        </View>
        <View style={{flexDirection: 'row', paddingLeft: 15}}>
          <View style={{flex: 1}}>
            <Image source={require('../../assets/images/unavailable.png')} />
          </View>
          <View style={{flex: 7, alignSelf: 'center'}}>
            <Text style={{color: 'grey'}}>Not Available</Text>
          </View>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  inputText: {
    marginTop: 10,
    paddingLeft: 10,
    borderRadius: 8,
    height: 50,
    alignSelf: 'stretch',
    borderWidth: 1,
    borderColor: '#cae3ff',
    justifyContent: 'center',
  },
  calender: {
    paddingTop: '7%',
  },
  meeting: {
    marginLeft: 20,
    fontWeight: 'bold',
    fontSize: 28,
  },
  heading: {
    // marginLeft: 20,
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 20,
  },
  modal: {
    // alignItems: 'center',
    paddingHorizontal: 20,
    paddingTop: 30,
    backgroundColor: '#fefefe',
    width: '100%',
    height: '100%',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#fff',
    bottom: 0,
    position: 'absolute',
  },
});

export default CalenderModal;
