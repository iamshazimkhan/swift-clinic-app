import React, {useEffect, useState} from 'react';
import AppContainer from './navigations';
import { StatusBar, Alert } from 'react-native'
import NavigationService from './navigations/NavigationService';
import {NavigationActions} from 'react-navigation';
import RNCallKeep from 'react-native-callkeep';
import AsyncStorage from '@react-native-community/async-storage'
import BackgroundTimer from 'react-native-background-timer';
import messaging from '@react-native-firebase/messaging';
import{displayIncomingCallNow} from './services/CallKeep';


async function requestUserPermission() {
  const authStatus = await messaging().requestPermission();
  const enabled =
    authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
    authStatus === messaging.AuthorizationStatus.PROVISIONAL;

  if (enabled) {
    console.log('Authorization status:', authStatus);
  }
}


const handleSelected = async remoteMessage => {
  const notification = JSON.parse(remoteMessage.data.message);
  console.log('Notification is ------------>',notification);
};

const isLoggedInPatient  = async () => {
  const d = await AsyncStorage.getItem('isLoggedIn');
  const l = await AsyncStorage.getItem('user_level');
    // console.log('Logged IN ----------------->', d);
    if (d === 'true') {
      if(l === '1'){
        return true;
      }
    }
    return false;
}


export default function App() {

  const isLoggedInPatient  = async (doctr,consult, channel) => {
    const d = await AsyncStorage.getItem('isLoggedIn');
    const l = await AsyncStorage.getItem('user_level');
      console.log('Logged IN App ----------------->', d,l);
      if (d === 'true') {
        if(l === '1'){
          displayIncomingCallNow(doctr,consult, channel);
        }else if(l === '2')
        {
          console.log('Doctor account')
        }
      }
  }


    useEffect(() => {
    requestUserPermission();  
    messaging().getInitialNotification().then(handleSelected);  
    const unsubscribe = messaging().onMessage(async remoteMessage => {
      console.log('A new FCM message arrived App.js!', JSON.stringify(remoteMessage));
      if(remoteMessage.data.videocall === true || remoteMessage.data.videocall === "true")
      {
        console.log('Videocall Notification A')
        isLoggedInPatient(remoteMessage.data.doctor_name,remoteMessage.data.consultation_id,remoteMessage.data.channel_id);
      }
    });

    return unsubscribe;
  }, []);

// ---------------------------- end call part --------------------


  return <AppContainer ref={navigatorRef => {
    NavigationService.setNavigator(navigatorRef);
  }} />;
}

