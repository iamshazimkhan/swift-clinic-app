import React, {Component} from 'react';
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  TextInput,
  Alert,
  Platform
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { SafeAreaView } from 'react-navigation';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { ChangePasswordRequest } from '../../api/ConsultationApi';
import MainHeader from '../../components/Headers/MainHeader';
import SpinnerComponent from '../../components/Spinner/Spinner';
import Toast from 'react-native-simple-toast';
import SvgGraph from '../../components/backgroundgraph';


class ChangePassword extends Component {

  state = {
    old_password: '',
    new_password : '',
    confirm_password: '',
    spinnerLoading: false
  }

  onChangePassword = async () => {
    const { old_password, new_password, confirm_password} = this.state;
    console.log('Change Password');
    if(old_password === '' || new_password === '' || confirm_password === '')
    {
      Alert.alert('Please fill all fields')
    }else if(new_password !== confirm_password)
    {
      Alert.alert('Passwords does not match')
    }
    else if(new_password.length < 6)
    {
      Alert.alert('Password must be greater than 6 characters.')
    }
    else{
this.submitPassword();
    }

  }

  submitPassword = async () => {
    await this.setState({spinnerLoading: true});
    const { old_password, new_password} = this.state;
    ChangePasswordRequest(old_password, new_password)
    .then(async (res) => {
      if(res.data.error)
      {
        this.setState({
          spinnerLoading: false,
        });
       Alert.alert('Old password is wrong.')
      }else{
      this.setState({
        spinnerLoading: false,
      });
      Toast.show('Password successfully changed.');
      this.props.navigation.navigate('Profile');
    }
    })
    .catch((err) => console.log('Error -------->', err));
  }

  backToScreen = async () => {
    this.props.navigation.goBack(null);
  };

  // ------------------------- End Location --------------------------

  render() {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#ffffff'}}>
        <ScrollView>
        <SvgGraph style={{right:-38}} />
      <View style={{marginTop: -280}}>
          <MainHeader Back={this.backToScreen}></MainHeader>
          <Text style={{
                textAlign: 'center', fontFamily: 'Axiforma-Medium', fontSize: 26, marginTop: 40
              }}>Change Password</Text>
             <View style={{marginTop: 40}}>
             <TextInput
              secureTextEntry={Platform.OS === 'android' ? ( this.state.old_password === '' ? false : true) : true}         
              placeholderTextColor="black"
              value={this.state.old_password}
              onChangeText={(old_password) => this.setState({old_password})}
              placeholder={'Enter current password'}
              style={styles.input}
            />
            <View style={{marginTop: 20}}>
                        <TextInput
              secureTextEntry={Platform.OS === 'android' ? ( this.state.new_password === '' ? false : true) : true}         
              placeholderTextColor="black"
              value={this.state.new_password}
              onChangeText={(new_password) => this.setState({new_password})}
              placeholder={'Enter New Password'}
              style={styles.input}
            />
                        <TextInput
              secureTextEntry={Platform.OS === 'android' ? ( this.state.confirm_password === '' ? false : true) : true}         
              placeholderTextColor="black"
              value={this.state.confirm_password}
              onChangeText={(confirm_password) => this.setState({confirm_password})}
              placeholder={'Re-Type new password'}
              style={styles.input}
            />

<LinearGradient colors={['#78C2CB', '#33909F']} style={styles.button}>
            <TouchableOpacity onPress = {this.onChangePassword} style={{width: '100%', height: '100%', justifyContent:'center'}}>
              <Text style={{color: '#ffffff', textAlign: 'center'}}>Confirm</Text>
            </TouchableOpacity>
            </LinearGradient>
            </View>
             </View>
             <SpinnerComponent
            Visible={this.state.spinnerLoading}></SpinnerComponent>
            </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  input: {
    marginTop: 5,
    paddingLeft: 10,
    borderRadius: 8,
    height: 50,
    alignSelf: 'stretch',
    marginLeft: 20,
    marginRight: 20,
    borderWidth: 1,
    borderColor: '#dcecef',
    fontFamily: 'Axiforma-Thin',
    marginBottom: 10,
  },
  button: {
    borderRadius: 9,
    marginLeft: 80,
    marginBottom: 20,
    height: 70,
    justifyContent: 'center',
    marginRight: 80,
    marginTop: 25,
    backgroundColor: '#0158ff',
    alignItems: 'center',
    alignSelf: 'stretch',
  },
})

export default ChangePassword;
