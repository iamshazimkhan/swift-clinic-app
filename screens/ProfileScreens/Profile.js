import React, {Component} from 'react';
import {
  TextInput,
  View,
  Text,
  StyleSheet,
  Image,
  ScrollView,
  Switch,
  Alert,
  TouchableOpacity,
  Platform,
  PermissionsAndroid,
  Linking
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {
  LoginManager,
  LoginButton,
  AccessToken,
  GraphRequest,
  GraphRequestManager,
} from 'react-native-fbsdk';
import { SafeAreaView } from 'react-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';
import Spinner from 'react-native-loading-spinner-overlay';
import * as constants from '../../constants';
import AsyncStorage from '@react-native-community/async-storage';
import AwesomeAlert from 'react-native-awesome-alerts';
import DropDownPicker from 'react-native-dropdown-picker';
import ImagePicker from 'react-native-image-picker';
import ProfileHeader from '../../components/Headers/ProfileHeader';
import SvgGraph from '../../components/backgroundgraph'; 
import {GetProfileData} from '../../api/ConsultationApi';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import ImagesHeader from '../../components/header';

export default class Login extends Component {
  //   static navigationOptions = {
  //     header: null,
  //   };

  constructor(props) {
    super(props);

    this.state = {
      showBack: false,
      full_name: '',
      first_name: '',
      last_name: '',
      email: '',
      phone_number: '',
      spinner: true,
      profile_pic: '',
      showAlert: false,
      showAlertService: false,
      showAlertService2: false,
      password: '',
      security_question_1: '3',
      security_answer_1: '',
      security_question_2: '4',
      security_answer_2: '',
      editable: false,
      switch: false,
      token: 'abcdsefbrjgbjbktbkkkkkkkkkkkkkklllllll',
      security_question_1_full: '',
      security_question_2_full: '',
      avatar: null,
      items: [
        {
          label: 'Select Question',
          value: '3',
        },
        {
          label: 'Select Question',
          value: '4',
        },
      ],
    };
  }

  async updateProfile() {
    const {
      first_name,
      last_name,
      email,
      phone_number,
      security_question_1,
      security_question_2,
      security_answer_1,
      security_answer_2,
    } = this.state;
    if (
      !first_name ||
      !last_name ||
      !email ||
      !phone_number ||
      !security_question_1 ||
      !security_question_2 ||
      !security_answer_1 ||
      !security_answer_2
    ) {
      Alert.alert('Please fill all Fields');
    } else {
      const token = await AsyncStorage.getItem('token');
      this.setState({spinner: true});
      fetch(constants.URL + 'api/update-profile', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + token,
        },
        body: JSON.stringify({
          email: this.state.email,
          first_name: this.state.first_name,
          last_name: this.state.last_name,
          security_question_1: this.state.security_question_1,
          security_question_2: this.state.security_question_2,
          phone_number: this.state.phone_number,
          security_answer_1: this.state.security_answer_1,
          security_answer_2: this.state.security_answer_2,
        }),
      })
        .then((response) => response.json())
        .then((responseJson) => {
          this.setState({spinner: false, editable: false});
          console.log('Response ======-------->', responseJson);
          if (responseJson.error === false) {
            this.getProfile();
          } else {
            this.setState({spinner: false, showAlert: true});
          }
        })
        .catch((error) => {
          this.setState({showAlert: true});
          console.error(error);
        });
    }
  }

  async updateImage(data) {
    let filename = "randmfile"
    if(data.fileName){
      filename = data.fileName
    }
    // console.log('Filename is ------------->', data.fileName);
    const token = await AsyncStorage.getItem('token');
    const dataImage = new FormData();
    dataImage.append('profile_pic', {
      uri: data.uri,
      type: data.type,
      name: filename,
    });
    fetch(constants.URL + 'api/update-profile-picture', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
        Authorization: 'Bearer ' + token,
      },
      body: dataImage,
    })
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({spinner: false, editable: false, showBack: false});
        console.log('Response ======-------->', responseJson);
      })
      .catch((error) => {
        this.setState({showAlert: true});
        console.error(error);
      });
  }

  async handlePicker() {
    // console.log('edit');
    ImagePicker.showImagePicker({}, (response) => {
      // console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        this.setState({avatar: response.uri, spinner: true });
        console.log('URI -------------->', response.uri);
        this.updateImage(response);
      }
    });
  }

  async onSwitch() {
    const token = await AsyncStorage.getItem('token');
    let v = !this.state.switch;
    this.setState({switch: v});
    let status = 0;
    if (v === true) {
      status = 1;
      // console.log('Status Value in Condition ------------>', status);
    }
    // console.log('-----------> Status Value outer -------------->', status);
    fetch(constants.URL + 'api/update-push-preference', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token,
      },
      body: JSON.stringify({
        push_status: status,
        push_token: this.state.token,
      }),
    })
      .then((response) => response.json())
      .then((responseJson) => {
        // console.log('Status Chnage-------------->', responseJson);
      })
      .catch((error) => {
        console.error(error);
      });
  }

  hideAlert = () => {
    this.setState({
      showAlert: false,
    });
    this.props.navigation.navigate('Auth');
  };

  async componentDidMount() {
    await this.getProfile();
    await this.getQuestions();
  }

  async addPermission()
  {
    if (Platform.OS === 'android') {
      // Calling the permission function
      // const granted = await PermissionsAndroid.request(
      //   PermissionsAndroid.PERMISSIONS.CAMERA,
      //   {
      //     title: 'Camera Permission',
      //     message: 'Swift Clinic needs access to your camera',
      //   },
      // );
      // if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      //   // Permission Granted
      //   this.setState({ showAlertService: true })
      // } else {
      //   // Permission Denied
      //   this.setState({ showAlertService: true })
      // }
      PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.CAMERA).then(response => {
        // console.log('Camera Permission ----------------->',response);
        if(response === true)
        {
        this.setState({showAlertService2: true})
        }
        else
        {
          this.setState({showAlertService: true}) 
        }
      });
    } else {
      Alert.alert('Permission is Granted');
    }
  }

  async grantPermission()
  {
      await this.setState({showAlertService: false})
      const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.CAMERA);
  }

  async getQuestions() {
    this.setState({spinner: true});
    await fetch(constants.URL + 'api/fetch-security-questions', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then((response) => response.json())
      .then((responseJson) => {
        const d = responseJson.data;
        // console.log('------Data ---------->', d);
        const allData = [];
        for (let index = 0; index < d.length; index++) {
          const result = {
            label: d[index].question,
            value: d[index].id,
          };
          // console.log('Item in loop---------->', result);
          allData.push(result);
        }
        // console.log('Complete Data---------->', allData);
        this.setState({items: allData, spinner: false});
        // console.log('Complete Data in State---------->', this.state.items);
      })
      .catch((error) => {
        this.setState({spinner: false});
        console.error(error);
      });
  }

  async logOut() {
    const fb_token = await AsyncStorage.getItem('fb_token');
    if(fb_token === null){
    await AsyncStorage.clear();
    this.props.navigation.navigate('LoginSignup');
    }else{
        let logout =
            await new GraphRequest(
                "me/permissions/",
                {
                    accessToken: fb_token,
                    httpMethod: 'DELETE'
                },
                (error, result) => {
                    if (error) {
                        console.log('Error fetching data: ' + error.toString());
                    } else {
                        LoginManager.logOut();
                        console.log('fb logout Successfully')
                    }
                });
        await new GraphRequestManager().addRequest(logout).start();
        await AsyncStorage.clear();
        this.props.navigation.navigate('LoginSignup');
    }
  }

  async getProfile() {
    this.setState({spinner: true, showBack: false});
    GetProfileData()
    .then(async res => {
        this.setState({spinner: false});
        let responseJson = res.data;
        let v = false;
        console.log('Profile pic ======-------->', responseJson.user.profile_pic);
          if (responseJson.user.push_notification_status === 1) {
            v = true;
          } else {
            v = false;
          }
          await this.setState({
            switch: v,
            first_name: responseJson.user.first_name,
            last_name: responseJson.user.last_name,
            email: responseJson.user.email,
            phone_number: responseJson.user.phone_number,
            security_question_1: responseJson.user.security_question_1,
            security_question_2: responseJson.user.security_question_2,
            security_question_1_full:
              responseJson.user.security_question_1_full,
            security_question_2_full:
              responseJson.user.security_question_2_full,
            security_answer_1: responseJson.user.security_answer_1,
            security_answer_2: responseJson.user.security_answer_2,
            avatar: responseJson.user.profile_pic,
            full_name:
              responseJson.user.first_name + ' ' + responseJson.user.last_name,
          });
        
      })
      .catch((error) => {
        this.setState({spinner: false});
        console.error(error);
      });
  }

  goBack = async () => {
   await this.setState({editable: false, showBack: false})
  }

  render() {
    return (
      <KeyboardAwareScrollView>
        <SafeAreaView style={styles.container}>
        <SvgGraph style={{right:-38}} />
      <View style={{marginTop: -280}}>
          <Spinner
            visible={this.state.spinner}
            textContent={''}
            textStyle={styles.spinnerTextStyle}
          />

          <ProfileHeader backShow={this.state.showBack} Back={this.goBack}></ProfileHeader>
          <View style={styles.dpFlex}>
            <View
              onStartShouldSetResponder={this.handlePicker.bind(this)}
              style={{flex: 1, flexDirection: 'row'}}>
                <View style={styles.dpBox}>
              <Image style={styles.dp} source={{uri: this.state.avatar}} />
              </View>
              <Text style={styles.Name}>{this.state.full_name}</Text>
            </View>
            <View
              onStartShouldSetResponder={() => this.setState({editable: true, showBack: true})}
              style={styles.editBox}>
              <Icon
                name="edit"
                size={25}
                style={{color: '#33909F', padding: 8}}
              />
            </View>
          </View>
          <View style={styles.headTitles}>
            <View style={{flex: 1, flexDirection: 'row'}}>
              <View style={styles.iconBox}>
                <Icon
                  name="bell"
                  size={15}
                  style={{color: '#33909F', padding: 8}}
                />
              </View>
              <Text style={styles.headTeext}>Push Notifications</Text>
            </View>
            <Switch
              style={styles.switchPush}
              onValueChange={this.onSwitch.bind(this)}
              value={this.state.switch}
            />
          </View>
          <View  onStartShouldSetResponder={() => Linking.openURL('mailto:contact@swiftclinic.com?subject=&body=')} style={styles.headTitles}>
            <View style={styles.iconBox}>
              <Icon
                name="file"
                size={14}
                style={{color: '#33909F', padding: 10}}
              />
            </View>
            <Text style={styles.headTeext}>Contact Us</Text>
          </View>
          <TouchableOpacity onPress={this.addPermission.bind(this)} style={styles.headTitles}>
            <View style={styles.iconBox}>
              <Icon
                name="microphone"
                size={17}
                style={{color: '#33909F', padding: 10}}
              />
            </View>
            <Text style={styles.headTeext}>Test Microphone/Camera</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={ () => this.props.navigation.navigate('ChangePassword')} style={styles.headTitles}>
            <View style={styles.iconBox}>
              <Icon
                name="unlock-alt"
                size={17}
                style={{color: '#33909F', padding: 10}}
              />
            </View>
            <Text style={styles.headTeext}>Change Password</Text>
          </TouchableOpacity>
          <View style={styles.items}>
            <Text style={styles.label}> Public Information: </Text>
            <TextInput
              value={this.state.first_name}
              onChangeText={(first_name) => this.setState({first_name})}
              placeholder={'First Name:'}
              editable={this.state.editable}
              style={styles.input}
            />
            <TextInput
              value={this.state.last_name}
              onChangeText={(last_name) => this.setState({last_name})}
              placeholder={'Last Name:'}
              editable={this.state.editable}
              style={styles.input}
            />
            <Text style={styles.label}> Information is not displayed: </Text>
            <TextInput
              value={this.state.email}
              onChangeText={(email) => this.setState({email})}
              placeholder={'Email'}
              editable={this.state.editable}
              style={styles.input}
            />
            <TextInput
              value={this.state.phone_number}
              keyboardType={'numeric'}
              onChangeText={(phone_number) => this.setState({phone_number: phone_number.replace(/[^0-9]/g, '') })}
              placeholder={'Phone Number'}
              maxLength={11}
              editable={this.state.editable}
              style={styles.input}
            />
            {/* <TextInput
              value={this.state.password}
              onChangeText={(password) => this.setState({password})}
              placeholder={'Password'}
              editable={false}
              style={styles.input}
            /> */}

            {this.state.editable ? (
              <DropDownPicker
                containerStyle={{marginLeft: 20, marginRight: 20}}
                editable={false}
                items={this.state.items}
                defaultValue={this.state.security_question_1}
                style={styles.inputDropDown}
                dropDownStyle={{backgroundColor: '#fafafa'}}
                onChangeItem={(item) =>
                  this.setState({
                    security_question_1: item.value,
                  })
                }
              />
            ) : (
              <TextInput
                value={this.state.security_question_1_full}
                onChangeText={(security_question_1_full) =>
                  this.setState({security_question_1_full})
                }
                placeholder={'Security Question 1'}
                editable={this.state.editable}
                style={styles.input}
              />
            )}

            <TextInput
              value={this.state.security_answer_1}
              onChangeText={(security_answer_1) =>
                this.setState({security_answer_1})
              }
              placeholder={'Security Answer 1'}
              editable={this.state.editable}
              style={styles.input}
            />

            {this.state.editable ? (
              <DropDownPicker
                containerStyle={{marginLeft: 20, marginRight: 20}}
                items={this.state.items}
                defaultValue={this.state.security_question_2}
                style={styles.inputDropDown}
                dropDownStyle={{backgroundColor: '#fafafa'}}
                onChangeItem={(item) =>
                  this.setState({
                    security_question_2: item.value,
                  })
                }
              />
            ) : (
              <TextInput
                value={this.state.security_question_2_full}
                onChangeText={(security_question_2_full) =>
                  this.setState({security_question_2_full})
                }
                placeholder={'Security Question 2'}
                editable={this.state.editable}
                style={styles.input}
              />
            )}

            <TextInput
              value={this.state.security_answer_2}
              onChangeText={(security_answer_2) =>
                this.setState({security_answer_2})
              }
              placeholder={'Security Answer 2'}
              editable={this.state.editable}
              style={styles.input}
            />
            {this.state.editable ? (
              <LinearGradient colors={['#78C2CB', '#33909F']} style={styles.button}>
              <TouchableOpacity
                style={{width: '100%', height: '100%', justifyContent: 'center'}}
                onPress={this.updateProfile.bind(this)}>
                <Text style={{color: 'white', textAlign: 'center'}}>
                  Confirm
                </Text>
              </TouchableOpacity>
              </LinearGradient>
            ) : null}
            <AwesomeAlert
              show={this.state.showAlert}
              showProgress={false}
              title="Token Expired"
              message="Please Login again!"
              closeOnTouchOutside={true}
              closeOnHardwareBackPress={false}
              // showCancelButton={true}
              showConfirmButton={true}
              // cancelText="No, cancel"
              confirmText="OK"
              confirmButtonColor="#33909F"
              onCancelPressed={() => {
                this.hideAlert();
              }}
              onConfirmPressed={() => {
                this.hideAlert();
              }}
            />
              <AwesomeAlert
              show={this.state.showAlertService}
              showProgress={false}
              title="Swift Clinic Permission"
              message="Permission is not Granted"
              closeOnTouchOutside={true}
              closeOnHardwareBackPress={false}
              showCancelButton={true}
              showConfirmButton={true}
              cancelText="No, cancel"
              confirmText="Allow Permission"
              confirmButtonColor="#33909F"
              onCancelPressed={() => {
                this.setState({ showAlertService: false })
              }}
              onConfirmPressed={this.grantPermission.bind(this)}
            />
              <AwesomeAlert
              show={this.state.showAlertService2}
              showProgress={false}
              title="Swift Clinic Permission"
              message="Permission already Granted"
              closeOnTouchOutside={true}
              closeOnHardwareBackPress={false}
              showCancelButton={false}
              showConfirmButton={true}
              cancelText="No,cancel"
              confirmText="Ok"
              confirmButtonColor="#33909F"
              onConfirmPressed={() => {
                this.setState({ showAlertService2: false })
              }}
            />
            
            <TouchableOpacity
              onPress={this.logOut.bind(this)}
              style={styles.button2}>
              <Text style={{color: 'black', fontFamily: 'Axiforma-Book'}}>Log Out</Text>
            </TouchableOpacity>

          </View>
          </View>
        </SafeAreaView>
      </KeyboardAwareScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flex: 1
  },
  inputDropDown: {
    marginTop: 10,
    paddingLeft: 10,
    borderRadius: 8,
    height: 49,
    alignSelf: 'stretch',
    borderWidth: 1,
    borderColor: '#cae3ff',
    marginBottom: 20,
  },
  dpFlex: {
    justifyContent: 'space-between',
    marginTop: 20,
    flex: 1,
    flexDirection: 'row',
    height: 90,
  },
  dp: {
    alignSelf: 'center',
    borderRadius: 37,
    width: 72,
    height: 72,
  },

  dpBox: {
    justifyContent: 'center',
    borderWidth: 2,
    borderColor: '#33909F',
    marginLeft: 20,
    borderRadius: 38,
    width: 76,
    height: 76,
  },
  Name: {
    marginLeft: 20,
    alignSelf: "center",
    fontSize: 22,
    maxWidth: 120,
    fontWeight: "500",
    textAlignVertical: 'center',
    fontFamily : 'Axiforma-Book',
  },
  headTitles: {
    marginTop: 10,
    flexDirection: 'row',
    height: 45,
  },
  iconBox: {
    marginTop: 5,
    marginLeft: 20,
    borderRadius: 7,
    borderColor: '#dcecef',
    borderWidth: 1,
    height: 34,
    width: 34,
    justifyContent: 'center',
  },
  editBox: {
    marginRight: 20,
    alignSelf: 'center',
    borderRadius: 7,
    height: 43,
    width: 49,
  },
  headTeext: {
    fontSize: 15,
    marginLeft: 30,
    alignSelf: "center",
    textAlignVertical: 'center',
    fontFamily : 'Axiforma-Book',
  },
  input: {
    marginTop: 5,
    paddingLeft: 10,
    borderRadius: 8,
    height: 50,
    alignSelf: 'stretch',
    marginLeft: 20,
    marginRight: 20,
    borderWidth: 1,
    borderColor: '#dcecef',
    marginBottom: 10,
    fontFamily : 'Axiforma-Thin',
    color: '#646464',
  },
  button: {
    borderRadius: 9,
    marginLeft: 80,
    marginBottom: 20,
    height: 70,
    justifyContent: 'center',
    marginRight: 80,
    marginTop: 15,
    backgroundColor: '#0158ff',
    alignItems: 'center',
    alignSelf: 'stretch',
    shadowColor: "dodgerblue",
    shadowOffset: { width: 0, height: 4 },
    shadowRadius: 6,
    shadowOpacity: 0.7,
    elevation: 7, 
    padding: 15

  },
  button2: {
    borderRadius: 9,
    borderWidth: 2,
    marginLeft: 80,
    marginBottom: 40,
    borderColor: '#33909F',
    height: 70,
    justifyContent: 'center',
    marginRight: 80,
    marginTop: 15,
    backgroundColor: '#ffffff',
    alignItems: 'center',
    alignSelf: 'stretch',
  },
  items: {
    marginTop: 28,
    textAlign: 'left',
  },
  label: {
    marginTop: 20,
    marginLeft: 20,
    fontSize: 16,
    fontWeight: '500',
  },
  spinnerTextStyle: {
    color: '#FFF',
  },
  switchPush:{
    marginRight: 20,
    transform: [{ scaleX: .8 }, { scaleY: .75 }],
  },
});
