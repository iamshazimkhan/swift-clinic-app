import AsyncStorage from '@react-native-community/async-storage';
import React, {Component} from 'react';
import {Image, StyleSheet, Text, View,Alert, ImageBackground} from 'react-native';
import SvgLogo from '../../components/Headers/ImageWithTitle';
import AlertCallRequest from '../.././components/Alerts/CallRequestAlert';
import NavigationService from '../../navigations/NavigationService';
import messaging from '@react-native-firebase/messaging';
import {NavigationActions} from 'react-navigation';
import DeviceInfo from 'react-native-device-info';
import SvgGraph from '../../components/backgroundgraph';

export default class LoginSignup extends Component {
state = {
  AlertShow: false
}


  async componentDidMount() {
    const t = await new Date();
    // await console.log('Date ---->', t.getTimeZone());
    // console.log(DeviceInfo.getTimezone());
    const tkn = await messaging().getToken();
    await AsyncStorage.setItem('fcmToken', tkn);
    await console.log('Token messaging --------->',tkn);
    const d = await AsyncStorage.getItem('isLoggedIn');
    const l = await AsyncStorage.getItem('user_level');
    // console.log('Logged IN ----------------->', d);
    if (d === 'true') {
      if(l === '1'){
      this.props.navigation.navigate('App');
      }else if(l === '2')
      {
        this.props.navigation.navigate('DoctorMode');
        // console.log('Doctor Mode', l);
      }
    }

  }


  async onC()
  {
Alert.alert('Clicked');
  }

  answerPressed = async () => {
    NavigationService.navigate(
      NavigationActions.navigate({
        routeName: 'CallScreenPatient',
      }),
    );
  }

  declinePressed = async () => {
    await this.setState({AlertShow: false})
  }

  render() {
    return (
      <View style={{backgroundColor: '#ffffff', flex: 1}}>
                <SvgGraph style={{right:-38}} />
                <View style={styles.container}>
        {/* <Image style={styles.image} source={require('../../heartfulllogo.png')} /> */}
        <SvgLogo style={{marginTop: 60}}></SvgLogo>

        <AlertCallRequest answerPressed={this.answerPressed} cancelPressed={this.declinePressed} visibleAlert={this.state.AlertShow} title="Calling..." message="Your doctor is calling you."></AlertCallRequest>
        <ImageBackground source={require('../../bkog.png')} imageStyle={{borderTopLeftRadius: 10, borderTopRightRadius: 10}} style={styles.bottom}>
          <Text style={{textAlign: 'center' ,fontFamily: 'Axiforma-Black', marginTop: 30, color: '#fff', fontSize: 60}}>Welcome</Text>
          <Text style={{textAlign: 'center' ,fontFamily: 'Axiforma-Black', marginTop: 10, color: '#fff', fontSize: 28}}>to Swift Clinic!</Text>
          <Text style={styles.tag}>
            Your Online Doctor App
          </Text>
          <View
            onStartShouldSetResponder={() =>
              this.props.navigation.navigate('ChooseLogin')
            }
            style={styles.buttonLogin}>
            <Text style={styles.Text}>Sign In </Text>
          </View>
          <View
            onStartShouldSetResponder={() =>
              this.props.navigation.navigate('ChooseSignup')
            }
            style={styles.button}>
            <Text style={styles.tagThree}>Sign Up </Text>
          </View>
          {/* <Content onClick={()=> this.onC()} />
          <AddConsultation /> */}
        </ImageBackground>
      </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: -240,
    alignItems: 'center',
  },
  button: {
    borderRadius: 9,
    marginLeft: 80,
    justifyContent: 'center',
    marginRight: 80,
    marginTop: 7,
    color: 'white',
    alignItems: 'center',
    alignSelf: 'stretch',
  },
  buttonLogin: {
    borderRadius: 9,
    marginLeft: 85,
    paddingTop: 16,
    paddingBottom: 16,
    height: 70,
    justifyContent: 'center',
    marginRight: 85,
    marginTop: 55,
    backgroundColor: '#ffffff',
    alignItems: 'center',
    alignSelf: 'stretch',
  },
  image: {
    marginTop: 60,
  },
  tag: {
    fontFamily: 'Axiforma-Thin',
    marginTop: 40,
    fontSize: 15,
    marginLeft: 20,
    paddingLeft: 50,
    paddingRight: 50,
    color: '#fefefe',
    fontWeight: '300',
    textAlign: 'center',
  },
  tagThree: {
    fontFamily: 'Axiforma-Book',
    marginTop: 30,
    fontSize: 17,
    paddingLeft: 50,
    paddingRight: 50,
    color: '#fefefe',
    textAlign: 'center',
    textDecorationLine: 'underline',
  },
  tagMain: {
    fontFamily: 'Axiforma-Black',
    marginTop: 30,
    fontSize: 60,
    paddingLeft: 50,
    paddingRight: 50,
    fontWeight: "700",
    color: 'white',
    textAlign: 'center',
  },
  tagSecond: {
    fontFamily: 'Axiforma-Black',
    marginTop: 10,
    fontSize: 28,
    paddingLeft: 50,
    paddingRight: 50,
    fontWeight: 'bold',
    color: 'white',
    textAlign: 'center',
  },
  bottom: {
    height: 700,
    marginTop: 70,
    backgroundColor: '#78c2cb',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    width:'100%',
  },
  Text: {
    fontFamily: 'Axiforma-Book',
    fontSize: 16,
  },
});
