import React, {Component} from 'react';
import {
  TextInput,
  View,
  Text,
  StyleSheet,
  ActivityIndicator,
  ScrollView,
  TouchableOpacity,
  Modal,
  Alert,
  KeyboardAvoidingView,
  Platform
} from 'react-native';
import { SafeAreaView } from 'react-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';
import DropDownPicker from 'react-native-dropdown-picker';
import AsyncStorage from '@react-native-community/async-storage';
import LinearGradient from 'react-native-linear-gradient';
import AwesomeAlert from 'react-native-awesome-alerts';
import Spinner from 'react-native-loading-spinner-overlay';
import IntlPhoneInput from 'react-native-intl-phone-input';
import ImagesHeader from '../../components/header';
import * as constants from '../../constants';
import AwesomeAlertComponent from '../../components/Alerts/AwesomeAlertFile';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import SvgGraph from '../../components/backgroundgraph';

export default class Signup extends Component {
  //   static navigationOptions = {
  //     header: null,
  //   };

  constructor(props) {
    super(props);

    this.state = {
      fcm_token: '',
      first_name: '',
      last_name: '',
      email: '',
      password: '',
      user_level: 1,
      security_question_1: '3',
      security_answer_1: '',
      security_question_2: '4',
      security_answer_2: '',
      phone_number: '12345',
      showAlert: false,
      spinner: false,
      items: [
        {
          label: 'Select security question',
          value: '3',
        },
        {
          label: 'Select security question',
          value: '4',
        },
      ],
      isVisible: false,
      isVerified: false,
      AlertComponentVisible: false,
      AlertComponentMessage: null,
    };
  }

  async componentDidMount() {
    let fcmToken = await AsyncStorage.getItem('fcmToken');
    await this.setState({fcm_token : fcmToken})
    await fetch(constants.URL + 'api/fetch-security-questions', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then((response) => response.json())
      .then((responseJson) => {
        const d = responseJson.data;
        // console.log('------Data ---------->', d);
        const allData = [];
        for (let index = 0; index < d.length; index++) {
          const result = {
            label: d[index].question,
            value: d[index].id,
          };
          // console.log('Item in loop---------->', result);
          allData.push(result);
        }
        // console.log('Complete Data---------->', allData);
        this.setState({items: allData});
        // console.log('Complete Data in State---------->', this.state.items);
      })
      .catch((error) => {
        this.setState({spinner: false});
        console.error(error);
      });
  }

  onVerifyCode() {
    const {digit1, digit2, digit3, digit4} = this.state;
    if (!digit1 || !digit2 || !digit3 || !digit4) {
      Alert.alert('Please Fill OTP');
    } else {
      this.onVerifyHit();
    }
  }

  closeAlertFunction = async () => {
    await this.setState({AlertComponentVisible: false});
  }

  async onVerifyHit() {
    const {digit1, digit2, digit3, digit4} = this.state;
    const otp = digit1 + digit2 + digit3 + digit4;
    console.log(' OTP --------------------->', otp);
    const token = await AsyncStorage.getItem('token');
    this.setState({spinner: true});
    fetch(constants.URL + 'api/phone-number-verification', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token,
      },
      body: JSON.stringify({
        otp: otp,
      }),
    })
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({spinner: false});
        console.log('Response ======-------->', responseJson);
        if (responseJson.error === false) {
          this.setState({spinner: false, isVisible: true});
          this.props.navigation.navigate('App');
        } else {
          this.setState({spinner: false});
          Alert.alert('Incorrect OTP');
        }
      })
      .catch((error) => {
        this.setState({showAlert: true});
        console.error(error);
      });
  }

  onSignup() {
    const {
      email,
      password,
      first_name,
      last_name,
      user_level,
      security_question_1,
      security_answer_1,
      security_question_2,
      security_answer_2,
      phone_number,
      isVerified,
    } = this.state;
    if (first_name == '') {
      Alert.alert('Please fill your name');
    } else if (last_name == '') {
      Alert.alert('Please fill your last name');
    } else if (phone_number == '') {
      Alert.alert('Please fill your number');
    } else if (email == '') {
      Alert.alert('Please fill your email');
    } else if (password == '') {
      Alert.alert('Please fill your password');
    } else if (security_question_1 == '') {
      Alert.alert('Please select question 1 first');
    } else if (security_question_2 == '') {
      Alert.alert('Please select question 2 first');
    } else if (security_answer_1 == '') {
      Alert.alert('Please fill answer 1');
    } else if (security_answer_2 == '') {
      Alert.alert('Please fill answer 2');
    } else if(password.length < 7)
    {
      Alert.alert('Password should be greater than 6 characters');
    } else if(isVerified !== true) {
      Alert.alert('Invalid phone number');
    }
     else {
      this.handlePress();
    }
  }

  hideAlert = () => {
    this.setState({
      showAlert: false,
    });
  };

  onChangeText = async ({
    dialCode,
    unmaskedPhoneNumber,
    phoneNumber,
    isVerified,
  }) => {
    console.log(dialCode, unmaskedPhoneNumber, phoneNumber, isVerified);
    // console.log('total', unmaskedPhoneNumber);
    if (dialCode == '+44' && unmaskedPhoneNumber.length == 10) {
      console.log('we are in custom function ', unmaskedPhoneNumber);
      await this.setState({
        phone_number: dialCode + unmaskedPhoneNumber,
        isVerified: true,
      });
    } else if(dialCode == '+44' && unmaskedPhoneNumber.length !== 10)
    {
      console.log('Make isVerified false -------> ', unmaskedPhoneNumber);
      await this.setState({
        isVerified: false,
      });
    }
     else {
      if (isVerified === true && dialCode !== '+44') {
        await console.log('we are in core function--------------->', unmaskedPhoneNumber);
        await this.setState({
          phone_number: dialCode + unmaskedPhoneNumber,
          isVerified: isVerified,
        });
        console.log('After State ----------------->', this.state.phone_number);
      }
    }
  };

  async handlePress() {
    this.setState({spinner: true});
    fetch(constants.URL + 'api/register', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: this.state.email,
        password: this.state.password,
        first_name: this.state.first_name,
        last_name: this.state.last_name,
        user_level: this.state.user_level,
        security_question_1: this.state.security_question_1,
        security_question_2: this.state.security_question_2,
        phone_number: this.state.phone_number,
        security_answer_1: this.state.security_answer_1,
        security_answer_2: this.state.security_answer_2,
        fcm_token: this.state.fcm_token
      }),
    })
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({spinner: false});
        if (responseJson.error === false) {
          this.setState({spinner: false});
                  console.log('Response ======-------->', responseJson.token);
          AsyncStorage.setItem('token', responseJson.token);
          AsyncStorage.setItem('phone', responseJson.user.phone_number);
          AsyncStorage.setItem('email', responseJson.user.email);
          if (responseJson.user.email_verified_at === null) {
            AsyncStorage.setItem('email_verified_at', 'notVerified');
          } else {
            AsyncStorage.setItem('email_verified_at', 'Verified');
          }
          if (responseJson.user.phone_no_verified_at === null) {
            this.props.navigation.navigate('PhoneVerify');
            AsyncStorage.setItem('isLoggedIn', 'false');
          } else if (responseJson.user.email_verified_at === null) {
            this.props.navigation.navigate('EmailVerify');
            AsyncStorage.setItem('isLoggedIn', 'false');
          } else {
            this.props.navigation.navigate('App');
            AsyncStorage.setItem('isLoggedIn', 'true');
          }
        } else {
          this.setState({AlertComponentVisible: true, AlertComponentMessage: responseJson.message});
        }
      })
      .catch((error) => {
        console.log('Error In Register --------->')
        // this.setState({AlertComponentVisible: true, AlertComponentMessage: error.data.message});
        // console.error(error);
      });
  }

  restrict = (event) => {
    const regex = new RegExp("/^[^!-\\/:-@\\[-`{-~]+$/;");
    const key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
    event.preventDefault();
    return false;
    }
    }

  render() {
    if (this.state.showIndicator) {
      return (
        <View style={styles.container}>
          {/*Code to show Activity Indicator*/}
          <ActivityIndicator size="large" color="#0000ff" />
          {/*Size can be large/ small*/}
        </View>
      );
    } else {
      return (
        <SafeAreaView style={styles.container}>
          <KeyboardAwareScrollView>
          <SvgGraph style={{right:-38}} />
      <View style={{marginTop: -255}}>
            <Spinner
              visible={this.state.spinner}
              textContent={''}
              textStyle={styles.spinnerTextStyle}
            />
            <View
              style={{
                marginTop: 0,
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}>
              <View
                onStartShouldSetResponder={() =>
                  this.props.navigation.goBack(null)
                }
                style={{alignSelf: 'center', flex: 1, flexDirection: 'row'}}>
                <Icon
                  style={{textAlignVertical: 'center', marginLeft: 20}}
                  color="#E73988"
                  size={18}
                  name="arrow-left"
                  // name="long-arrow-left"
                />
                <Text
                  style={{
                    textAlignVertical: 'center',
                    marginLeft: 10,
                    color: 'grey',
                  }}>
                  Back
                </Text>
              </View>
              <ImagesHeader />
              {/* <Image source={require('../assets/images/clinic_logo.png')} /> */}
            </View>
            <Text style={{fontFamily: 'Axiforma-Black', textAlign: 'center', fontSize: 32, marginTop: 50}}>Create your account</Text>
            <Text style={{textAlign: 'center', fontSize : 16, fontFamily : 'Axiforma-Thin', paddingLeft: 30, paddingRight: 30, marginTop: 10, lineHeight: 25.2}}>
              Please enter the information below to create your free account.
            </Text>
            <View style={styles.items}>
              <Text style={styles.label}> First name: </Text>
              <TextInput
                value={this.state.first_name}
                placeholderTextColor="grey"
                // onChangeText={(first_name) => this.setState({first_name})}
                onChangeText={async (e) => { let first_name = await e; first_name= await first_name.replace(/[^A-Za-z]/ig, ''); this.setState({first_name}) }}
                placeholder={'Enter your first name'}
                style={styles.input}
              />
              <Text style={styles.label}> Last name: </Text>
              <TextInput
                placeholderTextColor="grey"
                value={this.state.last_name}
                onChangeText={async (e) => { let last_name = await e; last_name= await last_name.replace(/[^A-Za-z]/ig, ''); this.setState({last_name}) }}
                placeholder={'Enter your last name'}
                style={styles.input}
              />
              <Text style={styles.label}> Phone no: </Text>
              <View style={styles.input}>
                <IntlPhoneInput
                  onChangeText={this.onChangeText}
                  placeholder="Enter your phone number"
                  defaultCountry="GB"
                />
              </View>
              <Text style={styles.label}> Email address: </Text>
              <TextInput
                placeholderTextColor="grey"
                value={this.state.email}
                onChangeText={(email) => this.setState({email})}
                placeholder={'Enter your e-mail '}
                style={styles.input}
              />
              <Text style={styles.label}> Password: </Text>
              <TextInput
                placeholderTextColor="grey"
                value={this.state.password}
                onChangeText={(password) => this.setState({password})}
                placeholder={'Enter your password'}
                secureTextEntry={Platform.OS === 'android' ? ( this.state.password === '' ? false : true) : true}
                style={styles.input}
              />

              <Text style={styles.label}> Select your primary security question: </Text>
              <DropDownPicker
                containerStyle={{marginLeft: 40, marginRight: 40}}
                items={this.state.items}
                defaultValue={this.state.security_question_1}
                style={styles.inputDropDown}
                dropDownStyle={{backgroundColor: '#fafafa'}}
                onChangeItem={(item) =>
                  this.setState({
                    security_question_1: item.value,
                  })
                }
              />

              <Text style={styles.label}> Your answer: </Text>
              <TextInput
                placeholderTextColor="grey"
                value={this.state.security_answer_1}
                onChangeText={(security_answer_1) =>
                  this.setState({security_answer_1})
                }
                placeholder={'Enter your answer'}
                style={styles.input}
              />

              <Text style={styles.label}> Select your secondary security question: </Text>
              <DropDownPicker
                containerStyle={{marginLeft: 40, marginRight: 40}}
                items={this.state.items}
                defaultValue={this.state.security_question_2}
                style={styles.inputDropDown}
                dropDownStyle={{backgroundColor: '#fafafa'}}
                onChangeItem={(item) =>
                  this.setState({
                    security_question_2: item.value,
                  })
                }
              />

              <Text style={styles.label}> Your answer: </Text>
              <TextInput
                placeholderTextColor="grey"
                value={this.state.security_answer_2}
                onChangeText={(security_answer_2) =>
                  this.setState({security_answer_2})
                }
                placeholder={'Enter your answer'}
                style={styles.input}
              />

<LinearGradient colors={['#78C2CB', '#33909F']} style={styles.button}>
              <TouchableOpacity
                onPress={this.onSignup.bind(this)}>
                <Text style={styles.submitText}>Create Account </Text>
              </TouchableOpacity>
</LinearGradient>              

              <AwesomeAlertComponent title="Try Again" visibleAlert={this.state.AlertComponentVisible} closeAlert={this.closeAlertFunction} message={this.state.AlertComponentMessage}></AwesomeAlertComponent>

              <AwesomeAlert
                show={this.state.showAlert}
                showProgress={false}
                title="Try again"
                message="Email or phone already exists!"
                closeOnTouchOutside={true}
                closeOnHardwareBackPress={false}
                // showCancelButton={true}
                showConfirmButton={true}
                // cancelText="No, cancel"
                confirmText="OK"
                confirmButtonColor="#0158ff"
                onCancelPressed={() => {
                  this.hideAlert();
                }}
                onConfirmPressed={() => {
                  this.hideAlert();
                }}
              />
            </View>
            </View>
          </KeyboardAwareScrollView>
        </SafeAreaView>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  input: {
    fontFamily: 'Axiforma-Thin',
    marginTop: 10,
    paddingLeft: 10,
    borderRadius: 8,
    height: 49,
    alignSelf: 'stretch',
    marginLeft: 40,
    marginRight: 40,
    borderWidth: 1,
    borderColor: '#dcecef',
    marginBottom: 20,
  },
  inputDropDown: {
    fontFamily: 'xiforma-Thin',
    marginTop: 10,
    paddingLeft: 10,
    borderRadius: 8,
    height: 49,
    alignSelf: 'stretch',
    borderWidth: 1,
    borderColor: '#cae3ff',
    marginBottom: 20,
  },
  inputCode: {
    marginTop: 10,
    borderRadius: 8,
    textAlign: 'center',
    height: 50,
    marginLeft: 3,
    marginRight: 7,
    borderWidth: 1,
    width: 49,
    borderColor: '#cae3ff',
    marginBottom: 20,
  },
  editBox: {
    marginRight: 20,
    alignSelf: 'center',
    borderRadius: 7,
    height: 43,
    width: 49,
  },
  screen: {
    marginTop: 40,
    fontSize: 30,
    fontWeight: '700',
    textAlign: 'center',
  },
  button: {
    marginBottom: 80,
    borderRadius: 9,
    marginLeft: 80,
    height: 70,
    justifyContent: 'center',
    marginRight: 80,
    marginTop: 15,
    backgroundColor: '#0158ff',
    alignItems: 'center',
    alignSelf: 'stretch',
    shadowColor: "dodgerblue",
    shadowOffset: { width: 0, height: 4 },
    shadowRadius: 6,
    shadowOpacity: 0.7,
    elevation: 7, 
    padding: 15
  },
  tag: {
    marginTop: 10,
    fontSize: 14,
    paddingLeft: 50,
    paddingRight: 50,
    color: 'grey',
    textAlign: 'center',
  },
  items: {
    marginTop: 50,
    textAlign: 'left',
  },
  label: {
    marginLeft: 40,
    fontSize: 14,
    fontWeight: '500',
  },
  submitText: {
    fontSize: 16,
    color: 'white',
    fontWeight: '500'
  },
  logoView: {
    // alignItems: 'flex-end',
  },
  modal: {
    paddingTop: 10,
    alignSelf: 'stretch',
    backgroundColor: '#ECF5FF',
    height: 500,
    borderRadius: 10,
    borderWidth: 1,
    width: '100%',
    flex: 1,
    flexDirection: 'column',
    borderColor: '#fff',
    marginTop: 80,
    position: 'absolute',
    bottom: 0,
  },
  text: {
    fontSize: 15,
    marginLeft: 10,
  },
  tagThree: {
    marginTop: -40,
    fontSize: 17,
    paddingLeft: 50,
    paddingRight: 50,
    color: '#0158ff',
    fontWeight: '300',
    textAlign: 'center',
    textDecorationLine: 'underline',
  },
});
