import React, {Component} from 'react';
import {
  TextInput,
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Alert,
  Platform,
} from 'react-native';
import {SafeAreaView} from 'react-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';
import AsyncStorage from '@react-native-community/async-storage';
import AwesomeAlert from 'react-native-awesome-alerts';
import Spinner from 'react-native-loading-spinner-overlay';
import LinearGradient from 'react-native-linear-gradient';

import ImagesHeader from '../../components/header';
import * as constants from '../../constants';
import {ScrollView} from 'react-native-gesture-handler';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import SvgGraph from '../../components/backgroundgraph';

export default class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: '',
      password: '',
      message: '',
      showAlert: false,
      spinner: false,
      fcm_token: '',
    };
  }

  componentDidMount = async () => {
    let fcmToken = await AsyncStorage.getItem('fcmToken');
    console.log('FCM Token -------->', fcmToken);
    await this.setState({fcm_token: fcmToken});
  };

  onLogin() {
    const {username, password} = this.state;
    if (!username || !password) {
      Alert.alert('Fill all Fields!');
    } else {
      this.handlePress(username, password);
    }
  }

  hideAlert = () => {
    this.setState({
      showAlert: false,
    });
  };

  async handlePress() {
    this.setState({spinner: true});
    fetch(constants.URL + 'api/login', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: this.state.username,
        password: this.state.password,
        fcm_token: this.state.fcm_token,
      }),
    })
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({spinner: false});
        console.log('Response Token ======-------->', responseJson.user);
        AsyncStorage.setItem('id', JSON.stringify(responseJson.user.id));

        if (responseJson.error) {
          this.setState({showAlert: true});
        } else if (responseJson.user.user_level === 2) {
          AsyncStorage.setItem('token', responseJson.token);
          // AsyncStorage.setItem('id', responseJson.user.id);
          AsyncStorage.setItem('isLoggedIn', 'true');
          AsyncStorage.setItem('user_level', '2');
          this.props.navigation.navigate('DoctorMode');
        } else {
          console.log('Token ----------------=====>', responseJson.token);
          AsyncStorage.setItem('token', responseJson.token);
          AsyncStorage.setItem('phone', responseJson.user.phone_number);
          AsyncStorage.setItem('email', responseJson.user.email);
          // AsyncStorage.setItem('id', responseJson.user.id);
          if (responseJson.user.email_verified_at === null) {
            AsyncStorage.setItem('email_verified_at', 'notVerified');
          } else {
            AsyncStorage.setItem('email_verified_at', 'Verified');
          }
          if (responseJson.user.phone_no_verified_at === null) {
            this.props.navigation.navigate('PhoneVerify');
            AsyncStorage.setItem('isLoggedIn', 'false');
          } else if (responseJson.user.email_verified_at === null) {
            this.props.navigation.navigate('EmailVerify');
            AsyncStorage.setItem('isLoggedIn', 'false');
          } else {
            AsyncStorage.setItem('isLoggedIn', 'true');
            AsyncStorage.setItem('user_level', '1');
            this.props.navigation.navigate('App');
          }
        }
      })
      .catch((error) => {
        this.setState({showAlert: true});
        console.error(error);
      });
  }

  render() {
    return (
      <KeyboardAwareScrollView style={styles.container}>
        <SafeAreaView>
          <ScrollView>
            <SvgGraph style={{right: -38}} />
            <View style={{marginTop: -255}}>
              <Spinner
                visible={this.state.spinner}
                textContent={''}
                textStyle={styles.spinnerTextStyle}
              />
              <View
                style={{
                  marginTop: 0,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <View
                  onStartShouldSetResponder={() =>
                    this.props.navigation.goBack(null)
                  }
                  style={{flex: 1, flexDirection: 'row', alignSelf: 'center'}}>
                  <Icon
                    style={{textAlignVertical: 'center', marginLeft: 20}}
                    color="#E73988"
                    size={18}
                    name="arrow-left"
                  />
                  <Text
                    style={{
                      textAlignVertical: 'center',
                      marginLeft: 10,
                      color: 'grey',
                    }}>
                    Back
                  </Text>
                </View>
                <ImagesHeader />
                {/* <Image source={require('../assets/images/clinic_logo.png')} /> */}
              </View>
              <Text
                style={{
                  fontFamily: 'Axiforma-Black',
                  textAlign: 'center',
                  fontSize: 32,
                  marginTop: 50,
                }}>
                Sign in
              </Text>
              <Text
                style={{
                  fontSize: 16,
                  fontFamily: 'Axiforma-Thin',
                  paddingLeft: 30,
                  paddingRight: 30,
                  marginTop: 10,
                  lineHeight: 25.2,
                }}>
                Please enter the information below to sign into your account. If
                you have forgotten your details, please select reset password.
              </Text>
              <View style={styles.items}>
                <Text style={styles.label}> Email: </Text>
                <TextInput
                  placeholderTextColor="grey"
                  value={this.state.username}
                  onChangeText={(username) => this.setState({username})}
                  placeholder={'Enter your e-mail'}
                  style={styles.input}
                />
                <Text style={styles.label}> Password: </Text>
                <TextInput
                  secureTextEntry={
                    Platform.OS === 'android'
                      ? this.state.password === ''
                        ? false
                        : true
                      : true
                  }
                  placeholderTextColor="grey"
                  value={this.state.password}
                  onChangeText={(password) => this.setState({password})}
                  placeholder={'Enter your Password'}
                  style={styles.input}
                />

                <LinearGradient
                  colors={['#78C2CB', '#33909F']}
                  style={styles.button}>
                  <TouchableOpacity
                    onPress={this.onLogin.bind(this)}
                    style={{
                      justifyContent: 'center',
                      alignContent: 'center',
                      width: '100%',
                      height: '100%',
                    }}>
                    <Text style={styles.submitText}>Confirm </Text>
                  </TouchableOpacity>
                </LinearGradient>

                <View
                  onStartShouldSetResponder={() =>
                    this.props.navigation.navigate('ResetPassword')
                  }>
                  <Text style={styles.tagThree}>Reset Password</Text>
                </View>

                <AwesomeAlert
                  show={this.state.showAlert}
                  showProgress={false}
                  title="Try again"
                  message="Password and email did not match!"
                  closeOnTouchOutside={true}
                  closeOnHardwareBackPress={false}
                  // showCancelButton={true}
                  showConfirmButton={true}
                  // cancelText="No, cancel"
                  confirmText="OK"
                  confirmButtonColor="#33909F"
                  onCancelPressed={() => {
                    this.hideAlert();
                  }}
                  onConfirmPressed={() => {
                    this.hideAlert();
                  }}
                />
              </View>
            </View>
          </ScrollView>
        </SafeAreaView>
      </KeyboardAwareScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  input: {
    fontFamily: 'Axiforma-Thin',
    marginTop: 10,
    paddingLeft: 10,
    borderRadius: 8,
    height: 50,
    alignSelf: 'stretch',
    marginLeft: 30,
    marginRight: 30,
    borderWidth: 1,
    borderColor: '#dcecef',
    marginBottom: 20,
  },
  button: {
    borderRadius: 9,
    marginLeft: 80,
    height: 70,
    justifyContent: 'center',
    marginRight: 80,
    marginTop: 15,
    backgroundColor: '#0158ff',
    alignItems: 'center',
    alignSelf: 'stretch',
    shadowColor: 'dodgerblue',
    shadowOffset: {width: 0, height: 4},
    shadowRadius: 6,
    shadowOpacity: 0.7,
    elevation: 7,
    padding: 15,
  },
  items: {
    marginTop: 40,
    textAlign: 'left',
  },
  label: {
    marginLeft: 30,
    fontSize: 16,
    fontWeight: '500',
  },
  screen: {
    marginTop: 40,
    fontSize: 32,
    fontWeight: '700',
    textAlign: 'center',
    fontFamily: 'Axiforma-Book',
  },
  tag: {
    fontFamily: 'Cochin',
    marginTop: 10,
    fontSize: 20,
    paddingLeft: 60,
    paddingRight: 60,
    color: 'grey',
    textAlign: 'center',
    lineHeight: 25.2,
    fontWeight: '900',
    fontStyle: 'normal',
  },
  submitText: {
    textAlign: 'center',
    fontSize: 16,
    color: 'white',
    fontWeight: '500',
  },
  logoView: {
    // alignItems: 'flex-end',
  },
  spinnerTextStyle: {
    color: '#FFF',
  },
  tagThree: {
    marginTop: 25,
    fontSize: 14,
    paddingLeft: 50,
    paddingRight: 50,
    color: '#33909F',
    fontWeight: '500',
    textAlign: 'center',
    textDecorationLine: 'underline',
  },
});
