import React, {Component} from 'react';
import {
  TextInput,
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Alert,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { SafeAreaView } from 'react-navigation';
import DropDownPicker from 'react-native-dropdown-picker';
import AwesomeAlert from 'react-native-awesome-alerts';
import Spinner from 'react-native-loading-spinner-overlay';
import Icon from 'react-native-vector-icons/FontAwesome';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import ImagesHeader from '../../components/header';
import * as constants from '../../constants';

export default class ResetPassword extends Component {
  //   static navigationOptions = {
  //     header: null,
  //   };

  constructor(props) {
    super(props);

    this.state = {
      email: '',
      message: '',
      security_question_1: '3',
      security_answer_1: '',
      showAlert: false,
      spinner: false,
      messageTitle1: 'Successfull',
      messageBody1: 'A new password is sent on your email.',
      items: [
        {
          label: 'Select Question',
          value: '3',
        },
        {
          label: 'Select Question',
          value: '4',
        },
      ],
    };
  }

  onLogin() {
    const {email, security_question_1, security_answer_1} = this.state;
    if (!email || !security_question_1 || !security_answer_1) {
      Alert.alert('Fill all Fields');
    } else {
      this.handlePress();
    }
  }

  hideAlert = async () => {
   await this.setState({ showAlert: false });
   const d = await this.state.messageTitle1;
   await console.log('Message ------>',d)
   if(d === "Successfull")
   {
   this.props.navigation.navigate('Login');
   }
  };

  async handlePress() {
    this.setState({spinner: true});
    fetch(constants.URL + 'api/reset-password', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: this.state.email,
        security_question: this.state.security_question_1,
        answer: this.state.security_answer_1,
      }),
    })
      .then((response) => response.json())
      .then(async (responseJson) => {
        this.setState({spinner: false});
        console.log('Response ======-------->', responseJson);
        if (responseJson.error === false) {
          await console.log('Error If------------->',responseJson.error)
          await this.setState({showAlert: true, messageTitle1: 'Successfull',
          messageBody1: 'A mail is sent to your email with password.'});
        } else {
          console.log('Error Else ------------->',responseJson.error)
          await this.setState({
            showAlert: true,
            messageTitle1: 'Try Again',
            messageBody1: 'Please check your Fields again',
          });
        }
      })
      .catch((error) => {
        this.setState({showAlert: true});
        console.error(error);
      });
  }

  async componentDidMount() {
    await fetch(constants.URL + 'api/fetch-security-questions', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then((response) => response.json())
      .then((responseJson) => {
        const d = responseJson.data;
        console.log('------Data ---------->', d);
        const allData = [];
        for (let index = 0; index < d.length; index++) {
          const result = {
            label: d[index].question,
            value: d[index].id,
          };
          // console.log('Item in loop---------->', result);
          allData.push(result);
        }
        // console.log('Complete Data---------->', allData);
        this.setState({items: allData});
        console.log('Complete Data in State---------->', this.state.items);
      })
      .catch((error) => {
        this.setState({spinner: false});
        console.error(error);
      });
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <KeyboardAwareScrollView>
          <Spinner
            visible={this.state.spinner}
            textContent={''}
            textStyle={styles.spinnerTextStyle}
          />
          <View
            style={{
              marginTop: 0,
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <View
              onStartShouldSetResponder={() =>
                this.props.navigation.goBack(null)
              }
              style={{marginTop: 20, flex: 1, flexDirection: 'row'}}>
              <Icon
                style={{textAlignVertical: 'center', marginLeft: 20}}
                color="#0375FF"
                size={18}
                name="arrow-left"
                // name="long-arrow-left"
              />
              <Text
                style={{
                  textAlignVertical: 'center',
                  marginLeft: 10,
                  color: 'grey',
                }}>
                Back
              </Text>
            </View>
            <ImagesHeader />
            {/* <Image source={require('../assets/images/clinic_logo.png')} /> */}
          </View>
          <Text style={styles.screen}>Reset Password</Text>
          <Text style={styles.tag}>
            Please enter the information below to reset your password
          </Text>
          <View style={styles.items}>
            <Text style={styles.label}> Email: </Text>
            <TextInput
              value={this.state.email}
              onChangeText={(email) => this.setState({email})}
              placeholder={'Enter your e-mail '}
              style={styles.input}
            />

            <Text style={styles.label}> Select a security question: </Text>
            <DropDownPicker
              containerStyle={{marginLeft: 40, marginRight: 40}}
              items={this.state.items}
              defaultValue={this.state.security_question_1}
              style={styles.inputDropDown}
              dropDownStyle={{backgroundColor: '#fafafa'}}
              onChangeItem={(item) =>
                this.setState({
                  security_question_1: item.value,
                })
              }
            />

            <Text style={styles.label}> Your answer: </Text>
            <TextInput
              value={this.state.security_answer_1}
              onChangeText={(security_answer_1) =>
                this.setState({security_answer_1})
              }
              placeholder={'Enter your Answer'}
              style={styles.input}
            />

<LinearGradient colors={['#78C2CB', '#33909F']} style={styles.button}>
            <TouchableOpacity
              onPress={this.onLogin.bind(this)}
              style={{justifyContent : 'center', alignContent : 'center' , width: '100%', height: '100%'}}>
              <Text style={styles.submitText}>Send New Password </Text>
            </TouchableOpacity>
            </LinearGradient>

            <AwesomeAlert
              show={this.state.showAlert}
              showProgress={false}
              title={this.state.messageTitle1}
              message={this.state.messageBody1}
              closeOnTouchOutside={true}
              closeOnHardwareBackPress={false}
              // showCancelButton={true}
              showConfirmButton={true}
              // cancelText="No, cancel"
              confirmText="OK"
              confirmButtonColor="#0158ff"
              onCancelPressed={() => {
                this.hideAlert();
              }}
              onConfirmPressed={() => {
                this.hideAlert();
              }}
            />
          </View>
        </KeyboardAwareScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  input: {
    fontFamily: 'Axiforma-Thin',
    marginTop: 10,
    paddingLeft: 10,
    borderRadius: 8,
    height: 50,
    alignSelf: 'stretch',
    marginLeft: 40,
    marginRight: 40,
    borderWidth: 1,
    borderColor: '#dcecef',
    marginBottom: 20,
  },
  inputDropDown: {
    fontFamily: 'Axiforma-Thin',
    marginTop: 10,
    paddingLeft: 10,
    borderRadius: 8,
    height: 49,
    alignSelf: 'stretch',
    borderWidth: 1,
    borderColor: '#dcecef',
    marginBottom: 20,
  },
  inputCode: {
    marginTop: 10,
    borderRadius: 8,
    textAlign: 'center',
    height: 50,
    marginLeft: 3,
    marginRight: 7,
    borderWidth: 1,
    width: 49,
    borderColor: '#cae3ff',
    marginBottom: 20,
  },
  editBox: {
    marginRight: 20,
    alignSelf: 'center',
    borderRadius: 7,
    height: 43,
    width: 49,
  },
  screen: {
    marginTop: 40,
    fontSize: 32,
    fontWeight: '700',
    textAlign: 'center',
  },
  button: {
    marginBottom: 80,
    borderRadius: 9,
    marginLeft: 80,
    height: 70,
    justifyContent: 'center',
    marginRight: 80,
    marginTop: 15,
    backgroundColor: '#0158ff',
    alignItems: 'center',
    alignSelf: 'stretch',
    shadowColor: "dodgerblue",
    shadowOffset: { width: 0, height: 4 },
    shadowRadius: 6,
    shadowOpacity: 0.7,
    elevation: 7, 
    padding: 15
  },
  tag: {
    fontFamily: 'Axiforma-Thin',
    lineHeight: 25.2,
    marginTop: 10,
    fontSize: 14,
    paddingLeft: 60,
    paddingRight: 60,
    color: 'grey',
    textAlign: 'center',
  },
  items: {
    marginTop: 60,
    textAlign: 'left',
  },
  label: {
    marginLeft: 40,
    fontSize: 16,
    fontWeight: '500',
  },
  submitText: {
    textAlign: 'center',
    fontSize: 16,
    color: 'white',
    fontWeight: '500',
  },
  logoView: {
    // alignItems: 'flex-end',
  },
  modal: {
    paddingTop: 10,
    alignSelf: 'stretch',
    backgroundColor: '#ECF5FF',
    height: 500,
    borderRadius: 10,
    borderWidth: 1,
    width: '100%',
    flex: 1,
    flexDirection: 'column',
    borderColor: '#fff',
    marginTop: 80,
    position: 'absolute',
    bottom: 0,
  },
  text: {
    fontSize: 15,
    marginLeft: 10,
  },
  tagThree: {
    marginTop: -40,
    fontSize: 17,
    paddingLeft: 50,
    paddingRight: 50,
    color: '#0158ff',
    fontWeight: '300',
    textAlign: 'center',
    textDecorationLine: 'underline',
  },
});
