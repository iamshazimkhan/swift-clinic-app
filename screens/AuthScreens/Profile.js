import React, {Component} from 'react';
import {
  TextInput,
  View,
  Text,
  StyleSheet,
  ActivityIndicator,
} from 'react-native';

import ImagesHeader from '../../components/header';

export default class Login extends Component {
  //   static navigationOptions = {
  //     header: null,
  //   };

  constructor(props) {
    super(props);

    this.state = {
      username: '',
      password: '',
      message: '',
      showIndicator: false,
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.logoView}>
          <ImagesHeader />
          {/* <Image source={require('../assets/images/clinic_logo.png')} /> */}
        </View>
        <Text style={styles.screen}>Sign in</Text>
        <Text style={styles.tag}>
          Lorem Ipsum is simply dummy text of the printing and typesettings.
        </Text>
        <View style={styles.items}>
          <Text style={styles.label}> Public Information: </Text>
          <TextInput
            value={this.state.username}
            onChangeText={(username) => this.setState({username})}
            placeholder={'First Name:'}
            style={styles.input}
          />
          <TextInput
            value={this.state.username}
            onChangeText={(username) => this.setState({username})}
            placeholder={'Last Name'}
            style={styles.input}
          />
          <Text style={styles.label}> Information is not Displayed: </Text>
          <TextInput
            value={this.state.password}
            onChangeText={(password) => this.setState({password})}
            placeholder={'Email'}
            secureTextEntry={true}
            style={styles.input}
          />
          <TextInput
            value={this.state.password}
            onChangeText={(password) => this.setState({password})}
            placeholder={'Phone Number'}
            secureTextEntry={true}
            style={styles.input}
          />
          <TextInput
            value={this.state.password}
            onChangeText={(password) => this.setState({password})}
            placeholder={'Password'}
            secureTextEntry={true}
            style={styles.input}
          />

          <View
            onStartShouldSetResponder={() =>
              this.props.navigation.navigate('Signup')
            }
            style={styles.button}>
            {/* <Button
                color="seagreen"
                width="300"
                title={'Login'}
                onPress={() => this.props.navigation.navigate('Signup')}
                  onPress={this.onLogin.bind(this)}
              /> */}
            <Text style={styles.submitText}>Login </Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  input: {
    marginTop: 10,
    paddingLeft: 10,
    borderRadius: 8,
    height: 50,
    alignSelf: 'stretch',
    marginLeft: 40,
    marginRight: 40,
    borderWidth: 1,
    borderColor: '#cae3ff',
    marginBottom: 20,
  },
  button: {
    borderRadius: 9,
    marginLeft: 80,
    height: 50,
    justifyContent: 'center',
    marginRight: 80,
    marginTop: 15,
    backgroundColor: '#0158ff',
    alignItems: 'center',
    alignSelf: 'stretch',
  },
  items: {
    marginTop: 60,
    textAlign: 'left',
  },
  label: {
    marginLeft: 40,
    fontSize: 20,
    fontWeight: 'bold',
  },
});
