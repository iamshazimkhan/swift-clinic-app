import React, {Component} from 'react';
import {
  View,
  Image,
  Text,
  StyleSheet,
  TouchableOpacity,
  PermissionsAndroid
} from 'react-native';
import {
  LoginManager,
  LoginButton,
  AccessToken,
  GraphRequest,
  GraphRequestManager
} from 'react-native-fbsdk';
import * as constants from '../../constants';
import AwesomeAlert from 'react-native-awesome-alerts';
import AsyncStorage from '@react-native-community/async-storage';
import LinearGradient from 'react-native-linear-gradient';
import SvgGraph from '../../components/backgroundgraph';
import Icon from 'react-native-vector-icons/FontAwesome';
import SvgLogo from '../../components/Headers/ImageWithTitle';

export default class ChooseSignup extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: '',
      password: '',
      message: '',
      showIndicator: false,
      userInfo: {},
      spinner: false,
      showAlert: false,
    };
  }


  async componentDidMount () {
    await this.onPress();
  }

  onPress = async () => {
    // We need to ask permission for Android only
    if (Platform.OS === 'android') {
      // Calling the permission function
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          title: 'Example App Camera Permission',
          message: 'Example App needs access to your camera',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        // Permission Granted
        console.log('Granted by user')
      } else {
        // Permission Denied
        console.log('Denied by user')
      }
    } else {
      console.log('Granted by user Already')
    }
  };

  hideAlert = () => {
    this.setState({
      showAlert: false,
    });
  };

    async facebookLogin() {
    // native_only config will fail in the case that the user has
    // not installed in his device the Facebook app. In this case we
    // need to go for webview.
    console.log('Entered in Facebook login');
    let result;
    try {
      LoginManager.setLoginBehavior('NATIVE_ONLY');
      result = await LoginManager.logInWithPermissions(['public_profile', 'email']);
    } catch (nativeError) {
      try {
        LoginManager.setLoginBehavior('WEB_ONLY');
        result = await LoginManager.logInWithPermissions(['public_profile', 'email']);
      } catch (webError) {
        // show error message to the user if none of the FB screens
        // did not open
      }
    }  // handle the case that users clicks cancel button in Login view
    if (result.isCancelled) {
    } else {
      // Create a graph request asking for user information
      this.FBGraphRequest('name,  first_name, last_name, email', this.FBLoginCallback);
    }
  }

  async FBGraphRequest(fields, callback) {
    console.log('Entered in FBGraphRequest');
    const accessData = await AccessToken.getCurrentAccessToken();  // Create a graph request asking for user information
    console.log('Access Token --------------->',accessData.accessToken);
    AsyncStorage.setItem('fb_token',accessData.accessToken);
    const infoRequest = new GraphRequest('/me', {
      accessToken: accessData.accessToken,
      parameters: {
        fields: {
          string: fields
        }
      }
    }, callback.bind(this));  // Execute the graph request created above
    new GraphRequestManager().addRequest(infoRequest).start();
  }

  async FBLoginCallback(error, result) {
    console.log('Entered in FBLoginCallback');
    if (error) {
    } else {
      // Retrieve and save user details in state. In our case with 
      // Redux and custom action saveUser
      console.log('Result of FbCallBack-------------->', result);
      this.fbVaildate(result);
    }
  }

  getInfoFromToken = (token) => {
    const PROFILE_REQUEST_PARAMS = {
      fields: {
        string: 'id, name,  first_name, last_name, email',
      },
    };
    const profileRequest = new GraphRequest(
      '/me',
      {token, parameters: PROFILE_REQUEST_PARAMS},
      (error, result) => {
        if (error) {
          console.log('login info has error: ' + error);
        } else {
          this.setState({userInfo: result});
          this.fbVaildate(result);
        }
      },
      {
        scope: 'email',
        return_scopes: true,
      },
    );
    new GraphRequestManager().addRequest(profileRequest).start();
  };

  async fbVaildate(result) {
    fetch(constants.URL + 'api/fb-validate', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        fb_id: result.id,
        first_name: result.first_name,
        last_name: result.last_name,
        email: result.email,
      }),
    })
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({spinner: false});
        console.log('Response of Validation API ---------->', responseJson);
        if (responseJson.error === false) {
          AsyncStorage.setItem('token', responseJson.token);
          AsyncStorage.setItem('isLoggedIn', 'true');
          this.props.navigation.navigate('App');
        }
      })
      .catch((error) => {
        this.setState({showAlert: true});
        console.error(error);
      });
  }

  render() {
    return (
      <View style={{backgroundColor: '#ffffff', flex: 1}}>
                <SvgGraph style={{right:-38}} />
                <View style={styles.container}>
                          {/* <Image style={styles.image} source={require('../../heartfulllogo.png')} /> */}
                          <SvgLogo style={{marginTop: 60}}></SvgLogo>

        <LinearGradient colors={['#78C2CB', '#33909F']} style={styles.buttonLogin}>                  
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate('Signup')}>
          <Text style={styles.Text1}>Sign Up with Email </Text>
        </TouchableOpacity>
        </LinearGradient>

          {/* <View onStartShouldSetResponder={this.facebookLogin.bind(this)} style={styles.tagLogin}>
          <Image style={{height:40, width: 40, marginLeft:-30}} source={require('../../facebook.png')}></Image>
          <Text style={styles.Text1}>Login with Facebook</Text>
        </View> */}
        
        <Text style={styles.tag}>If you already have an account? </Text>
        <View style={{flexDirection: 'row'}}>
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate('Login')}>
          <Text style={styles.tagSignup}>Simply Log In</Text>
        </TouchableOpacity>
        <Text style={styles.tag}>to the application </Text>
        </View>
        <AwesomeAlert
          show={this.state.showAlert}
          showProgress={false}
          title="Error"
          message="User already exists!"
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={false}
          // showCancelButton={true}
          showConfirmButton={true}
          // cancelText="No, cancel"
          confirmText="OK"
          confirmButtonColor="#0158ff"
          onCancelPressed={() => {
            this.hideAlert();
          }}
          onConfirmPressed={() => {
            this.hideAlert();
          }}
        />
      </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: -240,
    alignItems: 'center',
  },
  button: {
    borderRadius: 9,
    marginLeft: 80,
    height: 50,
    justifyContent: 'center',
    marginRight: 80,
    marginTop: 27,
    color: 'white',
    backgroundColor: '#3B5998',
    alignItems: 'center',
    alignSelf: 'stretch',
  },
  buttonLogin: {
    borderRadius: 9,
    marginLeft: 80,
    marginBottom:20,
    height: 70,
    justifyContent: 'center',
    marginRight: 80,
    marginTop: 95,
    backgroundColor: '#0044FF',
    alignItems: 'center',
    alignSelf: 'stretch',
  },
  image: {
    marginTop: 130,
  },
  tag: {
    fontFamily: 'Axiforma-Thin',
    fontWeight: '300',
    marginLeft: 7,
    marginTop: 5,
    fontSize: 15,
    color: 'black',
    textAlign: 'center',
  },
  tagSignup: {
    fontWeight: '300',
    width: '100%',
    textAlign: 'center',
    marginTop: 5,
    fontSize: 15,
    marginLeft: 4,
    color: '#33909F',
    textDecorationLine: 'underline',
  },
  Text: {
    color: 'white',
    fontSize:15,
    fontWeight:'500',
    fontFamily: 'Axiforma-Thin',
  },
  Text1: {
    fontFamily: 'Axiforma-Book',
    color: 'white',
    fontSize:15
  },
  tagLogin: {
    marginBottom: 20,
    borderRadius: 9,
    marginLeft: 80,
    flexDirection: 'row',
    height: 70,
    color: '#ffffff',
    justifyContent: 'center',
    marginRight: 80,
    marginTop: 5,
    backgroundColor: '#3f51b5',
    alignItems: 'center',
    alignSelf: 'stretch',
  },
});

          