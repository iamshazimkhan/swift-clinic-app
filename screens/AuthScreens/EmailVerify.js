import React, {Component} from 'react';
import {
  TextInput,
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Alert,
} from 'react-native';

import AsyncStorage from '@react-native-community/async-storage';
import AwesomeAlert from 'react-native-awesome-alerts';
import Spinner from 'react-native-loading-spinner-overlay';
import Icon from 'react-native-vector-icons/FontAwesome';
import ImagesHeader from '../../components/header';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import * as constants from '../../constants';

export default class EmailVerify extends Component {
  constructor(props) {
    super(props);
    this.state = {
      digit1: '',
      digit2: '',
      digit3: '',
      digit4: '',
      showAlert: false,
      spinner: false,
      email: '',
    };
  }

  async componentDidMount() {
    const email = await AsyncStorage.getItem('email');
    this.setState({email: email});
    console.log('email -------------->', email);
  }

  onVerifyCode() {
    const {digit1, digit2, digit3, digit4} = this.state;
    if (!digit1 || !digit2 || !digit3 || !digit4) {
      Alert.alert('Please Fill OTP');
    } else {
      this.onVerifyHit();
    }
  }

  hideAlert = () => {
    this.setState({
      showAlert: false,
    });
  };

  async onVerifyHit() {
    const {digit1, digit2, digit3, digit4} = this.state;
    const otp = parseInt(digit1 + digit2 + digit3 + digit4);
    console.log(' OTP --------------------->', parseInt(otp));
    const token = await AsyncStorage.getItem('token');
    this.setState({spinner: true});
    fetch(constants.URL + 'api/phone-number-verification', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token,
      },
      body: JSON.stringify({
        otp: otp,
      }),
    })
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({spinner: false});
        console.log('Response ======-------->', responseJson);
        if (responseJson.error === false) {
          this.setState({spinner: false, isVisible: true});
          this.props.navigation.navigate('App');
        } else {
          this.setState({spinner: false});
          Alert.alert('Incorrect OTP');
        }
      })
      .catch((error) => {
        this.setState({showAlert: true});
        console.error(error);
      });
  }

  async resendOTP() {
    this.setState({spinner: true});
    const token = await AsyncStorage.getItem('token');
    fetch(constants.URL + 'api/resend-verification-email', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token,
      },
    })
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({spinner: false});
        console.log('Email resend Change-------------->', responseJson);
      })
      .catch((error) => {
        console.error(error);
      });
  }

  async getProfile() {
    this.setState({spinner: true});
    const token = await AsyncStorage.getItem('token');
    await fetch(constants.URL + 'api/user', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token,
      },
    })
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({spinner: false});
        console.log('Response ======-------->', responseJson);
        if (responseJson.status) {
          this.props.navigation.navigate('Auth');
        } else {
          if (responseJson.user.email_verified_at === null) {
            Alert.alert('Email is not verified yet!');
          } else {
            AsyncStorage.setItem('isLoggedIn', 'true');
            // console.log('Else considition ---------->');
            this.props.navigation.navigate('App');
          }
        }
      })
      .catch((error) => {
        this.setState({spinner: false});
        console.error(error);
      });
  }

  render() {
    return (
      <View style={styles.container}>
      <View style={{
      marginTop: 20,
      flexDirection: 'row',
      justifyContent: 'space-between',
    }}>
        <View onStartShouldSetResponder={() => this.props.navigation.goBack(null)} 
        style={{marginTop:20, flex: 1, flexDirection: 'row'}}>
      <Icon
        style={{textAlignVertical: 'center', marginLeft: 20}}
        color="#0375FF"
        name="arrow-left"
      />
      <Text
        style={{textAlignVertical: 'center', marginLeft: 10, color: 'grey'}}>
        Back
      </Text>
    </View>
          <ImagesHeader />
          {/* <Image source={require('../assets/images/clinic_logo.png')} /> */}
        </View>
        <View style={styles.modal}>
          <Text style={styles.text}>Email</Text>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              height: 50,
            }}>
            <Text
              style={{
                fontSize: 21,
                color: '#0158ff',
                marginTop: 3,
                marginLeft: 10,
              }}>
              {this.state.email}
            </Text>
            {/* <View
              onStartShouldSetResponder={() =>
                this.setState({isVisible: false})
              }>
              <Icon
                name="edit"
                size={25}
                style={{color: '#0375FF', padding: 8}}
              />
            </View> */}
          </View>
          <View
            style={{
              flex: 1,
              flexDirection: 'column',
              backgroundColor: '#ffffff',
            }}>
            <Text style={styles.screen}>You are almost Done!</Text>
            <Text style={styles.tag}>
              We have sent a verification link to your email. Afer clicking on
              the link , please login again.
            </Text>
            <View
              onStartShouldSetResponder={this.resendOTP.bind(this)}>
              <Text style={styles.tagThree}>Resend Code</Text>
            </View>
            <TouchableOpacity
              onPress={this.getProfile.bind(this)}
              style={styles.button}>
              <Text style={{color: 'white'}}>Confirm Registration </Text>
            </TouchableOpacity>
            <AwesomeAlert
              show={this.state.showAlert}
              showProgress={false}
              title="Try again"
              message="Password and email did not match!"
              closeOnTouchOutside={true}
              closeOnHardwareBackPress={false}
              // showCancelButton={true}
              showConfirmButton={true}
              // cancelText="No, cancel"
              confirmText="OK"
              confirmButtonColor="#0158ff"
              onCancelPressed={() => {
                this.hideAlert();
              }}
              onConfirmPressed={() => {
                this.hideAlert();
              }}
            />
            <Spinner
              visible={this.state.spinner}
              textContent={''}
              textStyle={styles.spinnerTextStyle}
            />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  button: {
    borderRadius: 9,
    marginLeft: 80,
    height: 50,
    justifyContent: 'center',
    marginRight: 80,
    marginTop: 15,
    backgroundColor: '#0158ff',
    color: '#ffffff',
    alignItems: 'center',
    alignSelf: 'stretch',
  },
  input: {
    marginTop: 10,
    paddingLeft: 10,
    borderRadius: 8,
    height: 50,
    alignSelf: 'stretch',
    marginLeft: 40,
    marginRight: 40,
    borderWidth: 1,
    borderColor: '#cae3ff',
    marginBottom: 20,
  },
  inputCode: {
    marginTop: 10,
    borderRadius: 8,
    textAlign: 'center',
    height: 50,
    marginLeft: 3,
    marginRight: 7,
    borderWidth: 1,
    width: 49,
    borderColor: '#cae3ff',
    marginBottom: 20,
  },
  modal: {
    paddingTop: 10,
    alignSelf: 'stretch',
    backgroundColor: '#ECF5FF',
    height: 500,
    borderRadius: 10,
    borderWidth: 1,
    width: '100%',
    flex: 1,
    flexDirection: 'column',
    borderColor: '#fff',
    marginTop: 150,
    position: 'absolute',
  },
  text: {
    fontSize: 15,
    marginLeft: 10,
  },
  screen: {
    marginTop: 40,
    fontSize: 32,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  button: {
    marginBottom: 80,
    borderRadius: 9,
    marginLeft: 80,
    height: 50,
    justifyContent: 'center',
    marginRight: 80,
    marginTop: 15,
    backgroundColor: '#0158ff',
    alignItems: 'center',
    alignSelf: 'stretch',
  },
  tag: {
    marginTop: 10,
    fontSize: 17,
    paddingLeft: 50,
    paddingRight: 50,
    color: 'grey',
    textAlign: 'center',
  },
  tagThree: {
    marginTop: 40,
    fontSize: 17,
    paddingLeft: 50,
    paddingRight: 50,
    color: '#0158ff',
    fontWeight: '300',
    textAlign: 'center',
    textDecorationLine: 'underline',
  },
});
