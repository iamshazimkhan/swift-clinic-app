import React, {Component} from 'react';
import {
  TextInput,
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Alert,
  SafeAreaView,
  Keyboard
} from 'react-native';

import AsyncStorage from '@react-native-community/async-storage';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import LinearGradient from 'react-native-linear-gradient';
import AwesomeAlert from 'react-native-awesome-alerts';
import Spinner from 'react-native-loading-spinner-overlay';
import Icon from 'react-native-vector-icons/FontAwesome';
import ImagesHeader from '../../components/header';
import * as constants from '../../constants';
import IntlPhoneInput from 'react-native-intl-phone-input';

export default class PhoneVerify extends Component {
  constructor(props) {
    super(props);
    this.state = {
      digit1: '',
      digit2: '',
      digit3: '',
      digit4: '',
      showAlert: false,
      spinner: false,
      phone_number: '',
      isVerified: false,
      showAlert2: false,
      editable: false,
      email_verified_at: false,
    };
  }

  async componentDidMount() {
    const email = await AsyncStorage.getItem('email_verified_at');
    const phone = await AsyncStorage.getItem('phone');
    await this.setState({phone_number: phone});
    if (email === 'Verified') {
      await this.setState({email_verified_at: true});
    }
  }

  onVerifyCode() {
    const {digit1, digit2, digit3, digit4} = this.state;
    if (!digit1 || !digit2 || !digit3 || !digit4) {
      Alert.alert('Please Fill OTP');
    } else {
      this.setState({spinner: true});
      this.onVerifyHit();
    }
  }

  onChangeText = async ({
    dialCode,
    unmaskedPhoneNumber,
    phoneNumber,
    isVerified,
  }) => {
    console.log(dialCode, unmaskedPhoneNumber, phoneNumber, isVerified);
    if (isVerified === true) {
      await this.setState({
        phone_number: dialCode + unmaskedPhoneNumber,
        isVerified: isVerified,
      });
    }
  };

  async resendOTP() {
    this.setState({spinner: true});
    const token = await AsyncStorage.getItem('token');
    fetch(constants.URL + 'api/resend-otp', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token,
      },
    })
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({spinner: false});
        console.log('Status Change-------------->', responseJson);
      })
      .catch((error) => {
        console.error(error);
      });
  }

  hideAlert = () => {
    this.setState({
      showAlert: false,
    });
  };

  async onUpdateNumber() {
    this.setState({spinner: true});
    const token = await AsyncStorage.getItem('token');
    await fetch(constants.URL + 'api/update-phone-number', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token,
      },
      body: JSON.stringify({
        phone_number: this.state.phone_number,
      }),
    })
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({spinner: false});
        if (responseJson.error === false) {
          this.setState({editable: false, showAlert2: true});
        } else {
          Alert.alert('Please check your Number');
        }
        console.log('Status Chnage-------------->', responseJson);
      })
      .catch((error) => {
        console.error(error);
      });
  }

  async onVerifyHit() {
    const {digit1, digit2, digit3, digit4, email_verified_at} = this.state;
    const otp = digit1 + digit2 + digit3 + digit4;
    console.log(' OTP --------------------->', otp);
    const token = await AsyncStorage.getItem('token');
    this.setState({spinner: true});
    fetch(constants.URL + 'api/phone-number-verification', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token,
      },
      body: JSON.stringify({
        otp: otp,
      }),
    })
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({spinner: false});
        console.log('Response ======-------->', responseJson);
        if (responseJson.error === false) {
          this.setState({spinner: false, isVisible: true});
          if (email_verified_at === false) {
            AsyncStorage.setItem('isLoggedIn', 'false');
            this.props.navigation.navigate('EmailVerify');
          } else {
            AsyncStorage.setItem('isLoggedIn', 'true');
            this.props.navigation.navigate('App');
          }
        } else {
          this.setState({spinner: false, showAlert: true});
          //   Alert.alert('Incorrect OTP');
        }
      })
      .catch((error) => {
        this.setState({showAlert: true});
        console.error(error);
      });
  }

  render() {
    return (
      <View style={styles.container}>
              <View style={{
      marginTop: 20,
      flexDirection: 'row',
      justifyContent: 'space-between',
    }}>
        <View onStartShouldSetResponder={() => this.props.navigation.goBack(null)} 
        style={{marginTop:20, flex: 1, flexDirection: 'row'}}>
      <Icon
        style={{textAlignVertical: 'center', marginLeft: 20}}
        color="#E73988"
        name="arrow-left"
      />
      <Text
        style={{textAlignVertical: 'center', marginLeft: 10, color: 'grey'}}>
        Back
      </Text>
    </View>
          <ImagesHeader />
          {/* <Image source={require('../assets/images/clinic_logo.png')} /> */}
        </View>
        <View style={styles.modal}>
          <Text style={styles.text}>Phone Number</Text>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              height: 50,
            }}>
            <Text
              style={{
                fontSize: 21,
                color: '#33909F',
                marginTop: 3,
                marginLeft: 10,
              }}>
              {this.state.phone_number}
            </Text>
            <View
              onStartShouldSetResponder={() => { console.log('Editable ----->'); this.setState({editable: true, showAlert2: false})}}>
              <Icon
                name="edit"
                size={25}
                style={{color: '#33909F', padding: 8}}
              />
            </View>
          </View>
          {this.state.editable ? (
            <View style={{backgroundColor: '#ffffff', flex: 1}}>
              <Text style={styles.tag}> Update your phone number </Text>
              <SafeAreaView style={styles.input}>
                <IntlPhoneInput
                  onChangeText={this.onChangeText}
                  defaultCountry="IN"
                  placeholder="Enter your new number"
                  style={styles.input}
                />
              </SafeAreaView>


             <LinearGradient colors={['#78C2CB', '#33909F']} style={styles.button}>
              <TouchableOpacity
                onPress={this.onUpdateNumber.bind(this)}>
                <Text style={{color: '#ffffff'}}>Update </Text>
              </TouchableOpacity>
              </LinearGradient>

              <View
                onStartShouldSetResponder={() =>
                  this.setState({editable: false})
                }>
                <Text style={styles.tagThree}>Back</Text>
              </View>

              <AwesomeAlert
                show={this.state.showAlert2}
                showProgress={false}
                title="Successfully sent"
                message="OTP has sent to updated number!"
                closeOnTouchOutside={true}
                closeOnHardwareBackPress={false}
                // showCancelButton={true}
                showConfirmButton={true}
                // cancelText="No, cancel"
                confirmText="OK"
                confirmButtonColor="#0158ff"
                onCancelPressed={() => {
                  this.hideAlert();
                }}
                onConfirmPressed={() => {
                  this.setState({editable: false});
                }}
              />
            </View>
          ) : (
            <View
              style={{
                flex: 1,
                flexDirection: 'column',
                backgroundColor: '#ffffff',
              }}>
              <Text style={styles.screen}>You are almost Done!</Text>
              <Text style={styles.tag}>
                We have sent a verification code to your phone number.
              </Text>
              <View style={{flexDirection: 'row', justifyContent: 'center'}}>
                <TextInput
                  keyboardType={'numeric'}
                  value={this.state.digit1}
                  onChangeText={(digit1) => { this.setState({digit1}); if(digit1.length === 1) this.secondTextInput.focus(); }}
                  placeholder={'C'}
                  maxLength={1}
                  style={styles.inputCode}
                />
                <TextInput
                 ref={(input) => { this.secondTextInput = input; }}
                  value={this.state.digit2}
                  onChangeText={(digit2) => {  this.setState({digit2});  if(digit2.length === 1)  this.thirdTextInput.focus();}}
                  placeholder={'O'}
                  maxLength={1}
                  keyboardType={'numeric'}
                  style={styles.inputCode}
                />
                <TextInput
                  value={this.state.digit3}
                  ref={(input) => { this.thirdTextInput = input; }}
                  onChangeText={(digit3) => {this.setState({digit3}); if(digit3.length === 1)  this.fourthTextInput.focus(); }}
                  placeholder={'D'}
                  maxLength={1}
                  keyboardType={'numeric'}
                  style={styles.inputCode}
                />
                <TextInput
                  ref={(input) => { this.fourthTextInput = input; }}
                  value={this.state.digit4}
                  onChangeText={(digit4) => { this.setState({digit4}); if(digit4.length === 1) Keyboard.dismiss();  }}
                  placeholder={'E'}
                  maxLength={1}
                  keyboardType={'numeric'}
                  style={styles.inputCode}
                />
              </View>

              <LinearGradient colors={['#78C2CB', '#33909F']} style={styles.button}>
              <TouchableOpacity
                onPress={this.onVerifyCode.bind(this)}>
                <Text style={{color: '#ffffff'}}>Confirm registration</Text>
              </TouchableOpacity>
              </LinearGradient>

              <View onStartShouldSetResponder={this.resendOTP.bind(this)}>
                <Text style={styles.tagThree}>Resend Code</Text>
              </View>
              <AwesomeAlert
                show={this.state.showAlert}
                showProgress={false}
                title="Try again"
                message="Incorrect OTP"
                closeOnTouchOutside={true}
                closeOnHardwareBackPress={false}
                // showCancelButton={true}
                showConfirmButton={true}
                // cancelText="No, cancel"
                confirmText="OK"
                confirmButtonColor="#0158ff"
                onCancelPressed={() => {
                  this.hideAlert();
                }}
                onConfirmPressed={() => {
                  this.hideAlert();
                }}
              />
            </View>
          )}
        </View>
        <Spinner
          visible={this.state.spinner}
          textContent={''}
          textStyle={styles.spinnerTextStyle}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  input: {
    marginTop: 10,
    paddingLeft: 10,
    borderRadius: 8,
    height: 59,
    alignSelf: 'stretch',
    marginLeft: 40,
    marginRight: 40,
    borderWidth: 1,
    borderColor: '#dcecef',
    marginBottom: 20,
  },
  inputCode: {
    marginTop: 10,
    borderRadius: 8,
    textAlign: 'center',
    height: 50,
    marginLeft: 3,
    marginRight: 7,
    borderWidth: 1,
    width: 49,
    borderColor: '#dcecef',
    marginBottom: 20,
  },
  modal: {
    paddingTop: 10,
    alignSelf: 'stretch',
    backgroundColor: '#ECF5FF',
    height: 500,
    borderRadius: 10,
    borderWidth: 1,
    width: '100%',
    flex: 1,
    flexDirection: 'column',
    borderColor: '#fff',
    marginTop: 150,
    position: 'absolute',
  },
  text: {
    fontSize: 15,
    marginLeft: 10,
  },
  screen: {
    marginTop: 40,
    fontSize: 32,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  button: {
    marginBottom: 80,
    borderRadius: 9,
    marginLeft: 80,
    height: 70,
    justifyContent: 'center',
    marginRight: 80,
    marginTop: 15,
    alignItems: 'center',
    alignSelf: 'stretch',
  },
  tag: {
    marginTop: 10,
    fontSize: 17,
    paddingLeft: 50,
    paddingRight: 50,
    color: 'grey',
    textAlign: 'center',
  },
  tagThree: {
    marginTop: -40,
    fontSize: 17,
    paddingLeft: 50,
    paddingRight: 50,
    color: '#33909F',
    fontWeight: '300',
    textAlign: 'center',
    textDecorationLine: 'underline',
  },
});
