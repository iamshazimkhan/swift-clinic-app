import React, {Component} from 'react';
import {View, Text, FlatList,TouchableNativeFeedback, ScrollView} from 'react-native';
import TitleImage from '../../components/Headers/titleImage';
import ContentBox from '../../components/consultations/ContentBox';
import { SafeAreaView } from 'react-navigation';
import {GetDoctorsPreviousConsultations} from '../../api/ConsultationApi';
import SpinnerComponent from '../../components/Spinner/Spinner';
import SvgGraph from '../../components/backgroundgraph';
import NoData from '../../components/NoData';
import Moment from 'moment';

export default class DoctorConsultationHistory extends Component {
  state = {
    data: [],
    spinnerLoading: true,
    noData : false
  };

  componentDidMount = async () => {
    this.GetHistory();
    this.willFocusSubscription = this.props.navigation.addListener(
      'willFocus',
      () => {
      //  console.log('Console Data Random----------->',Math.floor(Math.random() * 100) + 1 );
       this.GetHistory();
      }
    );
  };

  GetHistory = async () => {
    GetDoctorsPreviousConsultations()
    .then(async (res) => {
      console.log('Response =====------->', res.data.data);
      if(res.data.data.length > 0)
      {
        this.setState({noData: false})
      }
      else{
        this.setState({noData: true})
      }
      await this.setState({data: res.data.data, spinnerLoading: false});
    })
    .catch((err) => console.log('Error -------->', err));
  }

  formatDateTime(datetime){
    return Moment.utc(datetime).local().format('LL')
 }

 navigateToActiveConsultationDetails = async (item) => {
  console.log('Item Consultation Details',item)
  this.props.navigation.navigate('ActiveConsultations',{id : item.id});
}

  render() {
    return (
      <SafeAreaView style={{backgroundColor: '#fff', flex: 1}}>
        <SvgGraph style={{right:-38}} />
        <View style={{marginTop: -270}}>
        <ScrollView>
        <TitleImage></TitleImage>
        <Text
          style={{
            textAlign: 'center', fontFamily: 'Axiforma-Medium', fontSize: 19, marginTop: 40
          }}>
          Consultation History
        </Text>
        <View style={{ padding: 20}}>

          {this.state.noData ?
         ( <NoData></NoData> )
          : (
            <FlatList
            scrollEnabled={false}
            data={this.state.data}
            horizontal={false}
            numColumns={1}
            keyExtractor={(item) => item.id.toString()}
            renderItem={({item}) => (
              <TouchableNativeFeedback onPress={() => this.navigateToActiveConsultationDetails(item)}>                
                <ContentBox
                  title={this.formatDateTime(item.slottime)}
                  doctorName={"Consultation with " +item.patient.first_name + " " + item.patient.last_name}
                  colour={item.doctor_response === null ? "white" : "white"}></ContentBox>
              </TouchableNativeFeedback>
            )}
          />
          )  }
          <SpinnerComponent
            Visible={this.state.spinnerLoading}></SpinnerComponent>
        </View>
        </ScrollView>
        </View>
      </SafeAreaView>
    );
  }
}
