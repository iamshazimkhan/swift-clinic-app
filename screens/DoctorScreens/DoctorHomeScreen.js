import React, {Component} from 'react';
import {
  Image,
  FlatList,
  View,
  TouchableNativeFeedback,
  Text,
  RefreshControl,
} from 'react-native';
import styles from '../../css/css';
import {SafeAreaView} from 'react-navigation';
import TitleImage from '../../components/Headers/titleImage';
import AddConsultation from '../../components/consultations/AddConsultation';
import ContentBox from '../../components/consultations/ContentBox';
import QuestionModal from '../../components/Modals/QuestionModal';
import AnswerQuestionBox from '../../components/consultations/AnswerQuestionBox';
import {ScrollView, TouchableOpacity} from 'react-native-gesture-handler';
import SvgGraph from '../../components/backgroundgraph';
import {getAllConsultationData} from '../../api/ConsultationApi';
import NoDataBox from '../../components/consultations/NoDataBox';
import SpinnerComponent from '../../components/Spinner/Spinner';
import Moment from 'moment';

class HomeScreen extends Component {
  state = {
    ModalVisibility: false,
    data: [],
    dataTwo: [],
    spinnerLoading: true,
    completed: false,
    refreshing: false,
    showNoData: false,
  };

  navigateToConsultationDetails = async (item) => {
    // console.log('Item Consultation Details',item)
    this.props.navigation.navigate('ConsultationStatus', {id: item.id});
  };

  navigateToActiveConsultationDetails = async (item) => {
    // console.log('Item Consultation Details',item)
    this.props.navigation.navigate('ActiveConsultations', {id: item.id});
  };

  componentDidMount = async () => {
    this.getUserUpcomingConsultant();
    this.willFocusSubscription = this.props.navigation.addListener(
      'willFocus',
      () => {
        console.log(
          'Console Data ----------->',
          Math.floor(Math.random() * 100) + 1,
        );
        this.getUserUpcomingConsultant();
      },
    );
  };

  componentWillUnmount() {
    this.willFocusSubscription.remove();
  }

  getUserUpcomingConsultant = async () => {
    getAllConsultationData()
      .then(async (res) => {
        console.log('Response -------> ', res);
        await this.setState({
          data: res.data.data.pending,
          dataTwo: res.data.data.active,
          spinnerLoading: false,
          refreshing: false,
          showNoData: true,
        });
      })
      .catch((err) => console.log('Error -------->', err));
  };

  formatDateTime(datetime) {
    return Moment.utc(datetime).local().format('LL');
  }

  _onRefresh = () => {
    console.log('Refreshing');
    this.setState({refreshing: true});
  };

  render() {
    return (
      <ScrollView style={{flex: 1, backgroundColor: '#fcfeff'}}>
        <SafeAreaView style={{backgroundColor: '#fcfeff'}}>
          <SvgGraph style={{right: -38}} />
          <View style={{padding: 10, marginTop: -260, width: '100%'}}>
            <TitleImage></TitleImage>
            <View style={{marginTop: 30, marginBottom: 20}}>
              {this.state.showNoData ? (
                <Text
                  style={{
                    textAlign: 'center',
                    fontFamily: 'Axiforma-Medium',
                    fontSize: 19,
                    marginTop: 20,
                  }}>
                  Requested Consultations
                </Text>
              ) : null}
              {this.state.data.length === 0 && this.state.showNoData ? (
                <NoDataBox title="No Requested Consultations found."></NoDataBox>
              ) : (
                <FlatList
                  scrollEnabled={false}
                  data={this.state.data}
                  horizontal={false}
                  numColumns={1}
                  keyExtractor={(item) => item.id.toString()}
                  renderItem={({item}) => (
                    <TouchableNativeFeedback
                      onPress={() => this.navigateToConsultationDetails(item)}>
                      <ContentBox
                        doctorName={
                          'Consultation with ' +
                          item.patient.first_name +
                          ' ' +
                          item.patient.last_name
                        }
                        colour="white"
                        title={this.formatDateTime(
                          item.slottime2 === null
                            ? item.slottime
                            : item.slottime2,
                        )}></ContentBox>
                    </TouchableNativeFeedback>
                  )}
                />
              )}
            </View>

            <View style={{marginTop: 30, marginBottom: 20}}>
              {this.state.showNoData ? (
                <Text
                  style={{
                    textAlign: 'center',
                    fontFamily: 'Axiforma-Medium',
                    fontSize: 19,
                  }}>
                  Active Consultations
                </Text>
              ) : null}
              {this.state.dataTwo.length === 0 && this.state.showNoData ? (
                <NoDataBox title="No Active Consultations found."></NoDataBox>
              ) : (
                <FlatList
                  scrollEnabled={false}
                  data={this.state.dataTwo}
                  horizontal={false}
                  numColumns={1}
                  keyExtractor={(item) => item.id.toString()}
                  renderItem={({item}) => (
                    <TouchableNativeFeedback
                      onPress={() =>
                        this.navigateToActiveConsultationDetails(item)
                      }>
                      <ContentBox
                        doctorName={
                          'Consultation with ' +
                          item.patient.first_name +
                          ' ' +
                          item.patient.last_name
                        }
                        colour="white"
                        title={this.formatDateTime(
                          item.slottime2 === null
                            ? item.slottime
                            : item.slottime2,
                        )}></ContentBox>
                    </TouchableNativeFeedback>
                  )}
                />
              )}
            </View>

            <SpinnerComponent
              Visible={this.state.spinnerLoading}></SpinnerComponent>
          </View>
        </SafeAreaView>
      </ScrollView>
    );
  }
}

export default HomeScreen;
// this.props.navigation.navigate('PhoneVerify');
