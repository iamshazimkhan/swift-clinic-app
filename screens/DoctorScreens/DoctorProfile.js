import React, {Component} from 'react';
import {
  TextInput,
  View,
  Text,
  StyleSheet,
  Image,
  ScrollView,
  Switch,
  Alert,
  TouchableOpacity,
  Platform,
  PermissionsAndroid,
  Linking,
  AlertIOS,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {
  LoginManager,
  LoginButton,
  AccessToken,
  GraphRequest,
  GraphRequestManager,
} from 'react-native-fbsdk';
import Icon from 'react-native-vector-icons/FontAwesome';
import Spinner from 'react-native-loading-spinner-overlay';
import * as constants from '../../constants';
import AsyncStorage from '@react-native-community/async-storage';
import {SafeAreaView} from 'react-navigation';
import AwesomeAlert from 'react-native-awesome-alerts';
import DropDownPicker from 'react-native-dropdown-picker';
import ImagePicker from 'react-native-image-picker';
import {
  AddAvailableTimmings,
  createDoctorAvailableTime,
} from '../../api/ConsultationApi';
import AwesomeAlertComponent from '../../components/Alerts/AwesomeAlertFile';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {
  faLock,
  faAirFreshener,
  faCalendar,
  faEnvelope,
} from '@fortawesome/free-solid-svg-icons';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import ProfileHeader from '../../components/Headers/ProfileHeader';
import AvailableTimmings from '../../components/Date/AvailableTimmings';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import Toast from 'react-native-simple-toast';
import SvgGraph from '../../components/backgroundgraph';
import Moment from 'moment';
import CalenderModal from '../../components/Date/CalenderModal';
import moment from 'moment';

export default class Login extends Component {
  //   static navigationOptions = {
  //     header: null,
  //   };

  constructor(props) {
    super(props);

    this.state = {
      showBack: false,
      full_name: '',
      first_name: '',
      last_name: '',
      email: '',
      phone_number: '',
      spinner: true,
      profile_pic: '',
      showAlert: false,
      showAlertService: false,
      showAlertService2: false,
      password: '',
      security_question_1: '3',
      security_answer_1: '',
      security_question_2: '4',
      CalenderModalVisibility: false,
      security_answer_2: '',
      editable: false,
      switch: false,
      token: 'abcdsefbrjgbjbktbkkkkkkkkkkkkkklllllll',
      security_question_1_full: '',
      security_question_2_full: '',
      avatar: null,
      about: null,
      showDate: 'Select Date',
      startTime: 'Start Time',
      eTime: null,
      sTime: null,
      startTimeForCompare: null,
      customCalenderArray: {},
      AlertComponentMessage: null,
      AlertComponentVisible: false,
      isStartTimePickerVisible: false,
      isEndTimePickerVisible: false,
      endTime: 'End Time',
      TimeVisible: false,
      items: [
        {
          label: 'Select Question',
          value: '3',
        },
        {
          label: 'Select Question',
          value: '4',
        },
      ],
    };
  }

  async updateProfile() {
    const {
      first_name,
      last_name,
      email,
      phone_number,
      security_question_1,
      security_question_2,
      security_answer_1,
      security_answer_2,
      about,
    } = this.state;
    if (
      !first_name ||
      !last_name ||
      !email ||
      !phone_number ||
      !security_question_1 ||
      !security_question_2 ||
      !security_answer_1 ||
      !security_answer_2 ||
      !about
    ) {
      Alert.alert('Please fill all Fields');
    } else {
      const token = await AsyncStorage.getItem('token');
      this.setState({spinner: true});
      fetch(constants.URL + 'api/update-profile', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + token,
        },
        body: JSON.stringify({
          email: this.state.email,
          first_name: this.state.first_name,
          last_name: this.state.last_name,
          security_question_1: this.state.security_question_1,
          security_question_2: this.state.security_question_2,
          phone_number: this.state.phone_number,
          security_answer_1: this.state.security_answer_1,
          security_answer_2: this.state.security_answer_2,
          about: this.state.about,
        }),
      })
        .then((response) => response.json())
        .then((responseJson) => {
          this.setState({spinner: false, editable: false, showBack: false});
          // console.log('Response ======-------->', responseJson);
          if (responseJson.error === false) {
            this.getProfile();
          } else {
            this.setState({spinner: false, showAlert: true});
          }
        })
        .catch((error) => {
          this.setState({showAlert: true});
          console.error(error);
        });
    }
  }

  async updateImage(data) {
    let filename = 'randmfile';
    if (data.fileName) {
      filename = data.fileName;
    }
    console.log('Filename is ------------->', filename);
    const token = await AsyncStorage.getItem('token');
    const dataImage = new FormData();
    dataImage.append('profile_pic', {
      uri: data.uri,
      type: data.type,
      name: filename,
    });
    fetch(constants.URL + 'api/update-profile-picture', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
        Authorization: 'Bearer ' + token,
      },
      body: dataImage,
    })
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({spinner: false, editable: false, showBack: false});
        // console.log('Response ======-------->', responseJson);
      })
      .catch((error) => {
        this.setState({showAlert: true});
        console.error(error);
      });
  }

  async handlePicker() {
    // console.log('edit');
    ImagePicker.showImagePicker({}, (response) => {
      // console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('App Error Fix here');
        Alert.alert('Please provide camera permission.');
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        this.setState({avatar: response.uri, spinner: true});
        console.log('URI -------------->', response.uri);
        this.updateImage(response);
      }
    });
  }

  async onSwitch() {
    const token = await AsyncStorage.getItem('token');
    let v = !this.state.switch;
    this.setState({switch: v});
    let status = 0;
    if (v === true) {
      status = 1;
      // console.log('Status Value in Condition ------------>', status);
    }
    // console.log('-----------> Status Value outer -------------->', status);
    fetch(constants.URL + 'api/update-push-preference', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token,
      },
      body: JSON.stringify({
        push_status: status,
        push_token: this.state.token,
      }),
    })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log('Status Chnage-------------->', responseJson);
      })
      .catch((error) => {
        console.error(error);
      });
  }

  hideAlert = () => {
    this.setState({
      showAlert: false,
    });
    this.props.navigation.navigate('Auth');
  };

  async componentDidMount() {
    await this.getProfile();
    console.log('ahshsa');
    await this.getQuestions();
  }

  async addPermission() {
    if (Platform.OS === 'android') {
      // Calling the permission function
      // const granted = await PermissionsAndroid.request(
      //   PermissionsAndroid.PERMISSIONS.CAMERA,
      //   {
      //     title: 'Camera Permission',
      //     message: 'Swift Clinic needs access to your camera',
      //   },
      // );
      // if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      //   // Permission Granted
      //   this.setState({ showAlertService: true })
      // } else {
      //   // Permission Denied
      //   this.setState({ showAlertService: true })
      // }
      PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.CAMERA).then(
        (response) => {
          // console.log('Camera Permission ----------------->', response);
          if (response === true) {
            this.setState({showAlertService2: true});
          } else {
            this.setState({showAlertService: true});
          }
        },
      );
    } else {
      Alert.alert('Permission is Granted');
    }
  }

  async grantPermission() {
    await this.setState({showAlertService: false});
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.CAMERA,
    );
  }

  async getQuestions() {
    this.setState({spinner: true});
    await fetch(constants.URL + 'api/fetch-security-questions', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then((response) => response.json())
      .then((responseJson) => {
        const d = responseJson.data;
        // console.log('------Data ---------->', d);
        const allData = [];
        for (let index = 0; index < d.length; index++) {
          const result = {
            label: d[index].question,
            value: d[index].id,
          };
          // console.log('Item in loop---------->', result);
          allData.push(result);
        }
        // console.log('Complete Data---------->', allData);
        this.setState({items: allData, spinner: false});
        // console.log('Complete Data in State---------->', this.state.items);
      })
      .catch((error) => {
        this.setState({spinner: false});
        console.error(error);
      });
  }

  async logOut() {
    const fb_token = await AsyncStorage.getItem('fb_token');
    if (fb_token === null) {
      await AsyncStorage.clear();
      this.props.navigation.navigate('LoginSignup');
    } else {
      let logout = await new GraphRequest(
        'me/permissions/',
        {
          accessToken: fb_token,
          httpMethod: 'DELETE',
        },
        (error, result) => {
          if (error) {
            // console.log('Error fetching data: ' + error.toString());
          } else {
            LoginManager.logOut();
            console.log('fb logout Successfully');
          }
        },
      );
      await new GraphRequestManager().addRequest(logout).start();
      await AsyncStorage.clear();
      this.props.navigation.navigate('LoginSignup');
    }
  }

  async getProfile() {
    const token = await AsyncStorage.getItem('token');
    let v = true;
    await fetch(constants.URL + 'api/user', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token,
      },
    })
      .then((response) => response.json())
      .then(async (responseJson) => {
        this.setState({spinner: false});
        console.log('Response ======-------->', responseJson);
        if (responseJson.status) {
          this.setState({showAlert: true});
        } else {
          let ras = responseJson.user.avilable_dates;
          var dateData = {};
          var key;
          let today = new Date();
          let day = moment(today).format('D');
          let month = moment(today).format('MM');
          let year = moment(today).format('YYYY');
          await ras.forEach(function (date) {
            key = date.date;
            let obj = {
              // marked: true,
              disabled: false,
              customStyles: {
                container: {
                  // backgroundColor: 'rgba(0, 107, 255, 0.065)',
                },
                text: {
                  color: '#33909F',
                  fontWeight: 'bold',
                },
              },
            };
            // let obj = {marked: true, disabled: false};
            dateData[key] = obj;
            let current = `${year}-${month}-${day}`;
            dateData[current] = {marked: true, disabled: false};
          });
          // await console.log(dateData);
          if (responseJson.user.push_notification_status === 1) {
            v = true;
          } else {
            v = false;
          }
          await this.setState({
            customCalenderArray: dateData,
            switch: v,
            first_name: responseJson.user.first_name,
            last_name: responseJson.user.last_name,
            about: responseJson.user.about,
            email: responseJson.user.email,
            phone_number: responseJson.user.phone_number,
            security_question_1: responseJson.user.security_question_1,
            security_question_2: responseJson.user.security_question_2,
            security_question_1_full:
              responseJson.user.security_question_1_full,
            security_question_2_full:
              responseJson.user.security_question_2_full,
            security_answer_1: responseJson.user.security_answer_1,
            security_answer_2: responseJson.user.security_answer_2,
            avatar: responseJson.user.profile_pic,
            full_name:
              responseJson.user.first_name + ' ' + responseJson.user.last_name,
          });
        }
      })
      .catch((error) => {
        this.setState({spinner: false});
        console.error(error);
      });
  }

  // --------------------------------------- Date Time --------------------------------------

  dateCallback = async (selectedDate) => {
    var gmtDateTime = Moment.utc(selectedDate, 'YYYY-MM-DD');
    var locale_date = gmtDateTime.local().format('YYYY-MM-DD');
    var current_time = Moment()
      .local()
      .subtract(1, 'days')
      .format('YYYY-MM-DD');
    console.log('After ------>', Moment(locale_date).isAfter(current_time));
    if (Moment(locale_date).isAfter(current_time)) {
      this.setState({
        CalenderModalVisibility: false,
        showDate: locale_date,
        TimeVisible: true,
      });
    } else {
      Alert.alert('Please select an appointment time from tomorrow  onwards.');
    }
    console.log('Calender Date -------->', selectedDate);
  };

  startTimeCallback = async (StartTime) => {
    const {showDate} = this.state;
    let d4 = Moment(StartTime.getTime());

    /////////////////////////////////////////
    if (showDate === 'Select Date') {
      await this.setState({startTime: 'Start Time', sTime: null});
      AlertIOS.alert('Please select date first');
    } else {
      const Todaydate = await Moment(new Date()).format('HH:mm:ss');
      const current_date = Moment().local().format('YYYY-MM-DD');
      const modifiedTodayDate = await Moment('2017-03-13' + ' ' + Todaydate);
      const selectedTime = d4.format('HH:mm:ss');
      const modifiedSelectedDate = await Moment(
        '2017-03-13' + ' ' + selectedTime,
      );

      console.log(
        'Out Date Right 2------>',
        Moment(showDate).isAfter(current_date),
        current_date,
        showDate,
      );

      if (
        Moment(modifiedSelectedDate).isAfter(modifiedTodayDate) === true ||
        Moment(showDate).isAfter(current_date) === true
      ) {
        console.log(
          'Today Date Right 2------>',
          Moment(showDate).isAfter(current_date),
        );
        let finalStartTime = d4.format('LT');
        let startTimeFinal = d4.format('HH:mm:ss');
        const d = await Moment('2017-03-13' + ' ' + startTimeFinal);
        const u = await Moment.utc(d).format('HH:mm:ss');
        await this.setState({
          startTime: finalStartTime,
          sTime: u,
          startTimeForCompare: modifiedSelectedDate,
        });
      } else {
        Toast.show(
          'Please select an appointment time from tomorrow  onwards.',
          Toast.SHORT,
        );
        await this.setState({startTime: 'Start Time', sTime: null});
      }
    }
  };

  ///////////////////////////////////////////////

  EndTimeCallback = async (EndTime) => {
    const {showDate, startTimeForCompare} = this.state;
    const current_date = Moment().local().format('YYYY-MM-DD');
    let d4 = await Moment(EndTime.getTime());
    const Todaydate = await Moment(new Date()).format('HH:mm:ss');
    const modifiedTodayDate = await Moment('2017-03-13' + ' ' + Todaydate);
    const selectedTime = d4.format('HH:mm:ss');
    const modifiedSelectedDate = await Moment(
      '2017-03-13' + ' ' + selectedTime,
    );

    if (showDate === 'Select Date') {
      Alert.alert('Please select date first');
      await this.setState({endTime: 'End Time', eTime: null});
    } else if (startTimeForCompare === null) {
      Alert.alert('Please select start time first.');
      await this.setState({endTime: 'End Time', eTime: null});
    } else if (
      Moment(modifiedSelectedDate).isAfter(startTimeForCompare) === false
    ) {
      Alert.alert('Please select end time bigger than start time.');
      await this.setState({endTime: 'End Time', eTime: null});
    } else {
      if (
        Moment(modifiedSelectedDate).isAfter(modifiedTodayDate) === true ||
        Moment(showDate).isAfter(current_date) === true
      ) {
        console.log(
          'Today Date Right 2------>',
          modifiedTodayDate,
          modifiedSelectedDate,
        );
        let finalStartTime = await d4.format('LT');
        let endTimeFinal = d4.format('HH:mm:ss');
        const d = await Moment('2017-03-13' + ' ' + endTimeFinal);
        const u = await Moment.utc(d).format('HH:mm:ss');

        await this.setState({endTime: finalStartTime, eTime: u});
      } else {
        Toast.show(
          'Please select an appointment time from tomorrow  onwards.',
          Toast.SHORT,
        );
        await this.setState({endTime: 'End Time', eTime: null});
      }
    }
  };

  closeTimeModal = async () => {
    await this.setState({TimeVisible: false});
  };

  addTime = async (item) => {
    const {showDate, sTime, eTime} = this.state;
    console.log('khurram', sTime);
    if (showDate === 'Select Date' || item.length == 0) {
      Alert.alert('Fill all Fields.');
    } else {
      await this.setState({spinner: true});
      console.log('Add Time API');
      AddAvailableTimmings(showDate, item)
        .then(async (res) => {
          console.log('Response U  =====------->', res.data);
          await this.setState({
            TimeVisible: false,
            spinner: false,
            AlertComponentMessage: 'Time Slot Created',
            AlertComponentVisible: true,
          });
        })
        .catch((err) => console.log('Error -------->', err));
    }
  };

  closeAlertFunction = async () => {
    await this.setState({AlertComponentVisible: false});
  };

  goBack = async () => {
    await this.setState({editable: false, showBack: false});
  };

  formatDateTime = async (startTime) => {
    const d = await Moment('2017-03-13' + ' ' + startTime);
    const u = await Moment.utc(d).format('HH:mm:ss');
    console.log('-------time Changes----->', u, '------------->', startTime);
  };

  formatDateTimeForDate(datetime) {
    return Moment.utc(datetime).local().format('YYYY-MM-DD');
  }

  showCalender = async () => {
    console.log('Hellooooo');
    this.setState({CalenderModalVisibility: true, TimeVisible: false});
  };

  closeCalenderModal = async () => {
    await this.getProfile();
    await this.getQuestions();
    this.setState({CalenderModalVisibility: false, TimeVisible: false});
  };

  // ------------------------------------ Timer -------------------------------------

  showStartTimePicker = async () => {
    await this.setState({isStartTimePickerVisible: true, TimeVisible: false});
  };

  hideStartTimePicker = async () => {
    console.log('hiding start time');
    await this.setState({isStartTimePickerVisible: false, TimeVisible: true});
  };

  handleStartTimeConfirm = async (date) => {
    console.log('A date has been picked: ', date);
    await this.hideStartTimePicker();

    await this.startTimeCallback(date);
  };

  // ----------------------------- End Time ---------------------------

  showEndTimePicker = async () => {
    await this.setState({isEndTimePickerVisible: true, TimeVisible: false});
  };

  hideEndTimePicker = async () => {
    await this.setState({isEndTimePickerVisible: false, TimeVisible: true});
  };

  handleEndTimeConfirm = async (date) => {
    console.log('A date has been picked: ', date);
    await this.hideEndTimePicker();
    await this.EndTimeCallback(date);
  };

  timeSelection = async (item) => {
    // this.setState({sTime: item});
    let time = [];
    item.forEach((it) => {
      time.push(moment(it, 'h:mm A').format('HH:mm'));
    });
    // let abc = await AsyncStorage.getAllKeys();
    // let item1 = await AsyncStorage.getItem(abc[0]);
    // let item2 = await AsyncStorage.getItem(abc[1]);
    // let item3 = await AsyncStorage.getItem(abc[2]);
    // let item4 = await AsyncStorage.getItem(abc[3]);
    // console.log(abc, 'khua', item1, item2, item3, item4);

    let result = await createDoctorAvailableTime({
      date: this.state.showDate,
      times: time,
    });
    if (result) {
      console.log('dinal array', result, 'date', time, this.state.showDate);
      Alert.alert('Time slot is created');
      this.closeTimeModal();
      await this.getProfile();
      await this.getQuestions();
    } else {
      Alert.alert('Error in server');
      // this.closeTimeModal();
    }

    // this.addTime(item);
  };

  render() {
    return (
      <KeyboardAwareScrollView>
        <SafeAreaView
          style={{
            flex: 1,
            backgroundColor: 'white',
            opacity:
              this.state.TimeVisible || this.state.CalenderModalVisibility
                ? 0.2
                : 1,
          }}>
          <SvgGraph style={{right: -38}} />
          <View style={{marginTop: -280}}>
            <Spinner
              visible={this.state.spinner}
              textContent={''}
              textStyle={styles.spinnerTextStyle}
            />
            {this.state.CalenderModalVisibility ? (
              <CalenderModal
                customDateArray={this.state.customCalenderArray}
                closeModal={this.closeCalenderModal}
                disabledDays={false}
                Visibilty={this.state.CalenderModalVisibility}
                selectedDateCallback={this.dateCallback}></CalenderModal>
            ) : null}

            {this.state.TimeVisible ? (
              <AvailableTimmings
                showCalender={this.showCalender}
                closeModal={this.closeTimeModal}
                TimeVisible={this.state.TimeVisible}
                Date={
                  this.state.showDate === 'Select Date'
                    ? 'Select Date'
                    : this.formatDateTimeForDate(this.state.showDate)
                }
                timeSelection={this.timeSelection}
                selectedDateCallback={this.dateCallback}
                startTime={this.state.startTime}
                endTime={this.state.endTime}
                showStartTimePicker={
                  this.state.showDate === 'Select Date'
                    ? null
                    : this.showStartTimePicker
                }
                showEndTimePicker={
                  this.state.showDate === 'Select Date'
                    ? null
                    : this.showEndTimePicker
                }
                onClick={this.addTime}></AvailableTimmings>
            ) : null}

            {/* ------------------------------ Timer ----------------------------- */}
            {this.state.isStartTimePickerVisible ? (
              <DateTimePickerModal
                isVisible={this.state.isStartTimePickerVisible}
                mode="time"
                headerTextIOS="Pick a time"
                onConfirm={this.handleStartTimeConfirm}
                onCancel={this.hideStartTimePicker}
              />
            ) : null}

            {this.state.isEndTimePickerVisible ? (
              <DateTimePickerModal
                isVisible={this.state.isEndTimePickerVisible}
                mode="time"
                headerTextIOS="Pick a time"
                onConfirm={this.handleEndTimeConfirm}
                onCancel={this.hideEndTimePicker}
              />
            ) : null}

            {/* ---------------------------------- Timer ------------------------------- */}

            <AwesomeAlertComponent
              title="Message"
              visibleAlert={this.state.AlertComponentVisible}
              closeAlert={this.closeAlertFunction}
              message={
                this.state.AlertComponentMessage
              }></AwesomeAlertComponent>

            <ProfileHeader
              backShow={this.state.showBack}
              Back={this.goBack}></ProfileHeader>

            <View style={styles.dpFlex}>
              <View
                onStartShouldSetResponder={this.handlePicker.bind(this)}
                style={{flex: 1, flexDirection: 'row'}}>
                <View style={styles.dpBox}>
                  <Image style={styles.dp} source={{uri: this.state.avatar}} />
                </View>
                <Text style={styles.Name}>{this.state.full_name}</Text>
              </View>
              <View
                onStartShouldSetResponder={() =>
                  this.setState({editable: true, showBack: true})
                }
                style={styles.editBox}>
                <Icon
                  name="edit"
                  size={25}
                  style={{color: '#33909F', padding: 8}}
                />
              </View>
            </View>
            <View style={styles.headTitles}>
              <View style={{flex: 1, flexDirection: 'row'}}>
                <View style={styles.iconBox}>
                  <Icon
                    name="bell"
                    size={15}
                    style={{color: '#33909F', padding: 8}}
                  />
                </View>
                <Text style={styles.headTeext}>Push Notifications</Text>
              </View>
              <Switch
                style={styles.switchPush}
                onValueChange={this.onSwitch.bind(this)}
                value={this.state.switch}
              />
            </View>
            <View
              onStartShouldSetResponder={() =>
                Linking.openURL('mailto:contact@swiftclinic.com?subject=&body=')
              }
              style={styles.headTitles}>
              <View style={[styles.iconBox, {paddingLeft: 6}]}>
                <FontAwesomeIcon
                  icon={faEnvelope}
                  size={14}
                  style={{color: '#33909F', padding: 10}}
                />
              </View>
              <Text style={styles.headTeext}>Contact Us</Text>
            </View>
            <TouchableOpacity
              onPress={this.addPermission.bind(this)}
              style={styles.headTitles}>
              <View style={styles.iconBox}>
                <Icon
                  name="microphone"
                  size={17}
                  style={{color: '#33909F', padding: 10}}
                />
              </View>
              <Text style={styles.headTeext}>Test Microphone/Camera</Text>
            </TouchableOpacity>
            <TouchableOpacity
              // onPress={() => this.setState({TimeVisible: true})}
              onPress={this.showCalender}
              style={styles.headTitles}>
              <View style={[styles.iconBox, {paddingLeft: 6}]}>
                <FontAwesomeIcon
                  icon={faCalendar}
                  size={17}
                  style={{color: '#33909F', padding: 10}}
                />
              </View>
              <Text style={styles.headTeext}>Available times</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate('ChangePasswordDoctor')
              }
              style={styles.headTitles}>
              <View style={styles.iconBox}>
                <Icon
                  name="unlock-alt"
                  size={17}
                  style={{color: '#33909F', padding: 10}}
                />
              </View>
              <Text style={styles.headTeext}>Change Password</Text>
            </TouchableOpacity>
            <View style={styles.items}>
              <Text style={styles.label}> Public Information: </Text>
              <TextInput
                value={this.state.first_name}
                onChangeText={(first_name) => this.setState({first_name})}
                placeholder={'First Name:'}
                editable={this.state.editable}
                style={styles.input}
              />
              <TextInput
                value={this.state.last_name}
                onChangeText={(last_name) => this.setState({last_name})}
                placeholder={'Last Name:'}
                editable={this.state.editable}
                style={styles.input}
              />
              <TextInput
                value={this.state.about}
                onChangeText={(about) => this.setState({about})}
                placeholder={'Bio:'}
                editable={this.state.editable}
                numberOfLines={4}
                multiline={true}
                style={styles.TextArea}
              />

              <Text style={styles.label}> Information is not displayed: </Text>
              <TextInput
                value={this.state.email}
                onChangeText={(email) => this.setState({email})}
                placeholder={'Email'}
                editable={this.state.editable}
                style={styles.input}
              />
              <TextInput
                value={this.state.phone_number}
                keyboardType={'numeric'}
                maxLength={11}
                onChangeText={(phone_number) =>
                  this.setState({
                    phone_number: phone_number.replace(/[^0-9]/g, ''),
                  })
                }
                placeholder={'Phone Number'}
                editable={this.state.editable}
                style={styles.input}
              />
              {/* <TextInput
              value={this.state.password}
              onChangeText={(password) => this.setState({password})}
              placeholder={'Password'}
              editable={false}
              style={styles.input}
            /> */}

              {this.state.editable ? (
                <DropDownPicker
                  editable={false}
                  containerStyle={{marginLeft: 20, marginRight: 20}}
                  items={this.state.items}
                  defaultValue={this.state.security_question_1}
                  style={styles.inputDropDown}
                  dropDownStyle={{backgroundColor: '#fafafa'}}
                  onChangeItem={(item) =>
                    this.setState({
                      security_question_1: item.value,
                    })
                  }
                />
              ) : (
                <TextInput
                  value={this.state.security_question_1_full}
                  onChangeText={(security_question_1_full) =>
                    this.setState({security_question_1_full})
                  }
                  placeholder={'Security Question 1'}
                  editable={this.state.editable}
                  style={styles.input}
                />
              )}

              <TextInput
                value={this.state.security_answer_1}
                onChangeText={(security_answer_1) =>
                  this.setState({security_answer_1})
                }
                placeholder={'Security Answer 1'}
                editable={this.state.editable}
                style={styles.input}
              />

              {this.state.editable ? (
                <DropDownPicker
                  items={this.state.items}
                  containerStyle={{marginLeft: 20, marginRight: 20}}
                  defaultValue={this.state.security_question_2}
                  style={styles.inputDropDown}
                  dropDownStyle={{backgroundColor: '#fafafa'}}
                  onChangeItem={(item) =>
                    this.setState({
                      security_question_2: item.value,
                    })
                  }
                />
              ) : (
                <TextInput
                  value={this.state.security_question_2_full}
                  onChangeText={(security_question_2_full) =>
                    this.setState({security_question_2_full})
                  }
                  placeholder={'Security Question 2'}
                  editable={this.state.editable}
                  style={styles.input}
                />
              )}

              <TextInput
                value={this.state.security_answer_2}
                onChangeText={(security_answer_2) =>
                  this.setState({security_answer_2})
                }
                placeholder={'Security Answer 2'}
                editable={this.state.editable}
                style={styles.input}
              />
              {this.state.editable ? (
                <LinearGradient
                  colors={['#78C2CB', '#33909F']}
                  style={styles.button}>
                  <TouchableOpacity
                    onPress={this.updateProfile.bind(this)}
                    style={{
                      width: '100%',
                      height: '100%',
                      justifyContent: 'center',
                    }}>
                    <Text style={{color: 'white', textAlign: 'center'}}>
                      Confirm
                    </Text>
                  </TouchableOpacity>
                </LinearGradient>
              ) : null}
              <AwesomeAlert
                show={this.state.showAlert}
                showProgress={false}
                title="Token Expired"
                message="Please Login again!"
                closeOnTouchOutside={true}
                closeOnHardwareBackPress={false}
                // showCancelButton={true}
                showConfirmButton={true}
                // cancelText="No, cancel"
                confirmText="OK"
                confirmButtonColor="#0158ff"
                onCancelPressed={() => {
                  this.hideAlert();
                }}
                onConfirmPressed={() => {
                  this.hideAlert();
                }}
              />
              <AwesomeAlert
                show={this.state.showAlertService}
                showProgress={false}
                title="Swift Clinic Permission"
                message="Permission is not Granted"
                closeOnTouchOutside={true}
                closeOnHardwareBackPress={false}
                showCancelButton={true}
                showConfirmButton={true}
                cancelText="No, cancel"
                confirmText="Allow Permission"
                confirmButtonColor="#0158ff"
                onCancelPressed={() => {
                  this.setState({showAlertService: false});
                }}
                onConfirmPressed={this.grantPermission.bind(this)}
              />
              <AwesomeAlert
                show={this.state.showAlertService2}
                showProgress={false}
                title="Swift Clinic Permission"
                message="Permission already Granted"
                closeOnTouchOutside={true}
                closeOnHardwareBackPress={false}
                showCancelButton={false}
                showConfirmButton={true}
                cancelText="No,cancel"
                confirmText="Ok"
                confirmButtonColor="#0158ff"
                onConfirmPressed={() => {
                  this.setState({showAlertService2: false});
                }}
              />
              <TouchableOpacity
                onPress={this.logOut.bind(this)}
                style={styles.button2}>
                <Text style={{color: 'black', fontFamily: 'Axiforma-Book'}}>
                  Log Out
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </SafeAreaView>
      </KeyboardAwareScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  dpFlex: {
    justifyContent: 'space-between',
    marginTop: 20,
    flex: 1,
    flexDirection: 'row',
    height: 90,
  },
  inputDropDown: {
    marginTop: 10,
    paddingLeft: 10,
    borderRadius: 8,
    height: 49,
    alignSelf: 'stretch',
    borderWidth: 1,
    borderColor: '#cae3ff',
    marginBottom: 20,
  },
  dp: {
    alignSelf: 'center',
    borderRadius: 37,
    width: 72,
    height: 72,
  },

  dpBox: {
    justifyContent: 'center',
    borderWidth: 2,
    borderColor: '#33909F',
    marginLeft: 20,
    borderRadius: 38,
    width: 76,
    height: 76,
  },
  Name: {
    marginLeft: 20,
    alignSelf: 'center',
    fontSize: 22,
    fontFamily: 'Axiforma-Book',
    maxWidth: 120,
    textAlignVertical: 'center',
  },
  headTitles: {
    marginTop: 10,
    flexDirection: 'row',
    height: 45,
  },
  iconBox: {
    marginTop: 6,
    marginLeft: 20,
    borderRadius: 7,
    borderColor: '#dcecef',
    borderWidth: 1,
    height: 34,
    width: 34,
    justifyContent: 'center',
  },
  editBox: {
    marginRight: 20,
    alignSelf: 'center',
    borderRadius: 7,
    borderColor: '#ffffff',
    borderWidth: 2,
    height: 43,
    width: 49,
  },
  headTeext: {
    fontSize: 15,
    marginLeft: 30,
    alignSelf: 'center',
    textAlignVertical: 'center',
    fontFamily: 'Axiforma-Medium',
  },
  input: {
    color: '#646464',
    marginTop: 5,
    paddingLeft: 10,
    borderRadius: 8,
    height: 50,
    alignSelf: 'stretch',
    marginLeft: 20,
    marginRight: 20,
    borderWidth: 1,
    borderColor: '#dcecef',
    marginBottom: 10,
    fontFamily: 'Axiforma-Thin',
  },
  button: {
    borderRadius: 9,
    marginLeft: 80,
    marginBottom: 20,
    height: 70,
    justifyContent: 'center',
    marginRight: 80,
    marginTop: 15,
    backgroundColor: '#0158ff',
    alignItems: 'center',
    alignSelf: 'stretch',
  },
  button2: {
    borderRadius: 9,
    borderWidth: 2,
    marginLeft: 80,
    marginBottom: 40,
    borderColor: '#33909F',
    height: 70,
    justifyContent: 'center',
    marginRight: 80,
    marginTop: 15,
    backgroundColor: '#ffffff',
    alignItems: 'center',
    alignSelf: 'stretch',
  },
  items: {
    marginTop: 28,
    textAlign: 'left',
  },
  label: {
    marginTop: 20,
    marginLeft: 20,
    fontSize: 16,
    fontWeight: '500',
  },
  spinnerTextStyle: {
    color: '#FFF',
  },
  switchPush: {
    marginRight: 20,
    transform: [{scaleX: 0.8}, {scaleY: 0.75}],
  },
  TextArea: {
    borderWidth: 1,
    paddingLeft: 10,
    marginLeft: 20,
    marginRight: 20,
    borderColor: '#cae3ff',
    borderRadius: 10,
    height: 80,
    color: '#646464',
    fontFamily: 'Axiforma-Thin',
  },
});
