import React, {Component} from 'react';
import {View, Text, FlatList,TouchableNativeFeedback, ScrollView} from 'react-native';
import TitleImage from '../../components/Headers/titleImage';
import NotificationBox from '../../components/consultations/NotificationBox';
import {GetPatientNotifications, seenNotification,confirmRescheduleDoctor,confirmReschedulePatient} from '../../api/ConsultationApi';
import SpinnerComponent from '../../components/Spinner/Spinner';
import Toast from 'react-native-simple-toast';
import { SafeAreaView } from 'react-navigation';
import SvgGraph from '../../components/backgroundgraph';
import NoData from '../../components/NoData';
import AcceptRejectModal from '../../components/Modals/AcceptRejectModal';
import Moment from 'moment';


export default class DoctorNotification extends Component {
    state = {
        data: [],
        spinnerLoading: false,
        noData : false,
        timeModalVisibility: false,
        selectedItem : null,
        text: ''
      };
    
      componentDidMount = async () => {
          await this.getAllNotification();
          this.willFocusSubscription = this.props.navigation.addListener(
            'willFocus',
           async () => {
            //  console.log('Console Data ----------->',Math.floor(Math.random() * 100) + 1 );
             await this.getAllNotification();
            }
          );
         }
      
         componentWillUnmount() {
          this.willFocusSubscription.remove();
        }
      getAllNotification = async () => {
        await this.setState({ spinnerLoading: true});
        GetPatientNotifications()
        .then(async (res) => {
          console.log('Response =====------->', res.data.data[0]);
          if(res.data.data.length > 0)
          {
            this.setState({noData: false})
          }
          else{
            this.setState({noData: true})
          }
          await this.setState({data: res.data.data, spinnerLoading: false});
        })
        .catch((err) => console.log('Error -------->', err));
      }



    
      formatDateTime(datetime){
        return Moment.utc(datetime).local().format('LL')
     }

     closeTimeModal = async () => {
       await this.setState({timeModalVisibility: false});
     }

     onClick = async (item) => {
        await console.log('Status Notification Page ------>', item);
        const finalData = item.data;
        const data =  await JSON.parse(JSON.stringify(finalData));
        const d = JSON.parse(data);
        const LocalTime = await d.consultation_id;
        const FormattedTime = d.requested_time;
        console.log('Time is there ------->',FormattedTime)
        if(FormattedTime === undefined)
        {
          console.log('Undefined it is ------->')
        }
          if(item.read === 1)
          {
            this.setState({spinnerLoading: false})
            //  --------------------------- Navigate To ------------
            if(item.consultation_status === "accepted_by_doctor"){
              this.props.navigation.navigate('ActiveConsultations',{id : LocalTime})
      } else if(item.consultation_status === "pending_by_doctor"){
        this.props.navigation.navigate('ConsultationStatus',{id : LocalTime})
      }
      else if(item.consultation_status === "rejected"){
       this.props.navigation.navigate('ConsultationStatus',{id : LocalTime})
       Toast.show('This appointment has been rejected.',Toast.SHORT);
      }
      else if(item.consultation_status === "request_new_time"){
        if(FormattedTime === undefined)
        {
          this.props.navigation.navigate('ConsultationStatus',{id : LocalTime})
          console.log('Undefined it is ------->')
        }else{
        await this.setState({timeModalVisibility: true, selectedItem: item, text: item.text +  " " + this.formatDateTime(FormattedTime)})
        }
       }else{
       Toast.show('This appointment is finished.',Toast.SHORT);
      }
          // -------------------------------End Navigate -------------------------
          }else{
            await this.setState({ spinnerLoading: true});
            this.setSeenNotification(item);
          }
      
      }

      setSeenNotification = async (item) => {
         console.log('Item ------>', item)
         const finalData = item.data;
         const data =  await JSON.parse(JSON.stringify(finalData));
         const d = JSON.parse(data);
         const LocalTime = await d.consultation_id;
         const FormattedTime = d.requested_time;
         console.log('Time is there 2 ------->',FormattedTime)
        seenNotification(item.id)
        .then(async (res) => {
          this.setState({spinnerLoading: false})
          console.log('Set Status Seen--------->')

             //  --------------------------- Navigate To ------------
             if(item.consultation_status === "accepted_by_doctor"){
              this.props.navigation.navigate('ActiveConsultations',{id : LocalTime})
      } else if(item.consultation_status === "pending_by_doctor"){
        this.props.navigation.navigate('ConsultationStatus',{id : LocalTime})
      }
      else if(item.consultation_status === "rejected"){
       this.props.navigation.navigate('ConsultationStatus',{id : LocalTime})
       Toast.show('This appointment has been rejected.',Toast.SHORT);
      }
      else if(item.consultation_status === "request_new_time"){
        if(FormattedTime === undefined)
        {
          this.props.navigation.navigate('ConsultationStatus',{id : LocalTime})
          console.log('Undefined it is ------->')
        }else{
        await this.setState({timeModalVisibility: true, selectedItem: item, text: item.text +  " " + this.formatDateTime(FormattedTime)})
        }
       }else{
       Toast.show('This appointment is finished.',Toast.SHORT);
      }
           // -------------------------------End Navigate -------------------------
          
        })
        .catch((err) => console.log('Error -------->', err));
      }

      onAcceptRequest = async () => {
        let status = 1;
        await this.setRescheduleStatus(status);
      }

      onRejectRequest = async () => {
        let status = 0;
        await this.setRescheduleStatus(status);
      }

      setRescheduleStatus = async (status) => {
        const {selectedItem} = await this.state;
        this.setState({spinnerLoading: true})
        console.log('Item ------>', selectedItem)
        const finalData = await selectedItem.data;
        const data =  await JSON.parse(JSON.stringify(finalData));
        const d = await JSON.parse(data);
        const LocalTime = await d.consultation_id;
        confirmRescheduleDoctor(LocalTime,status)
        .then(async (res) => {
          await console.log('Response --------->',res)
          await this.setState({timeModalVisibility: false})
          this.getAllNotification();
        //  await this.setSeenNotification(selectedItem);
        })
        .catch((err) => console.log('Error -------->', err));
      }
    
      render() {
        return (
          <View style={{backgroundColor: '#fff', flex: 1}}>
          <SvgGraph style={{right:-38}} />
          <View style={{marginTop: -240}}>
            <ScrollView style={{  opacity: this.state.timeModalVisibility ? 0.2 : 1}}>
              <AcceptRejectModal timeModal={this.state.timeModalVisibility} text={this.state.text} onAccept={this.onAcceptRequest} onReject={this.onRejectRequest} closeModal={this.closeTimeModal}></AcceptRejectModal>
          <SafeAreaView style={{ flex: 1}}>
            <TitleImage></TitleImage>
            <Text
              style={{
                textAlign: 'center', fontFamily: 'Axiforma-Medium', fontSize: 19, marginTop: 30
              }}>
              Notifications
            </Text>
            <View style={{ padding: 20}}>
    
              {this.state.noData ?
             ( <NoData></NoData> )
              : (
                <FlatList
                scrollEnabled={false}
                data={this.state.data}
                horizontal={false}
                numColumns={1}
                keyExtractor={(item) => item.id.toString()}
                renderItem={({item}) => (
                  <TouchableNativeFeedback
                    onPress={ () => this.onClick(item)}>
                    <NotificationBox
                      iCon= "#33909F"
                      title={item.title}
                      text={item.text}
                      colour={item.read}></NotificationBox>
                  </TouchableNativeFeedback>
                )}
              />
              )
               }
              <SpinnerComponent
                Visible={this.state.spinnerLoading}></SpinnerComponent>
            </View>
    
          </SafeAreaView>
          </ScrollView>
          </View>
          </View>
        );
      }
}
