import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  ScrollView,
  TouchableOpacity,
  Alert,
  RefreshControl,
  Modal,
  ToastAndroid,
  PermissionsAndroid,
  Platform,
} from 'react-native';
import MainHeader from '../../components/Headers/MainHeader';
import DatePicker from '../../components/Date/DatePicker';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import styles from '../../css/css';
import {
  GetConsultationsDetails,
  DoctorRescheduling,
  AcceptOrRejectApi,
  startCall,
  setStatusOfConsultation,
  uploadReport,
  addMriRequest,
} from '../../api/ConsultationApi';
import LinearGradient from 'react-native-linear-gradient';
import {SafeAreaView} from 'react-navigation';
import AwesomeAlertComponent from '../../components/Alerts/AwesomeAlertFile';
import SpinnerComponent from '../../components/Spinner/Spinner';
import DocumentPicker from 'react-native-document-picker';
import Button from '../../components/Buttons/button';
import SetAvailableTimmings from '../../components/Modals/SetAvailableTimmings';
import Icon from 'react-native-vector-icons/FontAwesome5';
import MultiSelect from 'react-native-multiple-select';
import Toast from 'react-native-simple-toast';
import CalenderModal from '../../components/Date/CalenderModal';
import MriRequestModal from '../../components/Modals/MriRequestModal';
import SvgGraph from '../../components/backgroundgraph';
import Moment from 'moment';
import {TextInput} from 'react-native-paper';
import RNFetchBlob from 'rn-fetch-blob';

class ActiveConsultations extends Component {
  state = {
    consultation_id: null,
    doctorName: '',
    showDate: 'Request new date time',
    AlertComponentVisible: false,
    AlertComponentMessage: '',
    upDown: false,
    spinnerLoading: true,
    doctor_pdf: null,
    symptomName: '',
    completed: 0,
    consulation_id: null,
    time: '',
    date: '',
    slotTime: '',
    startCallStatus: false,
    completeCallStatus: false,
    slotTime2: null,
    startCallStatusTwo: false,
    completeCallStatusTwo: false,
    singleFile: null,
    refreshing: false,
    specificTimeData: [],
    TimeVisible: false,
    customCalenderArray: {},
    CalenderModalVisibility: false,
    MriRequestModal: false,
    MriDescription: '',
    selectedItems: [],
    questionaire_link: null,
    test_required: null,
    specificTimeData: [],
    MriLinks: [],
    initial_call: 0,
    followup_call: 0,
    profile_pic: null,
    callStatus: '',
    items: [
      {
        label: 'Select security question',
        value: '3',
      },
      {
        label: 'Select security question',
        value: '4',
      },
    ],
  };

  _onRefresh = () => {
    console.log('Refreshing');
    this.setState({refreshing: true});
    this.getBookingDetails();
  };

  componentDidMount = async () => {
    const id = await this.props.navigation.getParam('id', 'nothing sent');
    console.log('ID -------->', id);
    this.setState({consulation_id: id});
    this.getBookingDetails();
  };

  // -------------------- file upload --------------------

  uploadImage = async () => {
    this.setState({spinnerLoading: true});
    const {singleFile, consulation_id} = this.state;
    if (singleFile != null) {
      uploadReport(consulation_id, singleFile)
        .then(async (res) => {
          console.log('Response =====------->', res.data);
          this.setState({
            spinnerLoading: false,
            AlertComponentMessage: 'Report Successfully Submitted',
            AlertComponentVisible: true,
          });
        })
        .catch((err) => console.log('Error -------->', err));
    } else {
      // If no file selected the show alert
      Alert.alert('Please Select File first');
    }
  };

  selectFile = async () => {
    try {
      const res = await DocumentPicker.pick({
        // Provide which type of file you want user to pick
        type: [DocumentPicker.types.allFiles],
        // There can me more options as well
        // DocumentPicker.types.allFiles
        // DocumentPicker.types.images
        // DocumentPicker.types.plainText
        // DocumentPicker.types.audio
        // DocumentPicker.types.pdf
      });
      // Printing the log realted to the file
      console.log('res : ' + JSON.stringify(res));
      // Setting the state to show single file attributes
      await this.setState({singleFile: res});
      await this.uploadImage();
    } catch (err) {
      await this.setState({singleFile: null});
      // Handling any exception (If any)
      if (DocumentPicker.isCancel(err)) {
        // If user canceled the document selection
        // Alert.alert('Canceled');
      } else {
        // For Unknown Error
        // Alert.alert('Unknown Error: ' + JSON.stringify(err));
        throw err;
      }
    }
  };

  actualDownload = async () => {
    const d_link = this.state.questionaire_link;
    if (d_link === null) {
      Platform.OS === 'android'
        ? ToastAndroid.show(
            'Health questionaire file is not present.',
            ToastAndroid.SHORT,
          )
        : Alert.alert('Message', 'Health questionaire file is not present.');
    } else {
      this.setState({spinnerLoading: true});
      const {dirs} = RNFetchBlob.fs;
      const dirToSave =
        Platform.OS == 'ios' ? dirs.DocumentDir : dirs.DownloadDir;
      const configfb = {
        fileCache: true,
        useDownloadManager: true,
        notification: true,
        mediaScannable: true,
        title: 'healthquestionaire-pdf.pdf',
        path: `${dirToSave}/healthquestionaire-pdf.pdf`,
      };
      const configOptions = Platform.select({
        ios: {
          fileCache: configfb.fileCache,
          title: configfb.title,
          path: configfb.path,
          appendExt: 'pdf',
        },
        android: configfb,
      });
      console.log('The file saved to 23233', configfb, dirs);

      RNFetchBlob.config(configOptions)
        .fetch(
          'GET',
          'https://platform.swiftclinic.com/questionaire-pdf/' + d_link,
          {},
        )
        .then((res) => {
          if (Platform.OS === 'ios') {
            // RNFetchBlob.fs.writeFile(configfb.path, res.data, 'base64');
            let pathh = res.path();
            setTimeout(() => {
              console.log('file savng : ' + pathh);
              RNFetchBlob.ios.previewDocument(pathh);
            }, 2000);
            console.log('showing up doc');
          }
          RNFetchBlob.fs
            .exists(res.path())
            .then((exist) => {
              console.log(`file ${exist ? '' : 'not'} exists`);
            })
            .catch((e) => {
              console.log('err :', e.message);
            });

          this.setState({spinnerLoading: false});

          console.log('The file saved to ', res.path());
          Toast.show('File Path : ' + res.path(), Toast.SHORT);
        })
        .catch((e) => {
          console.log('The file saved to ERROR', e.message);
        });
    }
  };

  downloadFile = async () => {
    console.log('Download file --------->');
    if (Platform.OS === 'ios') {
      console.log('iphone detected-----');
      try {
        this.actualDownload();
      } catch (err) {
        console.warn(err);
      }
    } else {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          this.actualDownload();
        } else {
          Alert.alert(
            'Permission Denied!',
            'You need to give storage permission to download the file',
          );
        }
      } catch (err) {
        console.warn(err);
      }
    }
  };

  //   ----------------- get Details -----------------

  completeStatus = async (text) => {
    await this.setState({spinnerLoading: true});
    const {consulation_id} = this.state;
    // console.log('Status Text ------>',consulation_id)
    setStatusOfConsultation(consulation_id, text)
      .then(async (res) => {
        console.log('Response =====------->', res.data);
        this.setState({
          spinnerLoading: false,
          AlertComponentMessage: 'Successfully completed consultation',
          AlertComponentVisible: true,
        });
        this.getBookingDetails();
      })
      .catch((err) => console.log('Error -------->', err));
  };

  getBookingDetails = async () => {
    const {consulation_id} = this.state;
    GetConsultationsDetails(consulation_id)
      .then(async (res) => {
        console.log(
          'Data in Consultation screen ------------>',
          JSON.stringify(res.data.data.questionnaire),
        );
        let consultationLetter = res.data.data.reports;
        var d_pdf = null;
        await consultationLetter.forEach(function (d) {
          if (d.mri === 2) {
            d_pdf = d.file_link;
          }
        });
        await this.setState({
          doctor_pdf: d_pdf,
          spinnerLoading: false,
          time: res.data.data.time,
          date: res.data.data.date,
          completed: res.data.data.completed,
          doctorName:
            res.data.data.patient.first_name +
            '  ' +
            res.data.data.patient.last_name,
          symptomName: res.data.data.symptom.symptom,
          slotTime: res.data.data.slottime,
          slotTime2: res.data.data.slottime2,
          refreshing: false,
          callStatus: res.data.data.current_status,
          test_required: res.data.data.test_required,
          questionaire_link:
            res.data.data.questionnaire === null
              ? null
              : res.data.data.questionnaire.link,
          profile_pic: res.data.data.patient.profile_pic,
          initial_call: res.data.data.initial_call || 0,
          followup_call: res.data.data.followup_call || 0,
          MriLinks:
            res.data.data.all_reports.mri_report === null
              ? []
              : res.data.data.all_reports.mri_report,
        });

        await this.addMinutes(this.state.slotTime);
        await this.addMinutesForSlotTimeTwo(this.state.slotTime2);
      })
      .catch((err) => console.log('Error -------->', err));
  };

  //   ---------------------- Back to Funcation ----------------

  addMinutesForSlotTimeTwo = async (datetime) => {
    // console.log('In add minutes Two');
    if (datetime !== null) {
      let afterTime = await Moment.utc(datetime)
        .local()
        .add(30, 'minutes')
        .format('LLL');
      let slotCurrentTime = await Moment.utc(datetime).local().format('LLL');
      let todayCurrentTime = await Moment.utc().local().format('LLL');
      if (Moment(todayCurrentTime).isAfter(slotCurrentTime) === true) {
        console.log('We can show Start Call 2');
        await this.setState({startCallStatusTwo: true});
      }

      if (Moment(todayCurrentTime).isAfter(slotCurrentTime) === true) {
        console.log('We can show complete Call 2');
        await this.setState({completeCallStatusTwo: true});
      }
    }
  };

  addMinutes = async (datetime) => {
    console.log('In add minutes');
    let afterTime = await Moment.utc(datetime)
      .local()
      .add(30, 'minutes')
      .format('LLL');
    let slotCurrentTime = await Moment.utc(datetime).local().format('LLL');
    let todayCurrentTime = await Moment.utc().local().format('LLL');
    if (Moment(todayCurrentTime).isAfter(slotCurrentTime) === true) {
      await this.setState({startCallStatus: true});
      //  console.log('We can show Start Call 1');
    }
    if (Moment(todayCurrentTime).isAfter(slotCurrentTime) === true) {
      await this.setState({completeCallStatus: true});
      // console.log('We can show complete Call 1');
    }
  };

  backToScreen = async () => {
    this.props.navigation.goBack(null);
  };

  //   --------------------- Close Alert Function ---------------

  closeAlertFunction = async () => {
    await this.setState({AlertComponentVisible: false});
    // this.props.navigation.navigate('DoctorHome');
  };

  // -------------------- Date Function ---------------
  dateCallback = async (selectedDate) => {
    var gmtDateTime = Moment.utc(selectedDate, 'YYYY-MM-DD');
    var locale_date = gmtDateTime.local().format('YYYY-MM-DD');
    var current_time = Moment()
      .local()
      .subtract(1, 'days')
      .format('YYYY-MM-DD');
    console.log('After ------>', Moment(locale_date).isAfter(current_time));
    if (Moment(locale_date).isAfter(current_time)) {
      const {TimeData} = this.state;
      let TTimeData = TimeData;
      TTimeData = TTimeData.filter(function (item) {
        return item.date == locale_date;
      }).map(function ({date, start_time, end_time}) {
        return {
          date,
          start_time: Moment.utc('2017-03-13' + ' ' + start_time)
            .local()
            .format('LT'),
          end_time: Moment.utc('2017-03-13' + ' ' + end_time)
            .local()
            .format('LT'),
        };
      });
      console.log('Filter Data -------->', TTimeData);
      this.setState({
        CalenderModalVisibility: false,
        showDate: locale_date,
        specificTimeData: TTimeData,
        TimeVisible: true,
      });
    } else {
      Alert.alert('Please select an appointment time from tomorrow  onwards.');
    }
    console.log('Selected Date Two -------->', selectedDate);
  };

  onSubmit = async () => {
    await this.setState({spinnerLoading: true});
    const {consulation_id, showDate} = this.state;
    DoctorRescheduling(consulation_id, showDate)
      .then(async (res) => {
        console.log('Reschedule Booking =====------->', res.data);
        this.setState({
          spinnerLoading: false,
          AlertComponentMessage: 'Successfully Submitted',
          AlertComponentVisible: true,
        });
      })
      .catch((err) => console.log('Error -------->', err));
  };

  //   ---------------------- Accept Function -----------

  onAccept = async () => {
    const {consulation_id} = this.state;
    const response = 1;
    AcceptOrRejectApi(consulation_id, response)
      .then(async (res) => {
        console.log('Response =====------->', res.data);
        this.setState({
          spinnerLoading: false,
          AlertComponentVisible: true,
          AlertComponentMessage: 'Successfully Accepted',
        });
      })
      .catch((err) => console.log('Error -------->', err));
  };

  // -------------------------- Start Call ---------------------------

  startCallButton = async () => {
    await this.setState({spinnerLoading: true});
    const {consulation_id} = this.state;
    console.log('Logs ----->', consulation_id);
    startCall(consulation_id)
      .then(async (res) => {
        await console.log(
          'Call Channel Id=====------->',
          res.data.data.channel_id,
        );
        this.setState({
          spinnerLoading: false,
        });

        // this.props.navigation.navigate('CallScreenJanusD', {channel_id:  1234567, consultation_id:consulation_id });
        this.props.navigation.navigate('JanusVideoRoomScreen', {
          channel_id: res.data.data.channel_id,
          consultation_id: consulation_id,
        });
      })
      .catch((err) => console.log('Error -------->', err));
  };

  // --------------- Reject Function --------

  onReject = async () => {
    const {consulation_id} = this.state;
    const response = 0;
    AcceptOrRejectApi(consulation_id, response)
      .then(async (res) => {
        console.log('Response =====------->', res.data);
        this.setState({
          spinnerLoading: false,
          AlertComponentVisible: true,
          AlertComponentMessage: 'Successfully Rejected',
        });
      })
      .catch((err) => console.log('Error -------->', err));
  };

  formatDateTime(datetime) {
    return Moment(datetime, 'YYYY-MM-DD h:mm').format('h:mm A');
    // return Moment.utc(datetime).local().format('LT');
  }

  formatDateTimeForDate(datetime) {
    return Moment(datetime, 'YYYY-MM-DD h:mm').format('DD.MM.YYYY');
    // return Moment.utc(datetime).local().format('DD.MM.YYYY');
  }

  startTimeCallback = async (StartTime) => {
    let TimeForDay = await this.state.specificTimeData;
    const {showDate} = this.state;
    console.log('Start Time Core ------>', StartTime.getTime());
    let d4 = Moment(StartTime.getTime());
    let finalStartTime = d4.format('LT');
    let startTimeFinal = d4.format('HH:mm:ss');

    var momentObj = await Moment(showDate + startTimeFinal, 'YYYY-MM-DDLT');
    // var dateTime = await momentObj.format('YYYY-MM-DD HH:MM:SS');
    console.log('TTTTTTTTT -------->', momentObj);

    const u = await Moment.utc(showDate + ' ' + startTimeFinal);
    const su = await Moment.utc(showDate + ' ' + startTimeFinal).format('LLL');
    console.log('Final =-------------', u);
    await this.setState({
      selectTime: finalStartTime,
      local_date_show: momentObj,
      showDate: su,
    });
  };

  addTime = async () => {
    const {selectTime} = this.state;
    if (selectTime === 'Select Time') {
      Alert.alert('Please select time first.');
    } else {
      await this.setState({TimeVisible: false});
    }
  };

  closeTimeModal = async () => {
    await this.setState({TimeVisible: false});
  };

  closeCalenderModal = async () => {
    console.log('Closseeee');
    this.setState({CalenderModalVisibility: false, MriRequestModal: false});
  };

  addMriRequest = async () => {
    var description = await this.state.MriDescription;
    const items = await this.state.selectedItems;
    console.log(
      'Length----->',
      items.length,
      '----->Description',
      this.state.MriDescription,
    );
    description = description.trim();
    if (items.length < 1 || description == null || description === '') {
      Alert.alert('Please fill all fields.');
      console.log('We ar3e out ');
    } else {
      console.log('We ar3e in ');
      await this.setState({spinnerLoading: true});
      const {consulation_id} = this.state;
      addMriRequest(consulation_id, description, items)
        .then(async (res) => {
          // console.log('Time=====------->', res.data);
          await this.setState({
            spinnerLoading: false,
            test_required: 1,
            MriRequestModal: false,
          });
          Platform.OS === 'android'
            ? Toast.show('MRI uploaded successfully.', Toast.SHORT)
            : Alert.alert('MRI uploaded successfully.');
        })
        .catch((err) => console.log('Error -------->', err));
    }
  };

  onSelectedItemsChange = (selectedItems) => {
    console.log('Selected Items -------->', selectedItems);
    this.setState({selectedItems: selectedItems});
  };

  onClickMriRequest = async () => {
    const status = this.state.test_required;
    await console.log('On Click Mri Request ----------->');
    if (status && status < 0) {
      await this.setState({MriRequestModal: true});
    } else {
      Platform.OS === 'android'
        ? Toast.show('MRI already uploaded successfully.', Toast.SHORT)
        : Alert.alert('MRI already uploaded successfully.');
    }
  };

  MRIactualDownload = async (d_link) => {
    if (d_link === null) {
      Platform.OS === 'android'
        ? Toast.show('MRI has not uploaded it yet.', Toast.SHORT)
        : Alert.alert('MRI has not uploaded it yet.');
    } else {
      this.setState({spinnerLoading: true});
      const {dirs} = RNFetchBlob.fs;
      const dirToSave =
        Platform.OS == 'ios' ? dirs.DocumentDir : dirs.DownloadDir;
      const configfb = {
        fileCache: true,
        useDownloadManager: true,
        notification: true,
        mediaScannable: true,
        title: 'MRI-Report.pdf',
        path: `${dirToSave}/MRI-Report.pdf`,
      };
      const configOptions = Platform.select({
        ios: {
          fileCache: configfb.fileCache,
          title: configfb.title,
          path: configfb.path,
          appendExt: 'pdf',
        },
        android: configfb,
      });
      console.log('The file saved to 23233', configfb, dirs);

      RNFetchBlob.config(configOptions)
        .fetch('GET', d_link, {})
        .then((res) => {
          if (Platform.OS === 'ios') {
            // RNFetchBlob.fs.writeFile(configfb.path, res.data, 'base64');
            let pathh = res.path();
            setTimeout(() => {
              console.log('file savng : ' + pathh);
              RNFetchBlob.ios.previewDocument(pathh);
            }, 2000);
            console.log('showing up doc');
          }
          RNFetchBlob.fs
            .exists(res.path())
            .then((exist) => {
              console.log(`file ${exist ? '' : 'not'} exists`);
            })
            .catch((e) => {
              console.log('err :', e.message);
            });

          this.setState({spinnerLoading: false});

          console.log('The file saved to ', res.path());
          Toast.show('File Path : ' + res.path(), Toast.SHORT);
        })
        .catch((e) => {
          console.log('The file saved to ERROR', e.message);
        });
    }
  };

  MRIdownloadFile = async (link) => {
    console.log('Download file --------->');
    if (Platform.OS === 'ios') {
      console.log('iphone detected-----');
      try {
        this.MRIactualDownload(link);
      } catch (err) {
        console.warn(err);
      }
    } else {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          this.MRIactualDownload(link);
        } else {
          Alert.alert(
            'Permission Denied!',
            'You need to give storage permission to download the file',
          );
        }
      } catch (err) {
        console.warn(err);
      }
    }
  };

  mriScan = async (index) => {
    const {MriLinks} = this.state;
    const link = MriLinks[index].link;
    console.log('Datatatataat------------>', link);
    await this.MRIdownloadFile(link);
  };

  renderlist() {
    const {MriLinks} = this.state;
    let list = [];
    for (let i = 0; i < MriLinks.length; i++) {
      list.push(
        <View
          onStartShouldSetResponder={() => this.mriScan(i)}
          style={{flexDirection: 'row', marginTop: 17}}>
          <Icon
            style={{alignSelf: 'flex-end', marginRight: 17}}
            color="#33909F"
            size={24}
            name="file-pdf"
          />
          <Text
            style={{
              textDecorationLine: 'underline',
              fontFamily: 'Axiforma-Medium',
            }}>
            MRI Scan
          </Text>
        </View>,
      );
    }
    return list;
  }

  consultationActualDownload = async () => {
    var d_link = null;
    var file_name = 'pdf';

    console.log('Consultation Letter');
    d_link = this.state.doctor_pdf;
    file_name = 'consultationletter-pdf.pdf';

    if (d_link === null) {
      Platform.OS === 'android'
        ? Toast.show('File has not been uploaded yet.', Toast.SHORT)
        : Alert.alert('File has not been uploaded yet.');
    } else {
      this.setState({spinnerLoading: true});
      const {dirs} = RNFetchBlob.fs;
      const dirToSave =
        Platform.OS == 'ios' ? dirs.DocumentDir : dirs.DownloadDir;
      const configfb = {
        fileCache: true,
        useDownloadManager: true,
        notification: true,
        mediaScannable: true,
        title: file_name,
        path: `${dirToSave}/` + file_name,
      };
      const configOptions = Platform.select({
        ios: {
          fileCache: configfb.fileCache,
          title: configfb.title,
          path: configfb.path,
          appendExt: 'pdf',
        },
        android: configfb,
      });
      console.log('The file saved to 23233', configfb, dirs);

      RNFetchBlob.config(configOptions)
        .fetch('GET', d_link, {})
        .then((res) => {
          if (Platform.OS === 'ios') {
            // RNFetchBlob.fs.writeFile(configfb.path, res.data, 'base64');
            let pathh = res.path();
            setTimeout(() => {
              console.log('file savng : ' + pathh);
              RNFetchBlob.ios.previewDocument(pathh);
            }, 2000);
            console.log('showing up doc');
          }
          RNFetchBlob.fs
            .exists(res.path())
            .then((exist) => {
              console.log(`file ${exist ? '' : 'not'} exists`);
            })
            .catch((e) => {
              console.log('err :', e.message);
            });

          this.setState({spinnerLoading: false});

          console.log('The file saved to ', res.path());
          Toast.show('File Path : ' + res.path(), Toast.SHORT);
        })
        .catch((e) => {
          console.log('The file saved to ERROR', e.message);
        });
    }
  };

  consultationDownloadFile = async () => {
    console.log('Download file --------->');
    if (Platform.OS === 'ios') {
      console.log('iphone detected-----');
      try {
        this.consultationActualDownload();
      } catch (err) {
        console.warn(err);
      }
    } else {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          this.consultationActualDownload();
        } else {
          Alert.alert(
            'Permission Denied!',
            'You need to give storage permission to download the file',
          );
        }
      } catch (err) {
        console.warn(err);
      }
    }
  };

  // ------------------------- End Location --------------------------

  render() {
    return (
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: '#ffffff',
          opacity: this.state.MriRequestModal ? 0.2 : 1,
        }}>
        <KeyboardAwareScrollView>
          <SvgGraph style={{right: -38}} />
          <View style={{marginTop: -280}}>
            <ScrollView
              refreshControl={
                <RefreshControl
                  refreshing={this.state.refreshing}
                  onRefresh={this._onRefresh}
                />
              }>
              <CalenderModal
                disabled={true}
                customDateArray={this.state.customCalenderArray}
                closeModal={this.closeCalenderModal}
                Visibilty={this.state.CalenderModalVisibility}
                selectedDateCallback={this.dateCallback}></CalenderModal>

              {/* <MriRequestModal MriData={this.state.items}></MriRequestModal> */}

              {/* -------------------------------------- MRI ------------------------------------ */}

              <Modal
                animationType={'slide'}
                transparent={true}
                visible={this.state.MriRequestModal}
                onRequestClose={() => {
                  console.log('Modal has been closed.');
                }}>
                <View style={styles.modalTwo}>
                  <Icon
                    onPress={this.closeCalenderModal}
                    style={{alignSelf: 'flex-end', marginRight: 17}}
                    color="#33909F"
                    size={24}
                    name="times-circle"
                  />
                  <Text style={styles.mriText}>MRI Request</Text>
                  <View style={{width: '100%'}}>
                    <MultiSelect
                      style={{marginTop: 10}}
                      hideTags
                      items={[
                        {
                          id: 'Cervical spine',
                          name: 'Cervical spine',
                        },
                        {
                          id: 'Thoracic spine',
                          name: 'Thoracic spine',
                        },
                        {
                          id: 'Lumbar spine',
                          name: 'Lumbar spine',
                        },
                        {
                          id: 'Whole spine',
                          name: 'Whole spine',
                        },
                      ]}
                      uniqueKey="id"
                      ref={(component) => {
                        this.multiSelect = component;
                      }}
                      onSelectedItemsChange={this.onSelectedItemsChange}
                      selectedItems={this.state.selectedItems}
                      selectText="Select area"
                      searchInputPlaceholderText="Search Items..."
                      onChangeInput={(text) => console.log(text)}
                      tagRemoveIconColor="#CCC"
                      tagBorderColor="#CCC"
                      tagTextColor="#CCC"
                      selectedItemTextColor="#CCC"
                      selectedItemIconColor="#CCC"
                      itemTextColor="#000"
                      displayKey="name"
                      searchInputStyle={{color: '#CCC'}}
                      submitButtonColor="#0076fe"
                      submitButtonText="Add"
                    />

                    <TextInput
                      value={this.state.about}
                      onChangeText={(MriDescription) =>
                        this.setState({MriDescription})
                      }
                      placeholder={'Enter Description:'}
                      numberOfLines={8}
                      multiline={true}
                      style={styles.textArea}></TextInput>
                    <View>
                      {/* {this.multiSelect &&
            this.multiSelect.getSelectedItemsExt(selectedItems)} */}
                    </View>

                    <Button
                      title="Add Request"
                      onClick={this.addMriRequest}></Button>
                  </View>
                </View>
              </Modal>

              {/* --------------------------------------------- End MRI ------------------------------- */}

              <SetAvailableTimmings
                specificTimeData={this.state.specificTimeData}
                closeModal={this.closeTimeModal}
                TimeVisible={this.state.TimeVisible}
                onClick={this.addTime}
                Date={
                  this.state.selectTime === 'Select Time'
                    ? 'Select Time'
                    : this.state.selectTime
                }
                selectedDateCallback={
                  this.startTimeCallback
                }></SetAvailableTimmings>

              <MainHeader Back={this.backToScreen}></MainHeader>
              <View>
                <Text
                  style={{
                    fontSize: 19,
                    marginTop: 30,
                    textAlign: 'center',
                    fontFamily: 'Axiforma-Medium',
                  }}>
                  {this.state.symptomName}
                </Text>
                <Text
                  style={{
                    fontSize: 14,
                    textAlign: 'center',
                    color: '#646464',
                    marginTop: 10,
                    fontFamily: 'Axiforma-Thin',
                  }}>
                  {this.state.completed === 0
                    ? 'Initial Consultation'
                    : 'Follow up Consultation'}
                </Text>

                {/* ----------------------------- Doctor Box ---------------------- */}

                <View style={{marginTop: 30}}>
                  <View style={{flex: 1}}>
                    <Image
                      style={styles.dp}
                      source={{
                        uri:
                          this.state.profile_pic === null
                            ? 'https://www.pngitem.com/pimgs/m/516-5167304_transparent-background-white-user-icon-png-png-download.png'
                            : 'https://platform.swiftclinic.com/storage/profile/' +
                              this.state.profile_pic,
                      }}></Image>
                  </View>
                  <View style={{padding: 10, flex: 3}}>
                    <Text
                      style={{
                        fontSize: 17,
                        textAlign: 'center',
                        fontFamily: 'Axiforma-Medium',
                      }}>
                      {' '}
                      {this.state.doctorName}
                    </Text>
                  </View>
                </View>

                {/* ------------------------------ doctor Initial up -------------- */}

                {this.state.slotTime2 === null ? (
                  <View style={{padding: 20}}>
                    <View
                      style={[
                        styles.shadow,
                        {
                          flexDirection: 'row',
                          justifyContent: 'center',
                          marginLeft: 40,
                          marginRight: 40,
                          paddingTop: 20,
                          paddingBottom: 20,
                          backgroundColor: 'white',
                          borderRadius: 14,
                        },
                      ]}>
                      <Text
                        style={{
                          color: '#33909F',
                          flex: 1,
                          fontSize: 18,
                          textAlign: 'right',
                          marginRight: 3,
                          fontFamily: 'Axiforma-Medium',
                        }}>
                        {this.state.slotTime === ''
                          ? ''
                          : this.formatDateTime(this.state.slotTime)}
                      </Text>
                      <Text
                        style={{
                          color: 'black',
                          fontSize: 18,
                          textAlign: 'left',
                          flex: 1,
                          marginLeft: 3,
                          alignSelf: 'center',
                          fontFamily: 'Axiforma-Medium',
                        }}>
                        {' '}
                        {this.state.slotTime === ''
                          ? ''
                          : this.formatDateTimeForDate(this.state.slotTime)}
                      </Text>
                    </View>
                  </View>
                ) : null}

                {/* --------------------------- end --------------------------- */}

                {/* ------------------------------ doctor Follow up -------------- */}

                {this.state.slotTime2 !== null ? (
                  <View style={{padding: 20}}>
                    <View
                      style={[
                        styles.shadow,
                        {
                          flexDirection: 'row',
                          justifyContent: 'center',
                          marginLeft: 40,
                          marginRight: 40,
                          paddingTop: 20,
                          paddingBottom: 20,
                          backgroundColor: 'white',
                          borderRadius: 14,
                        },
                      ]}>
                      <Text
                        style={{
                          color: '#33909F',
                          fontWeight: 'bold',
                          flex: 1,
                          fontSize: 18,
                          textAlign: 'right',
                          marginRight: 3,
                        }}>
                        {this.state.slotTime === ''
                          ? ''
                          : this.formatDateTime(this.state.slotTime2)}
                      </Text>
                      <Text
                        style={{
                          color: 'black',
                          fontSize: 18,
                          textAlign: 'left',
                          flex: 1,
                          marginLeft: 3,
                        }}>
                        {' '}
                        {this.state.slotTime === ''
                          ? ''
                          : this.formatDateTimeForDate(this.state.slotTime2)}
                      </Text>
                    </View>
                  </View>
                ) : null}

                {/* --------------------------- end --------------------------- */}

                <View>
                  <Text
                    style={{
                      padding: 20,
                      fontSize: 16,
                      fontWeight: '100',
                      fontFamily: 'Axiforma-Thin',
                      lineHeight: 24,
                    }}>
                    Here are the details of your upcoming consultation Below are
                    the attached documents associated with this patient Please
                    initiate the call on the time and date shown by pressing the
                    start a call button{' '}
                  </Text>
                  <Text
                    style={{
                      fontWeight: 'bold',
                      paddingLeft: 20,
                      color: '#0375FF',
                      textDecorationLine: 'underline',
                    }}>
                    {/* All details */}
                  </Text>
                </View>

                <View
                  style={{
                    padding: 20,
                    backgroundColor: '#ECF5FF',
                    marginTop: 20,
                  }}>
                  <Text style={{fontWeight: 'bold'}}>Documents:</Text>
                  {this.state.questionaire_link === null ? null : (
                    <View
                      onStartShouldSetResponder={this.downloadFile}
                      style={{flexDirection: 'row', marginTop: 17}}>
                      <Icon
                        style={{alignSelf: 'flex-end', marginRight: 17}}
                        color="#33909F"
                        size={24}
                        name="file-pdf"
                      />
                      <Text
                        style={{
                          textDecorationLine: 'underline',
                          fontFamily: 'Axiforma-Medium',
                        }}>
                        Health Questionaire
                      </Text>
                    </View>
                  )}

                  {this.state.doctor_pdf === null ? null : (
                    <View
                      onStartShouldSetResponder={this.consultationDownloadFile}
                      style={{flexDirection: 'row', marginTop: 17}}>
                      <Icon
                        style={{alignSelf: 'flex-end', marginRight: 17}}
                        color="#33909F"
                        size={24}
                        name="file-pdf"
                      />
                      <Text
                        style={{
                          textDecorationLine: 'underline',
                          fontFamily: 'Axiforma-Medium',
                        }}>
                        Consultation letter
                      </Text>
                    </View>
                  )}

                  {this.renderlist()}
                </View>

                {/* ------------------------- upload report ------------------------- */}

                {this.state.completed === 3 ? (
                  <View
                    onStartShouldSetResponder={this.selectFile}
                    style={{flexDirection: 'row', padding: 20}}>
                    <Icon
                      style={{alignSelf: 'flex-end', marginRight: 17}}
                      color="#33909F"
                      size={24}
                      name="upload"
                    />
                    <Text style={{textDecorationLine: 'underline'}}>
                      Upload report
                    </Text>
                  </View>
                ) : null}

                {this.state.test_required && this.state.test_required < 0 ? (
                  <View
                    onStartShouldSetResponder={this.onClickMriRequest}
                    style={{
                      flexDirection: 'row',
                      paddingLeft: 20,
                      paddingRight: 20,
                      marginBottom: 20,
                    }}>
                    <Icon
                      style={{
                        alignSelf: 'flex-end',
                        marginRight: 17,
                        alignSelf: 'center',
                        marginLeft: 10,
                      }}
                      color="#33909F"
                      size={32}
                      name="caret-right"
                    />
                    <Text
                      style={{
                        textDecorationLine: 'underline',
                        alignSelf: 'center',
                        fontFamily: 'Axiforma-Medium',
                      }}>
                      MRI Request
                    </Text>
                  </View>
                ) : null}

                {this.state.startCallStatus &&
                this.state.slotTime2 === null &&
                this.state.callStatus !== 'initial_consultation_completed' ? (
                  <LinearGradient
                    colors={['#78C2CB', '#33909F']}
                    style={styles.gbutton}>
                    <TouchableOpacity
                      onPress={this.startCallButton}
                      style={{
                        width: '100%',
                        height: '100%',
                        justifyContent: 'center',
                      }}>
                      <Text
                        style={{
                          color: '#ffffff',
                          textAlign: 'center',
                          fontSize: 17,
                        }}>
                        Start a call
                      </Text>
                    </TouchableOpacity>
                  </LinearGradient>
                ) : null}

                {this.state.completeCallStatus &&
                this.state.slotTime2 === null &&
                this.state.completed === 0 &&
                this.state.initial_call === 1 ? (
                  <LinearGradient
                    colors={['#78C2CB', '#33909F']}
                    style={styles.gbuttonL}>
                    <TouchableOpacity
                      onPress={() =>
                        this.completeStatus('initial_consultation_completed')
                      }
                      style={{
                        width: '100%',
                        height: '100%',
                        justifyContent: 'center',
                      }}>
                      <Text
                        style={{
                          color: '#ffffff',
                          textAlign: 'center',
                          fontSize: 17,
                        }}>
                        Complete Call
                      </Text>
                    </TouchableOpacity>
                  </LinearGradient>
                ) : null}

                {this.state.startCallStatusTwo === true &&
                this.state.completed !== 3 ? (
                  <LinearGradient
                    colors={['#78C2CB', '#33909F']}
                    style={styles.gbutton}>
                    <TouchableOpacity
                      onPress={this.startCallButton}
                      style={{
                        width: '100%',
                        height: '100%',
                        justifyContent: 'center',
                      }}>
                      <Text
                        style={{
                          color: '#ffffff',
                          textAlign: 'center',
                          fontSize: 17,
                        }}>
                        Start a call
                      </Text>
                    </TouchableOpacity>
                  </LinearGradient>
                ) : null}

                {this.state.completeCallStatusTwo &&
                this.state.followup_call === 1 &&
                this.state.completed !== 3 ? (
                  <LinearGradient
                    colors={['#78C2CB', '#33909F']}
                    style={styles.gbuttonL}>
                    <TouchableOpacity
                      onPress={() =>
                        this.completeStatus('followup_consultation_completed')
                      }
                      style={{
                        width: '100%',
                        height: '100%',
                        justifyContent: 'center',
                      }}>
                      <Text
                        style={{
                          color: '#ffffff',
                          textAlign: 'center',
                          fontSize: 17,
                        }}>
                        Complete Call
                      </Text>
                    </TouchableOpacity>
                  </LinearGradient>
                ) : null}
              </View>

              {/* ---------------------------------- Accept Reject  ----------------------------  */}

              {/* ------------------------------- Components --------------------------------- */}
              <AwesomeAlertComponent
                title="Message"
                visibleAlert={this.state.AlertComponentVisible}
                closeAlert={this.closeAlertFunction}
                message={
                  this.state.AlertComponentMessage
                }></AwesomeAlertComponent>
              <SpinnerComponent
                Visible={this.state.spinnerLoading}></SpinnerComponent>
              {/* ---------------------------- End of Components ----------------- */}
            </ScrollView>
          </View>
        </KeyboardAwareScrollView>
      </SafeAreaView>
    );
  }
}

export default ActiveConsultations;
