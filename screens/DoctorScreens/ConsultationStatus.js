import React, {Component} from 'react';
import {View, Text, Image, ScrollView, TouchableOpacity} from 'react-native';
import MainHeader from '../../components/Headers/MainHeader';
import DatePicker from '../../components/Date/DatePicker';
import Toast from 'react-native-simple-toast';
import styles from '../../css/css';
import {
  GetConsultationsDetails,
  DoctorRescheduling,
  AcceptOrRejectApi,
} from '../../api/ConsultationApi';
import {SafeAreaView} from 'react-navigation';
import AwesomeAlertComponent from '../../components/Alerts/AwesomeAlertFile';
import SpinnerComponent from '../../components/Spinner/Spinner';
import SvgGraph from '../../components/backgroundgraph';
import Moment from 'moment';

class ConsultationStatus extends Component {
  state = {
    consultation_id: null,
    doctorName: '',
    showDate: 'Tue, 23 Feb 2021 06:50:00 GMT',
    AlertComponentVisible: false,
    AlertComponentMessage: '',
    upDown: false,
    spinnerLoading: true,
    symptomName: '',
    completed: 0,
    consulation_id: null,
    time: '',
    date: '',
    slotTime: '',
    WhichConsultation: null,
    acceptActive: false,
    rejectActive: false,
    requestActive: false,
    consultation_status: 'reject',
    rejectStatus: false,
    pendingStatus: false,
  };

  componentDidMount = async () => {
    const id = await this.props.navigation.getParam('id', 'nothing sent');
    console.log('Const ID -------->', id);
    this.setState({consulation_id: id});
    this.getBookingDetails();
  };

  //   ----------------- get Details -----------------

  getBookingDetails = async () => {
    await this.setState({spinnerLoading: true});
    const {consulation_id} = this.state;
    GetConsultationsDetails(consulation_id)
      .then(async (res) => {
        await console.log(
          'Status Page C  =====------->',
          res.data.data.consultation_status,
        );
        var profile_pic;
        if (res.data.data.patient.profile_pic) {
          profile_pic =
            'https://platform.swiftclinic.com/storage/profile/' +
            res.data.data.patient.profile_pic;
        } else {
          profile_pic =
            'https://www.pngitem.com/pimgs/m/516-5167304_transparent-background-white-user-icon-png-png-download.png';
        }
        await this.setState({
          spinnerLoading: false,
          time: res.data.data.time,
          date: res.data.data.date,
          patientImage: profile_pic,
          doctorName:
            res.data.data.patient.first_name +
            ' ' +
            res.data.data.patient.last_name,
          symptomName: res.data.data.symptom.symptom + ' consultation',
          slotTime: res.data.data.slottime,
          WhichConsultation: res.data.data.date2,
          slottime2: res.data.data.slottime2,
          consultation_status: res.data.data.consultation_status,
        });
      })
      .catch((err) => console.log('Error -------->', err));
  };

  //   ---------------------- Back to Funcation ----------------

  backToScreen = async () => {
    this.props.navigation.goBack(null);
  };

  //   --------------------- Close Alert Function ---------------

  closeAlertFunction = async () => {
    await this.setState({AlertComponentVisible: false});
    this.props.navigation.navigate('DoctorHome');
  };

  // -------------------- Date Function ---------------

  dateCallback = async (selectedDate) => {
    let time = await selectedDate.toUTCString();
    await console.log('Time ------>', time);
    this.setState({showDate: time});
    this.onSubmit();
  };

  onSubmit = async () => {
    await this.setState({
      requestActive: true,
      acceptActive: false,
      rejectActive: false,
    });
    await this.setState({spinnerLoading: true});
    const {consulation_id, showDate} = this.state;
    console.log('Time in State ------>', showDate);
    DoctorRescheduling(consulation_id, showDate)
      .then(async (res) => {
        console.log('Reschedule Booking =====------->', res.data);
        this.setState({
          spinnerLoading: false,
          AlertComponentMessage: 'Successfully Submitted new time request',
          AlertComponentVisible: true,
        });
      })
      .catch(async (err) => {
        this.setState({spinnerLoading: false});
        Toast.show('Already submitted request for time change.');
        console.log('Error -------->', err);
      });
  };

  //   ---------------------- Accept Function -----------

  onAccept = async () => {
    console.log('Hello');
    await this.setState({
      acceptActive: true,
      requestActive: false,
      rejectActive: false,
    });
    const {consulation_id} = this.state;
    const response = 1;
    await this.setState({spinnerLoading: true});
    AcceptOrRejectApi(consulation_id, response)
      .then(async (res) => {
        console.log('Response =====------->', res.data);
        this.setState({
          spinnerLoading: false,
          AlertComponentVisible: true,
          AlertComponentMessage: 'Successfully Accepted',
        });
      })
      .catch((err) => {
        this.setState({spinnerLoading: false});
        Toast.show('Already submitted request');
      });
  };

  // --------------- Reject Function --------

  onReject = async () => {
    await this.setState({
      rejectActive: true,
      acceptActive: false,
      requestActive: false,
    });
    const {consulation_id} = this.state;
    const response = 0;
    await this.setState({spinnerLoading: true});
    AcceptOrRejectApi(consulation_id, response)
      .then(async (res) => {
        console.log('Response =====------->', res.data);
        this.setState({
          spinnerLoading: false,
          AlertComponentVisible: true,
          AlertComponentMessage: 'Successfully Rejected',
        });
      })
      .catch((err) => {
        this.setState({spinnerLoading: false});
        Toast.show('Already submitted request');
      });
  };

  formatDateTime(datetime) {
    return Moment(datetime, 'YYYY-MM-DD h:mm').format('h:mm A');
  }

  formatDateTimeForDate(datetime) {
    return Moment(datetime, 'YYYY-MM-DD h:mm').format('YYYY-MM-DD');
  }

  // ------------------------- End Location --------------------------

  render() {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#ffffff'}}>
        <ScrollView>
          <SvgGraph style={{right: -38}} />
          <View style={{marginTop: -280}}>
            <MainHeader Back={this.backToScreen}></MainHeader>
            <View>
              <Text
                style={{
                  fontSize: 18,
                  textAlign: 'center',
                  marginTop: 30,
                  fontFamily: 'Axiforma-Medium',
                }}>
                {this.state.symptomName}
              </Text>
              <Text
                style={{
                  fontSize: 13,
                  textAlign: 'center',
                  color: '#646464',
                  marginTop: 10,
                  fontFamily: 'Axiforma-Thin',
                }}>
                {this.state.WhichConsultation === null
                  ? 'Initial Consultation'
                  : 'Follow-Up Consultation'}
              </Text>
              <View style={{marginTop: 30}}>
                <View style={{flex: 1}}>
                  <Image
                    style={styles.dp}
                    source={{uri: this.state.patientImage}}></Image>
                </View>
                <View style={{padding: 10, flex: 3}}>
                  <Text
                    style={{
                      fontSize: 17,
                      textAlign: 'center',
                      fontFamily: 'Axiforma-Book',
                    }}>
                    {' '}
                    {this.state.doctorName}
                  </Text>
                </View>
              </View>

              {/* --------------------------- end --------------------------- */}
              <View>
                <Text
                  style={{
                    padding: 20,
                    fontSize: 14,
                    fontFamily: 'Axiforma-Thin',
                    lineHeight: 25,
                  }}>
                  A patient has requested a consultation with yourself on the
                  time and date below.{'\n'}Please accept this appointment or
                  propose a more suitable time and date.{' '}
                </Text>
                <Text
                  style={{
                    fontWeight: 'bold',
                    paddingLeft: 20,
                    textAlign: 'center',
                    color: '#33909F',
                    textDecorationLine: 'underline',
                  }}>
                  {this.state.consultation_status === 'reject'
                    ? 'This appointment has been rejected.'
                    : null}
                  {this.state.consultation_status === 'reqeuested_new_datetime'
                    ? 'You requested the patient for new date time.'
                    : null}
                </Text>
              </View>
            </View>
            {this.state.consultation_status === 'reject' ||
            this.state.consultation_status ===
              'reqeuested_new_datetime' ? null : (
              <View style={{padding: 20}}>
                <View style={{flexDirection: 'row', justifyContent: 'center'}}>
                  <Text
                    style={{
                      color: '#33909F',
                      fontSize: 18,
                      alignSelf: 'center',
                      flex: 1,
                      textAlign: 'right',
                      fontFamily: 'Axiforma-Medium',
                    }}>
                    {this.state.slotTime === ''
                      ? ''
                      : this.formatDateTime(
                          this.state.slottime2 === null
                            ? this.state.slotTime
                            : this.state.slottime2,
                        )}
                  </Text>
                  <Text
                    style={{
                      color: 'grey',
                      flex: 1,
                      alignSelf: 'center',
                      fontSize: 18,
                      textAlign: 'left',
                      fontFamily: 'Axiforma-Book',
                    }}>
                    {' '}
                    {this.state.slotTime === ''
                      ? ''
                      : this.formatDateTimeForDate(
                          this.state.slottime2 === null
                            ? this.state.slotTime
                            : this.state.slottime2,
                        )}
                  </Text>
                </View>
              </View>
            )}

            {/* ---------------------------------- Accept Reject  ----------------------------  */}

            {this.state.consultation_status === 'reject' ||
            this.state.consultation_status ===
              'reqeuested_new_datetime' ? null : (
              <View style={{flexDirection: 'row', marginTop: 20}}>
                <TouchableOpacity
                  onPress={this.onAccept}
                  style={[
                    styles.inputTextReject,
                    {
                      flex: 1,
                      justifyContent: 'center',
                      margin: 5,
                      backgroundColor: this.state.acceptActive
                        ? '#0486FF'
                        : '#ffffff',
                    },
                  ]}>
                  <Text
                    style={{
                      textAlign: 'center',
                      fontFamily: 'Axiforma-Book',
                      color: this.state.acceptActive ? 'white' : 'black',
                    }}>
                    Accept
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={this.onReject}
                  style={[
                    styles.inputTextReject,
                    {
                      flex: 1,
                      justifyContent: 'center',
                      margin: 5,
                      backgroundColor: this.state.rejectActive
                        ? '#0486FF'
                        : '#ffffff',
                    },
                  ]}>
                  <Text
                    style={{
                      textAlign: 'center',
                      fontFamily: 'Axiforma-Book',
                      color: this.state.rejectActive ? 'white' : 'black',
                    }}>
                    Reject
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={this.onSubmit}
                  style={[
                    styles.inputTextReject,
                    {
                      flex: 1,
                      justifyContent: 'center',
                      margin: 5,
                      backgroundColor: this.state.requestActive
                        ? '#0486FF'
                        : '#ffffff',
                    },
                  ]}>
                  <Text
                    style={{
                      textAlign: 'center',
                      fontFamily: 'Axiforma-Book',
                      color: this.state.requestActive ? 'white' : 'black',
                    }}>
                    Request new time
                  </Text>
                </TouchableOpacity>
              </View>
            )}

            {/* ------------------------------- Components --------------------------------- */}
            <AwesomeAlertComponent
              title="Message"
              visibleAlert={this.state.AlertComponentVisible}
              closeAlert={this.closeAlertFunction}
              message={
                this.state.AlertComponentMessage
              }></AwesomeAlertComponent>
            <SpinnerComponent
              Visible={this.state.spinnerLoading}></SpinnerComponent>
            {/* ---------------------------- End of Components ----------------- */}
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

export default ConsultationStatus;
