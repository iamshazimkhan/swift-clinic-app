import React, {Component} from 'react';
import {Image, FlatList, View, TouchableNativeFeedback, Text, RefreshControl} from 'react-native';
import styles from '../../css/css';
import TitleImage from '../../components/Headers/titleImage';
import AddConsultation from '../../components/consultations/AddConsultation';
import ContentBox from '../../components/consultations/ContentBox';
import QuestionModal from '../../components/Modals/QuestionModal';
import { SafeAreaView } from 'react-navigation';
import AnswerQuestionBox from '../../components/consultations/AnswerQuestionBox';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import {GetUpcomingConsultations,GetProfileConsultations} from '../../api/ConsultationApi';
import SpinnerComponent from '../../components/Spinner/Spinner';
import PaymentSuccessfull from '../../components/Modals/PaymentSuccessfull';
import SvgGraph from '../../components/backgroundgraph';
import ConsentBox from '../../components/consultations/ConsentBox'
import Moment from 'moment';

class HomeScreen extends Component {
    state={
        ModalVisibility: false,
        data: [],
        spinnerLoading: true,
        completed: false,
        taskData : [],
        selectedConstId: null,
        profileStatus: true,
        questionnaire_pending: false,
        upcomingDataStatus: false,
    }

    getTimeToLocal = async (slotTime) => {
      var gmtDateTime = await Moment.utc(slotTime, "YYYY-MM-DD HH")
      var locale_slot_time = await gmtDateTime.local().format('LLL');
      return locale_slot_time;
    }

   ShowQuestionModal = async (item) => {
     console.log('Selected Const Id ------>',item.id);
     if(item.questionaire === 0 && item.followup_pending ===false )
     {
     this.setState({ selectedConstId: item.id, ModalVisibility: true })
     }else if(item.questionaire === 1 && item.followup_pending === false){
      this.props.navigation.navigate('ConsentForm',{id : item.id})
     }else if(item.followup_pending === true){
      this.props.navigation.navigate('ConsultationDetails',{id : item.id})
    }
   }

   closeQuestionModal = async () => {
     this.setState({ ModalVisibility: false })
   }

   navigateToQuestionaire = async () => {
     await this.setState({ ModalVisibility: false })
     const { selectedConstId } = this.state;
    this.props.navigation.navigate('QuestionaireScreen',{id: selectedConstId});
   }

   navigateToSymptomsListScreen = async () => {
   this.props.navigation.navigate('SymptomsListScreen')
  }

  navigateToProfileScreen = async () => {
    this.props.navigation.navigate('Profile')
   }


  navigateToConsultationDetails = async (item) => {
    console.log('Item Consultation Details',item)
    this.props.navigation.navigate('ConsultationDetails',{id : item.id})
   }


   componentDidMount = async () => {
    await this.getUserProfileOnLoading();
    this.willFocusSubscription = this.props.navigation.addListener(
      'willFocus',
      () => {
       console.log('Console Data ----------->',Math.floor(Math.random() * 100) + 1 );
       this.getUserProfileOnLoading();
      }
    );
   }

   componentWillUnmount() {
    this.willFocusSubscription.remove();
  }

   getUserUpcomingConsultant = async () => {
    GetUpcomingConsultations()
    .then(async res => {
    console.log('Response U  =====------->',res.data.data)
    if(res.data.data.length > 0)
    {
      console.log('Data Length ---->',res.data.data.length);
      await this.setState({upcomingDataStatus: true }); 
    }
    await this.setState({data: res.data.data, spinnerLoading:false, refreshing: false});
    })
    .catch(err => console.log('Error -------->',err))
   }

   getUserProfileOnLoading = async () => {
    GetProfileConsultations()
    .then(async res => {
    // console.log('Response P  =====------->',res.data.data)
    await this.setState({spinnerLoading:false, taskData: res.data.data.pending_questionaire_data, profileStatus: res.data.data.profile_completed, questionnaire_pending: res.data.data.questionnaire_pending});
    this.getUserUpcomingConsultant();
    })
    .catch(err => console.log('Error -------->',err))
   }



   _onRefresh = async () => {
    this.setState({refreshing: true});
    await this.getUserProfileOnLoading();
  }

  formatDateTime(datetime){
    return Moment.utc(datetime).local().format('LL')
 }

    
  render() {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#fcfeff'}}>
        <ScrollView style={{ backgroundColor: '#fcfeff'}}>
        <SvgGraph style={{right:-38}} />
        <View style={{padding: 10, marginTop: -260,width: '100%'}}>
          <TitleImage/>
                <AddConsultation onClick={this.navigateToSymptomsListScreen} Top={50} Bottom={50}></AddConsultation>
        {this.state.questionnaire_pending === true || this.state.profileStatus === false ? (
        <View style={{paddingBottom: 40}}>
        <Text style={{fontSize: 18,fontWeight: '700', marginLeft: 13}}>Tasks to complete</Text>
        <QuestionModal Visibilty={this.state.ModalVisibility} closeModal={this.closeQuestionModal} onClickAnswer={this.navigateToQuestionaire}></QuestionModal>
        {!this.state.profileStatus ? (
        <ContentBox onClick={this.navigateToProfileScreen} title="Ensure you personal data is up to date" doctorName="Complete your profile to continue" colour="white"></ContentBox>
        ) : null }
        <FlatList
        scrollEnabled={false}
          data={this.state.taskData}
          horizontal={false}
          numColumns={1}
          keyExtractor={(item) => item.id.toString()}
          renderItem={({item}) => (
            <TouchableNativeFeedback onPress={() => this.ShowQuestionModal(item)}>
              {item.questionaire === 0 && item.followup_pending === false ? (
                            <AnswerQuestionBox doctorName={"Consultation with Dr. " + item.doctor.first_name + " " + item.doctor.last_name + " on " + this.formatDateTime(item.slottime)}></AnswerQuestionBox>
              ) : (
                <ConsentBox  followup_pending={item.followup_pending}  doctorName={item.followup_pending === false ? ("Consultation with Dr. " + item.doctor.first_name + " " + item.doctor.last_name + " on " + this.formatDateTime(item.slottime)) :  ("Consultation with your doctor. ") }></ConsentBox>
              ) }
                      </TouchableNativeFeedback>
          )}
        />
        </View>
        ) : null
  }
        
      
       {this.state.upcomingDataStatus ? (
        <View style={{marginTop: 10, marginBottom: 20}}>
        <Text style={{fontSize: 18,fontWeight: '700', marginLeft: 13}}>Upcoming Consultations</Text>
        <FlatList
        scrollEnabled={false}
          data={this.state.data}
          horizontal={false}
          numColumns={1}
          keyExtractor={(item) => item.id.toString()}
          renderItem={({item}) => (
            <TouchableNativeFeedback onPress={() => this.navigateToConsultationDetails(item)}>
                      <ContentBox doctorName={"Consultation with Dr. " + item.doctor.first_name + " " + item.doctor.last_name} title={this.formatDateTime(item.slottime2 === null ? item.slottime : item.slottime2)} colour={item.doctor_response === null ? "white" : "white"}></ContentBox>
                      </TouchableNativeFeedback>
          )}
        />
        </View>
       ) : null }
       
        <SpinnerComponent Visible={this.state.spinnerLoading}></SpinnerComponent>
        </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

export default HomeScreen;
// this.props.navigation.navigate('PhoneVerify');
