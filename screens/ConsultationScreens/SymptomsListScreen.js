import React, {Component} from 'react';
import {
  FlatList,
  View,
  TextInput,
  Text,
  Image,
  Alert,
  TouchableOpacity,
  ToastAndroid,
} from 'react-native';
import styles from '../../css/css';
import {SafeAreaView} from 'react-navigation';
import SymtomBox from '../../components/consultations/SymptomsBox';
import SymptomHeader from '../../components/Headers/SymptomHeader';
import Icon from 'react-native-vector-icons/FontAwesome';
import {GetSymptoms} from '../../api/ConsultationApi';
import SpinnerComponent from '../../components/Spinner/Spinner';
import Toast from 'react-native-simple-toast';
import exampleImage from '../../bulb.png';
import {ScrollView} from 'react-native-gesture-handler';
import SvgGraph from '../../components/backgroundgraph';
import TitleImage from '../../components/Headers/titleImage';

const exampleImageUri = Image.resolveAssetSource(exampleImage).uri;

class SymptomsListScreen extends Component {
  state = {
    spinnerLoading: true,
    data: [],
    avatar: null,
    noDataFound: false,
  };

  arrayholder = [];

  componentDidMount() {
    GetSymptoms()
      .then(async (res) => {
        // console.log('Response =====------->',res.data)
        await this.setState({data: res.data.data, spinnerLoading: false});
        this.arrayholder = res.data.data;
      })
      .catch((err) => console.log('Error -------->', err));
  }

  navigateToDoctorsListScreen = async (item) => {
    // console.log('Method Clicked',item)
    if (item.live === 1) {
      this.props.navigation.navigate('DoctorsListScreen', {id: item.id});
    } else {
      Toast.show('This Symptom will be coming soon.');
    }
  };

  backToScreen = async () => {
    this.props.navigation.goBack(null);
  };

  /** -------------------- Search Filter --------------------------- */

  searchFilterFunction = async (text) => {
    const newData = await this.arrayholder.filter((item) => {
      const itemData = `${item.symptom.toUpperCase()}   
          ${item.symptom.toUpperCase()} ${item.symptom.toUpperCase()}`;

      const textData = text.toUpperCase();

      return itemData.indexOf(textData) > -1;
    });

    await console.log('new data length', newData.length);
    if (newData.length === 0) {
      await this.setState({noDataFound: true});
    } else {
      await this.setState({noDataFound: false});
    }

    this.setState({data: newData});
  };

  /** ------------------End of Search Filter -------------------- */

  render() {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
        <ScrollView>
          <SvgGraph style={{right: -38}} />
          <View style={{marginTop: -280, paddingLeft: 20, paddingRight: 20}}>
            {/* <SymptomHeader title="Back" Back={this.backToScreen}></SymptomHeader> */}
            <TitleImage></TitleImage>
            <SpinnerComponent
              Visible={this.state.spinnerLoading}></SpinnerComponent>
            <Text
              style={{
                fontSize: 19,
                marginTop: 40,
                textAlign: 'center',
                fontFamily: 'Axiforma-Black',
              }}>
              Symptom location
            </Text>
            <View style={styles.searchBarBox}>
              <TextInput
                placeholder="What are you looking for ?"
                onChangeText={(text) => this.searchFilterFunction(text)}
                style={{flex: 4}}></TextInput>
              <Icon
                style={{
                  marginLeft: 6,
                  flex: 1,
                  textAlign: 'right',
                  marginRight: 10,
                  alignSelf: 'center',
                }}
                color="#EB5757"
                size={19}
                name="search"
              />
            </View>
            <View style={{justifyContent: 'center', alignItems: 'center'}}>
              <FlatList
                style={{marginTop: 10}}
                data={this.state.data}
                horizontal={false}
                numColumns={3}
                keyExtractor={(item) => item.id}
                renderItem={({item}) => (
                  <TouchableOpacity
                    onPress={() => this.navigateToDoctorsListScreen(item)}>
                    <SymtomBox
                      URL={
                        item.image_link === null
                          ? exampleImageUri
                          : item.image_link
                      }
                      title={item.symptom}
                      opacity={item.live === 1 ? 1 : 0.3}
                      color={
                        item.live === 1 ? 'black' : 'lightgrey'
                      }></SymtomBox>
                  </TouchableOpacity>
                )}
              />
              {this.state.noDataFound ? (
                <Image
                  style={{width: 300, height: 300}}
                  source={require('../../nodatafound.png')}></Image>
              ) : null}
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

export default SymptomsListScreen;
