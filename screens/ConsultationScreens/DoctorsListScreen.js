import React, {Component} from 'react';
import {
  FlatList,
  View,
  TouchableOpacity,
  Text,
  Image,
  Alert,
} from 'react-native';
import styles from '../../css/css';
import {SafeAreaView} from 'react-navigation';
import SymptomHeader from '../../components/Headers/SymptomHeader';
import DoctorBox from '../../components/consultations/DoctorBox';
import {GetDoctors} from '../../api/ConsultationApi';
import SpinnerComponent from '../../components/Spinner/Spinner';
import Icon from 'react-native-vector-icons/FontAwesome';
import {ScrollView} from 'react-native-gesture-handler';
import SvgGraph from '../../components/backgroundgraph';

class DoctorsListScreen extends Component {
  constructor() {
    super();
    this.state = {
      spinnerLoading: true,
      data: [],
      symptom_id: null,
      show: false,
      paid_status: 0,
      consultation_id: null,
      showPage: false,
    };
  }

  backToScreen = async () => {
    this.props.navigation.goBack(null);
  };

  navigateToDoctorProfile = async (item) => {
    console.log('Method Clicked', item);
    this.props.navigation.navigate('DoctorProfile', {
      profile_pic: item.profile_pic,
      id: item.first_name,
      doctor_id: item.doctor,
      symptom_id: item.symptom,
      about: item.about,
      reviewStar: item.review,
      total_review: item.total_review,
      paid_status: this.state.paid_status,
      consultation_id: this.state.consultation_id,
    });
  };

  componentDidMount = async () => {
    const id = await this.props.navigation.getParam('id', 'nothing sent');
    const paid_status = await this.props.navigation.getParam(
      'paid_status',
      'nothing sent',
    );
    const consultation_id = await this.props.navigation.getParam(
      'consultation_id',
      'nothing sent',
    );
    console.log('ComponentDidMount -------->', id);
    await this.setState({
      symptom_id: id,
      paid_status: paid_status,
      consultation_id: consultation_id,
    });
    await GetDoctors(id)
      .then(async (res) => {
        console.log('Response =====------->', res.data.data.length);
        const list = await res.data.data;
        list.length > 0
          ? await this.setState({show: true})
          : await this.setState({show: false});
        await this.setState({
          spinnerLoading: false,
          data: res.data.data,
          showPage: true,
        });
        const fdata = this.state.data;
        // await console.log('Data in State -------->', fdata[0])
        // data: res.data.data,
      })
      .catch((err) => console.log('Error -------->', err));
  };

  sortListById = async () => {
    //Sort ArrayList by ascendingorder
    const data = this.state.data;
    console.log('data is state -------->', data);
    data.sort(function (obj1, obj2) {
      // Ascending: first id less than the previous
      return obj2.review - obj1.review;
    });

    this.setState((previousState) => ({data: previousState.data}));
  };

  render() {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#ffffff'}}>
        <ScrollView>
          <SvgGraph style={{right: -38}} />
          <View style={{marginTop: -280}}>
            <SymptomHeader
              title="Symtom location"
              Back={this.backToScreen}></SymptomHeader>
            <SpinnerComponent
              Visible={this.state.spinnerLoading}></SpinnerComponent>
            <View
              style={{
                flexDirection: 'row',
                marginTop: 30,
                paddingBottom: 10,
                paddingRight: 20,
                paddingLeft: 20,
              }}>
              <View style={{flex: 1}}>
                <Text
                  style={{
                    fontSize: 19,
                    fontWeight: 'bold',
                  }}>
                  Our Doctors
                </Text>
              </View>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  justifyContent: 'flex-end',
                }}>
                <Text style={{marginTop: 3}}>Sort by </Text>
                <View
                  onStartShouldSetResponder={this.sortListById}
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignContent: 'center',
                    backgroundColor: '#ecf5ff',
                    paddingRight: 4,
                    paddingLeft: 4,
                    paddingTop: 3,
                    paddingBottom: 3,
                    borderRadius: 4,
                  }}>
                  <Text style={{marginTop: 3}}>Rating</Text>
                  <Icon
                    style={{
                      textAlignVertical: 'center',
                      marginLeft: 6,
                      marginTop: 5,
                    }}
                    color="#E73988"
                    size={12}
                    name="chevron-down"
                  />
                </View>
              </View>
            </View>
            {this.state.showPage ? (
              <View>
                {this.state.show ? (
                  <FlatList
                    style={{marginTop: 10}}
                    data={this.state.data}
                    horizontal={false}
                    numColumns={1}
                    keyExtractor={(item) => item.id}
                    renderItem={({item}) => (
                      <TouchableOpacity
                        style={{paddingRight: 20, paddingLeft: 20}}
                        onPress={() => this.navigateToDoctorProfile(item)}>
                        <DoctorBox
                          about={item.about}
                          imageUrl={item.profile_pic}
                          Reviews={'(' + item.total_review + ' Reviews' + ')'}
                          reviewStar={item.review}
                          name={
                            'Dr. ' + item.first_name + ' ' + item.last_name
                          }></DoctorBox>
                      </TouchableOpacity>
                    )}
                  />
                ) : (
                  <Image
                    style={{
                      marginTop: 40,
                      height: 200,
                      width: 200,
                      alignSelf: 'center',
                    }}
                    source={require('../../nodatafound.png')}></Image>
                )}
              </View>
            ) : null}
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

export default DoctorsListScreen;
