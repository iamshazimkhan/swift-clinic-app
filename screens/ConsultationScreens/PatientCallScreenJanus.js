import {mediaDevices, MediaStream, RTCIceCandidate, RTCPeerConnection, RTCSessionDescription, RTCView} from 'react-native-webrtc';
import React from 'react';
import {Text,Alert, Dimensions, FlatList,Image, SafeAreaView, StatusBar, View} from 'react-native';
import NavigationService from '../../navigations/NavigationService';
// import {Theme} from '../../../../services/theme';
import {Janus, JanusVideoRoomPlugin} from 'react-native-janus';
import {NavigationActions} from 'react-navigation';

import { Icon } from 'react-native-elements'
import InCallManager from 'react-native-incall-manager';
import { fas } from '@fortawesome/free-solid-svg-icons';
 
Janus.setDependencies({
    RTCPeerConnection,
    RTCSessionDescription,
    RTCIceCandidate,
    MediaStream,
});
 
class PatientVideoScreen extends React.Component {
    constructor(props) {
        super(props);
        this.stream = null;
        this.isStreamAlreadyRemoving = false;
        this.isJanusRemoving = false;
        this.videoRoom = null;
        this.peerVideoRoom = null;
 
        this.state = {
            connectingStatus : 'Connecting to the doctor...',
            callIconStatus : false, 
            stream: null,
            localStream : null,
            showLocal : false,
            callStatus:0,
            publishers: [],
            consultation_id: null,
            channel_id: null,
            audioMute: false,
            videoMute: false,
            speaker: false
        };
    }
 
    async receivePublisher(publisher) {
        try {
          this.peerVideoRoom = new JanusVideoRoomPlugin(this.janus);
            console.log("----channeeelllllllllll------")
            
            // Alert.alert("room id : "+this.state.channel_id)
            const chnl_id = await this.props.navigation.getParam('channel_id',1234);
            let room_id = parseInt(chnl_id)
            console.log("type of room id is :"+typeof(room_id)+ "   -- and room id is : "+room_id)
            
            this.peerVideoRoom.setRoomID(room_id);
            this.peerVideoRoom.setOnStreamListener((stream) => {
                this.setState(state => ({
                    publishers: [
                        ...state.publishers,
                        {
                            publisher: publisher,
                            stream: stream,
                        },
                    ],
                  }), () => {
                    this.setState({showLocal : true, callStatus:1})
                    // console.log("recv_pub_callstatus")
                  });
            });
 
            let tData = await this.peerVideoRoom.createPeer();
            await this.peerVideoRoom.connect();
            await this.peerVideoRoom.receive(this.videoRoom.getUserPrivateID(), publisher);
        } catch (e) {
            console.error(e);
        }
    }
 
    async removePublisher(publisherID) {
        try {
            this.setState(state => ({
                publishers: state.publishers.filter(pub => pub.publisher == null || pub.publisher.id !== publisherID),
            }));
        } catch (e) {
            console.error(e);
        }
    }
 
    async initJanus(stream) {
        try {
            this.setState(state => ({
                publishers: [
                    {
                        publisher: null,
                        stream: stream,
                    },
                ],
            }));
 
            this.janus = new Janus('wss://swift-video.swiftclinic.com/');
            this.janus.setApiSecret('janusrocks');
            await this.janus.init();
 
            this.videoRoom = new JanusVideoRoomPlugin(this.janus);
            const chnl_id = await this.props.navigation.getParam('channel_id',1234);
            let room_id = parseInt(chnl_id)

            this.videoRoom.setRoomID(room_id);
            this.videoRoom.setDisplayName('can');
            this.videoRoom.setOnPublishersListener((publishers) => {
                for (let i = 0; i < publishers.length; i++) {
                    this.receivePublisher(publishers[i]);
                }
            });
            this.videoRoom.setOnPublisherJoinedListener((publisher) => {
                InCallManager.setSpeakerphoneOn(true);
                InCallManager.turnScreenOn();
                this.setState({speaker: true});
                this.receivePublisher(publisher);
            });
            this.videoRoom.setOnPublisherLeftListener((publisherID) => {
                this.removePublisher(publisherID);
            });
            this.videoRoom.setOnWebRTCUpListener(async () => {
 
            });
            this.videoRoom.setOnDestoryListener(async (message) => {
                console.log("destroy listner");
                this.removeStream();
                this.endCallNavigate();
            });
 
            await this.videoRoom.createPeer();
            await this.videoRoom.connect();
            const isIncomingCall = await this.props.navigation.getParam('isIncomingCall',null);
            if(isIncomingCall === null) {
                // console.log('dialing to the patient, creating room')
                await this.videoRoom.create(this.state.channel_id);
            } 

            await this.videoRoom.join();
            await this.videoRoom.publish(this.stream);
 
        } catch (e) {
            console.error('main', JSON.stringify(e));
        }
    }
 
    getMediaStream = async () => {
        let isFront = true;
        let sourceInfos = await mediaDevices.enumerateDevices();
        let videoSourceId;
        for (let i = 0; i < sourceInfos.length; i++) {
            const sourceInfo = sourceInfos[i];
            console.log(sourceInfo);
            if (sourceInfo.kind == 'videoinput' && sourceInfo.facing == (isFront ? 'front' : 'environment')) {
                videoSourceId = sourceInfo.deviceId;
            }
        }
 
        this.stream = await mediaDevices.getUserMedia({
            audio: true,
            video: {
                facingMode: (isFront ? 'user' : 'environment'),
            },
        });
        await this.initJanus(this.stream);
    };
 
    async componentDidMount() {
        const chnl_id = await this.props.navigation.getParam('channel_id',null);
        const cnslt_id = await this.props.navigation.getParam('consultation_id',null);
        const incomingCall = await this.props.navigation.getParam('isIncomingCall',null);
        // await console.log('channnel_id && consultation_id ---------->', chnl_id, cnslt_id)
        // Alert.alert("room id : "+chnl_id)
        await this.setState({
            consultation_id : cnslt_id,
            channel_id: chnl_id,
            incomingCall: incomingCall,
            speaker:true
        })
         
        await InCallManager.start();
        await InCallManager.setKeepScreenOn(true);
        await InCallManager.setForceSpeakerphoneOn(true);
        this.getMediaStream();
    }

  endCallNavigate(){
    let callstatus = '';
        if(this.state.callStatus===1){
          callstatus = 'Call Ended'
        }else{
          if(this.state.connectingStatus==='Call Ended'){
            // console.log('no_need_to_update, call ended')
            callstatus = 'Call Ended'
          }else{
            // console.log('udpating, call declined')
            callstatus= 'We could not connect to the doctor';
          }
        }
          this.setState({
            callIconStatus : true,
            connectingStatus : callstatus,
            showLocal : false,
            callStatus : 2
          }, () => {
            // console.log('call status message : '+callstatus)
            // console.log('call_call_status_enddddd: '+this.state.callstatus)
            setTimeout(() => {
              NavigationService.navigate(
                NavigationActions.navigate({
                  routeName: 'Home',
                }),
              );
            }, 3000);
          })
  }

    removeStream() {
      if(this.stream && this.isStreamAlreadyRemoving === false) {
        this.isStreamAlreadyRemoving = true;
        let tracks = this.stream.getTracks();
        tracks.forEach(function(track) {
          track.stop();
        });
        this.stream.release();
        this.stream = null;
      }
    }

  janusDisconnect = async () => {
      try {
      await this.removeStream();
      if(this.janus && this.isJanusRemoving === false){
          this.isJanusRemoving = true;
        InCallManager.stop();
            await this.janus.destroy();
        if(this.janus.RTCPeerConnection) {
          this.janus.RTCPeerConnection.getTransceivers().forEach((transceiver) => {
            transceiver.stop();
          });
        }
        if(this.peerVideoRoom) {
          this.peerVideoRoom.pc.close();
          this.peerVideoRoom.pc = null;
          this.peerVideoRoom = null;
        }
        if(this.videoRoom) {
          await this.videoRoom.pc.close();
          this.videoRoom.pc = null;
          this.videoRoom = null;
        }
          this.janus.socket = null;
          this.janus = null;
        }
      } catch (error) {
        console.log(error);
        }
    
        }
 
    componentWillUnmount = async () => {
      this.janusDisconnect();
    };
 
    renderView() {
    }

    toggleAudioMute = () => {
        
        let audioTracks = this.stream.getAudioTracks()[0];
        let muted = audioTracks.muted;
        if(muted){
          this.stream.getAudioTracks()[0].enabled = true;
          this.setState({ audioMute: false });
        }else{
          this.stream.getAudioTracks()[0].enabled = false;
          this.setState({ audioMute: true });
        }
      }
  
      toggleVideoMute = () => {
        let videoTracks = this.stream.getVideoTracks()[0];
        let videomuted = videoTracks.muted;
        if(videomuted){
          this.setState({ videoMute: false });
          this.stream.getVideoTracks()[0].enabled = true;
        }else{
          this.stream.getVideoTracks()[0].enabled = false;
          this.setState({ videoMute: true });
        }
      }
  
      toggleSpeaker = () => {
        if(this.state.speaker){
          this.setState({speaker: false});
          InCallManager.setSpeakerphoneOn(false)
          InCallManager.setForceSpeakerphoneOn(false)
        }else{
          this.setState({speaker: true});
          InCallManager.setSpeakerphoneOn(true)
          InCallManager.setForceSpeakerphoneOn(true)
        }
      }

      endCall = async () => {
        await this.janusDisconnect();
        this.endCallNavigate();
      }

      switchVideoType() {
        let videoTracks = this.stream.getVideoTracks();
        videoTracks.forEach((track) => {
            track._switchCamera()
         })
      }
 
      render() {
        const txt = this.state.connectingStatus
          return (

             <View style={{flex: 1, width: '100%', height: '100%'}}>
             <StatusBar translucent={true} barStyle={'light-content'}/>
            <View style={{ flex :1, flexDirection : 'column'}}>
              {/* {console.log('Publishers ::::',this.state.publishers, this.state.showLocal)}
              {console.log('cst_connected_state:: ',this.state.callStatus)}
              {console.log('cst_connected_message:: ',this.state.connectingStatus)} */}
                
                <RTCView style={{
                  flex: 1,
                  width: '100%'
                }} objectFit={'cover'} streamURL={this.state.publishers.length !== 0 ? this.state.publishers[0].stream.toURL() : null}/>

                {this.state.publishers.length === 2 ? (

              <RTCView style={{
                  width: '100%',
                  flex: 1
                }} objectFit={'cover'} streamURL={ this.state.publishers[1].stream.toURL() }/>

                ) : (
                  <View style={{flex: 1, width: '100%', justifyContent: 'center'}}>
                          <Image style={{alignSelf: 'center'}} source={this.state.callIconStatus === false ? require('../../assets/images/connecting.png') : require('../../assets/images/ended.png')}></Image>  
                          <Text style={{marginTop: 20, fontWeight: '600', textAlign: 'center'}}> {txt} </Text>
                   </View> 
                ) }
            </View>
           
              <View style={{flexDirection: 'row',position:'absolute', justifyContent: 'center',bottom:25,right:0,left:0}}>
              <View style={{ flexDirection: 'row',justifyContent: 'center'}}>
              { this.state.audioMute ? 
                <Icon
                  raised
                  name='microphone-off'
                  type='material-community'
                  color='grey'
                  onPress={() => this.toggleAudioMute()} /> : 
                <Icon
                  raised
                  name='microphone'
                  type='material-community'
                  color='black'
                  onPress={() => this.toggleAudioMute()} /> }

              { this.state.videoMute ? 
                <Icon
                  raised
                  name='video-off'
                  type='material-community'
                  color='grey'
                  onPress={() => this.toggleVideoMute()} /> : 
                <Icon
                  raised
                  name='video'
                  type='material-community'
                  color='black'
                  onPress={() => this.toggleVideoMute()} /> }

              { this.state.speaker ? 
                <Icon
                  raised
                  name='volume-up'
                  type='FontAwesome'
                  color='black'
                  onPress={() => this.toggleSpeaker()} /> : 
                <Icon
                    raised
                    name='volume-down'
                    type='FontAwesome'
                    color='black'
                    onPress={() => this.toggleSpeaker()} /> }

              <Icon
                raised
                name='video-switch'
                type='material-community'
                color='black'
                onPress={() => this.switchVideoType()} />
              <Icon
                raised
                name='phone-hangup'
                type='material-community'
                color='red'
                onPress={() => this.endCall()} />
            </View>
                    </View>
           
           </View>
              
          );
      }
}
 
export default PatientVideoScreen;
