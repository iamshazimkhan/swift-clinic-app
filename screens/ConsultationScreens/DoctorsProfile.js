import React, {Component} from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Rating} from 'react-native-ratings';
import {View, Text, Image, ScrollView, Alert, Platform} from 'react-native';
import SymptomHeader from '../../components/Headers/SymptomHeader';
import DatePickerTwo from '../../components/Date/DatePickerTwo';
import {Calendar, CalendarList, Agenda} from 'react-native-calendars';
import styles from '../../css/css';
import Button from '../../components/Buttons/button';
import moment from 'moment';
import {
  AddBooking,
  AddPayment,
  getAllUserCards,
  AddNewCard,
  GetMRILocation,
  GetDoctorTime,
  resetConsultation,
} from '../../api/ConsultationApi';
import {SafeAreaView} from 'react-navigation';
import AwesomeAlertComponent from '../../components/Alerts/AwesomeAlertFile';
import SpinnerComponent from '../../components/Spinner/Spinner';
import PaymentModal from '../../components/Modals/PaymentModal';
import ChooseLocation from '../../components/Modals/ChooseLocation';
import PaymentSuccessfull from '../../components/Modals/PaymentSuccessfull';
import SetAvailableTimmingsTwo from '../../components/Modals/SetAvailableTimmingsTwo';
import SvgGraph from '../../components/backgroundgraph';
import CalenderModal from '../../components/Date/CalenderModal';
import PaymentForm from '../PaymentScreens/PaymentForm';
import Toast from 'react-native-simple-toast';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import Moment from 'moment';

class DoctorProfile extends Component {
  state = {
    name: null,
    showDate: 'Select Date',
    arrayholder: [],
    ModalVisibility: false,
    basic_price: null,
    addon_price: null,
    disableButton: false,
    location: null,
    doctor_id: null,
    symptom_id: null,
    AlertComponentVisible: false,
    AlertComponentVisibleTwo: false,
    AlertComponentMessage: null,
    spinnerLoading: false,
    paymentModalVisibility: false,
    cardModalVisibility: false,
    consulation_id: null,
    location_id: null,
    amount: 220,
    currentLongitude: '',
    currentLatitude: '',
    locationStatus: '',
    cardData: [],
    locationData: [],
    defaultCardNumber: 'No Card Found',
    defaultCardID: null,
    cardFlatList: false,
    cardNumber: '4000000000000099',
    CardCVV: 123,
    cardMonth: 10,
    cardYear: 2022,
    cardName: '',
    cardStatus: false,
    PsVisibilityModal: false,
    local_date_show: 'Select Date',
    about: '',
    reviewStar: null,
    total_review: null,
    TimeVisible: false,
    selectTime: 'Select Time',
    TimeData: [],
    specificTimeData: [],
    CalenderModalVisibility: false,
    customCalenderArray: {},
    TimePickerModal: false,
    paid_status: null,
    consultation_id: null,
    profile_pic: null,
    dateOk: false,
  };

  componentDidMount = async () => {
    const profile_pic = await this.props.navigation.getParam(
      'profile_pic',
      'nothing sent',
    );
    const id = await this.props.navigation.getParam('id', 'nothing sent');
    const paid_status = await this.props.navigation.getParam(
      'paid_status',
      'nothing sent',
    );
    const consultation_id = await this.props.navigation.getParam(
      'consultation_id',
      'nothing sent',
    );
    const doctor_id = await this.props.navigation.getParam(
      'doctor_id',
      'nothing sent',
    );
    const symptom_id = await this.props.navigation.getParam(
      'symptom_id',
      'nothing sent',
    );
    const reviewStar = await this.props.navigation.getParam('reviewStar', 1);
    const about = await this.props.navigation.getParam('about', 'nothing sent');
    const total_review = await this.props.navigation.getParam(
      'total_review',
      'No Reviews',
    );

    await this.GetTimeOfDoctor(doctor_id);
    console.log('All params D, S---------->', paid_status);
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    today = mm + '/' + dd + '/' + yyyy;
    await this.setState({
      name: 'Dr. ' + id,
      doctor_id: doctor_id,
      symptom_id: symptom_id,
      about: about,
      reviewStar: reviewStar,
      total_review: total_review,
      paid_status: paid_status,
      consultation_id: consultation_id,
      profile_pic: profile_pic,
    });
  };

  GetTimeOfDoctor = async (doctor_id) => {
    await this.setState({spinnerLoading: true});
    GetDoctorTime(doctor_id)
      .then(async (res) => {
        // console.log('Response Doctor time=====------->',res.data)

        let ras = res.data.data;
        console.log('Time :::::', ras);
        var dateData = {};
        var key;
        let today = new Date();
        let day = moment(today).format('D');
        let month = moment(today).format('MM');
        let year = moment(today).format('YYYY');

        res.data.data.forEach(function (date) {
          key = date.date;
          let obj = {
            // marked: true,
            disabled: false,
            disableTouchEvent: false,
            customStyles: {
              container: {
                // backgroundColor: 'rgba(0, 107, 255, 0.065)',
              },
              text: {
                color: '#33909F',
                fontWeight: 'bold',
              },
            },
          };
          dateData[key] = obj;
        });
        console.log(`${day} / ${month} / ${year}`, 'current', dateData);
        let current = `${year}-${month}-${day}`;
        dateData[current] = {marked: true, disabled: false};
        await this.setState({
          spinnerLoading: false,
          TimeData: res.data.data,
          customCalenderArray: dateData,
        });
      })
      .catch((err) => console.log('Error -------->', err));
  };

  backToScreen = async () => {
    this.props.navigation.goBack(null);
  };

  closeAlertFunctionTwo = async () => {
    this.setState({AlertComponentVisibleTwo: false});
  };

  dateCallback = async (selectedDate) => {
    var gmtDateTime = Moment.utc(selectedDate, 'YYYY-MM-DD');
    var locale_date = gmtDateTime.local().format('YYYY-MM-DD');
    console.log('khurram', moment(locale_date).format('DD/MM/YYYY'));
    var current_time = Moment().local().format('YYYY-MM-DD');
    console.log('After ------>', Moment(locale_date).isAfter(current_time));
    if (Moment(locale_date).isAfter(current_time)) {
      const {TimeData} = this.state;
      let TTimeData = TimeData;
      TTimeData = TTimeData.filter(function (item) {
        return item.date == locale_date;
      }).map(function ({date, start_time, end_time}) {
        return {
          date,
          start_time: Moment.utc('2017-03-13' + ' ' + start_time)
            .local()
            .format('LT'),
          end_time: Moment.utc('2017-03-13' + ' ' + end_time)
            .local()
            .format('LT'),
        };
      });
      console.log('Filter Data -------->', TTimeData);
      this.setState({
        CalenderModalVisibility: false,
        showDate: locale_date,
        local_date_show: locale_date,
        specificTimeData: TTimeData,
        TimeVisible: true,
        selectTime: 'Select Time',
      });
    } else {
      Alert.alert('Please select an appointment time from tomorrow  onwards.');
    }
    console.log('Selected Date Two -------->', selectedDate);
  };

  ShowLocationModal = async () => {
    await this.setState({spinnerLoading: true});
    GetMRILocation()
      .then(async (res) => {
        // console.log('Response Card Data=====------->', res.data.data);
        await this.setState({
          locationData: res.data.data,
          spinnerLoading: false,
          ModalVisibility: true,
        });
      })
      .catch((err) => console.log('Error -------->', err));
  };

  closeLocationModal = async () => {
    this.setState({
      ModalVisibility: false,
      paymentModalVisibility: false,
      cardModalVisibility: false,
    });
  };

  closeCardModal = async () => {
    this.setState({
      cardModalVisibility: false,
      paymentModalVisibility: true,
    });
  };

  onSubmit = async () => {
    if (this.state.paid_status === 1) {
      console.log('Paid');
      this.onResetBooking();
    } else {
      const {
        symptom_id,
        doctor_id,
        local_date_show,
        location,
        location_id,
        selectTime,
      } = this.state;

      if (
        local_date_show === 'Select Date' ||
        location === null ||
        selectTime === 'Select Time'
      ) {
        Alert.alert('Please fill all fields');
      } else {
        let UtcTime = moment(selectTime, 'h:mm:ss A').format('HH:mm:ss');
        console.log('awq', UtcTime, 'dat', local_date_show);
        let time = local_date_show + 'T' + UtcTime + 'Z';
        // console.log('All Data to submit --------- *** ------>',symptom_id,doctor_id,local_date_show,location_id)
        await this.setState({spinnerLoading: true});
        AddBooking(symptom_id, doctor_id, time, location_id)
          .then(async (res) => {
            console.log('Response of Add Booking------>', res.data);
            if (res.data.error === false) {
              await this.setState({consulation_id: res.data.data.id});
              this.getUserCards();
            } else {
              this.setState({spinnerLoading: false});
              Platform.OS === 'android'
                ? Toast.show(
                    'This Doctor already has an appointment booked for this time & date, please choose another',
                    Toast.SHORT,
                  )
                : Alert.alert(
                    'Message',
                    'This Doctor already has an appointment booked for this time & date, please choose another',
                  );
            }
          })
          .catch((err) =>
            this.setState({
              AlertComponentVisibleTwo: true,
              spinnerLoading: false,
              AlertComponentMessage: 'Some error Occured',
            }),
          );
      }
    }
  };

  onResetBooking = async () => {
    const {
      consultation_id,
      doctor_id,
      symptom_id,
      local_date_show,
      selectTime,
    } = this.state;
    if (local_date_show === 'Select Date' || selectTime === 'Select Time') {
      Platform.OS === 'android'
        ? Toast.show('Please select time to continue.', Toast.SHORT)
        : Alert.alert('Please select time to continue.');
    } else {
      console.log(
        'Data before -------->',
        consultation_id,
        doctor_id,
        symptom_id,
        local_date_show,
      );
      await this.setState({spinnerLoading: true});
      resetConsultation(consultation_id, symptom_id, doctor_id, local_date_show)
        .then(async (res) => {
          console.log('Resssss------------>', res.data);
          await this.setState({spinnerLoading: false});
          Platform.OS === 'android'
            ? Toast.show('Booking is successfully completed.', Toast.SHORT)
            : Alert.alert('Booking is successfully completed.');
          this.props.navigation.navigate('Home');
        })
        .catch((err) => console.log('Error -------->', err));
    }
  };

  closeAlertFunction = async () => {
    await console.log('Hello -------> Payment successfull');
    await this.setState({
      AlertComponentVisible: false,
      paymentModalVisibility: false,
      PsVisibilityModal: false,
    });
    this.props.navigation.navigate('QuestionaireScreen', {
      id: this.state.consulation_id,
    });
  };

  onAddPayment = async () => {
    const {consulation_id, amount, defaultCardID, disableButton} = this.state;
    if (disableButton === false) {
      let type = 'new';
      console.log('Payment option is working');
      if (defaultCardID !== null && disableButton === false) {
        console.log('Card ID ------------>', defaultCardID);
        await this.setState({spinnerLoading: true, disableButton: true});
        AddPayment(consulation_id, amount, defaultCardID, type)
          .then(async (res) => {
            console.log('Response =====------->', res.data);
            this.setState({
              AlertComponentMessage: res.data.message,
              AlertComponentVisible: false,
              PsVisibilityModal: true,
              spinnerLoading: false,
              paymentModalVisibility: false,
              disableButton: false,
            });
          })
          .catch((err) => console.log('Error -------->', err));
      } else {
        Alert.alert('Please add card first.');
      }
    }
  };

  AddCard = async () => {
    console.log('Add Card Modal Clicked B', this.state.cardModalVisibility);
    await this.setState({
      paymentModalVisibility: false,
      cardModalVisibility: true,
    });
    console.log('Add Card Modal Clicked A', this.state.cardModalVisibility);
  };

  _onChange = async (form) => {
    console.log(form.valid);
    let expiry = await form.values.expiry;
    let res = await expiry.split('/');
    console.log('Expiry date of card ---->', res);
    await this.setState({
      CardCVV: form.values.cvc,
      cardName: form.values.name,
      cardMonth: res[0],
      cardYear: '20' + res[1],
      cardNumber: form.values.number,
      cardStatus: form.valid,
    });
    await console.log('Card year ----->', this.state.cardYear);
  };

  AddCardApi = async () => {
    console.log('Add Card Api hit');
    const {cardNumber, CardCVV, cardMonth, cardYear, cardName, cardStatus} =
      this.state;
    if (cardStatus === true) {
      this.AddNewCardApi();
    } else {
      // this.setState({spinnerLoading: true})
      Alert.alert('Card Details are incomplete or invalid.');
    }
  };

  onSelectCard = async (item) => {
    console.log('Select Card Clicked', item);
    await this.setState({
      defaultCardNumber: '**** ' + item.card_digit,
      defaultCardID: item.id,
      cardFlatList: false,
      cardName: item.card_name,
    });
  };

  onSelectLocation = async (item) => {
    console.log('Select location Clicked', item);
    await this.setState({
      location: item.address,
      location_id: item.id,
      ModalVisibility: false,
    });
  };

  showCardFlatList = async () => {
    const {cardData, cardFlatList} = this.state;
    if (cardData.length > 0) {
      if (cardFlatList === true) {
        await this.setState({cardFlatList: false});
      } else {
        await this.setState({cardFlatList: true});
      }
    } else {
      Alert.alert('No cards found');
    }
  };

  getUserCards = async () => {
    await this.setState({spinnerLoading: true});
    getAllUserCards()
      .then(async (res) => {
        // console.log('Response Card Data=====------->', res.data.data);
        await this.setState({
          cardData: res.data.data,
          basic_price: res.data.basic_price,
          addon_price: res.data.addon_price,
          spinnerLoading: false,
          paymentModalVisibility: true,
        });
        if (res.data.data.length > 0) {
          // console.log('default card Data ------>', res.data.data[0]);
          await this.setState({
            defaultCardNumber: '**** ' + res.data.data[0].card_digit,
            defaultCardID: res.data.data[0].id,
            cardName: res.data.data[0].card_name,
          });
        }
      })
      .catch((err) => console.log('Error -------->', err));
  };

  AddNewCardApi = async () => {
    const {cardNumber, CardCVV, cardMonth, cardYear, cardName, cardStatus} =
      this.state;
    await this.setState({spinnerLoading: true});
    AddNewCard(cardNumber, CardCVV, cardMonth, cardYear, cardName)
      .then(async (res) => {
        console.log('Response Add Card Data=====------->', res.data.data);
        await this.setState({
          cardModalVisibility: false,
          paymentModalVisibility: true,
          spinnerLoading: false,
        });
        this.getUserCards();
      })
      .catch((err) =>
        this.setState({
          AlertComponentVisibleTwo: true,
          spinnerLoading: false,
          AlertComponentMessage: 'Card Details are not correct.',
        }),
      );
  };

  startTimeCallback = (StartTime) => {
    // let TimeForDay = await this.state.specificTimeData;
    const {showDate} = this.state;
    // let d4 = Moment(StartTime.getTime());
    // let finalStartTime = d4.format('LT');
    // let startTimeFinal = d4.format('HH:mm');
    // var momentObj = await Moment(showDate + startTimeFinal, 'YYYY-MM-DDLT');
    // const u = await Moment.utc(showDate + ' ' + startTimeFinal);
    // const su = await Moment.utc(showDate + ' ' + startTimeFinal).format('LLL');
    this.setState({
      selectTime: StartTime,
      local_date_show: showDate,
      selectTime: StartTime,
      showDate: `${showDate}  ${StartTime}`,
    });
  };

  addTime = async () => {
    const {selectTime} = this.state;
    if (selectTime === 'Select Time') {
      Alert.alert('Please select time first.');
    } else {
      await this.setState({TimeVisible: false});
    }
  };

  closeTimeModal = async () => {
    await this.setState({TimeVisible: false});
  };

  clickSelectedDate = async (day) => {
    console.log('Selected Date -------->', day.dateString);
  };

  showCalender = async () => {
    console.log('Hellooooo');
    this.setState({CalenderModalVisibility: true});
  };

  closeCalenderModal = async () => {
    console.log('Closseeee');
    this.setState({CalenderModalVisibility: false});
  };

  // ------------------------------------- Time Picker ---------------------

  showDatePicker = async () => {
    await console.log('Time Picker');
    await this.setState({TimePickerModal: true, TimeVisible: false});
  };

  hideDatePicker = async () => {
    await console.log('Time Picker Close');
    await this.setState({TimePickerModal: false, TimeVisible: true});
  };

  handleConfirm = async (StartTime) => {
    const {showDate} = this.state;
    const {specificTimeData} = this.state;
    console.log('Specific Time Data ------------->', specificTimeData);
    const u = await Moment.utc(StartTime).local().format('LT');
    var amIBetween = false;

    console.log('Loooooppppinggg--------------------------', amIBetween);

    this.seTime(StartTime);
  };

  seTime = async (StartTime) => {
    this.setState({TimeVisible: false});
    this.startTimeCallback(StartTime);
  };

  render() {
    return (
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: '#fff',
          opacity:
            this.state.ModalVisibility ||
            this.state.cardModalVisibility ||
            this.state.paymentModalVisibility ||
            this.state.CalenderModalVisibility ||
            this.state.TimeVisible
              ? 0.2
              : 1,
        }}>
        <ScrollView>
          <SvgGraph style={{right: -38}} />
          <View style={{marginTop: -280}}>
            {this.state.TimePickerModal ? (
              <DateTimePickerModal
                headerTextIOS="Pick a time"
                isVisible={this.state.TimePickerModal}
                mode="time"
                onConfirm={(date) => this.handleConfirm(date)}
                onCancel={this.hideDatePicker}
              />
            ) : null}

            {this.state.TimeVisible ? (
              <SetAvailableTimmingsTwo
                specificTimeData={this.state.specificTimeData}
                closeModal={this.closeTimeModal}
                TimeVisible={this.state.TimeVisible}
                showDatePicker={this.showDatePicker}
                onClick={this.addTime}
                id={this.state.doctor_id}
                setState={this.handleConfirm}
                dat={this.state.showDate}
                name={this.state.name}
                showCalender={this.showCalender}
                Date={
                  this.state.selectTime === 'Select Time'
                    ? 'Select Time'
                    : this.state.selectTime
                }></SetAvailableTimmingsTwo>
            ) : null}

            {/* --------------------------- Header ----------------------- */}
            <SymptomHeader
              title="Our Doctors"
              Back={this.backToScreen}></SymptomHeader>
            <View style={styles.mainBox}>
              <View style={{flex: 1}}>
                <Image
                  style={styles.dp}
                  source={{
                    uri:
                      this.state.profile_pic === null
                        ? 'https://tvscheduleindia.com/avatar_doctor.png'
                        : 'https://platform.swiftclinic.com/storage/profile/' +
                          this.state.profile_pic,
                  }}></Image>
              </View>
              <View
                style={{padding: 10, flex: 3, marginLeft: 10, marginTop: 20}}>
                <Text style={{fontWeight: 'bold', fontSize: 17}}>
                  {' '}
                  {this.state.name}
                </Text>
                <View style={{flexDirection: 'row'}}>
                  <Rating
                    style={{
                      paddingTop: 10,
                      paddingBottom: 10,
                      alignSelf: 'flex-start',
                    }}
                    readonly={true}
                    imageSize={15}
                    startingValue={this.state.reviewStar}
                  />
                  <Text
                    style={{marginTop: 7, marginLeft: 10, color: '#646464'}}>
                    {'     ' + this.state.total_review + ' Reviews '}
                  </Text>
                </View>
              </View>
            </View>
            {/* <Text
              style={{
                padding: 20,
                fontSize: 15,
                color: 'grey',
                fontFamily: 'Axiforma-Thin',
                lineHeight: 25.2,
              }}>
              {this.state.about}
            </Text> */}
            <Text
              style={{
                paddingRight: 20,
                paddingTop: 20,
                paddingLeft: 20,
                fontSize: 12,
                color: '#171717',
                fontFamily: 'Axiforma-Regular',
                lineHeight: 25.2,
              }}>
              This content is dependent upon what the doctor has entered on
              their profile.
            </Text>

            {/* ----------------------------------Date Time ----------------------------  */}

            <View style={{margin: 20}}>
              <Text style={{fontWeight: 'bold', fontSize: 17, marginTop: 20}}>
                Select date and time:
              </Text>
              <DatePickerTwo
                Date={this.state.showDate}
                Time={this.state.selectTime}
                showCalender={this.showCalender}
              />
              {this.state.CalenderModalVisibility ? (
                <CalenderModal
                  disabled={true}
                  name={this.state.name}
                  disabledDays={true}
                  customDateArray={this.state.customCalenderArray}
                  closeModal={this.closeCalenderModal}
                  Visibilty={this.state.CalenderModalVisibility}
                  selectedDateCallback={this.dateCallback}></CalenderModal>
              ) : null}
            </View>

            {/* ------------------------------- Scan Location --------------------------------- */}

            {this.state.PsVisibilityModal ? (
              <PaymentSuccessfull
                setPaymentComplete={this.closeAlertFunction}
                PSVisibility={
                  this.state.PsVisibilityModal
                }></PaymentSuccessfull>
            ) : null}

            {this.state.paymentModalVisibility ? (
              <PaymentModal
                amount={
                  this.state.basic_price ? '£' + this.state.basic_price : '£450'
                }
                extra={false}
                disableButton={this.state.disableButton}
                showCardFaltList={this.showCardFlatList}
                defaultCardName={this.state.cardName}
                defaultCardNumber={this.state.defaultCardNumber}
                onSelectListCard={this.onSelectCard}
                Visible={this.state.cardFlatList}
                cardData={this.state.cardData}
                addCard={this.AddCard}
                Visibilty={this.state.paymentModalVisibility}
                closeModal={this.closeLocationModal}
                addPayment={this.onAddPayment}></PaymentModal>
            ) : null}

            <ChooseLocation
              onSelectListCard={this.onSelectLocation}
              cardData={this.state.locationData}
              addCard={this.AddCard}
              Visibilty={this.state.ModalVisibility}
              closeModal={this.closeLocationModal}></ChooseLocation>

            {/* <LocationModal findLocation={this.locationGet} Visibilty={this.state.ModalVisibility} closeModal={this.closeLocationModal} locationValue={this.state.location} onChangeValue={(location) => this.setState({location})} setLocation={this.setLocationOn}></LocationModal> */}
            <AwesomeAlertComponent
              title="Message"
              visibleAlert={this.state.AlertComponentVisible}
              closeAlert={this.closeAlertFunction}
              message={
                this.state.AlertComponentMessage
              }></AwesomeAlertComponent>
            <AwesomeAlertComponent
              title="Message"
              visibleAlert={this.state.AlertComponentVisibleTwo}
              closeAlert={this.closeAlertFunctionTwo}
              message={
                this.state.AlertComponentMessage
              }></AwesomeAlertComponent>
            <SpinnerComponent
              Visible={this.state.spinnerLoading}></SpinnerComponent>

            {this.state.cardModalVisibility ? (
              <PaymentForm
                paymentSpinner={this.state.spinnerLoading}
                closeModal={this.closeCardModal}
                onClick={this.AddCardApi}
                _onChange={this._onChange}
                Visibilty={this.state.cardModalVisibility}></PaymentForm>
            ) : null}

            {/* -------------------------------Location Modal -------------------------------- */}

            <View style={{marginHorizontal: 20}}>
              <Text style={{fontWeight: 'bold', fontSize: 17}}>
                Scan location:
              </Text>
              {console.log('Xcccc', `${this.state.location}`)}
              <View
                onStartShouldSetResponder={this.ShowLocationModal}
                style={[
                  styles.inputText,
                  {
                    alignItems: 'center',
                    flexDirection: 'row',
                    position: 'relative',
                  },
                ]}>
                <Text style={{flexDirection: 'row', color: '#646464'}}>
                  {this.state.location &&
                    this.state.location.replace(/(\r\n|\n|\r)/gm, ', ')}
                </Text>
                <Image
                  style={{position: 'absolute', top: '45%', right: 17}}
                  source={require('../../assets/angle-right.png')}
                />
              </View>
            </View>
            <View style={styles.sideMargin}>
              <Button
                borderRadius={10}
                title="Confirm"
                onClick={this.onSubmit}></Button>
            </View>
          </View>
        </ScrollView>
        {/* </BlurView> */}
      </SafeAreaView>
    );
  }
}

export default DoctorProfile;
