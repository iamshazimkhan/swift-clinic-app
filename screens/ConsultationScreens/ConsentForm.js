import React, {Component} from 'react';
import {View, Text, TextInput, StyleSheet, Alert} from 'react-native';
import MainHeader from '../../components/Headers/MainHeader';
import CheckBox from '@react-native-community/checkbox';
import Toast from 'react-native-simple-toast';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import Button from '../../components/Buttons/button';
import {SafeAreaView} from 'react-navigation';
import {AddConsentForm} from '../../api/ConsultationApi';
import SpinnerComponent from '../../components/Spinner/Spinner';
import SvgGraph from '../../components/backgroundgraph';
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from 'react-native-simple-radio-button';

export default class ConsentForm extends Component {
  state = {
    name: null,
    weight: null,
    height: null,
    dob: null,
    address: null,
    phone: null,
    one: null,
    two: null,
    three: null,
    four: null,
    five: null,
    six: null,
    seven: null,
    eight: null,
    nine: null,
    ten: null,
    eleven: null,
    tweleve: null,
    thirteen: null,
    fourteen: null,
    fifteen: null,
    sixteen: null,
    seventeen: null,
    eighteen: null,
    nineteen: null,
    twenty: null,
    twentyone: null,
    twentytwo: null,
    twentythree: null,
    twentyfour: null,
    twentyfive: null,
    twentysix: null,
    twentyseven: null,
    twentyeight: null,
    twentynine: null,
    thirty: null,
    spinnerLoading: false,
    consultation_id: null,
    value: null,
  };

  backToScreen = async () => {
    this.props.navigation.goBack(null);
  };

  componentDidMount = async () => {
    await console.log('Id from Payment is -------->');
    const id = await this.props.navigation.getParam('id', 'nothing sent');
    await this.setState({consultation_id: id});
    console.log('Id from Payment is -------->', id);
  };

  onSubmit = async () => {
    const {name, phone, address, weight, height, dob} = this.state;
    if (
      name === null ||
      phone === null ||
      address === null ||
      weight === null ||
      height === null ||
      dob === null
    ) {
      Alert.alert('Please Fill personal details.');
    } else {
      await this.setState({spinnerLoading: true});
      const d = await this.state;
      const consultation_id = await this.state.consultation_id;
      console.log('Response =====------->', consultation_id);
      AddConsentForm(d, consultation_id)
        .then(async (res) => {
          console.log('Response =====------->', res.data.message);
          await this.setState({
            spinnerLoading: false,
          });
          Toast.show('Form submitted successfully.');
          this.props.navigation.navigate('Home');
        })
        .catch((err) => console.log('Error -------->', err));
    }
  };

  // ----------------------------- Params -----------------------------

  radio_props = [
    {label: 'Yes  ', value: true},
    {label: 'No', value: false},
  ];

  render() {
    return (
      <KeyboardAwareScrollView>
        <SafeAreaView style={{flex: 1, backgroundColor: '#ffffff'}}>
        <SvgGraph style={{right:-38}} />
      <View style={{marginTop: -280}}>
          <MainHeader Back={this.backToScreen}></MainHeader>
          <SpinnerComponent
            Visible={this.state.spinnerLoading}></SpinnerComponent>
          <View style={{margin: 20}}>
            <Text style={{marginLeft: 20, marginBottom: 10}}>
              Personal Details:
            </Text>
            <TextInput
              onChangeText={(name) => this.setState({name})}
              placeholder={'Enter your name'}
              style={styles.input}
            />
            <View
              style={{flexDirection: 'row', marginRight: 20, marginLeft: 20}}>
              <TextInput
                onChangeText={(weight) => this.setState({weight})}
                placeholder={'Your weight'}
                style={styles.inputhalf}
              />
              <TextInput
                onChangeText={(height) => this.setState({height})}
                placeholder={'Your height'}
                style={styles.inputhalf}
              />
            </View>

            <TextInput
              onChangeText={(address) => this.setState({address})}
              placeholder={'Enter your address'}
              style={styles.input}
            />
            <TextInput
              onChangeText={(phone) => this.setState({phone})}
              placeholder={'Enter your Phone'}
              style={styles.input}
            />
            <TextInput
              onChangeText={(dob) => this.setState({dob})}
              placeholder={'Enter your birth date'}
              style={styles.input}
            />
          </View>

          <View style={{marginRight: 20, marginLeft: 20}}>
            <Text style={{marginLeft: 20, marginBottom: 10}}>
              Safety Questions:
            </Text>

            {/* //--------------- One ---------------------// */}

            <View style={{marginLeft: 20}}>
              <Text style={styles.question}>
                1. Do you wear hearing aids or do you have hearing difficulties
                ?
              </Text>
              <View style={{flexDirection: 'row'}}>
                <View style={{flexDirection: 'row', justifyContent: 'center'}}>
                  <RadioForm
                    idSeparator=","
                    radio_props={this.radio_props}
                    onPress={(value) => {
                      this.setState({one: value});
                    }}
                    formHorizontal={true}
                    initial={-1}
                  />
                </View>
              </View>

              {this.state.one === true ? (
                <View style={{marginLeft: 20, marginRight: 20}}>
                  <Text>If ‘yes’ have your hearing aids been removed?</Text>
                  <View style={{flexDirection: 'row'}}>
                    <View
                      style={{flexDirection: 'row', justifyContent: 'center'}}>
                  <RadioForm
                    idSeparator=","
                    radio_props={this.radio_props}
                    onPress={(value) => {
                      this.setState({two: value});
                    }}
                    formHorizontal={true}
                    initial={-1}
                  />
                    </View>
                  </View>
                </View>
              ) : null}
            </View>

            {/* //--------------- Three ---------------------// */}

            <View style={{marginLeft: 20}}>
              <Text style={styles.question}>
                2. Do you have any of the following implants?
              </Text>

              <View style={{marginLeft: 20, marginRight: 20}}>
                <Text>
                  Cardiac (heart) pacemaker and/or internal cardiac
                  defibrillator ?
                </Text>
                <View style={{flexDirection: 'row'}}>
                  <View
                    style={{flexDirection: 'row', justifyContent: 'center'}}>
                  <RadioForm
                    idSeparator=","
                    radio_props={this.radio_props}
                    onPress={(value) => {
                      this.setState({three: value});
                    }}
                    formHorizontal={true}
                    initial={-1}
                  />
                  </View>
                </View>
              </View>

              {/* ------------------ Four ---------------------------- */}

              <View style={{marginLeft: 20, marginRight: 20}}>
                <Text>Artificial heart valve or Loop Recorder device ?</Text>
                <View style={{flexDirection: 'row'}}>
                  <View
                    style={{flexDirection: 'row', justifyContent: 'center'}}>
                  <RadioForm
                    idSeparator=","
                    radio_props={this.radio_props}
                    onPress={(value) => {
                      this.setState({four: value});
                    }}
                    formHorizontal={true}
                    initial={-1}
                  />
                  </View>
                </View>
              </View>

              {/* ------------------ Five ---------------------------- */}

              <View style={{marginLeft: 20, marginRight: 20}}>
                <Text>Aneurysm clips in your brain?</Text>
                <View style={{flexDirection: 'row'}}>
                  <View
                    style={{flexDirection: 'row', justifyContent: 'center'}}>
                  <RadioForm
                    idSeparator=","
                    radio_props={this.radio_props}
                    onPress={(value) => {
                      this.setState({five: value});
                    }}
                    formHorizontal={true}
                    initial={-1}
                  />
                  </View>
                </View>
              </View>

              {/* ------------------ Six ---------------------------- */}

              <View style={{marginLeft: 20, marginRight: 20}}>
                <Text>Programmable hydrocephalus shunt?</Text>
                <View style={{flexDirection: 'row'}}>
                  <View
                    style={{flexDirection: 'row', justifyContent: 'center'}}>
                  <RadioForm
                    idSeparator=","
                    radio_props={this.radio_props}
                    onPress={(value) => {
                      this.setState({six: value});
                    }}
                    formHorizontal={true}
                    initial={-1}
                  />
                  </View>
                </View>
              </View>

              {/* ------------------ Seven ---------------------------- */}

              <View style={{marginLeft: 20, marginRight: 20}}>
                <Text>Cochlear or other ear implant?</Text>
                <View style={{flexDirection: 'row'}}>
                  <View
                    style={{flexDirection: 'row', justifyContent: 'center'}}>
                  <RadioForm
                    idSeparator=","
                    radio_props={this.radio_props}
                    onPress={(value) => {
                      this.setState({seven: value});
                    }}
                    formHorizontal={true}
                    initial={-1}
                  />
                  </View>
                </View>
              </View>

              {/* ------------------ Eight ---------------------------- */}

              <View style={{marginLeft: 20, marginRight: 20}}>
                <Text>A stent inserted in any part of your body?</Text>
                <View style={{flexDirection: 'row'}}>
                  <View
                    style={{flexDirection: 'row', justifyContent: 'center'}}>
                  <RadioForm
                    idSeparator=","
                    radio_props={this.radio_props}
                    onPress={(value) => {
                      this.setState({eight: value});
                    }}
                    formHorizontal={true}
                    initial={-1}
                  />
                  </View>
                </View>
              </View>

              {/* ------------------ Nine ---------------------------- */}

              <View style={{marginLeft: 20, marginRight: 20}}>
                <Text>Implanted pain control or drug infusion device?</Text>
                <View style={{flexDirection: 'row'}}>
                  <View
                    style={{flexDirection: 'row', justifyContent: 'center'}}>
                  <RadioForm
                    idSeparator=","
                    radio_props={this.radio_props}
                    onPress={(value) => {
                      this.setState({nine: value});
                    }}
                    formHorizontal={true}
                    initial={-1}
                  />
                  </View>
                </View>
              </View>

              {/* ------------------ Ten ---------------------------- */}

              <View style={{marginLeft: 20, marginRight: 20}}>
                <Text>
                  Clips, pins, plates, joint replacements or embolisation coils?
                </Text>
                <View style={{flexDirection: 'row'}}>
                  <View
                    style={{flexDirection: 'row', justifyContent: 'center'}}>
                  <RadioForm
                    idSeparator=","
                    radio_props={this.radio_props}
                    onPress={(value) => {
                      this.setState({ten: value});
                    }}
                    formHorizontal={true}
                    initial={-1}
                  />
                  </View>
                </View>
              </View>

              {/* ------------------ Eleven ---------------------------- */}

              <View style={{marginLeft: 20, marginRight: 20}}>
                <Text>Have you had any operations in the last 6 weeks?</Text>
                <View style={{flexDirection: 'row'}}>
                  <View
                    style={{flexDirection: 'row', justifyContent: 'center'}}>
                  <RadioForm
                    idSeparator=","
                    radio_props={this.radio_props}
                    onPress={(value) => {
                      this.setState({eleven: value});
                    }}
                    formHorizontal={true}
                    initial={-1}
                  />
                  </View>
                </View>
              </View>
            </View>

            {/* //--------------- Tweleve ---------------------// */}

            <View style={{marginLeft: 20}}>
              <Text style={styles.question}>
                3. Have you ever had any metal fragments go into your eyes?
              </Text>
              <View style={{flexDirection: 'row'}}>
                <View style={{flexDirection: 'row', justifyContent: 'center'}}>
                <RadioForm
                    idSeparator=","
                    radio_props={this.radio_props}
                    onPress={(value) => {
                      this.setState({tweleve: value});
                    }}
                    formHorizontal={true}
                    initial={-1}
                  />
                </View>
              </View>

              {/* ----------------------------- Thirteen ------------------------ */}
              {this.state.tweleve === true ? (
                <View>
                  <View style={{marginLeft: 20, marginRight: 20}}>
                    <Text>
                      If ‘yes’ did you receive medical advice from a Doctor?
                    </Text>
                    <View style={{flexDirection: 'row'}}>
                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'center',
                        }}>
                  <RadioForm
                    idSeparator=","
                    radio_props={this.radio_props}
                    onPress={(value) => {
                      this.setState({thirteen: value});
                    }}
                    formHorizontal={true}
                    initial={-1}
                  />
                      </View>
                    </View>
                  </View>

                  {/* ----------------------------- Fourteen ------------------------ */}

                  <View style={{marginLeft: 20, marginRight: 20}}>
                    <Text>Was everything completely removed?</Text>
                    <View style={{flexDirection: 'row'}}>
                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'center',
                        }}>
                  <RadioForm
                    idSeparator=","
                    radio_props={this.radio_props}
                    onPress={(value) => {
                      this.setState({fourteen: value});
                    }}
                    formHorizontal={true}
                    initial={-1}
                  />
                      </View>
                    </View>
                  </View>
                </View>
              ) : null}
            </View>

            {/* ----------------------- Fifteen ------------------------ */}

            <View style={{marginLeft: 20}}>
              <Text style={styles.question}>
                4. Have you ever had any other surgery?
              </Text>
              <View style={{flexDirection: 'row'}}>
                <View style={{flexDirection: 'row', justifyContent: 'center'}}>
                <RadioForm
                    idSeparator=","
                    radio_props={this.radio_props}
                    onPress={(value) => {
                      this.setState({fifteen: value});
                    }}
                    formHorizontal={true}
                    initial={-1}
                  />
                </View>
              </View>

              {/* ----------------------------- Sixteen ------------------------ */}
              {this.state.fifteen === true ? (
                <View>
                  <TextInput
                    onChangeText={(sixteen) => this.setState({sixteen})}
                    placeholder={'Enter details'}
                    style={styles.input}
                  />
                </View>
              ) : null}
            </View>

            {/* ------------------------------- Seventeen ----------------------------- */}

            <View style={{marginLeft: 20}}>
              <Text style={styles.question}>
                5. Have you had any shrapnel or gunshot/bomb blast injuries?
              </Text>
              <View style={{flexDirection: 'row'}}>
                <View style={{flexDirection: 'row', justifyContent: 'center'}}>
                <RadioForm
                    idSeparator=","
                    radio_props={this.radio_props}
                    onPress={(value) => {
                      this.setState({seventeen: value});
                    }}
                    formHorizontal={true}
                    initial={-1}
                  />
                </View>
              </View>
            </View>

            {/* ------------------------------- Eighteen ----------------------------- */}

            <View style={{marginLeft: 20}}>
              <Text style={styles.question}>
                6. Are you, or could you be pregnant?
              </Text>
              <View style={{flexDirection: 'row'}}>
                <View style={{flexDirection: 'row', justifyContent: 'center'}}>
                <RadioForm
                    idSeparator=","
                    radio_props={this.radio_props}
                    onPress={(value) => {
                      this.setState({eighteen: value});
                    }}
                    formHorizontal={true}
                    initial={-1}
                  />
                </View>
              </View>
            </View>

            {/* -------------------------------- Nineteen --------------------------------------- */}

            <View style={{marginLeft: 20}}>
              <Text style={styles.question}>
                7. Have you had a procedure involving swallowing a small camera?
              </Text>
              <View style={{flexDirection: 'row'}}>
                <View style={{flexDirection: 'row', justifyContent: 'center'}}>
                <RadioForm
                    idSeparator=","
                    radio_props={this.radio_props}
                    onPress={(value) => {
                      this.setState({nineteen: value});
                    }}
                    formHorizontal={true}
                    initial={-1}
                  />
                </View>
              </View>

              {/* ----------------------------- Twenty ------------------------ */}

              {this.state.nineteen === true ? (
                <View>
                  <View style={{marginLeft: 20, marginRight: 20}}>
                    <Text>If ‘yes’ did you ‘pass’ the camera?</Text>
                    <View style={{flexDirection: 'row'}}>
                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'center',
                        }}>
                  <RadioForm
                    idSeparator=","
                    radio_props={this.radio_props}
                    onPress={(value) => {
                      this.setState({twenty: value});
                    }}
                    formHorizontal={true}
                    initial={-1}
                  />
                      </View>
                    </View>
                  </View>

                  {/* ----------------------------- Twenty One ------------------------ */}

                  <View style={{marginLeft: 20, marginRight: 20}}>
                    <Text>
                      If ‘yes’ might it still be present in your body?
                    </Text>
                    <View style={{flexDirection: 'row'}}>
                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'center',
                        }}>
                  <RadioForm
                    idSeparator=","
                    radio_props={this.radio_props}
                    onPress={(value) => {
                      this.setState({twentyone: value});
                    }}
                    formHorizontal={true}
                    initial={-1}
                  />
                      </View>
                    </View>
                  </View>
                </View>
              ) : null}
            </View>

            {/* --------------------------- Twenty Two ---------------------- */}

            <View style={{marginLeft: 20}}>
              <Text style={styles.question}>
                8. Do you have any new tattoos less than 2 weeks old/still
                scabbing?
              </Text>
              <View style={{flexDirection: 'row'}}>
                <View style={{flexDirection: 'row', justifyContent: 'center'}}>
                <RadioForm
                    idSeparator=","
                    radio_props={this.radio_props}
                    onPress={(value) => {
                      this.setState({twentytwo: value});
                    }}
                    formHorizontal={true}
                    initial={-1}
                  />
                </View>
              </View>
            </View>

            {/* --------------------------- Twenty Three ---------------------- */}

            <View style={{marginLeft: 20}}>
              <Text style={styles.question}>
                9. Do you have any other tattoos? (in addition to any new
                tattoos )
              </Text>
              <View style={{flexDirection: 'row'}}>
                <View style={{flexDirection: 'row', justifyContent: 'center'}}>
                <RadioForm
                    idSeparator=","
                    radio_props={this.radio_props}
                    onPress={(value) => {
                      this.setState({twentythree: value});
                    }}
                    formHorizontal={true}
                    initial={-1}
                  />
                </View>
              </View>
            </View>

            {/* --------------------------- Twenty Four ---------------------- */}

            <View style={{marginLeft: 20}}>
              <Text style={styles.question}>10. Are you breastfeeding?</Text>
              <View style={{flexDirection: 'row'}}>
                <View style={{flexDirection: 'row', justifyContent: 'center'}}>
                <RadioForm
                    idSeparator=","
                    radio_props={this.radio_props}
                    onPress={(value) => {
                      this.setState({twentyfour: value});
                    }}
                    formHorizontal={true}
                    initial={-1}
                  />
                </View>
              </View>
            </View>

            {/* --------------------------- Twenty Five ---------------------- */}

            <View style={{marginLeft: 20}}>
              <Text style={styles.question}>
                11. Do you have any of the following conditions? (Epilepsy,
                blackouts, angina, asthma)
              </Text>
              <View style={{flexDirection: 'row'}}>
                <View style={{flexDirection: 'row', justifyContent: 'center'}}>
                <RadioForm
                    idSeparator=","
                    radio_props={this.radio_props}
                    onPress={(value) => {
                      this.setState({twentyfive: value});
                    }}
                    formHorizontal={true}
                    initial={-1}
                  />
                </View>
              </View>
            </View>

            {/* --------------------------- Twenty Six ---------------------- */}

            <View style={{marginLeft: 20}}>
              <Text style={styles.question}>
                12. Are you wearing false magnetic eyelashes?
              </Text>
              <View style={{flexDirection: 'row'}}>
                <View style={{flexDirection: 'row', justifyContent: 'center'}}>
                <RadioForm
                    idSeparator=","
                    radio_props={this.radio_props}
                    onPress={(value) => {
                      this.setState({twentysix: value});
                    }}
                    formHorizontal={true}
                    initial={-1}
                  />
                </View>
              </View>
            </View>

            {/* --------------------------- Twenty Seven ---------------------- */}

            <View style={{marginLeft: 20}}>
              <Text style={styles.question}>
                13. Do you have any other implants e.g. pessary ?
              </Text>
              <View style={{flexDirection: 'row'}}>
                <View style={{flexDirection: 'row', justifyContent: 'center'}}>
                <RadioForm
                    idSeparator=","
                    radio_props={this.radio_props}
                    onPress={(value) => {
                      this.setState({twentyseven: value});
                    }}
                    formHorizontal={true}
                    initial={-1}
                  />
                </View>
              </View>
            </View>
            <Button title="Submit" onClick={this.onSubmit}></Button>
          </View>
          </View>
        </SafeAreaView>
      </KeyboardAwareScrollView>
    );
  }
}

const styles = StyleSheet.create({
  question: {
    fontWeight: '900',
    fontSize: 16,
    marginTop: 10,
    color: '#4c73a8',
  },
  input: {
    marginTop: 5,
    paddingLeft: 10,
    borderRadius: 8,
    height: 50,
    alignSelf: 'stretch',
    marginLeft: 20,
    marginRight: 20,
    borderWidth: 1,
    borderColor: '#dcecef',
    marginBottom: 10,
    fontFamily : 'Axiforma-Thin'
  },
  button: {
    borderRadius: 9,
    marginLeft: 80,
    marginBottom: 20,
    height: 50,
    justifyContent: 'center',
    marginRight: 80,
    marginTop: 15,
    backgroundColor: '#0158ff',
    alignItems: 'center',
    alignSelf: 'stretch',
  },
  inputhalf: {
    marginLeft: 1,
    flex: 1,
    marginTop: 5,
    paddingLeft: 10,
    borderRadius: 8,
    height: 50,
    borderWidth: 1,
    borderColor: '#dcecef',
    marginBottom: 10,
    fontFamily : 'Axiforma-Thin'
  },
});
