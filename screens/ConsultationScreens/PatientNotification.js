import React, {Component} from 'react';
import {
  View,
  Text,
  FlatList,
  TouchableNativeFeedback,
  ScrollView,
} from 'react-native';
import TitleImage from '../../components/Headers/titleImage';
import NotificationBox from '../../components/consultations/NotificationBox';
import {SafeAreaView} from 'react-navigation';
import {
  GetPatientNotifications,
  seenNotification,
  confirmRescheduleDoctor,
  confirmReschedulePatient,
} from '../../api/ConsultationApi';
import SpinnerComponent from '../../components/Spinner/Spinner';
import Toast from 'react-native-simple-toast';
import NoData from '../../components/NoData';
import AcceptRejectModal from '../../components/Modals/AcceptRejectModal';
import SvgGraph from '../../components/backgroundgraph';
import Moment from 'moment';

export default class PatientNotification extends Component {
  state = {
    data: [],
    spinnerLoading: false,
    noData: false,
    timeModalVisibility: false,
    selectedItem: null,
    text: '',
    consultation_id: null,
    GMTTime: null,
  };

  componentDidMount = async () => {
    await this.getAllNotification();
    this.willFocusSubscription = this.props.navigation.addListener(
      'willFocus',
      async () => {
        console.log(
          'Console Data ----------->',
          Math.floor(Math.random() * 100) + 1,
        );
        await this.getAllNotification();
      },
    );
  };

  componentWillUnmount() {
    this.willFocusSubscription.remove();
  }

  getAllNotification = async () => {
    await this.setState({spinnerLoading: true});
    GetPatientNotifications()
      .then(async (res) => {
        console.log('Response =====------->', res.data.data);
        if (res.data.data.length > 0) {
          this.setState({noData: false});
        } else {
          this.setState({noData: true});
        }
        await this.setState({data: res.data.data, spinnerLoading: false});
      })
      .catch((err) => console.log('Error -------->', err));
  };

  formatDateTime(datetime) {
    return Moment.utc(datetime).local().format('LL');
  }

  onClick = async (item) => {
    // console.log('Symptom Id------->',symptom_id, 'Parsed',d);
    // console.log('Consultation id  ------->',LocalTime);

    if (item.read === 1) {
      console.log('Read --------> 1', item.type);
      /** ------------------------- Navigate Data -------------- */

      this.navigationFuncation(item);
      /** ------------------------------------------------------------ */
    } else {
      console.log('Read --------> 0');
      this.setSeenNotification(item);
    }
  };

  navigationFuncation = async (item) => {
    const finalData = item.data;
    const data = await JSON.parse(JSON.stringify(finalData));
    const d = JSON.parse(data);
    const LocalTime = await d.consultation_id;
    console.log('Item Details:', data);
    const symptom_id = await d.symptom_id;
    console.log('Type -------->', item.type);
    if (item.type === 'update_consultation_schedule') {
      this.props.navigation.navigate('ConsultationDetails', {id: LocalTime});
    } else if (item.type === 'doctor_decline_consultancy') {
      this.props.navigation.navigate('DoctorsListScreen', {
        id: symptom_id,
        paid_status: 1,
        consultation_id: LocalTime,
      });
    } else if (item.consultation_status === 'accepted_by_doctor') {
      this.props.navigation.navigate('ConsultationDetails', {id: LocalTime});
    } else if (item.type === 'doctor_accepted_consultancy') {
      this.props.navigation.navigate('ConsultationDetails', {id: LocalTime});
    } else if (item.type === 'new_consultation_booked') {
      this.props.navigation.navigate('ConsultationDetails', {id: LocalTime});
    } else {
      Toast.show('Notification already read');
      this.getAllNotification();
    }
  };

  setSeenNotification = async (item) => {
    this.setState({spinnerLoading: true});
    seenNotification(item.id)
      .then(async (res) => {
        this.setState({spinnerLoading: false});
        this.navigationFuncation(item);
      })
      .catch((err) => console.log('Error -------->', err));
  };

  onAcceptRequest = () => {
    let status = 1;
    this.setRescheduleStatus(status);
  };

  onRejectRequest = () => {
    let status = 0;
    this.setRescheduleStatus(status);
  };

  setRescheduleStatus = async (status) => {
    this.setState({spinnerLoading: true});
    const {selectedItem, consultation_id} = this.state;
    confirmReschedulePatient(consultation_id, status)
      .then(async (res) => {
        console.log('Response --------->', res.data);
        this.setState({timeModalVisibility: false});
        await this.setSeenNotification(selectedItem);
      })
      .catch((err) => console.log('Error -------->', err));
  };

  closeModal = async () => {
    await this.setState({timeModalVisibility: false});
  };

  render() {
    return (
      <ScrollView style={{flex: 1, backgroundColor: '#fff'}}>
        <AcceptRejectModal
          closeModal={this.closeModal}
          timeModal={this.state.timeModalVisibility}
          text={this.state.text}
          onAccept={this.onAcceptRequest}
          onReject={this.onRejectRequest}></AcceptRejectModal>
        <SafeAreaView style={{flex: 1}}>
          <SvgGraph style={{right: -38}} />
          <View style={{marginTop: -270}}>
            <TitleImage></TitleImage>
            <Text
              style={{
                textAlign: 'center',
                marginTop: 40,
                textAlign: 'center',
                fontFamily: 'Axiforma-Medium',
                fontSize: 19,
              }}>
              Notifications
            </Text>
            <View style={{padding: 20}}>
              {this.state.noData ? (
                <NoData></NoData>
              ) : (
                <FlatList
                  scrollEnabled={false}
                  data={this.state.data}
                  horizontal={false}
                  numColumns={1}
                  keyExtractor={(item) => item.id.toString()}
                  renderItem={({item}) => (
                    <TouchableNativeFeedback onPress={() => this.onClick(item)}>
                      <NotificationBox
                        iCon="#33909F"
                        title={item.title}
                        text={item.text}
                        colour={item.read}></NotificationBox>
                    </TouchableNativeFeedback>
                  )}
                />
              )}
              <SpinnerComponent
                Visible={this.state.spinnerLoading}></SpinnerComponent>
            </View>
          </View>
        </SafeAreaView>
      </ScrollView>
    );
  }
}
