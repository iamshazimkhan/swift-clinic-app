import React, {Component} from 'react';
import { View, TextInput, Text, Image, Alert, TouchableOpacity, SafeAreaView} from 'react-native';
import styles from '../../css/css';
import MainHeader from '../../components/Headers/MainHeader';
import Icon from 'react-native-vector-icons/FontAwesome';
import {GetSymptoms} from '../../api/ConsultationApi';
import SpinnerComponent from '../../components/Spinner/Spinner';
import {Rating, AirbnbRating} from 'react-native-ratings';
import exampleImage from '../../bulb.png'
import Button from '../../components/Buttons/button';
import Toast from 'react-native-simple-toast';
import { addReview } from '../../api/ConsultationApi';
import AsyncStorage from '@react-native-community/async-storage';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

const exampleImageUri = Image.resolveAssetSource(exampleImage).uri

class FeedBackScreen extends Component {
    state={
        spinnerLoading: false,
        review: '',
        consultation_id: '',
        doctor_id: '',
        rating: 2,
        user_id: 46,
        profile_pic: null,
        doctor_name: null
    }

    componentDidMount = async () => {
      const cnst_id = await this.props.navigation.getParam('consultation_id', 'nothing sent');
      const profile_pic = await this.props.navigation.getParam('profile_pic', null);
      const dct_id = await this.props.navigation.getParam('doctor_id', 'nothing sent');
      const doctor_name = await this.props.navigation.getParam('doctor_name', 'Your doctor');
      await this.setState({consultation_id: cnst_id, doctor_id: dct_id, profile_pic: profile_pic, doctor_name: doctor_name});
      await console.log('Id from home screen is -------->', cnst_id,dct_id,profile_pic);
    };
  
    sendReview = async () => {
      
      const {consultation_id,doctor_id,rating,review, user_id} = this.state;
      console.log('Review ----------->', review)
      var newReview = review.trim();
      if(newReview !== null && newReview !== '' && newReview !== ' ' ) {
        Toast.show('Review :::' +newReview)
      await this.setState({spinnerLoading: true});
      addReview(consultation_id,doctor_id,rating,review, user_id)
        .then(async (res) => {
          console.log('Response =====------->', res.data);
          await this.setState({spinnerLoading: false})
          Toast.show('Review added successfully.', Toast.SHORT);
          this.props.navigation.navigate('Home');
        })
        .catch((err) => console.log('Error -------->', err));
      }else{
        Alert.alert('Please add review message also.')
      }
    };

       backToScreen = async () => {
        this.props.navigation.goBack(null);
      };

      ratingCompleted = async (t) => {
        console.log('rating completed',t)
        await this.setState({rating: t});
      }

  render() {
    return (
      <KeyboardAwareScrollView style={styles.container}>
      <SafeAreaView>
        <MainHeader Back={this.backToScreen}></MainHeader>
        <SpinnerComponent Visible={this.state.spinnerLoading}></SpinnerComponent>
        <Text
          style={{
            fontSize: 19,
            fontWeight: 'bold',
            marginTop: 30,
            textAlign: 'center',
          }}>
          Please leave a review
        </Text>


        <View style={{marginTop: 30}}>
          <View>
            <Image
              style={styles.dp}
              source={{uri: this.state.profile_pic === null ? 'https://www.pngitem.com/pimgs/m/516-5167304_transparent-background-white-user-icon-png-png-download.png' : 'https://platform.swiftclinic.com/storage/profile/' +  this.state.profile_pic}}></Image>
              </View>
          <View>
            <Text style={{marginTop: 20, fontWeight: 'bold', fontSize: 17, textAlign: 'center'}}>
              {this.state.doctor_name}
            </Text>
            <View style={{alignSelf: 'center' , marginTop: 20}}>
              {/* <Rating
                onFinishRating={t => this.ratingCompleted(t) }
                style={{
                  paddingTop: 30,
                  paddingBottom: 10,
                  alignSelf: 'flex-start',
                }}
                readonly={false}
                imageSize={30}
                startingValue={this.state.rating}
              /> */}
              <AirbnbRating
                onFinishRating={t => this.ratingCompleted(t) }
                count={5}
                defaultRating={2}
                unSelectedColor="grey"
                size={30}
                showRating={false}
              />
            </View>
          </View>
        </View>
      <View style={{marginTop: 30}}>
        <TextInput
              value={this.state.review}
              onChangeText={(review) => this.setState({review})}
              placeholder={'Please Leave a Review'}
              numberOfLines={4}
              multiline={true}
              style={styles.TextArea}
            />

</View>

        <TouchableOpacity onPress={this.sendReview} style={{ alignSelf: 'center' ,width: 200, backgroundColor: '#0158ff', borderRadius: 4, height: 70, justifyContent: 'center',marginTop: 30, marginBottom: 60}}>
            <Text style={{color: '#ffffff', textAlign: 'center'}}>Add Feedback</Text>
        </TouchableOpacity>



      </SafeAreaView>
      </KeyboardAwareScrollView>
    );
  }
}

export default FeedBackScreen;
