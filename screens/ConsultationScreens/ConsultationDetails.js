import React, {Component} from 'react';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {Rating, AirbnbRating} from 'react-native-ratings';
import {
  StyleSheet,
  View,
  TextInput,
  Text,
  Image,
  ScrollView,
  PermissionsAndroid,
  TouchableOpacity,
  Platform,
  Alert,
} from 'react-native';
import MainHeader from '../../components/Headers/MainHeader';
import DatePicker from '../../components/Date/Date';
import styles from '../../css/css';
import Button from '../../components/Buttons/button';
import {getAllUserCards} from '../../api/ConsultationApi';
import RNFetchBlob from 'rn-fetch-blob';
// import RNFetchBlob from 'react-native-fetch-blob';
import {
  followUp,
  AddBooking,
  AddPayment,
  GetConsultationsDetails,
  PatientRescheduling,
  setStatusOfConsultation,
  AddNewCard,
} from '../../api/ConsultationApi';
import {SafeAreaView} from 'react-navigation';
import AwesomeAlertComponent from '../../components/Alerts/AwesomeAlertFile';
import SpinnerComponent from '../../components/Spinner/Spinner';
import Moment from 'moment';
import PaymentForm from '../PaymentScreens/PaymentForm';
import PaymentModal from '../../components/Modals/PaymentModal';
import SetAvailableTimmings from '../../components/Modals/SetAvailableTimmings';
import SetAvailableTimmingsTwo from '../../components/Modals/SetAvailableTimmingsTwo';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import CalenderModal from '../../components/Date/CalenderModal';
import DatePickerTwo from '../../components/Date/DatePickerTwo';
import SvgGraph from '../../components/backgroundgraph';
import Toast from 'react-native-simple-toast';

class ConsultationDetails extends Component {
  state = {
    consultation_id: null,
    name: '',
    showDate: 'Request new date time',
    AlertComponentVisible: false,
    AlertComponentMessage: '',
    RequestedNewDateTimeAlert: false,
    upDown: false,
    spinnerLoading: true,
    symptomName: '',
    completed: 0,
    time: '',
    date: '',
    slotTime: null,
    slotTime2: null,
    addon_price: null,
    disableButton: false,
    doctorId: '',
    cardName: 'Name on card',
    cardModalVisibility: false,
    defaultCardNumber: 'No Card Found',
    cardFlatList: false,
    cardData: [],
    paymentModalVisibility: false,
    local_date_show: '',
    local_time: '',
    local_date: '',
    local_slotTime: '',
    doctor_pdf: null,
    amount: 80,
    review: 5,
    total_review: null,
    TimeVisible: false,
    selectTime: 'Select Time',
    TimeData: [],
    specificTimeData: [],
    CalenderModalVisibility: false,
    customCalenderArray: {},
    questionaire_link: null,
    TimeData: [],
    RescheduleTime: '',
    profile_pic: null,
    pending: null,
    allReports: [],
    MriLinks: [],
    current_status: null,
    isFeedBackCompleted: null,
    TimePickerModal: false,
  };

  getTimeToLocal = async () => {
    const {slotTime} = this.state;
    var gmtDateTime = await Moment.utc(slotTime, 'YYYY-MM-DD HH');
    var locale_time = await gmtDateTime.local().format('h:mm A');
    var locale_date = await gmtDateTime.local().format('DD-MMM-YYYY');
    var locale_slot_time = await Moment(slotTime, 'YYYY-MM-DD h:mm:A').format(
      'DD-MMM-YYYY h:mm A',
    );
    await this.setState({
      local_time: gmtDateTime,
      local_date: locale_date,
      local_slotTime: locale_slot_time,
    });
  };

  showCardFlatList = async () => {
    const {cardData, cardFlatList} = this.state;
    if (cardData.length > 0) {
      if (cardFlatList === false) {
        await this.setState({cardFlatList: true});
      } else {
        await this.setState({cardFlatList: false});
      }
    } else {
      Alert.alert('No cards found');
    }
  };

  onSelectCard = async (item) => {
    console.log('Select Card Clicked', item);
    await this.setState({
      defaultCardNumber: '**** ' + item.card_digit,
      defaultCardName: item.card_name,
      cardName: item.card_name,
      defaultCardID: item.id,
      cardFlatList: false,
    });
  };

  AddCard = async () => {
    // console.log('Add Card Clicked');
    await this.setState({
      cardModalVisibility: true,
      paymentModalVisibility: false,
    });
  };

  closeLocationModal = async () => {
    this.setState({
      ModalVisibility: false,
      paymentModalVisibility: false,
      cardModalVisibility: false,
    });
  };

  onAddPayment = async () => {
    let type = 'extended';
    const {consultation_id, amount, defaultCardID, disableButton} = this.state;
    if (defaultCardID !== null) {
      // await console.log('Consultaion Id ----->', consultation_id, '-----defaulrt Card ID ----->',defaultCardID, '-----TYPE ------------>',type);
      await this.setState({spinnerLoading: true, disableButton: true});
      AddPayment(consultation_id, amount, defaultCardID, type)
        .then(async (res) => {
          console.log('Response =====------->', res.data);
          this.setState({spinnerLoading: false, paymentModalVisibility: false});
          if (res.data.error) {
            Platform.OS === 'android'
              ? Toast.show(
                  'Payment is already done for this consultation.',
                  Toast.SHORT,
                )
              : Alert.alert('Payment is already done for this consultation.');
          } else {
            Platform.OS === 'android'
              ? Toast.show(
                  'Extended consultation time added successfully.',
                  Toast.SHORT,
                )
              : Alert.alert('Extended consultation time added successfully.');
            this.setState({
              AlertComponentMessage: res.data.message,
              AlertComponentVisible: false,
              PsVisibilityModal: true,
              spinnerLoading: false,
              paymentModalVisibility: false,
              disableButton: false,
            });
          }
        })
        .catch((err) => console.log('Error -------->', err));
    } else {
      Alert.alert('Please add card first.');
    }
  };

  _onChange = async (form) => {
    console.log(form.valid);
    let expiry = await form.values.expiry;
    let res = await expiry.split('/');
    console.log('Expiry date of card ---->', res);
    await this.setState({
      CardCVV: form.values.cvc,
      cardName: form.values.name,
      cardMonth: res[0],
      cardYear: '20' + res[1],
      cardNumber: form.values.number,
      cardStatus: form.valid,
    });
    await console.log('Card year ----->', this.state.cardYear);
  };

  AddCardApi = async () => {
    console.log('Add Card Api hit');
    const {cardNumber, CardCVV, cardMonth, cardYear, cardName, cardStatus} =
      this.state;
    if (cardStatus === true) {
      this.AddNewCardApi();
    } else {
      Alert.alert('Card Details are incomplete or invalid.');
    }
  };

  AddNewCardApi = async () => {
    const {cardNumber, CardCVV, cardMonth, cardYear, cardName, cardStatus} =
      this.state;
    await this.setState({spinnerLoading: true});
    AddNewCard(cardNumber, CardCVV, cardMonth, cardYear, cardName)
      .then(async (res) => {
        // console.log('Response Add Card Data=====------->', res.data.data);
        await this.setState({
          cardModalVisibility: false,
          paymentModalVisibility: true,
          spinnerLoading: false,
        });
        this.getUserCards();
      })
      .catch((err) => {
        Platform.OS === 'android'
          ? Toast.show('Card Details are not correct.', Toast.SHORT)
          : Alert.alert('Card Details are not correct.');
        this.setState({
          // AlertComponentVisibleTwo: true,
          spinnerLoading: false,
          // AlertComponentMessage: 'Card Details are not correct.',
        });
      });
  };

  getUserCards = async () => {
    await this.setState({spinnerLoading: true});
    getAllUserCards()
      .then(async (res) => {
        //  console.log('Response Card Data=====------->',res.data.data)
        await this.setState({
          cardData: res.data.data,
          spinnerLoading: false,
          paymentModalVisibility: true,
        });
        if (res.data.data.length > 0) {
          //  console.log('default card Data ------>', res.data.data[0])
          await this.setState({
            defaultCardNumber: '**** ' + res.data.data[0].card_digit,
            defaultCardID: res.data.data[0].id,
            cardName: res.data.data[0].card_name,
            addon_price: res.data.addon_price,
          });
        }
      })
      .catch((err) => console.log('Error -------->', err));
  };

  showPaymetnModal = async () => {
    this.getUserCards();
  };

  // ------------------------------ Payment Modal -------------------------

  componentDidMount = async () => {
    const id = await this.props.navigation.getParam('id', 34);
    await this.setState({consultation_id: id});
    console.log('Id from home screen is -------->', id);
    await this.getBookingDetails();
  };

  getBookingDetails = async () => {
    const {consultation_id} = this.state;
    GetConsultationsDetails(consultation_id)
      .then(async (res) => {
        console.log(
          'Data in Consultation screen ------------>',
          res.data.data.doctor.id,
        );

        let ras = res.data.data.doctor.avilable_dates;
        let consultationLetter = res.data.data.reports;
        var dateData = {};
        var key;
        var d_pdf = null;
        if (res.data.data.consultation_status === 'reqeuested_new_datetime') {
          await this.setState({RequestedNewDateTimeAlert: true});
        }
        await ras.forEach(function (date) {
          key = date.date;
          let obj = {marked: true, disabled: false};
          dateData[key] = obj;
        });
        await consultationLetter.forEach(function (d) {
          if (d.mri === 2) {
            d_pdf = d.file_link;
          }
        });
        console.log('loop', res.data.data.slottime);
        await this.setState({
          questionaire_link:
            res.data.data.questionnaire === null
              ? null
              : res.data.data.questionnaire.link,
          spinnerLoading: false,
          symptomName: res.data.data.symptom.symptom,
          name:
            'Dr. ' +
            res.data.data.doctor.first_name +
            ' ' +
            res.data.data.doctor.last_name,
          completed: res.data.data.completed,
          time: res.data.data.time,
          date: res.data.data.date,
          slotTime: res.data.data.slottime,
          doctorId: res.data.data.doctor.id,
          allReports: res.data.data.all_reports,
          doctor_pdf: d_pdf,
          review: res.data.data.doctor.review,
          total_review: '(' + res.data.data.doctor.total_review + ' Reviews)',
          TimeData: res.data.data.doctor.avilable_dates,
          customCalenderArray: dateData,
          profile_pic: res.data.data.doctor.profile_pic,
          pending: res.data.data.doctor_response,
          consultation_status: res.data.data.consultation_status,
          isFeedBackCompleted: res.data.data.review,
          current_status: res.data.data.current_status,
          slotTime2: res.data.data.slottime2,
          showDate:
            this.state.slotTime2 === null
              ? Moment(res.data.data.slottime, 'YYYY-MM-DD h:mm:A').format(
                  'YYYY-MM-DD h:mm A',
                )
              : this.formatDateTimeTwo(this.state.slotTime2),
          MriLinks:
            res.data.data.all_reports.mri_report === null
              ? []
              : res.data.data.all_reports.mri_report,
        });

        await this.getTimeToLocal();
      })
      .catch((err) => console.log('Error -------->', err));
  };

  backToScreen = async () => {
    this.props.navigation.goBack(null);
  };

  closeAlertFunction = async () => {
    await this.setState({AlertComponentVisible: false});
    this.getBookingDetails();
  };

  closeRequestedDateTimeAlert = async () => {
    await this.setState({RequestedNewDateTimeAlert: false, upDown: true});
  };

  upDown = async () => {
    await this.setState({upDown: !this.state.upDown});
  };

  onSubmit = async (date) => {
    var momentObj = await Moment(date, 'YYYY-MM-DDLT');
    // await this.setState({spinnerLoading: true});
    const {consultation_id, local_date_show, RescheduleTime} = await this.state;
    // console.log(
    //   'Consukltation ID "' +
    //   consultation_id +
    //     '" Local Date State--> D "' +
    //     RescheduleTime +
    //     '------ Params ------->"' + momentObj,
    // );
    PatientRescheduling(consultation_id, local_date_show)
      .then(async (res) => {
        console.log('Response =====------->', res.data);
        this.setState({
          spinnerLoading: false,
          AlertComponentVisible: true,
          AlertComponentMessage: 'Successfully Submitted',
        });
      })
      .catch((err) => console.log('Error -------->', err));
  };

  addFeedback = async () => {
    this.props.navigation.navigate('FeedBackScreen', {
      doctor_id: this.state.doctorId,
      consultation_id: this.state.consultation_id,
      profile_pic: this.state.profile_pic,
      doctor_name: this.state.name,
    });
  };

  completeStatus = async () => {
    await this.setState({spinnerLoading: false});
    const {consultation_id} = this.state;
    setStatusOfConsultation(consultation_id, amount)
      .then(async (res) => {
        console.log('Response =====------->', res.data);
        await this.setState({spinnerLoading: false});
      })
      .catch((err) => console.log('Error -------->', err));
  };

  //  -------------------------- Download File ------------------------------

  HealthactualDownload = async (text) => {
    var d_link = null;
    var file_name = 'pdf';
    if (text === 'health') {
      d_link =
        (await this.state.questionaire_link) === null
          ? null
          : 'https://platform.swiftclinic.com/questionaire-pdf/' +
            this.state.questionaire_link;
      console.log(
        'Health Questionaire',
        d_link,
        'Link half',
        this.state.questionaire_link,
      );
      file_name = 'healthquestionaire-pdf.pdf';
    } else if (text === 'letter') {
      console.log('Consultation Letter');
      d_link = this.state.doctor_pdf;
      file_name = 'consultationletter-pdf.pdf';
    } else {
    }
    if (d_link === null) {
      Platform.OS === 'android'
        ? Toast.show('File has not been uploaded yet.', Toast.SHORT)
        : Alert.alert('File has not been uploaded yet.');
    } else {
      this.setState({spinnerLoading: true});
      const {dirs} = RNFetchBlob.fs;
      const dirToSave =
        Platform.OS == 'ios' ? dirs.DocumentDir : dirs.DownloadDir;
      const configfb = {
        fileCache: true,
        useDownloadManager: true,
        notification: true,
        mediaScannable: true,
        title: file_name,
        path: `${dirToSave}/` + file_name,
      };
      const configOptions = Platform.select({
        ios: {
          fileCache: configfb.fileCache,
          title: configfb.title,
          path: configfb.path,
          appendExt: 'pdf',
        },
        android: configfb,
      });
      console.log('The file saved to 23233', configfb, dirs);

      RNFetchBlob.config(configOptions)
        .fetch('GET', d_link, {})
        .then((res) => {
          if (Platform.OS === 'ios') {
            // RNFetchBlob.fs.writeFile(configfb.path, res.data, 'base64');
            let pathh = res.path();
            setTimeout(() => {
              console.log('file savng : ' + pathh);
              RNFetchBlob.ios.previewDocument(pathh);
            }, 2000);
            console.log('showing up doc');
          }
          RNFetchBlob.fs
            .exists(res.path())
            .then((exist) => {
              console.log(`file ${exist ? '' : 'not'} exists`);
            })
            .catch((e) => {
              console.log('err :', e.message);
            });

          this.setState({spinnerLoading: false});

          console.log('The file saved to ', res.path());
          Toast.show('File Path : ' + res.path(), Toast.SHORT);
        })
        .catch((e) => {
          console.log('The file saved to ERROR', e.message);
        });
    }
  };

  HealthdownloadFile = async (text) => {
    console.log('Download file --------->');
    if (Platform.OS === 'ios') {
      console.log('iphone detected-----');
      try {
        this.HealthactualDownload(text);
      } catch (err) {
        console.warn(err);
      }
    } else {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          this.HealthactualDownload(text);
        } else {
          Alert.alert(
            'Permission Denied!',
            'You need to give storage permission to download the file',
          );
        }
      } catch (err) {
        console.warn(err);
      }
    }
  };

  formatDateTime(datetime) {
    console.log(
      'qwsdde',
      Moment(datetime, 'YYYY-MM-DD h:mm').format('YYYY-MM-DD h:mm A'),
    );

    return Moment(datetime, 'YYYY-MM-DD h:mm').format('h:mm A');
    // return Moment.utc(datetime).local().format('LT');
  }

  formatDateOnly(datetime) {
    return Moment(datetime, 'YYYY-MM-DD h:mm').format('DD-MMM-YYYY');
    // return Moment.utc(datetime).local().format('DD-MMM-YYYY');
  }

  formatDateTimeTwo(datetime) {
    return Moment(datetime, 'YYYY-MM-DD h:mm').format('DD-MMM-YYYY h:mm A');
    // return Moment.utc(datetime).local().format('LLL');
  }

  startTimeCallback = async (StartTime) => {
    console.log('Time Call');
    let TimeForDay = await this.state.specificTimeData;
    const {showDate, consultation_id} = this.state;
    let d4 = Moment(StartTime.getTime());
    let finalStartTime = d4.format('LT');
    let startTimeFinal = d4.format('HH:mm:ss');
    var momentObj = await Moment(showDate + startTimeFinal, 'YYYY-MM-DDLT');
    // const u = await Moment.utc(showDate + ' ' + startTimeFinal);
    // const su = await Moment.utc(showDate + ' ' + startTimeFinal).format('LLL');
    // await this.setState({
    //   selectTime: finalStartTime,
    //   local_date_show: momentObj,
    //   showDate: su,
    //   RescheduleTime: momentObj,
    //   TimePickerModal: false,
    //   TimeVisible: true,
    // });
    console.log(momentObj, 'qwasd');
    this.addTime();
  };

  addTime = async () => {
    console.log('Add Time Initial one');
    const {selectTime, local_date_show, consultation_id} = this.state;
    console.log('finalti', consultation_id);
    // if (selectTime === 'Select Time') {
    //   Alert.alert('Please select time first.');
    // } else {
    //   if (this.state.completed === 0) {
    //     console.log('Add Time Initial');
    //     await this.setState({spinnerLoading: true});
    //     await PatientRescheduling(consultation_id, local_date_show)
    //       .then(async (res) => {
    //         console.log('aqmk', res);
    //         this.setState({
    //           spinnerLoading: false,
    //           TimeVisible: false,
    //           CalenderModalVisibility: false,
    //           upDown: false,
    //         });
    //         await this.getBookingDetails();
    //         Alert.alert(
    //           'Message',
    //           'Rescheduling request successsfully submitted.',
    //         );
    //       })
    //       .catch((err) => console.log('Error -------->', err));
    //   } else {
    //     console.log('Followup API', local_date_show);
    //     await this.setState({spinnerLoading: true});
    //     await followUp(consultation_id, local_date_show)
    //       .then(async (res) => {
    //         console.log('Response =====------->', res.data);
    //         this.setState({
    //           spinnerLoading: false,
    //           TimeVisible: false,
    //           CalenderModalVisibility: false,
    //           upDown: false,
    //         });
    //         await this.getBookingDetails();
    //         Alert.alert('Message', 'Follow-up time successsfully submitted.');
    //       })
    //       .catch((err) => console.log('Error -------->', err));
    //   }
    // }
  };

  closeTimeModal = async () => {
    await this.setState({TimeVisible: false});
  };

  showCalender = async () => {
    console.log('Hellooooo');
    this.setState({CalenderModalVisibility: true});
  };

  closeCalenderModal = async () => {
    console.log('Closseeee');
    this.setState({CalenderModalVisibility: false});
  };

  dateCallback = async (selectedDate) => {
    console.log('Call Func', selectedDate);
    var gmtDateTime = Moment.utc(selectedDate, 'YYYY-MM-DD');
    var locale_date = gmtDateTime.local().format('YYYY-MM-DD');
    var current_time = Moment()
      .local()
      // .subtract(1, 'days')
      .format('YYYY-MM-DD');
    console.log('After ------>', Moment(locale_date).isAfter(current_time));
    if (Moment(locale_date).isAfter(current_time)) {
      const {TimeData} = this.state;
      let TTimeData = TimeData;
      TTimeData = TTimeData.filter(function (item) {
        return item.date == locale_date;
      }).map(function ({date, start_time, end_time}) {
        return {
          date,
          start_time: Moment.utc('2017-03-13' + ' ' + start_time)
            .local()
            .format('LT'),
          end_time: Moment.utc('2017-03-13' + ' ' + end_time)
            .local()
            .format('LT'),
        };
      });
      console.log('LOCALE DATE ========> ', locale_date);
      this.setState({
        CalenderModalVisibility: false,
        showDate: locale_date,
        local_date_show: locale_date,
        specificTimeData: TTimeData,
        TimeVisible: true,
      });
    } else {
      Alert.alert('Please select an appointment time from tomorrow  onwards.');
    }
  };

  /** ---------------------------------------------- MRI Links Download -------------------- */

  MRIactualDownload = async (d_link) => {
    if (d_link === null) {
      Platform.OS === 'android'
        ? Toast.show('MRI has not uploaded it yet.', Toast.SHORT)
        : Alert.alert('MRI has not uploaded it yet.');
    } else {
      this.setState({spinnerLoading: true});
      const {dirs} = RNFetchBlob.fs;
      const dirToSave =
        Platform.OS == 'ios' ? dirs.DocumentDir : dirs.DownloadDir;
      const configfb = {
        fileCache: true,
        useDownloadManager: true,
        notification: true,
        mediaScannable: true,
        title: 'MRI-Report.pdf',
        path: `${dirToSave}/MRI-Report.pdf`,
      };
      const configOptions = Platform.select({
        ios: {
          fileCache: configfb.fileCache,
          title: configfb.title,
          path: configfb.path,
          appendExt: 'pdf',
        },
        android: configfb,
      });
      console.log('The file saved to 23233', configfb, dirs);

      RNFetchBlob.config(configOptions)
        .fetch('GET', d_link, {})
        .then((res) => {
          if (Platform.OS === 'ios') {
            // RNFetchBlob.fs.writeFile(configfb.path, res.data, 'base64');
            let pathh = res.path();
            setTimeout(() => {
              console.log('file savng : ' + pathh);
              RNFetchBlob.ios.previewDocument(pathh);
            }, 2000);
            console.log('showing up doc');
          }
          RNFetchBlob.fs
            .exists(res.path())
            .then((exist) => {
              console.log(`file ${exist ? '' : 'not'} exists`);
            })
            .catch((e) => {
              console.log('err :', e.message);
            });

          this.setState({spinnerLoading: false});

          console.log('The file saved to ', res.path());
          Toast.show('File Path : ' + res.path(), Toast.SHORT);
        })
        .catch((e) => {
          console.log('The file saved to ERROR', e.message);
        });
    }
  };

  MRIdownloadFile = async (link) => {
    console.log('Download file --------->');
    if (Platform.OS === 'ios') {
      console.log('iphone detected-----');
      try {
        this.MRIactualDownload(link);
      } catch (err) {
        console.warn(err);
      }
    } else {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          this.MRIactualDownload(link);
        } else {
          Alert.alert(
            'Permission Denied!',
            'You need to give storage permission to download the file',
          );
        }
      } catch (err) {
        console.warn(err);
      }
    }
  };

  mriScan = async (index) => {
    const {MriLinks} = this.state;
    const link = MriLinks[index].link;
    console.log('Datatatataat------------>', link);
    await this.MRIdownloadFile(link);
  };

  renderlist() {
    const {MriLinks} = this.state;
    let list = [];
    for (let i = 0; i < MriLinks.length; i++) {
      list.push(
        <View
          onStartShouldSetResponder={() => this.mriScan(i)}
          style={{flexDirection: 'row', marginTop: 17}}>
          <Icon
            style={{alignSelf: 'flex-end', marginRight: 17}}
            color="#33909F"
            size={24}
            name="file-pdf"
          />
          <Text
            style={{
              textDecorationLine: 'underline',
              fontFamily: 'Axiforma-Medium',
            }}>
            MRI Scan
          </Text>
        </View>,
      );
    }
    return list;
  }

  showDatePicker = async () => {
    await console.log('Time Picker');
    await this.setState({TimePickerModal: true, TimeVisible: false});
  };

  hideDatePicker = async () => {
    await console.log('Time Picker Close');
    await this.setState({TimePickerModal: false, TimeVisible: true});
  };

  handleConfirm = async (StartTime) => {
    const {showDate, consultation_id} = this.state;
    const {specificTimeData} = this.state;
    console.log('Specific Time Data ------------->', StartTime);
    const u = await Moment.utc(StartTime).local().format('LT');
    var amIBetween = false;

    console.log(
      consultation_id,
      showDate,
      StartTime,
      'Loooooppppinggg--------------------------',
      amIBetween,
    );

    await PatientRescheduling(consultation_id, `${showDate}T${StartTime}Z`)
      .then(async (res) => {
        console.log('aqmk', this.state.current_status);
        let message =
          this.state.completed === 1 &&
          this.state.current_status === 'mri_completed' &&
          this.state.completed !== 2 &&
          this.state.completed !== 3
            ? 'Appointment request submitted. You will receive a notification when your doctor has confirmed your appointment'
            : 'Rescheduling request successsfully submitted.';

        Alert.alert('Message', message, [
          {
            text: 'ok',
            onPress: () => {
              this.setState({
                TimeVisible: false,
                showDate: `${showDate} ${StartTime}`,
              });
            },
          },
        ]);
      })
      .catch((err) => console.log('Error -------->', err));
    // this.setState({TimeVisible: false, showDate: `${showDate} ${StartTime}`});
    // this.seTime(StartTime);
  };

  seTime = async (StartTime) => {
    this.startTimeCallback(StartTime);
  };

  // ------------------------- End Location --------------------------

  render() {
    return (
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: '#ffffff',
          opacity:
            this.state.cardModalVisibility || this.state.paymentModalVisibility
              ? 0.2
              : 1,
        }}>
        <ScrollView>
          <SvgGraph style={{right: -38}} />
          <View style={{marginTop: -280}}>
            {this.state.CalenderModalVisibility ? (
              <CalenderModal
                disabled={true}
                customDateArray={this.state.customCalenderArray}
                closeModal={this.closeCalenderModal}
                disabledDays={true}
                Visibilty={this.state.CalenderModalVisibility}
                selectedDateCallback={this.dateCallback}></CalenderModal>
            ) : null}

            {this.state.TimeVisible ? (
              <SetAvailableTimmingsTwo
                id={this.state.doctorId}
                dat={this.state.showDate}
                specificTimeData={this.state.specificTimeData}
                closeModal={this.closeTimeModal}
                TimeVisible={this.state.TimeVisible}
                showDatePicker={this.showDatePicker}
                onClick={this.addTime}
                setState={this.handleConfirm}
                Date={
                  this.state.selectTime === 'Select Time'
                    ? 'Select Time'
                    : this.state.selectTime
                }></SetAvailableTimmingsTwo>
            ) : null}
            <MainHeader Back={this.backToScreen}></MainHeader>

            {this.state.TimePickerModal ? (
              <DateTimePickerModal
                headerTextIOS="Pick a time"
                // style={{width: 10, height: auto}}
                isVisible={this.state.TimePickerModal}
                mode="time"
                onConfirm={(date) => this.startTimeCallback(date)}
                onCancel={this.hideDatePicker}
              />
            ) : null}

            <View>
              <Text
                style={{
                  fontSize: 19,
                  textAlign: 'center',
                  marginTop: 30,
                  fontFamily: 'Axiforma-Medium',
                }}>
                {this.state.symptomName}
              </Text>
              <View>
                <Text
                  style={{
                    padding: 20,
                    fontSize: 12,
                    color: 'grey',
                    fontFamily: 'Axiforma-Thin',
                    lineHeight: 21.2,
                  }}>
                  Below are the details of your upcoming consultation with our
                  doctor. The doctor will call you so all you have to do is keep
                  your device switched on. You can change the date and time of
                  your appointment below{' '}
                </Text>
                <Text
                  style={{
                    fontWeight: 'bold',
                    paddingLeft: 20,
                    color: '#0575ff',
                    textDecorationLine: 'underline',
                  }}>
                  {this.state.pending === null
                    ? 'Your consultation approval is pending.'
                    : null}
                </Text>
              </View>
            </View>

            {/* --------------------------------- Center Box ---------------------------- */}
            {(this.state.completed === 0 ||
              this.state.current_status === 'followup_schedule' ||
              this.state.slotTime2 !== null) &&
            this.state.completed !== 3 ? (
              <View
                style={{
                  marginLeft: 20,
                  marginRight: 20,
                  marginTop: 20,
                  padding: 10,
                  backgroundColor: 'white',
                  elevation: 7,
                  borderRadius: 10,
                  shadowColor: 'dodgerblue',
                  shadowOffset: {width: 0, height: 4},
                  shadowRadius: 6,
                  shadowOpacity: 0.2,
                }}>
                <View style={{flexDirection: 'row'}}>
                  <View style={{flex: 3, borderRightWidth: 1}}>
                    <Text style={{fontFamily: 'Axiforma-Thin'}}>
                      Keep your device switched on
                    </Text>
                    <Text
                      style={{
                        color: 'grey',
                        fontFamily: 'Axiforma-Thin',
                        lineHeight: 18.2,
                        fontSize: 12,
                        marginTop: 5,
                      }}>
                      Your doctor will call you on the time and date. Keep the
                      app open or in background mode.
                    </Text>
                  </View>
                  <View style={{flex: 1}}>
                    <Text
                      style={{
                        color: '#33909F',
                        fontWeight: 'bold',
                        fontSize: 17,
                        textAlign: 'center',
                      }}>
                      {this.state.slotTime
                        ? this.formatDateTime(
                            this.state.slotTime2 === null
                              ? this.state.showDate
                              : this.state.slotTime2,
                          )
                        : null}
                    </Text>
                    <Text
                      style={{textAlign: 'center', fontSize: 12, marginTop: 6}}>
                      {this.state.slotTime
                        ? this.formatDateOnly(
                            this.state.slotTime2 === null
                              ? this.state.showDate
                              : this.state.slotTime2,
                          )
                        : null}
                    </Text>
                    <Icon
                      onPress={this.upDown}
                      style={{
                        alignSelf: 'center',
                        marginRight: 17,
                        marginTop: 10,
                      }}
                      color="#33909F"
                      size={22}
                      name={this.state.upDown ? 'chevron-up' : 'chevron-down'}
                    />
                  </View>
                </View>
                {this.state.upDown ? (
                  <View
                    style={{
                      flexDirection: 'row',
                      marginLeft: 2,
                      marginRight: 20,
                    }}>
                    <View style={{flex: 9}}>
                      <DatePickerTwo
                        Date={
                          this.state.slotTime2 === null
                            ? this.state.showDate
                            : this.state.slotTime2
                        }
                        showCalender={this.showCalender}
                      />
                    </View>
                    <View style={{flex: 2}}>
                      <Text></Text>
                    </View>
                  </View>
                ) : null}
              </View>
            ) : null}

            {this.state.completed === 1 &&
            this.state.current_status === 'mri_completed' &&
            this.state.completed !== 2 &&
            this.state.completed !== 3 ? (
              <View style={{marginLeft: 20, marginRight: 20}}>
                <Text style={{textAlign: 'center', color: 'grey'}}>
                  Follow-up Consultation
                </Text>
                <DatePickerTwo
                  Date={
                    this.state.slotTime2 === null
                      ? 'Select Date'
                      : this.formatDateTimeTwo(this.state.slotTime2)
                  }
                  showCalender={this.showCalender}
                />
              </View>
            ) : null}

            {/* ----------------------------- Doctor Box ---------------------- */}
            <Text
              style={{
                paddingLeft: 20,
                marginTop: 20,
                fontSize: 19,
                fontFamily: 'Axiforma-Medium',
              }}>
              Your Doctor:
            </Text>
            <View style={styles.mainBox}>
              <View style={{flex: 1}}>
                <Image
                  style={styles.dp}
                  source={{
                    uri:
                      this.state.profile_pic === null
                        ? 'https://tvscheduleindia.com/avatar_doctor.png'
                        : 'https://platform.swiftclinic.com/storage/profile/' +
                          this.state.profile_pic,
                  }}></Image>
              </View>
              <View
                style={{
                  padding: 10,
                  flex: 3,
                  justifyContent: 'center',
                  alignContent: 'center',
                  marginLeft: 10,
                }}>
                <Text style={{fontSize: 17, fontFamily: 'Axiforma-Book'}}>
                  {' '}
                  {this.state.name}
                </Text>
                <View style={{flexDirection: 'row'}}>
                  <Rating
                    style={{
                      paddingTop: 10,
                      paddingBottom: 10,
                      alignSelf: 'flex-start',
                    }}
                    readonly={true}
                    imageSize={15}
                    startingValue={this.state.review ? this.state.review : 0}
                  />
                  <Text style={{marginTop: 7, marginLeft: 10}}>
                    {this.state.total_review
                      ? this.state.total_review
                      : 0 + ' Reviews'}
                  </Text>
                </View>
              </View>
            </View>

            {/* ------------------------------ doctor Follow up -------------- */}

            <View style={{padding: 20}}>
              {/* <View style={{flexDirection: 'row'}}>
          <Icon
                  style={{alignSelf: 'flex-end', marginRight: 17}}
                  color="#0375FF"
                  size={20}
                  name="plus-circle"
                />
              <Text>MRI Scan </Text>
            </View> */}
              {this.state.completed === 1 ? null : (
                <View
                  onStartShouldSetResponder={this.showPaymetnModal}
                  style={{flexDirection: 'row', marginTop: 0}}>
                  {/* <Icon
                  style={{alignSelf: 'flex-end', marginRight: 17}}
                  color="#33909F"
                  size={20}
                  name="plus-circle"
                />
              <Text style={{fontFamily: 'Axiforma-Book'}}>Add extend consultation time </Text> */}
                </View>
              )}
              <View style={{flexDirection: 'row', marginTop: 0}}>
                <Text style={{fontFamily: 'Axiforma-Book'}}>
                  Initial consultation:{' '}
                </Text>
                <Text style={{color: 'grey', fontFamily: 'Axiforma-Thin'}}>
                  {this.state.local_slotTime}
                </Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Text style={{fontFamily: 'Axiforma-Book'}}>
                  Follow up consultation:
                </Text>
                <Text style={{color: 'grey', fontFamily: 'Axiforma-Thin'}}>
                  {' '}
                  {this.state.slotTime2 === null
                    ? ''
                    : this.formatDateTimeTwo(this.state.slotTime2)}
                </Text>
              </View>
            </View>

            {/* --------------------- Document Box ------------------------ */}

            {this.state.completed ? (
              <View
                style={{
                  padding: 20,
                  backgroundColor: '#ECF5FF',
                  borderRadius: 20,
                }}>
                <Text style={{fontWeight: 'bold'}}>Documents:</Text>
                <View
                  onStartShouldSetResponder={() =>
                    this.HealthdownloadFile('health')
                  }
                  style={{flexDirection: 'row', marginTop: 17}}>
                  <Icon
                    style={{alignSelf: 'flex-end', marginRight: 17}}
                    color="#33909F"
                    size={24}
                    name="file-pdf"
                  />
                  <Text
                    style={{
                      textDecorationLine: 'underline',
                      fontFamily: 'Axiforma-Medium',
                    }}>
                    Health Questionaire
                  </Text>
                </View>
                {this.renderlist()}
                {this.state.doctor_pdf === null ? null : (
                  <View
                    onStartShouldSetResponder={() =>
                      this.HealthdownloadFile('letter')
                    }
                    style={{flexDirection: 'row', marginTop: 17}}>
                    <Icon
                      style={{alignSelf: 'flex-end', marginRight: 17}}
                      color="#33909F"
                      size={24}
                      name="file-pdf"
                    />
                    <Text
                      style={{
                        textDecorationLine: 'underline',
                        fontFamily: 'Axiforma-Medium',
                      }}>
                      Consultation letter
                    </Text>
                  </View>
                )}

                {this.state.isFeedBackCompleted === null &&
                (this.state.completed === 2 || this.state.completed === 3) ? (
                  <TouchableOpacity
                    onPress={this.addFeedback}
                    style={{
                      alignSelf: 'stretch',
                      backgroundColor: '#0158ff',
                      borderRadius: 10,
                      height: 50,
                      justifyContent: 'center',
                      marginTop: 40,
                      marginBottom: 60,
                      marginLeft: 30,
                      marginRight: 30,
                    }}>
                    <Text
                      style={{
                        color: '#ffffff',
                        textAlign: 'center',
                        fontSize: 17,
                      }}>
                      Add Feedback
                    </Text>
                  </TouchableOpacity>
                ) : null}
              </View>
            ) : null}

            {/* ----------------------------------Date Time ----------------------------  */}
            {this.state.cardModalVisibility ? (
              <PaymentForm
                paymentSpinner={this.state.spinnerLoading}
                closeModal={this.closeLocationModal}
                onClick={this.AddCardApi}
                _onChange={this._onChange}
                Visibilty={this.state.cardModalVisibility}></PaymentForm>
            ) : null}

            {this.state.paymentModalVisibility ? (
              <PaymentModal
                amount={
                  this.state.addon_price ? '£' + this.state.addon_price : '£80'
                }
                extra={true}
                disableButton={this.state.disableButton}
                showCardFaltList={this.showCardFlatList}
                defaultCardName={this.state.cardName}
                defaultCardNumber={this.state.defaultCardNumber}
                onSelectListCard={this.onSelectCard}
                Visible={this.state.cardFlatList}
                cardData={this.state.cardData}
                addCard={this.AddCard}
                Visibilty={this.state.paymentModalVisibility}
                closeModal={this.closeLocationModal}
                addPayment={this.onAddPayment}></PaymentModal>
            ) : null}
            {/* ------------------------------- Components --------------------------------- */}
            <AwesomeAlertComponent
              title="Message"
              visibleAlert={this.state.AlertComponentVisible}
              closeAlert={this.closeAlertFunction}
              message={
                this.state.AlertComponentMessage
              }></AwesomeAlertComponent>
            <AwesomeAlertComponent
              title="Message"
              visibleAlert={this.state.RequestedNewDateTimeAlert}
              closeAlert={this.closeRequestedDateTimeAlert}
              message="Your chosen doctor has requested you select an alternative appointment date and time as unfortunately they are not available."></AwesomeAlertComponent>

            <SpinnerComponent
              Visible={this.state.spinnerLoading}></SpinnerComponent>

            {/* ---------------------------- End of Components ----------------- */}
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

export default ConsultationDetails;
