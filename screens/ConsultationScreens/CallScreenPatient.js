import React, {useEffect, useState, useCallback} from 'react';
import {View, StyleSheet, Alert, Image} from 'react-native';
import {Text} from 'react-native-paper';
import {Button} from 'react-native-paper';
import AsyncStorage from '@react-native-community/async-storage';
import Icon from 'react-native-vector-icons/FontAwesome';
import {onendCallFunction} from '../../services/CallKeep';
import NavigationService from '../../navigations/NavigationService';
import InCallManager from "react-native-incall-manager";
import {NavigationActions} from 'react-navigation';

import {
  RTCPeerConnection,
  RTCIceCandidate,
  RTCSessionDescription,
  RTCView,
  MediaStream,
  MediaStreamTrack,
  mediaDevices,
  registerGlobals,
} from 'react-native-webrtc';



export default function CallScreenPatient({ navigation }) {

  // ---------------------------------------------------------------------- Call Part -------------------------------------
  let name;
  let connectedUser;
  const [isFront,setIsFront] = useState(true);
  const [microphoneStatus, setMicrophoneStatus] = useState(true);
  const [consultation_id,setConsultation_id] = useState(null);
  const [userId, setUserId] = useState('patient');
  const [channel_id, setChannel_id] = useState(101);
  const [socketActive, setSocketActive] = useState(false);
  const [calling, setCalling] = useState(false);
  // Video Scrs
  const [localStream, setLocalStream] = useState({toURL: () => null});
  const [remoteStream, setRemoteStream] = useState({toURL: () => null});
  const [conn, setConn] = useState(new WebSocket('wss://us-nyc-1.websocket.me/v3/1?api_key=BHoDIIvYTrp3a7gGP6gTDvXRPwEscySVe48HqXz4&notify_self&CHANNEL_ID=' + channel_id));
  const [yourConn, setYourConn] = useState(
    //change the config as you need
    new RTCPeerConnection({
      iceServers: [
        {
          urls: 'stun:stun.l.google.com:19302',  
        }, {
          urls: 'stun:stun1.l.google.com:19302',    
        }, {
          urls: 'stun:stun2.l.google.com:19302',    
        }

      ],
    }),
  );

  const [offer, setOffer] = useState(null);

  const [callToUsername, setCallToUsername] = useState(null);



  /**
   * Calling Stuff
   */


  const onLogin = () => {};

  useEffect(() => {

    (async () => {
      const chnl_id = await navigation.getParam('channel_id',{});
      const cnslt_id = await navigation.getParam('consultation_id',{});
      await console.log('channnel_id && consultation_id ---------->', chnl_id, cnslt_id)
      await setChannel_id(chnl_id);
      await setConsultation_id(cnslt_id);
  
      await InCallManager.start();
      await InCallManager.setKeepScreenOn(true);
      await InCallManager.setForceSpeakerphoneOn(true);
  })(); 

    /**
     *
     * Sockets Signalling
     */
    conn.onopen = () => {
      console.log('Connected to the signaling server');
      setSocketActive(true);
      onCall();
    };

    // onCall();
    //when we got a message from a signaling server
    conn.onmessage = msg => {
      let data;
      if (msg.data === 'Hello world') {
        data = {};
      } else {
        data = JSON.parse(msg.data);
        console.log('Data of push Notifcation--------------------->', data);
        switch (data.type) {
          case 'login':
            console.log('Login');
            break;
          //when somebody wants to call us
          case 'offer':
            handleOffer(data.offer, data.name);
            console.log('Offer');
            break;
          case 'answer':
            handleAnswer(data.answer);
            console.log('Answer');
            break;
          //when a remote peer sends an ice candidate to us
          case 'candidate':
            handleCandidate(data.candidate);
            console.log('Candidate');
            break;
          case 'leave':
            handleLeave();
            console.log('Leave');
            break;
          default:
            break;
        }
      }
    };
    conn.onerror = function(err) {
      console.log('Got error', err);
    };
    /**
     * Socjket Signalling Ends
     */

    mediaDevices.enumerateDevices().then(sourceInfos => {
      let videoSourceId;
      for (let i = 0; i < sourceInfos.length; i++) {
        const sourceInfo = sourceInfos[i];
        if (
          sourceInfo.kind == 'videoinput' &&
          sourceInfo.facing == (isFront ? 'front' : 'environment')
        ) {
          videoSourceId = sourceInfo.deviceId;
        }
      }
      mediaDevices
        .getUserMedia({
          audio: true,
          video: {
            mandatory: {
              minWidth: 500, // Provide your own width, height and frame rate here
              minHeight: 300,
              minFrameRate: 30,
            },
            facingMode: isFront ? 'user' : 'environment',
            optional: videoSourceId ? [{sourceId: videoSourceId}] : [],
          },
        })
        .then(stream => {
          // Got stream!
          setLocalStream(stream);

          // setup stream listening
          yourConn.addStream(stream);
        })
        .catch(error => {
          // Log error
        });
    });

    yourConn.onaddstream = event => {
      console.log('On Add Stream', event);
      setRemoteStream(event.stream);
    };

    // Setup ice handling
    yourConn.onicecandidate = event => {
      if (event.candidate) {
        send({
          type: 'candidate',
          candidate: event.candidate,
        });
      }
    };
  }, []);

  const send = message => {
    //attach the other peer username to our messages
    if (connectedUser) {
      message.name = connectedUser;
      console.log('Connected iser in end----------', message);
    }

    conn.send(JSON.stringify(message));
  };

  const onCall = async () => {
    // setCalling(true);

    connectedUser = 'doctor';
    console.log('Caling to', callToUsername);

    // displayIncomingCallNow();

    // if (enabled) {
    //   getToken();
    // } else {
    //   // If permission hasn’t been granted to our app, request user in requestPermission method. 
    //   requestPermission();
    // }

    yourConn.createOffer().then(offer => {
      yourConn.setLocalDescription(offer).then(() => {
        console.log('Sending Ofer');
        console.log(offer);
        send({
          type: 'offer',
          offer: offer,
        });
        // Send pc.localDescription to peer
      });
    });
  };

  //when somebody sends us an offer
  const handleOffer = async (offer, name) => {
    console.log(name + ' is calling you.');

    console.log('Accepting Call===========>', offer);
    connectedUser = name;

    try {
      await yourConn.setRemoteDescription(new RTCSessionDescription(offer));

      const answer = await yourConn.createAnswer();

      await yourConn.setLocalDescription(answer);
      send({
        type: 'answer',
        answer: answer,
      });
    } catch (err) {
      console.log('Offerr Error', err);
    }
  };

  //when we got an answer from a remote user
  const handleAnswer = answer => {
    yourConn.setRemoteDescription(new RTCSessionDescription(answer));
  };

  //when we got an ice candidate from a remote user
  const handleCandidate = candidate => {
    setCalling(false);
    console.log('Candidate ----------------->', candidate);
    yourConn.addIceCandidate(new RTCIceCandidate(candidate));
  };

  //hang up
  const hangUp = async () => {
    await send({
      type: 'leave',
    });

    await handleLeave();
  };

  const handleLeave = async () => {
    await InCallManager.stop();
    connectedUser = null;
    setRemoteStream({toURL: () => null});

    yourConn.close();
    const RouteName = 'Patient';
    await onendCallFunction(RouteName, consultation_id);
  };

  const onEndCall = async () => {
    await hangUp();
  }

  const muteMedia = async () => {
    if(microphoneStatus === true)
    {
     await setMicrophoneStatus(false)
    }else if(microphoneStatus === false)
    {
     await setMicrophoneStatus(true)
    }
    await console.log('Mute Meddia --------->')
    localStream.getTracks().forEach((t) => {
      if (t.kind === 'audio') t.enabled = !t.enabled;
  });
  }

  const changeCamera = async () => {
    localStream.getVideoTracks().forEach((track) => {
      track._switchCamera()
   })
  }
  /**
   * Calling Stuff Ends
   */

  return (
    <View style={styles.root}>

      <View style={styles.videoContainer}>
        <View style={[styles.videos, styles.localVideos]}>
          <RTCView
           objectFit={'cover'}
           streamURL={remoteStream.toURL()}
           style={styles.localVideo} />
        </View>
        <View style={[styles.videos, styles.remoteVideos]}>
          <RTCView
            objectFit={'cover'}
            streamURL={localStream.toURL()} 
            style={styles.remoteVideo}
          />
        </View>

        <View style={{flexDirection: 'row', justifyContent: 'center',marginTop: -90}}>
        <View style={{flex: 1, alignItems: 'center'}}>
          <View onStartShouldSetResponder={muteMedia} style={{borderWidth:1, borderColor: 'white', justifyContent: 'center', height: 60, width: 60, borderRadius: 60/2}}>
        <Icon
            style={{ alignSelf: 'center'}}  
            color="white"
            size={40}
            name={microphoneStatus ? "microphone" : "microphone-slash"}
          />
          </View>
</View>
          <View style={{flex: 1, alignItems: 'center'}}>
          <View onStartShouldSetResponder={onEndCall} style={{borderWidth:1, borderColor: '#bf1e2e', justifyContent: 'center', height: 60, width: 60,borderRadius: 60/2, backgroundColor: '#bf1e2e'}}>
          <Image style={{width: 60, height: 60, alignSelf: 'center'}} source={require('../../callend.png')}></Image>
          </View>
          </View>
          <View style={{flex: 1, alignItems: 'center'}}>
          <View onStartShouldSetResponder={changeCamera} style={{borderWidth:1, borderColor: 'white', justifyContent: 'center', height: 60, width: 60, borderRadius: 60/2}}>
          <Image style={{width: 45, height: 45, alignSelf: 'center'}} source={require('../../switchcamera.png')}></Image>
          </View>
</View>
          </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  iconsVideo: {
    justifyContent: 'center'
  },
  root: {
    backgroundColor: '#fff',
    flex: 1,
  },
  inputField: {
    marginBottom: 10,
    flexDirection: 'column',
  },
  videoContainer: {
    flex: 1,
    minHeight: 450,
  },
  videos: {
    width: '100%',
    flex: 1,
    position: 'relative',
    overflow: 'hidden',
    borderRadius: 1,
  },
  localVideos: {
    height: 100,
    marginBottom: 10,
  },
  remoteVideos: {
    height: 400,
  },
  localVideo: {
    backgroundColor: '#f2f2f2',
    height: '100%',
    width: '100%',
  },
  remoteVideo: {
    backgroundColor: '#f2f2f2',
    height: '100%',
    width: '100%',
  },
});