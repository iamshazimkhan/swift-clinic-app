import React, {Component} from 'react';
import {View, Text, FlatList,TouchableOpacity, ScrollView, TouchableNativeFeedback} from 'react-native';
import TitleImage from '../../components/Headers/titleImage';
import ContentBox from '../../components/consultations/ContentBox';
import {GetPreviousConsultations,GetDoctorsPreviousConsultations} from '../../api/ConsultationApi';
import SpinnerComponent from '../../components/Spinner/Spinner';
import SvgGraph from '../../components/backgroundgraph';
import { SafeAreaView } from 'react-navigation';
import NoData from '../../components/NoData';
import Icon from 'react-native-vector-icons/FontAwesome';
import Moment from 'moment';

export default class PatientHistory extends Component {
  state = {
    data: [],
    spinnerLoading: true,
    noData : false
  };

  componentDidMount = async () => {
     this.GetPreviousConsultationsFunction();
    this.willFocusSubscription = this.props.navigation.addListener(
      'willFocus',
      () => {
      //  console.log('Console Data Random----------->',Math.floor(Math.random() * 100) + 1 );
       this.GetPreviousConsultationsFunction();
      }
    );
  };

  GetPreviousConsultationsFunction = async() => {
    GetPreviousConsultations()
    .then(async (res) => {
      // console.log('Response =====------->', res.data.data);
      if(res.data.data.length > 0)
      {
        this.setState({noData: false})
      }
      else{
        this.setState({noData: true})
      }
      await this.setState({data: res.data.data, spinnerLoading: false});
    })
    .catch((err) => console.log('Error -------->', err));
  }

  formatDateTime(datetime){
    return Moment.utc(datetime).local().format('LL')
 }

 navigateToConsultationDetails = async (item) => {
  console.log('Item Consultation Details',item)
  this.props.navigation.navigate('ConsultationDetails',{id : item.id})
 }

  render() {
    return (
      <ScrollView style={{ flex: 1, backgroundColor: '#fff'}}>
      <SafeAreaView>
      <SvgGraph style={{right:-38}} />
      <View style={{marginTop: -270}}>
        <TitleImage></TitleImage>
        <Text
          style={{
            textAlign: 'center',
            marginTop: 40,
            textAlign: 'center',
            fontFamily: 'Axiforma-Medium',
            fontSize: 19,
          }}>
          Consultation History
        </Text>
        <View style={{ padding: 20}}>

          {this.state.noData ?
         ( <NoData></NoData> )
          : (
            <FlatList
            scrollEnabled={false}
            data={this.state.data}
            horizontal={false}
            numColumns={1}
            keyExtractor={(item) => item.id.toString()}
            renderItem={({item}) => (
              <TouchableNativeFeedback onPress={() => this.navigateToConsultationDetails(item)}>
                <ContentBox
                  title={this.formatDateTime(item.slottime)}
                  doctorName={"Consultation with " +item.doctor.first_name + " " + item.doctor.last_name}
                  colour={item.doctor_response === null ? "white" : "white"}></ContentBox>
              </TouchableNativeFeedback>
            )}
          />
          )  }
          <SpinnerComponent
            Visible={this.state.spinnerLoading}></SpinnerComponent>
        </View>
      </View>
      </SafeAreaView>
      </ScrollView>
    );
  }
}
