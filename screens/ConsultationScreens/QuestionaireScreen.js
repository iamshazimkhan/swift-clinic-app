import React, {Component} from 'react';
import {ScrollView, View, TextInput, Text, Alert} from 'react-native';
import styles from '../../css/css';
import MainHeader from '../../components/Headers/MainHeader';
import Slider from '@react-native-community/slider';
import CheckBox from '@react-native-community/checkbox';
import Button from '../../components/Buttons/button';
import {AddQuestionaire} from '../../api/ConsultationApi';
import { SafeAreaView } from 'react-navigation';
import AwesomeAlertComponent from '../../components/Alerts/AwesomeAlertFile';
import Toast from 'react-native-simple-toast';
import SpinnerComponent from '../../components/Spinner/Spinner';
import SvgGraph from '../../components/backgroundgraph';

import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from 'react-native-simple-radio-button';


class QuestionaireScreen extends Component {
  state = {
    one: null,   //--------------- Started --------
    two: null,    //--------------- get Worse -------
    three: 0,   //---------------- Slider
    four: null,
    five: null,
    // /------------------- Are you having following difficulties ----------------
    six: null,
    seven: null,
    eight: null,
    nine: null,
    ten: null,
    eleven: null,
    twelve: null,
    thirteen: null,
    fourteen: null,
    fifteen: null,
    sixteen: null,
    seventeen: null,
    eighteen: null,

    // -------------------- numbness or  tingling ----------------

    nineteen: null,
    twenty: '',

    // ------------------ Did an injury cause the problem -----------------

    twentyOne: null,
    twentyTwo: null,
    twentyThree: null,
    twentyFour: '',
    twentyFive: '',
    twentySix: null,

    twentySeven: '',     //Past surgery
    twentyEight: '',       //Any Previous treatments
    twentyNine: '',     //Allergies
    
    // ----------------- Smoke ciagrettes -------------
    thirty: null,  
    thirtyOne: '',
    thirtyTwo: '',
    thirtyThree: '',
    
    // ------------------- Alcohol -----------------
    thirtyFour: null,
    thirtyFive: '',
    thirtySix: '',

    // ------------------ Past Medical History -------------------

    //---------Constitutional ---------
    thirtySeven: null,
    thirtyEight: null,
    thirtyNine: null,
    fourty: null,

    // ----------- Ear Nose Throats ------------

    fourtyOne: null,
    fourtyTwo: null,
    fourtyThree: null,
    fourtyFour: null,
    fourtyFive: null,
    fourtySix: null,

    // ------------------CardioVuscular ---------------------

    fourtySeven: null,
    fourtyEight: null,
    fourtyNine: null,
    fifty: null,
    fiftyOne: null,

    // ------------------- Respiratory -----------------

    fiftyTwo: null,
    fiftyThree: null,
    fiftyFour: null,

    // ------------------- GestroIntestinal -----------------

    fiftyFive: null,
    fiftySix: null,
    fiftySeven: null,
    fiftyEight: null,

    // ----------------------- Musculoskeleton ----------------

    fiftyNine: null,
    sixty: null,
    sixtyOne: null,
    sixtyTwo: null,
    sixtyThree: null,

    // ---------------------- Eyes ----------------------------

    sixtyFour: null,
    sixtyFive: null,
    sixtySix: null,
    sixtySeven: null,

    // ------------------------- Skin ------------------------

    sixtyEight: null,
    sixtyNine: null,
    seventy: null,

    // ---------------------- Neurologic --------------------

    seventyOne: null,
    seventyTwo: null,
    seventyThree: null,
    seventyFour: null,
    seventyFive: null,
    seventySix: null,

    // ---------------------- EndoCrine -----------------

    seventySeven: null,
    seventyEight: null,
    
    //------------------------- Cancer ---------------------

    seventyNine: '',

    //---------------- Genitourinary -----------------

    eighty: null,
    eightyOne: null,
    eightyTwo: null,
    eightyThree: null,

    // ------------------------- Mental Health ----------------

    eightyFour: null,
    eightyFive: null,

    // ------------------------Other List -------------

    eightySix: '',

    //--------------------- 24 ----------- Family History ----------

    eightySeven: null,
    eightyEight: null,
    eightyNine: null,
    ninety: null,
    ninetyOne: null,
    ninetyTwo: null,
    ninetyThree: null,
    ninetyFour: null,
    ninetyFive: null,
    ninetySix: null,
    ninetySeven: null,
    ninetyEight: null,
    ninetyNine: null,
    hundred: null,
    oneHundredOne: null,
    oneHundredTwo: null,
    oneHundredThree: null,
    oneHundredFour: null,
    oneHundredFive: null,
    oneHundredSix: null,
    oneHundredSeven: null,
    oneHundredEight: null,
    oneHundredNine: null,
    oneHundredTen: null,
    confirmationTick1: null,
    confirmationTick2: null,

    AlertComponentMessage: '',
    AlertComponentVisible: false,
    spinnerLoading: false,
    consultation_id : null

  };

  radio_props = [
    {label: 'Yes  ', value: true},
    {label: 'No', value: false},
  ];


  backToScreen = async () => {
    this.props.navigation.navigate('Home');
  };

  componentDidMount = async () => {
    await console.log('Id from Payment is -------->',)
    const id = await this.props.navigation.getParam('id', 'nothing sent');
    await this.setState({consultation_id: id})
    console.log('Id from Payment is -------->',id)
  };

  setSliderValue = async (value) => {
    // console.log('Chnaged Value slider ------->', value);
    // Toast.show('Slider value : "' + value + '"', Toast.SHORT);
    this.setState({three: value});
  };

  closeAlertFunction = async () => {
    await this.setState({AlertComponentVisible: false});
    const { consultation_id } = this.state;
    this.props.navigation.navigate('ConsentForm',{id: consultation_id});
  };

  onSubmit = async () => {
      const d = this.state;
      const {one, two, three, four,five,six,seven,eight,nine,ten,eleven, nineteen, twentyOne, thirty, thirtyFour, confirmationTick1, confirmationTick2} = this.state;
      console.log("Dummy ----------->",d);
      // if(one === null || two === null || three === 0 || four === null || five === null || one === '' || two === '' || four === '' || five === '' || nineteen === null || twentyOne === null || thirty === null || thirtyFour === null)
      if(confirmationTick1 === null || confirmationTick1 === false || confirmationTick2 === null || confirmationTick2 === false)
      {
        Alert.alert('Please agree to the following terms below.')
    }else{
      await this.setState({spinnerLoading: true})
      const consultation_id = await this.state.consultation_id;
      console.log('Response =====------->', consultation_id);
      AddQuestionaire(d, consultation_id)
      .then(async (res) => {
        console.log('Response =====------->', res.data.message);
        this.setState({
          spinnerLoading: false,
          AlertComponentVisible: true,
          AlertComponentMessage: "Health Questionaire submitted succcessfully."
        });
      })
      .catch((err) => console.log('Error -------->', err));
    }
  }

  render() {
    const {selectedItems, selectedItemsTwo} = this.state;
    return (
      <ScrollView>
      <SafeAreaView style={styles.container}>
                    {/* ------------------------------- Components --------------------------------- */}
                    <AwesomeAlertComponent
            title="Success"
            visibleAlert={this.state.AlertComponentVisible}
            closeAlert={this.closeAlertFunction}
            message={this.state.AlertComponentMessage}></AwesomeAlertComponent>
          <SpinnerComponent
            Visible={this.state.spinnerLoading}></SpinnerComponent>
          {/* ---------------------------- End of Components ----------------- */}
          <SafeAreaView style={{flex: 1}}>

          <SvgGraph style={{right:-38}} />
      <View style={{marginTop: -280}}>

            {/* ---------------- Header ---------------- */}

            <MainHeader Back={this.backToScreen}></MainHeader>

            <View>
              <Text style={styles.titleText}>Health Questionaire</Text>
            </View>
            <View style={{marginTop: 14}}>
              {/* ---------------------- First Question --------------- */}
              <Text>When did your symptoms first start?</Text>
              <TextInput
                value={this.state.one}
                onChangeText={(one) => this.setState({one})}
                placeholder={'Answer the questions'}
                style={styles.inputText}></TextInput>

              {/* ---------------------- Second Question --------------- */}
              <Text>When did your symptoms get worse?</Text>
              <TextInput
                value={this.state.two}
                onChangeText={(two) => this.setState({two})}
                placeholder={'Answer the questions'}
                style={styles.inputText}></TextInput>

              {/* ---------------------- Third Question --------------- */}
              <Text>
                What is your current pain level? (Please check; 0: no pain, 10:
                worst pain imaginable)
              </Text>
              <View style={{flexDirection: 'row'}}>
              <Slider
                style={{height: 40, width: '100%', flex: 5}}
                step={1}
                minimumValue={0}
                maximumValue={10}
                onValueChange={(ChangedValue) =>
                  this.setSliderValue(ChangedValue)
                }
                minimumTrackTintColor="red"
                maximumTrackTintColor="blue"
              />
              <Text style={{alignSelf: 'center' , flex: 1}}>{this.state.three}</Text>
              </View>
              {/* ---------------------- Fourth Question --------------- */}
              <Text>
                What makes your pain worse? (sitting, standing walking, lying
                down, bending forward, leaning back, etc)
              </Text>
              <TextInput
                value={this.state.four}
                onChangeText={(four) => this.setState({four})}
                placeholder={'Answer the questions'}
                style={styles.inputText}></TextInput>

              {/* ---------------------- Fifth Question --------------- */}
              <Text>
                What makes your pain better? (sitting, standing walking, lying
                down, bending forward, leaning back, etc){' '}
              </Text>
              <TextInput
                value={this.state.five}
                onChangeText={(five) => this.setState({five})}
                placeholder={'Answer the questions'}
                style={styles.inputText}></TextInput>

              {/* ---------------------- Sixth Question --------------- */}
              <Text>
                Are you having any of the following difficulties? (Please check
                if applicable){' '}
              </Text>
              <View style={{flexDirection: 'row', margin: 3}}>
                  <CheckBox
                    title="Click Here"
                    value={this.state.six}
                    onValueChange={(value) =>
                      this.setState({
                        six: !this.state.six,
                      })
                    }
                  />
                  <Text style={{marginLeft: 10, alignSelf: 'center'}}> Loss of bladder control</Text>
                </View>
                <View style={{flexDirection: 'row', margin: 3}}>
                  <CheckBox
                    title="Click Here"
                    value={this.state.seven}
                    onValueChange={(value) =>
                      this.setState({
                        seven: !this.state.seven,
                      })
                    }
                  />
                  <Text style={{marginLeft: 10, alignSelf: 'center'}}>  Difficulty walking</Text>
                </View>
                <View style={{flexDirection: 'row', margin: 3}}>
                  <CheckBox
                    title="Click Here"
                    value={this.state.eight}
                    onValueChange={(value) =>
                      this.setState({
                        eight: !this.state.eight,
                      })
                    }
                  />
                  <Text style={{marginLeft: 10, alignSelf: 'center'}}>Use of cane, crutch, or walker</Text>
                </View>
                <View style={{flexDirection: 'row', margin: 3}}>
                  <CheckBox
                    title="Click Here"
                    value={this.state.nine}
                    onValueChange={(value) =>
                      this.setState({
                        nine: !this.state.nine,
                      })
                    }
                  />
                  <Text style={{marginLeft: 10, alignSelf: 'center'}}> Problems with balance</Text>
                </View>
                <View style={{flexDirection: 'row', margin: 3}}>
                  <CheckBox
                    title="Click Here"
                    value={this.state.ten}
                    onValueChange={(value) =>
                      this.setState({
                        ten: !this.state.ten,
                      })
                    }
                  />
                  <Text style={{marginLeft: 10, alignSelf: 'center'}}> Urgent desire to urinate</Text>
                </View>
                <View style={{flexDirection: 'row', margin: 3}}>
                  <CheckBox
                    title="Click Here"
                    value={this.state.eleven}
                    onValueChange={(value) =>
                      this.setState({
                        eleven: !this.state.eleven,
                      })
                    }
                  />
                  <Text style={{marginLeft: 10, alignSelf: 'center'}}>  Loss   of   sensation   in   genitalia   and   anal
region</Text>
                </View>
                <View style={{flexDirection: 'row', margin: 3}}>
                  <CheckBox
                    title="Click Here"
                    value={this.state.twelve}
                    onValueChange={(value) =>
                      this.setState({
                        twelve: !this.state.twelve,
                      })
                    }
                  />
                  <Text style={{marginLeft: 10, alignSelf: 'center'}}>Limping</Text>
                </View>
                <View style={{flexDirection: 'row', margin: 3}}>
                  <CheckBox
                    title="Click Here"
                    value={this.state.thirteen}
                    onValueChange={(value) =>
                      this.setState({
                        thirteen: !this.state.thirteen,
                      })
                    }
                  />
                  <Text style={{marginLeft: 10, alignSelf: 'center'}}> Use of wheelchair</Text>
                </View>
                <View style={{flexDirection: 'row', margin: 3}}>
                  <CheckBox
                    title="Click Here"
                    value={this.state.fourteen}
                    onValueChange={(value) =>
                      this.setState({
                        fourteen: !this.state.fourteen,
                      })
                    }
                  />
                  <Text style={{marginLeft: 10, alignSelf: 'center'}}>I have none of these</Text>
                </View>

              {/* ---------------------- Seventh Question --------------- */}
              <Text>Do you have any weakness?</Text>
              <View style={{flexDirection: 'row', margin: 3}}>
                  <CheckBox
                    title="Click Here"
                    value={this.state.fifteen}
                    onValueChange={(value) =>
                      this.setState({
                        fifteen: !this.state.fifteen,
                      })
                    }
                  />
                  <Text style={{marginLeft: 10, alignSelf: 'center'}}>Arms </Text>
                </View>
                <View style={{flexDirection: 'row', margin: 3}}>
                  <CheckBox
                    title="Click Here"
                    value={this.state.sixteen}
                    onValueChange={(value) =>
                      this.setState({
                        sixteen: !this.state.sixteen,
                      })
                    }
                  />
                  <Text style={{marginLeft: 10, alignSelf: 'center'}}>Hands</Text>
                </View>
                <View style={{flexDirection: 'row', margin: 3}}>
                  <CheckBox
                    title="Click Here"
                    value={this.state.seventeen}
                    onValueChange={(value) =>
                      this.setState({
                        seventeen: !this.state.seventeen,
                      })
                    }
                  />
                  <Text style={{marginLeft: 10, alignSelf: 'center'}}> Legs</Text>
                </View>
                <View style={{flexDirection: 'row', margin: 3}}>
                  <CheckBox
                    title="Click Here"
                    value={this.state.eighteen}
                    onValueChange={(value) =>
                      this.setState({
                        eighteen: !this.state.eighteen,
                      })
                    }
                  />
                  <Text style={{marginLeft: 10, alignSelf: 'center'}}>Feets</Text>
                </View>

              {/* ---------------------- Eighth Question --------------- */}
              <Text style={{marginTop: 20}}>Do you have numbness or tingling?</Text>
              <View style={{flexDirection: 'row'}}>
              <RadioForm
                    
                    buttonColor="grey"
                    idSeparator=","
                    radio_props={this.radio_props}
                    onPress={(value) => {
                      this.setState({nineteen: value});
                    }}
                    formHorizontal={true}
                    initial={-1}
                  />
              </View>

              {/* ---------------------- Ninth Question --------------- */}
              {this.state.nineteen ? (
                <TextInput
                  value={this.state.twenty}
                  onChangeText={(twenty) => this.setState({twenty})}
                  placeholder={'Where ?'}
                  style={styles.inputText}></TextInput>
              ) : null}

              {/* ---------------------- Injury Question --------------- */}
              <Text style={{marginTop: 20}}>Did an injury cause the current problems?</Text>
              <View style={{flexDirection: 'row'}}>
              <RadioForm
                    buttonColor="grey"
                    idSeparator=","
                    radio_props={this.radio_props}
                    onPress={(value) => {
                      this.setState({twentyOne: value});
                    }}
                    formHorizontal={true}
                    initial={-1}
                  />
              </View>
              {this.state.twentyOne ? (
                <View>
                  {/* ---------------------- Work Related Question --------------- */}
                  <Text>Work-related? </Text>
                  <View style={{flexDirection: 'row'}}>
                  <RadioForm
                    buttonColor="grey"
                    idSeparator=","
                    radio_props={this.radio_props}
                    onPress={(value) => {
                      this.setState({twentyTwo: value});
                    }}
                    formHorizontal={true}
                    initial={-1}
                  />
                  </View>

                  {/* ---------------------- Auto Accident Question --------------- */}
                  <Text>Auto accident?</Text>
                  <View style={{flexDirection: 'row'}}>
                  <RadioForm
                    buttonColor="grey"
                    idSeparator=","
                    radio_props={this.radio_props}
                    onPress={(value) => {
                      this.setState({twentyThree: value});
                    }}
                    formHorizontal={true}
                    initial={-1}
                  />
                  </View>
                  {/* ------------------------- Other Type of Injury -------------------- */}
                  <TextInput
                    value={this.state.twentyFour}
                    onChangeText={(twentyFour) => this.setState({twentyFour})}
                    placeholder={'Other type of injury'}
                    style={styles.inputText}></TextInput>

                  {/* ------------------------- Date of Injury -------------------- */}
                  <TextInput
                    value={this.state.twentyFive}
                    onChangeText={(twentyFive) => this.setState({twentyFive})}
                    placeholder={'Date of injury'}
                    style={styles.inputText}></TextInput>

                  {/* -----------------Law Suit Pending -------------------------------- */}
                  <Text>Law suit Pending?</Text>
                  <View style={{flexDirection: 'row'}}>
                  <RadioForm
                    buttonColor="grey"
                    idSeparator=","
                    radio_props={this.radio_props}
                    onPress={(value) => {
                      this.setState({twentySix: value});
                    }}
                    formHorizontal={true}
                    initial={-1}
                  />
                  </View>
                </View>
              ) : null}

              {/* ----------------------Injury End ---------------------------- */}

              {/* ---------------------- Fifth Question --------------- */}
              <Text>
                Past Surgical History (Please list all surgeries and approximate
                dates)
              </Text>
              <TextInput
                value={this.state.twentySeven}
                onChangeText={(twentySeven) => this.setState({twentySeven})}
                placeholder={'Answer the questions'}
                style={styles.inputText}></TextInput>

              {/* ---------------------- Fifth Question --------------- */}
              <Text>
                Any previous treatments for your symptoms? (Chiropractor,
                Physiotherapy, acupuncture, injections)
              </Text>
              <TextInput
                value={this.state.twentyEight}
                onChangeText={(twentyEight) => this.setState({twentyEight})}
                placeholder={'Answer the questions'}
                style={styles.inputText}></TextInput>

              {/* ---------------------- Fifth Question --------------- */}
              <Text>
                Allergies (Please list all medication and latex allergies and
                describe reaction)
              </Text>
              <TextInput
                value={this.state.twentyNine}
                onChangeText={(twentyNine) => this.setState({twentyNine})}
                placeholder={'Answer the questions'}
                style={styles.inputText}></TextInput>

              {/* --------------------------- Ciagrettes ---------------------------- */}

              <Text>Do you smoke cigarettes? </Text>
              <View style={{flexDirection: 'row'}}>
              <RadioForm
                    buttonColor="grey"
                    idSeparator=","
                    radio_props={this.radio_props}
                    onPress={(value) => {
                      this.setState({thirty: value});
                    }}
                    formHorizontal={true}
                    initial={-1}
                  />
              </View>
              {this.state.thirty ? (
                <View>
                  {/* ------------- How Many per Day --------------- */}
                  <Text>If, yes how many per day?</Text>
                  <TextInput
                    value={this.state.thirtyOne}
                    onChangeText={(thirtyOne) => this.setState({thirtyOne})}
                    placeholder={'Answer the questions'}
                    style={styles.inputText}></TextInput>

                  {/* -----------------------How many years ---------------- */}
                  <Text>How many years?</Text>
                  <TextInput
                    value={this.state.thirtyTwo}
                    onChangeText={(thirtyTwo) => this.setState({thirtyTwo})}
                    placeholder={'Answer the questions'}
                    style={styles.inputText}></TextInput>

                  {/* -----------------------If you quit ---------------- */}
                  <Text>If you quit, when?</Text>
                  <TextInput
                    value={this.state.thirtyThree}
                    onChangeText={(thirtyThree) => this.setState({thirtyThree})}
                    placeholder={'Answer the questions'}
                    style={styles.inputText}></TextInput>
                </View>
              ) : null}

              {/* --------------------------- Alcohol ---------------------------- */}

              <Text style={{marginTop: 20}}>Do you drink alcohol? </Text>
              <View style={{flexDirection: 'row'}}>
              <RadioForm
                    buttonColor="grey"
                    idSeparator=","
                    radio_props={this.radio_props}
                    onPress={(value) => {
                      this.setState({thirtyFour: value});
                    }}
                    formHorizontal={true}
                    initial={-1}
                  />
              </View>
              {this.state.thirtyFour ? (
                <View>
                  {/* -----------------------How many years ---------------- */}
                  <Text>How often and how much? </Text>
                  <TextInput
                    value={this.state.thirtyFive}
                    onChangeText={(thirtyFive) => this.setState({thirtyFive})}
                    placeholder={'Answer the questions'}
                    style={styles.inputText}></TextInput>

                  {/* -----------------------If you quit ---------------- */}
                  <Text>Current Occupation : </Text>
                  <TextInput
                    value={this.state.thirtySix}
                    onChangeText={(thirtySix) => this.setState({thirtySix})}
                    placeholder={'Answer the questions'}
                    style={styles.inputText}></TextInput>
                </View>
              ) : null}

              <View style={styles.headTitleBoxes}>
              <Text style={{fontSize: 20}}>Past Medical History</Text>
              </View>

              {/* --------------------------- Past Medical History -------------------------- */}
              {/* --------------------------------------------- */}
              {/* ---------------------------------- */}
              {/* ------------------------- */}

              <View>
                <Text style={styles.checkboxTitle}> Constitutional </Text>
                <View style={styles.checkBoxValue}>
                  <Text style={{marginRight: 10, alignSelf: 'center'}}>1. Recent weight loss </Text>
                  <CheckBox
                    title="Click Here"
                    value={this.state.thirtySeven}
                    onValueChange={(value) =>
                      this.setState({
                        thirtySeven: !this.state.thirtySeven,
                      })
                    }
                  />
                </View>
                <View style={styles.checkBoxValue}>
                  <Text style={{marginRight: 10, alignSelf: 'center'}}>2. Recent fever or chills. </Text>
                  <CheckBox
                    title="Click Here"
                    value={this.state.thirtyEight}
                    onValueChange={(value) =>
                      this.setState({
                        thirtyEight: !this.state.thirtyEight,
                      })
                    }
                  />
                </View>
                <View style={styles.checkBoxValue}>
                  <Text style={{marginRight: 10, alignSelf: 'center'}}>3. Night Sweats </Text>
                  <CheckBox
                    title="Click Here"
                    value={this.state.thirtyNine}
                    onValueChange={(value) =>
                      this.setState({
                        thirtyNine: !this.state.thirtyNine,
                      })
                    }
                  />
                </View>
                <View style={styles.checkBoxValue}>
                  <Text style={{marginRight: 10, alignSelf: 'center'}}>4. Difficulty sleeping </Text>
                  <CheckBox
                    title="Click Here"
                    value={this.state.fourty}
                    onValueChange={(value) =>
                      this.setState({
                        fourty: !this.state.fourty,
                      })
                    }
                  />
                </View>
              </View>

              {/* -------------------------- Ear, Nose, Throats ------------------- */}

              <View>
                <Text style={styles.checkboxTitle}> Ear, Nose, Throats </Text>
                <View style={styles.checkBoxValue}>
                  <Text style={{marginRight: 10, alignSelf: 'center'}}>1. Hearing loss </Text>
                  <CheckBox
                    title="Click Here"
                    value={this.state.fourtyOne}
                    onValueChange={(value) =>
                      this.setState({
                        fourtyOne: !this.state.fourtyOne,
                      })
                    }
                  />
                </View>
                <View style={styles.checkBoxValue}>
                  <Text style={{marginRight: 10, alignSelf: 'center'}}>2. Ringing in ears. </Text>
                  <CheckBox
                    title="Click Here"
                    value={this.state.fourtyTwo}
                    onValueChange={(value) =>
                      this.setState({
                        fourtyTwo: !this.state.fourtyTwo,
                      })
                    }
                  />
                </View>
                <View style={styles.checkBoxValue}>
                  <Text style={{marginRight: 10, alignSelf: 'center'}}>3. Sinus Problems </Text>
                  <CheckBox
                    title="Click Here"
                    value={this.state.fourtyThree}
                    onValueChange={(value) =>
                      this.setState({
                        fourtyThree: !this.state.fourtyThree,
                      })
                    }
                  />
                </View>
                <View style={styles.checkBoxValue}>
                  <Text style={{marginRight: 10, alignSelf: 'center'}}>4. Sore Throat </Text>
                  <CheckBox
                    title="Click Here"
                    value={this.state.fourtyFour}
                    onValueChange={(value) =>
                      this.setState({
                        fourtyFour: !this.state.fourtyFour,
                      })
                    }
                  />
                </View>
                <View style={styles.checkBoxValue}>
                  <Text style={{marginRight: 10, alignSelf: 'center'}}>5. Active dental issues </Text>
                  <CheckBox
                    title="Click Here"
                    value={this.state.fourtyFive}
                    onValueChange={(value) =>
                      this.setState({
                        fourtyFive: !this.state.fourtyFive,
                      })
                    }
                  />
                </View>
                <View style={styles.checkBoxValue}>
                  <Text style={{marginRight: 10, alignSelf: 'center'}}>5. Wear hearing aid or dentures</Text>
                  <CheckBox
                    title="Click Here"
                    value={this.state.fourtySix}
                    onValueChange={(value) =>
                      this.setState({
                        fourtySix: !this.state.fourtySix,
                      })
                    }
                  />
                </View>
              </View>

              {/* ------------------------------- CardioVascular ----------------------- */}

              <View>
                <Text style={styles.checkboxTitle}> CardioVascular </Text>
                <View style={styles.checkBoxValue}>
                  <Text style={{marginRight: 10, alignSelf: 'center'}}>1. Irregular Heart Beat </Text>
                  <CheckBox
                    title="Click Here"
                    value={this.state.fourtySeven}
                    onValueChange={(value) =>
                      this.setState({
                        fourtySeven: !this.state.fourtySeven,
                      })
                    }
                  />
                </View>
                <View style={styles.checkBoxValue}>
                  <Text style={{marginRight: 10, alignSelf: 'center'}}>2. Chest pain ,angeina. </Text>
                  <CheckBox
                    title="Click Here"
                    value={this.state.fourtyEight}
                    onValueChange={(value) =>
                      this.setState({
                        fourtyEight: !this.state.fourtyEight,
                      })
                    }
                  />
                </View>
                <View style={styles.checkBoxValue}>
                  <Text style={{marginRight: 10, alignSelf: 'center'}}>3. Bleeding Problems </Text>
                  <CheckBox
                    title="Click Here"
                    value={this.state.fourtyNine}
                    onValueChange={(value) =>
                      this.setState({
                        fourtyNine: !this.state.fourtyNine,
                      })
                    }
                  />
                </View>
                <View style={styles.checkBoxValue}>
                  <Text style={{marginRight: 10, alignSelf: 'center'}}>4. Blood Clots </Text>
                  <CheckBox
                    title="Click Here"
                    value={this.state.fifty}
                    onValueChange={(value) =>
                      this.setState({
                        fifty: !this.state.fifty,
                      })
                    }
                  />
                </View>
                <View style={styles.checkBoxValue}>
                  <Text style={{marginRight: 10, alignSelf: 'center'}}>5. Swealing arms or legs. </Text>
                  <CheckBox
                    title="Click Here"
                    value={this.state.fiftyOne}
                    onValueChange={(value) =>
                      this.setState({
                        fiftyOne: !this.state.fiftyOne,
                      })
                    }
                  />
                </View>
              </View>

              {/* ---------------------------------------------- Respiratory ------------------------------------------ */}

              <View>
                <Text style={styles.checkboxTitle}> Respiratory </Text>
                <View style={styles.checkBoxValue}>
                  <Text style={{marginRight: 10, alignSelf: 'center'}}>1. Shortness of breath </Text>
                  <CheckBox
                    title="Click Here"
                    value={this.state.fiftyTwo}
                    onValueChange={(value) =>
                      this.setState({
                        fiftyTwo: !this.state.fiftyTwo,
                      })
                    }
                  />
                </View>
                <View style={styles.checkBoxValue}>
                  <Text style={{marginRight: 10, alignSelf: 'center'}}>2. Cough. </Text>
                  <CheckBox
                    title="Click Here"
                    value={this.state.fiftyThree}
                    onValueChange={(value) =>
                      this.setState({
                        fiftyThree: !this.state.fiftyThree,
                      })
                    }
                  />
                </View>
                <View style={styles.checkBoxValue}>
                  <Text style={{marginRight: 10, alignSelf: 'center'}}>3. Breathing Problems </Text>
                  <CheckBox
                    title="Click Here"
                    value={this.state.fiftyFour}
                    onValueChange={(value) =>
                      this.setState({
                        fiftyFour: !this.state.fiftyFour,
                      })
                    }
                  />
                </View>
              </View>

              {/* --------------------------------------------- Gestrointestinal -------------------------------------- */}

              <View>
                <Text style={styles.checkboxTitle}> Gestrointestinal </Text>
                <View style={styles.checkBoxValue}>
                  <Text style={{marginRight: 10, alignSelf: 'center'}}>1. HeartBurn </Text>
                  <CheckBox
                    title="Click Here"
                    value={this.state.fiftyFive}
                    onValueChange={(value) =>
                      this.setState({
                        fiftyFive: !this.state.fiftyFive,
                      })
                    }
                  />
                </View>
                <View style={styles.checkBoxValue}>
                  <Text style={{marginRight: 10, alignSelf: 'center'}}>2. Nauseia and or vommiting. </Text>
                  <CheckBox
                    title="Click Here"
                    value={this.state.fiftySix}
                    onValueChange={(value) =>
                      this.setState({
                        fiftySix: !this.state.fiftySix,
                      })
                    }
                  />
                </View>
                <View style={styles.checkBoxValue}>
                  <Text style={{marginRight: 10, alignSelf: 'center'}}>3. Changes in bowels habits. </Text>
                  <CheckBox
                    title="Click Here"
                    value={this.state.fiftySeven}
                    onValueChange={(value) =>
                      this.setState({
                        fiftySeven: !this.state.fiftySeven,
                      })
                    }
                  />
                </View>
                <View style={styles.checkBoxValue}>
                  <Text style={{marginRight: 10, alignSelf: 'center'}}>4. Blood in bowel movements. </Text>
                  <CheckBox
                    title="Click Here"
                    value={this.state.fiftyEight}
                    onValueChange={(value) =>
                      this.setState({
                        fiftyEight: !this.state.fiftyEight,
                      })
                    }
                  />
                </View>
              </View>

              {/* ------------------------------------------- Musculoskeletal ----------------------------------------------- */}

              <View>
                <Text style={styles.checkboxTitle}> Musculoskeletal </Text>
                <View style={styles.checkBoxValue}>
                  <Text style={{marginRight: 10, alignSelf: 'center'}}>1. Joint Pain </Text>
                  <CheckBox
                    title="Click Here"
                    value={this.state.fiftyNine}
                    onValueChange={(value) =>
                      this.setState({
                        fiftyNine: !this.state.fiftyNine,
                      })
                    }
                  />
                </View>
                <View style={styles.checkBoxValue}>
                  <Text style={{marginRight: 10, alignSelf: 'center'}}>2. Limb Pain. </Text>
                  <CheckBox
                    title="Click Here"
                    value={this.state.sixty}
                    onValueChange={(value) =>
                      this.setState({
                        sixty: !this.state.sixty,
                      })
                    }
                  />
                </View>
                <View style={styles.checkBoxValue}>
                  <Text style={{marginRight: 10, alignSelf: 'center'}}>3. Muscle weakness. </Text>
                  <CheckBox
                    title="Click Here"
                    value={this.state.sixtyOne}
                    onValueChange={(value) =>
                      this.setState({
                        sixtyOne: !this.state.sixtyOne,
                      })
                    }
                  />
                </View>
                <View style={styles.checkBoxValue}>
                  <Text style={{marginRight: 10, alignSelf: 'center'}}>4. Difficult moving arm/leg. </Text>
                  <CheckBox
                    title="Click Here"
                    value={this.state.sixtyTwo}
                    onValueChange={(value) =>
                      this.setState({
                        sixtyTwo: !this.state.sixtyTwo,
                      })
                    }
                  />
                </View>
                <View style={styles.checkBoxValue}>
                  <Text style={{marginRight: 10, alignSelf: 'center'}}>5. Swelling limb/joints. </Text>
                  <CheckBox
                    title="Click Here"
                    value={this.state.sixtyThree}
                    onValueChange={(value) =>
                      this.setState({
                        sixtyThree: !this.state.sixtyThree,
                      })
                    }
                  />
                </View>
              </View>

              {/* ----------------------------------------------- Eyes ---------------------------------------------------------- */}

              <View>
                <Text style={styles.checkboxTitle}> Eyes </Text>
                <View style={styles.checkBoxValue}>
                  <Text style={{marginRight: 10, alignSelf: 'center'}}>1. Wearing glass or contacts. </Text>
                  <CheckBox
                    title="Click Here"
                    value={this.state.sixtyFour}
                    onValueChange={(value) =>
                      this.setState({
                        sixtyFour: !this.state.sixtyFour,
                      })
                    }
                  />
                </View>
                <View style={styles.checkBoxValue}>
                  <Text style={{marginRight: 10, alignSelf: 'center'}}>2. Cataracts. </Text>
                  <CheckBox
                    title="Click Here"
                    value={this.state.sixtyFive}
                    onValueChange={(value) =>
                      this.setState({
                        sixtyFive: !this.state.sixtyFive,
                      })
                    }
                  />
                </View>
                <View style={styles.checkBoxValue}>
                  <Text style={{marginRight: 10, alignSelf: 'center'}}>3. Glaucoma. </Text>
                  <CheckBox
                    title="Click Here"
                    value={this.state.sixtySix}
                    onValueChange={(value) =>
                      this.setState({
                        sixtySix: !this.state.sixtySix,
                      })
                    }
                  />
                </View>
                <View style={styles.checkBoxValue}>
                  <Text style={{marginRight: 10, alignSelf: 'center'}}>4. Vision Problems. </Text>
                  <CheckBox
                    title="Click Here"
                    value={this.state.sixtySeven}
                    onValueChange={(value) =>
                      this.setState({
                        sixtySeven: !this.state.sixtySeven,
                      })
                    }
                  />
                </View>
              </View>

              {/* --------------------------------------------------- Skin --------------------------------------------------- */}

              <View>
                <Text style={styles.checkboxTitle}> Skin </Text>
                <View style={styles.checkBoxValue}>
                  <Text style={{marginRight: 10, alignSelf: 'center'}}>1. Psoriasis or cezema. </Text>
                  <CheckBox
                    title="Click Here"
                    value={this.state.sixtyEight}
                    onValueChange={(value) =>
                      this.setState({
                        sixtyEight: !this.state.sixtyEight,
                      })
                    }
                  />
                </View>
                <View style={styles.checkBoxValue}>
                  <Text style={{marginRight: 10, alignSelf: 'center'}}>2. Open sores or cuts. </Text>
                  <CheckBox
                    title="Click Here"
                    value={this.state.sixtyNine}
                    onValueChange={(value) =>
                      this.setState({
                        sixtyNine: !this.state.sixtyNine,
                      })
                    }
                  />
                </View>
                <View style={styles.checkBoxValue}>
                  <Text style={{marginRight: 10, alignSelf: 'center'}}>3. Dermatis - rash. </Text>
                  <CheckBox
                    title="Click Here"
                    value={this.state.seventy}
                    onValueChange={(value) =>
                      this.setState({
                        seventy: !this.state.seventy,
                      })
                    }
                  />
                </View>
              </View>

              {/* ------------------------------------------------- Neurologic ------------------------------------------ */}

              <View>
                <Text style={styles.checkboxTitle}> Neurologic </Text>
                <View style={styles.checkBoxValue}>
                  <Text style={{marginRight: 10, alignSelf: 'center'}}>1. Headache. </Text>
                  <CheckBox
                    title="Click Here"
                    value={this.state.seventyOne}
                    onValueChange={(value) =>
                      this.setState({
                        seventyOne: !this.state.seventyOne,
                      })
                    }
                  />
                </View>
                <View style={styles.checkBoxValue}>
                  <Text style={{marginRight: 10, alignSelf: 'center'}}>2. Dizziness. </Text>
                  <CheckBox
                    title="Click Here"
                    value={this.state.seventyTwo}
                    onValueChange={(value) =>
                      this.setState({
                        seventyTwo: !this.state.seventyTwo,
                      })
                    }
                  />
                </View>
                <View style={styles.checkBoxValue}>
                  <Text style={{marginRight: 10, alignSelf: 'center'}}>3. Falls. </Text>
                  <CheckBox
                    title="Click Here"
                    value={this.state.seventyThree}
                    onValueChange={(value) =>
                      this.setState({
                        seventyThree: !this.state.seventyThree,
                      })
                    }
                  />
                </View>
                <View style={styles.checkBoxValue}>
                  <Text style={{marginRight: 10, alignSelf: 'center'}}>4. Memory Problems. </Text>
                  <CheckBox
                    title="Click Here"
                    value={this.state.seventyFour}
                    onValueChange={(value) =>
                      this.setState({
                        seventyFour: !this.state.seventyFour,
                      })
                    }
                  />
                </View>
                <View style={styles.checkBoxValue}>
                  <Text style={{marginRight: 10, alignSelf: 'center'}}>5. Balance Problems. </Text>
                  <CheckBox
                    title="Click Here"
                    value={this.state.seventyFive}
                    onValueChange={(value) =>
                      this.setState({
                        seventyFive: !this.state.seventyFive,
                      })
                    }
                  />
                </View>
                <View style={styles.checkBoxValue}>
                  <Text style={{marginRight: 10, alignSelf: 'center'}}>6. Numbness. </Text>
                  <CheckBox
                    title="Click Here"
                    value={this.state.seventySix}
                    onValueChange={(value) =>
                      this.setState({
                        seventySix: !this.state.seventySix,
                      })
                    }
                  />
                </View>
              </View>

              {/* --------------------------------------------- EndoCrine --------------------------------------------- */}

              <View>
                <Text style={styles.checkboxTitle}> EndoCrine </Text>
                <View style={styles.checkBoxValue}>
                  <Text style={{marginRight: 10, alignSelf: 'center'}}>1. Diabetes. </Text>
                  <CheckBox
                    title="Click Here"
                    value={this.state.seventySeven}
                    onValueChange={(value) =>
                      this.setState({
                        seventySeven: !this.state.seventySeven,
                      })
                    }
                  />
                </View>
                <View style={styles.checkBoxValue}>
                  <Text style={{marginRight: 10, alignSelf: 'center'}}>2. Thyroid Disorder. </Text>
                  <CheckBox
                    title="Click Here"
                    value={this.state.seventyEight}
                    onValueChange={(value) =>
                      this.setState({
                        seventyEight: !this.state.seventyEight,
                      })
                    }
                  />
                </View>
              </View>

              {/* ----------------------------------------- Cancer --------------------------------------------------- */}

              <View>
                <Text style={styles.checkboxTitle}> Cancer </Text>
                <View>
                  <Text>What kind ?. </Text>
                  <TextInput
                    value={this.state.seventyNine}
                    onChangeText={(seventyNine) => this.setState({seventyNine})}
                    placeholder={'Answer the questions'}
                    style={styles.inputText}></TextInput>
                </View>
              </View>

              {/* ----------------------------------------Genitourinary -------------------------------------- */}

              <View>
                <Text style={styles.checkboxTitle}> Genitourinary </Text>
                <View style={styles.checkBoxValue}>
                  <Text style={{marginRight: 10, alignSelf: 'center'}}>1. Frequesnt bladder infections. </Text>
                  <CheckBox
                    title="Click Here"
                    value={this.state.eighty}
                    onValueChange={(value) =>
                      this.setState({
                        eighty: !this.state.eighty,
                      })
                    }
                  />
                </View>
                <View style={styles.checkBoxValue}>
                  <Text style={{marginRight: 10, alignSelf: 'center'}}>2. Painful urination. </Text>
                  <CheckBox
                    title="Click Here"
                    value={this.state.eightyOne}
                    onValueChange={(value) =>
                      this.setState({
                        eightyOne: !this.state.eightyOne,
                      })
                    }
                  />
                </View>
                <View style={styles.checkBoxValue}>
                  <Text style={{marginRight: 10, alignSelf: 'center'}}>3. Difficulty starting urination. </Text>
                  <CheckBox
                    title="Click Here"
                    value={this.state.eightyTwo}
                    onValueChange={(value) =>
                      this.setState({
                        eightyTwo: !this.state.eightyTwo,
                      })
                    }
                  />
                </View>
                <View style={styles.checkBoxValue}>
                  <Text style={{marginRight: 10, alignSelf: 'center'}}>4. Blood in urine. </Text>
                  <CheckBox
                    title="Click Here"
                    value={this.state.eightyThree}
                    onValueChange={(value) =>
                      this.setState({
                        eightyThree: !this.state.eightyThree,
                      })
                    }
                  />
                </View>
              </View>

              {/* ------------------------------------- Mental Health --------------------------------------- */}

              <View>
                <Text style={styles.checkboxTitle}> Mental Health </Text>
                <View style={styles.checkBoxValue}>
                  <Text style={{marginRight: 10, alignSelf: 'center'}}>1. Depression. </Text>
                  <CheckBox
                    title="Click Here"
                    value={this.state.eightyFour}
                    onValueChange={(value) =>
                      this.setState({
                        eightyFour: !this.state.eightyFour,
                      })
                    }
                  />
                </View>
                <View style={styles.checkBoxValue}>
                  <Text style={{marginRight: 10, alignSelf: 'center'}}>2. Anxiety. </Text>
                  <CheckBox
                    title="Click Here"
                    value={this.state.eightyFive}
                    onValueChange={(value) =>
                      this.setState({
                        eightyFive: !this.state.eightyFive,
                      })
                    }
                  />
                </View>
              </View>

              {/* --------------------------------- Other ------------------------------------------- */}

              <View>
                <Text style={styles.checkboxTitle}> Other </Text>
                <View>
                  <Text>List ?. </Text>
                  <TextInput
                    value={this.state.eightySix}
                    onChangeText={(eightySix) => this.setState({eightySix})}
                    placeholder={'Answer the questions'}
                    style={styles.inputText}></TextInput>
                </View>
              </View>

              {/* ----------------------------- End -------------------------------------- */}
              <View style={styles.headTitleBoxes}>
              <Text style={{fontSize: 20}}>Family History</Text>
              </View>

              {/* ------------------------- Extra Fields -------------------- */}

              <View style={{flexDirection: 'row', margin: 3}}>
                  <CheckBox
                    title="Click Here"
                    value={this.state.eightySeven}
                    onValueChange={(value) =>
                      this.setState({
                        eightySeven: !this.state.eightySeven,
                      })
                    }
                  />
                  <Text style={{marginLeft: 10, alignSelf: 'center'}}>Diabetes</Text>
                </View>
                <View style={{flexDirection: 'row', margin: 3}}>
                  <CheckBox
                    title="Click Here"
                    value={this.state.eightyEight}
                    onValueChange={(value) =>
                      this.setState({
                        eightyEight: !this.state.eightyEight,
                      })
                    }
                  />
                  <Text style={{marginLeft: 10, alignSelf: 'center'}}>Lung Disease</Text>
                </View>
                <View style={{flexDirection: 'row', margin: 3}}>
                  <CheckBox
                    title="Click Here"
                    value={this.state.eightyNine}
                    onValueChange={(value) =>
                      this.setState({
                        eightyNine: !this.state.eightyNine,
                      })
                    }
                  />
                  <Text style={{marginLeft: 10, alignSelf: 'center'}}>Tuberculosis</Text>
                </View>
                <View style={{flexDirection: 'row', margin: 3}}>
                  <CheckBox
                    title="Click Here"
                    value={this.state.ninety}
                    onValueChange={(value) =>
                      this.setState({
                        ninety: !this.state.ninety,
                      })
                    }
                  />
                  <Text style={{marginLeft: 10, alignSelf: 'center'}}>Asthma</Text>
                </View>
                <View style={{flexDirection: 'row', margin: 3}}>
                  <CheckBox
                    title="Click Here"
                    value={this.state.ninetyOne}
                    onValueChange={(value) =>
                      this.setState({
                        ninetyOne: !this.state.ninetyOne,
                      })
                    }
                  />
                  <Text style={{marginLeft: 10, alignSelf: 'center'}}>Alcoholism</Text>
                </View>
                <View style={{flexDirection: 'row', margin: 3}}>
                  <CheckBox
                    title="Click Here"
                    value={this.state.ninetyTwo}
                    onValueChange={(value) =>
                      this.setState({
                        ninetyTwo: !this.state.ninetyTwo,
                      })
                    }
                  />
                  <Text style={{marginLeft: 10, alignSelf: 'center'}}>Ulcers</Text>
                </View>
                <View style={{flexDirection: 'row', margin: 3}}>
                  <CheckBox
                    title="Click Here"
                    value={this.state.ninetyThree}
                    onValueChange={(value) =>
                      this.setState({
                        ninetyThree: !this.state.ninetyThree,
                      })
                    }
                  />
                  <Text style={{marginLeft: 10, alignSelf: 'center'}}>Hepatitis A/B/C</Text>
                </View>
                <View style={{flexDirection: 'row', margin: 3}}>
                  <CheckBox
                    title="Click Here"
                    value={this.state.ninetyFour}
                    onValueChange={(value) =>
                      this.setState({
                        ninetyFour: !this.state.ninetyFour,
                      })
                    }
                  />
                  <Text style={{marginLeft: 10, alignSelf: 'center'}}>Gestrointestinal Disease</Text>
                </View>
                <View style={{flexDirection: 'row', margin: 3}}>
                  <CheckBox
                    title="Click Here"
                    value={this.state.ninetyFive}
                    onValueChange={(value) =>
                      this.setState({
                        ninetyFive: !this.state.ninetyFive,
                      })
                    }
                  />
                  <Text style={{marginLeft: 10, alignSelf: 'center'}}>Heart Disease</Text>
                </View>
                <View style={{flexDirection: 'row', margin: 3}}>
                  <CheckBox
                    title="Click Here"
                    value={this.state.ninetySix}
                    onValueChange={(value) =>
                      this.setState({
                        ninetySix: !this.state.ninetySix,
                      })
                    }
                  />
                  <Text style={{marginLeft: 10, alignSelf: 'center'}}>High Blood Pressure</Text>
                </View>
                <View style={{flexDirection: 'row', margin: 3}}>
                  <CheckBox
                    title="Click Here"
                    value={this.state.ninetySeven}
                    onValueChange={(value) =>
                      this.setState({
                        ninetySeven: !this.state.ninetySeven,
                      })
                    }
                  />
                  <Text style={{marginLeft: 10, alignSelf: 'center'}}>Stroke</Text>
                </View>
                <View style={{flexDirection: 'row', margin: 3}}>
                  <CheckBox
                    title="Click Here"
                    value={this.state.ninetyEight}
                    onValueChange={(value) =>
                      this.setState({
                        ninetyEight: !this.state.ninetyEight,
                      })
                    }
                  />
                  <Text style={{marginLeft: 10, alignSelf: 'center'}}>Heart Attack</Text>
                </View>
                <View style={{flexDirection: 'row', margin: 3}}>
                  <CheckBox
                    title="Click Here"
                    value={this.state.ninetyNine}
                    onValueChange={(value) =>
                      this.setState({
                        ninetyNine: !this.state.ninetyNine,
                      })
                    }
                  />
                  <Text style={{marginLeft: 10, alignSelf: 'center'}}>Blood Clots</Text>
                </View>
                <View style={{flexDirection: 'row', margin: 3}}>
                  <CheckBox
                    title="Click Here"
                    value={this.state.hundred}
                    onValueChange={(value) =>
                      this.setState({
                        hundred: !this.state.hundred,
                      })
                    }
                  />
                  <Text style={{marginLeft: 10, alignSelf: 'center'}}>Bleeding Tendencies</Text>
                </View>
                <View style={{flexDirection: 'row', margin: 3}}>
                  <CheckBox
                    title="Click Here"
                    value={this.state.oneHundredOne}
                    onValueChange={(value) =>
                      this.setState({
                        oneHundredOne: !this.state.oneHundredOne,
                      })
                    }
                  />
                  <Text style={{marginLeft: 10, alignSelf: 'center'}}>Coronary Artery Disease</Text>
                </View>
                <View style={{flexDirection: 'row', margin: 3}}>
                  <CheckBox
                    title="Click Here"
                    value={this.state.oneHundredTwo}
                    onValueChange={(value) =>
                      this.setState({
                        oneHundredTwo: !this.state.oneHundredTwo,
                      })
                    }
                  />
                  <Text style={{marginLeft: 10, alignSelf: 'center'}}>Peripheral vuscular Disease</Text>
                </View>
                <View style={{flexDirection: 'row', margin: 3}}>
                  <CheckBox
                    title="Click Here"
                    value={this.state.oneHundredThree}
                    onValueChange={(value) =>
                      this.setState({
                        oneHundredThree: !this.state.oneHundredThree,
                      })
                    }
                  />
                  <Text style={{marginLeft: 10, alignSelf: 'center'}}>Cancer</Text>
                </View>
                <View style={{flexDirection: 'row', margin: 3}}>
                  <CheckBox
                    title="Click Here"
                    value={this.state.oneHundredFour}
                    onValueChange={(value) =>
                      this.setState({
                        oneHundredFour: !this.state.oneHundredFour,
                      })
                    }
                  />
                  <Text style={{marginLeft: 10, alignSelf: 'center'}}>Arthritis</Text>
                </View>
                <View style={{flexDirection: 'row', margin: 3}}>
                  <CheckBox
                    title="Click Here"
                    value={this.state.oneHundredFive}
                    onValueChange={(value) =>
                      this.setState({
                        oneHundredFive: !this.state.oneHundredFive,
                      })
                    }
                  />
                  <Text style={{marginLeft: 10, alignSelf: 'center'}}>Osteoporosis</Text>
                </View>
                <View style={{flexDirection: 'row', margin: 3}}>
                  <CheckBox
                    title="Click Here"
                    value={this.state.oneHundredSix}
                    onValueChange={(value) =>
                      this.setState({
                        oneHundredSix: !this.state.oneHundredSix,
                      })
                    }
                  />
                  <Text style={{marginLeft: 10, alignSelf: 'center'}}>Seizure Disorders</Text>
                </View>
                <View style={{flexDirection: 'row', margin: 3}}>
                  <CheckBox
                    title="Click Here"
                    value={this.state.oneHundredSeven}
                    onValueChange={(value) =>
                      this.setState({
                        oneHundredSeven: !this.state.oneHundredSeven,
                      })
                    }
                  />
                  <Text style={{marginLeft: 10, alignSelf: 'center'}}>Depressions</Text>
                </View>
                <View style={{flexDirection: 'row', margin: 3}}>
                  <CheckBox
                    title="Click Here"
                    value={this.state.oneHundredEight}
                    onValueChange={(value) =>
                      this.setState({
                        oneHundredEight: !this.state.oneHundredEight,
                      })
                    }
                  />
                  <Text  style={{marginLeft: 10, alignSelf: 'center'}}>Mental Illness</Text>
                </View>

                <View style={{flexDirection: 'row', margin: 3}}>
                  <CheckBox
                    title="Click Here"
                    value={this.state.oneHundredNine}
                    onValueChange={(value) =>
                      this.setState({
                        oneHundredNine: !this.state.oneHundredNine,
                      })
                    }
                  />
                  <Text style={{marginLeft: 10, alignSelf: 'center'}}>Thyroid Disease</Text>
                </View>

                <View style={{flexDirection: 'row', margin: 3}}>
                  <CheckBox
                    title="Click Here"
                    value={this.state.oneHundredTen}
                    onValueChange={(value) =>
                      this.setState({
                        oneHundredTen: !this.state.oneHundredTen,
                      })
                    }
                  />
                  <Text style={{marginLeft: 10, alignSelf: 'center'}}>Kidney Disease</Text>
                </View>

                <View style={{flexDirection: 'row', marginRight: 3, marginLeft: 3, marginTop: 20, marginBottom: 20, borderTopWidth: 1, borderTopColor: 'grey', paddingTop: 10, paddingRight: 10}}>
                  <CheckBox
                    title="Click Here"
                    value={this.state.confirmationTick1}
                    onValueChange={(value) =>
                      this.setState({
                        confirmationTick1: !this.state.confirmationTick1,
                      })
                    }
                  />
                  <Text style={{marginLeft: 10, alignSelf: 'center'}}>I confirm that I am over 18, a resident of the UK and that I have provided accurate information.</Text>
                </View>

                <View style={{flexDirection: 'row', margin: 3}}>
                  <CheckBox
                    title="Click Here"
                    value={this.state.confirmationTick2}
                    onValueChange={(value) =>
                      this.setState({
                        confirmationTick2: !this.state.confirmationTick2,
                      })
                    }
                  />
                  <Text style={{marginLeft: 10, alignSelf: 'center'}}>I confirm that I have read to the Terms and Condition of Swift Clinic and understand that by proceeding I am agree to be bound by them.</Text>
                </View>


            </View>
            <Button title="Submit" onClick={this.onSubmit}></Button>
            </View>
          </SafeAreaView>
      </SafeAreaView>
      </ScrollView>
    );
  }
}

export default QuestionaireScreen;
