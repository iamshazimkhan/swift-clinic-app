import { CreditCardInput } from "react-native-credit-card-input";
import React from 'react';
import {View, Modal, SafeAreaView} from 'react-native';
import Button from '../../components/Buttons/button';
import Icon from 'react-native-vector-icons/FontAwesome';
import SpinnerComponent from '../../components/Spinner/Spinner';
import MainHeader from '../../components/Headers/MainHeader';

const PaymentForm = (props) => {

  return(
    <Modal
    animationType={'slide'}
    transparent={true}
    visible={props.Visibilty}
    onRequestClose={() => {
      console.log('Modal has been closed.');
    }}>
<SafeAreaView style={{flex: 1, backgroundColor: '#ffffff'}}>
<SpinnerComponent
            Visible={props.paymentSpinner}></SpinnerComponent>
<MainHeader Back={props.closeModal}></MainHeader>
    <View style={{marginTop: 40}}>
<CreditCardInput requiresName={true} onChange={ props._onChange} allowScroll={true} />
</View>
<View style={{margin: 10}}>
    <Button title="Add Card" onClick={props.onClick}></Button>
</View>
</SafeAreaView>
</Modal>
    )
}

export default PaymentForm;