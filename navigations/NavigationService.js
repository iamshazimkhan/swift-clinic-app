let _navigator;

function setNavigator(navRef) {
  _navigator = navRef;
}

function navigate(navAction) {
  _navigator.dispatch( 
     navAction
  );
}

// add other navigation functions that you need and export them

export default {
  navigate,
  setNavigator,
};