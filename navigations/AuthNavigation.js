import {createStackNavigator} from 'react-navigation-stack';
import Login from '../screens/AuthScreens/Login';
import Signup from '../screens/AuthScreens/Signup';
import LoginSignup from '../screens/AuthScreens/LoginOrSignup';
import ChooseLogin from '../screens/AuthScreens/ChooseLogin';
import ChooseSignup from '../screens/AuthScreens/ChooseSignup';
import ResetPassword from '../screens/AuthScreens/ResetPassword';
import PhoneVerify from '../screens/AuthScreens/PhoneVerify';
import EmailVerify from '../screens/AuthScreens/EmailVerify';
import CallScreen from '../screens/DoctorScreens/CallScreen';
import JanusVideoRoomScreen from '../screens/DoctorScreens/CallScreenJanus'
import QuestionaireScreen from '../screens/ConsultationScreens/QuestionaireScreen';

const AuthNavigation = createStackNavigator(
  {
    Login: {screen: Login},
    Signup: {screen: Signup},
    LoginSignup: {screen: LoginSignup},
    ChooseLogin: {screen: ChooseLogin},
    ChooseSignup: {screen: ChooseSignup},
    ResetPassword: {screen: ResetPassword},
    PhoneVerify: {screen: PhoneVerify},
    EmailVerify: {screen: EmailVerify},
    JanusVideoRoomScreen: {screen: JanusVideoRoomScreen},
    CallScreen: {screen: CallScreen}
  },
  {
    initialRouteName: 'LoginSignup',
    headerMode: 'none',
  },
);

export default AuthNavigation;
