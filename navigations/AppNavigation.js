import React, {Component} from 'react';
import {createStackNavigator} from 'react-navigation-stack';
import Profile from '../screens/ProfileScreens/Profile';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import Icon from 'react-native-vector-icons/FontAwesome5';
import HomeScreen from '../screens/ConsultationScreens/homeScreen';
import PatientVideoScreen from '../screens/ConsultationScreens/PatientCallScreenJanus';
import QuestionaireScreen from '../screens/ConsultationScreens/QuestionaireScreen';
import SymptomsListScreen from '../screens/ConsultationScreens/SymptomsListScreen';
import DoctorsListScreen from '../screens/ConsultationScreens/DoctorsListScreen';
import DoctorProfile from '../screens/ConsultationScreens/DoctorsProfile';
import PatientHistory from '../screens/ConsultationScreens/PatientHistory';
import PatientNotification from '../screens/ConsultationScreens/PatientNotification';
import {View} from 'react-native';
import ConsultationDetails from '../screens/ConsultationScreens/ConsultationDetails';
import PaymentForm from '../screens/PaymentScreens/PaymentForm';
import FeedBackScreen from '../screens/ConsultationScreens/FeedBackScreen';
import CallScreenPatient from '../screens/ConsultationScreens/CallScreenPatient';
import ChangePassword from '../screens/ProfileScreens/ChangePassword';
import ConsentForm from '../screens/ConsultationScreens/ConsentForm';


const HomeScreenNavigation = createStackNavigator(
    {
      Home: {screen: HomeScreen},
      QuestionaireScreen: {screen: QuestionaireScreen},
      SymptomsListScreen: {screen: SymptomsListScreen},
      DoctorsListScreen: {screen: DoctorsListScreen},
      DoctorProfile: {screen: DoctorProfile},
      ConsultationDetails: {screen: ConsultationDetails},
      PaymentForm: {screen: PaymentForm},
      FeedBackScreen: {screen: FeedBackScreen},
      CallScreenPatient: {screen: CallScreenPatient},
      ConsentForm: {screen: ConsentForm},
      PatientVideoScreen : {screen : PatientVideoScreen}
    },
    {
      initialRouteName: 'Home',
      headerMode: 'none',
    },
  );

  const ProfileScreenNavigation = createStackNavigator(
    {
      Profile : {screen: Profile},
      ChangePassword: {screen: ChangePassword}
    },
    {
      initialRouteName: 'Profile',
      headerMode: 'none',
    },
  );

  HomeScreenNavigation.navigationOptions = ({ navigation }) => {
    const currentRoute = navigation.state.routes[navigation.state.index];
    const { routeName } = currentRoute;
  
    let tabBarVisible = true;
    if (routeName === 'PatientVideoScreen') {  
        tabBarVisible = false;
    }
  
    return {
      tabBarVisible
    }
  };



const AppNavigation = createBottomTabNavigator(
  {    
    Home: { screen: HomeScreenNavigation,  
        navigationOptions:{  
            tabBarIcon: ({ tintColor }) => (  
                <View>  
                    <Icon style={[{color: tintColor}]} size={25} name={'home'}/>  
                </View>),  
            activeColor: '#33909F',  
            inactiveColor: '#f65a22',  
            barStyle: { backgroundColor: '#f69b31' },  
        }  
    },
    PatientHistory: { screen: PatientHistory,  
      navigationOptions:{  
          tabBarIcon: ({ tintColor }) => (  
              <View>  
                  <Icon style={[{color: tintColor}]} size={25} name={'clipboard-list'}/>  
              </View>),  
          activeColor: '#33909F',  
          inactiveColor: '#f65a22',  
          barStyle: { backgroundColor: '#f69b31' },  
      }  
  },
      PatientNotification: { screen: PatientNotification,  
        navigationOptions:{  
            tabBarIcon: ({ tintColor }) => (  
                <View>  
                    <Icon style={[{color: tintColor}]} size={25} name={'bell'} solid/>  
                </View>),  
            activeColor: '#33909F',  
            inactiveColor: '#f65a22',  
            barStyle: { backgroundColor: '#f69b31' },  
        }  
    },
    Profile: { screen: ProfileScreenNavigation,  
        navigationOptions:{
            tabBarIcon: ({ tintColor }) => (  
                <View>  
                    <Icon style={[{color: tintColor}]} size={25} name={'user'} solid/>  
                </View>),  
        }  
    } 
},  
{  tabBarOptions: {
    backgroundColor: 'black',
    showLabel: false,
    inactiveBackgroundColor: 'black',
    activeBackgroundColor: 'black',
    inactiveTintColor: '#fff',
    activeTintColor : '#33909F',
    style:{
        borderTopLeftRadius:10,
        borderTopRightRadius:10,
        overflow:'hidden',
        backgroundColor:'#000'
      }
    },
    initialRouteName: "Home",  
    activeColor: 'green',  
    backgroundColor: 'black',
    inactiveColor: '#226557',  
    barStyle: { backgroundColor: 'black' },  
  }, 
);
export default AppNavigation;
