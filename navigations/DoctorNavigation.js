import React, {Component} from 'react';
import {createStackNavigator} from 'react-navigation-stack';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import Icon from 'react-native-vector-icons/FontAwesome5';
import DoctorProfile from '../screens/DoctorScreens/DoctorProfile';
import DoctorNotification from '../screens/DoctorScreens/DoctorNotification';
import DoctorConsultationHistory from '../screens/DoctorScreens/DoctorConsultationHistory';
import DoctorHomeScreen from '../screens/DoctorScreens/DoctorHomeScreen';
import ConsultationStatus from '../screens/DoctorScreens/ConsultationStatus';
import JanusVideoRoomScreen from '../screens/DoctorScreens/CallScreenJanus'
import ActiveConsultations from '../screens/DoctorScreens/ActiveConsultations';
import ChangePassword from '../screens/ProfileScreens/ChangePassword';
import ChangePasswordDoctor from '../screens/ProfileScreens/ChangePasswordDoctor';
import CallScreen from '../screens/DoctorScreens/CallScreen';

import {View} from 'react-native';


const DoctorHomeScreenNavigation = createStackNavigator(
    {
      DoctorHome: {screen: DoctorHomeScreen},
      ConsultationStatus: {screen: ConsultationStatus},
      ActiveConsultations: {screen: ActiveConsultations},
      JanusVideoRoomScreen : {screen: JanusVideoRoomScreen},
      CallScreen: {screen: CallScreen}
    },
    {
      initialRouteName: 'DoctorHome',
      headerMode: 'none',
    },
  );

  DoctorHomeScreenNavigation.navigationOptions = ({ navigation }) => {
    const currentRoute = navigation.state.routes[navigation.state.index];
    const { routeName } = currentRoute;
  
    let tabBarVisible = true;
    if (routeName === 'JanusVideoRoomScreen') {  
        tabBarVisible = false;
    }
  
    return {
      tabBarVisible
    }
  };

  const ProfileScreenNavigation = createStackNavigator(
    {
      DoctorProfile : {screen: DoctorProfile},
      ChangePasswordDoctor: {screen: ChangePasswordDoctor}
    },
    {
      initialRouteName: 'DoctorProfile',
      headerMode: 'none',
    },
  );

const DoctorNavigation = createBottomTabNavigator(
  {
    DoctorHomeTab: {
      screen: DoctorHomeScreenNavigation,
      navigationOptions: {
        tabBarIcon: ({tintColor}) => (
          <View>
            <Icon style={[{color: tintColor}]} size={25} name={'home'} />
          </View>
        ),
        activeColor: '#33909F',
        inactiveColor: '#f65a22',
        barStyle: {backgroundColor: '#f69b31'},
      },
    },
    DoctorConsultationHistory: {
      screen: DoctorConsultationHistory,
      navigationOptions: {
        tabBarIcon: ({tintColor}) => (
          <View>
            <Icon style={[{color: tintColor}]} size={25} name={'clipboard-list'} />
          </View>
        ),
        activeColor: '#33909F',
        inactiveColor: '#f65a22',
        barStyle: {backgroundColor: '#f69b31'},
      },
    },
    DoctorNotification: {
      screen: DoctorNotification,
      navigationOptions: {
        tabBarIcon: ({tintColor}) => (
          <View>
            <Icon style={[{color: tintColor}]} size={25} name={'bell'} solid />
          </View>
        ),
        activeColor: '#33909F',
        inactiveColor: '#f65a22',
        barStyle: {backgroundColor: '#f69b31'},
      },
    },
    DoctorProfileTab: {
      screen: ProfileScreenNavigation,
      navigationOptions: {
        tabBarIcon: ({tintColor}) => (
          <View>
            <Icon style={[{color: tintColor}]} size={25} name={'user'} solid />
          </View>
        ),
        activeColor: '#33909F',
        inactiveColor: '#f65a22',
        barStyle: {backgroundColor: '#f69b31'},
      },
    },
  },
  {
    tabBarOptions: {
      backgroundColor: 'black',
      activeTintColor : '#33909F',
      showLabel: false,
      inactiveBackgroundColor: 'black',
      activeBackgroundColor: 'black',
      inactiveTintColor: '#fff',
      style:{
        borderTopLeftRadius:10,
        borderTopRightRadius:10,
        overflow:'hidden',
        backgroundColor:'#000'
      }
    },
    initialRouteName: 'DoctorHomeTab',
    activeColor: '#33909F',
    backgroundColor: 'black',
    inactiveColor: '#226557',
    barStyle: {backgroundColor: 'black'},
  },
);
export default DoctorNavigation;
